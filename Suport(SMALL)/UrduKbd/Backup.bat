@echo on
SET ver=0.1
IF %OS%==Windows_NT Goto WinNT

ECHO Windows Xp Backup Routine by: Shehzad Ashiq Ali
:Win9x
CLS
Echo.
Echo Windows 9x or ME Detected.
Echo Sorry you can only install this file under Windows 2000/Xp
Echo Wassallam :(
Goto End

:WinNT
CLS
Echo Installer %ver%
Echo.
Echo Windows NT or 2000 Detected.
Echo Windows is in : %windir%
Echo Dumping Files...

IF NOT EXIST Backup\Shehzad.txt md Backup
Echo DO NOT DELETE! This is a dummy file created for Dump/Restore validation > Backup\shehzad.txt

Echo   * kbdurdu.dll
IF EXIST %windir%\system32\kbdurdu.dll (
  copy %windir%\system32\kbdurdu.dll Backup > nul
) ELSE (
  Echo    [ file does not exist, backup not possible ]
)
Echo Done!
Echo.
Echo Current Urdu Keyboard Layout is Backed Up.
Goto End



End:
:End
SET ver=
pause
rem exit
