VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{D750EAB9-EB7B-450B-B9B6-0A0A17E13DCA}#1.0#0"; "xp.ocx"
Begin VB.Form frmProductRate 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Product Rates for Party"
   ClientHeight    =   7290
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   9855
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "ProductRates.frx":0000
   ScaleHeight     =   7290
   ScaleWidth      =   9855
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox T1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0E0FF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   375
      Left            =   2160
      MaxLength       =   60
      TabIndex        =   0
      Top             =   240
      Width           =   1095
   End
   Begin VB.TextBox Txt1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   1680
      MaxLength       =   60
      TabIndex        =   6
      Top             =   2040
      Visible         =   0   'False
      Width           =   975
   End
   Begin VB.TextBox TXTPID 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   330
      Left            =   0
      MaxLength       =   60
      TabIndex        =   5
      Top             =   0
      Visible         =   0   'False
      Width           =   375
   End
   Begin MSFlexGridLib.MSFlexGrid FG1 
      Height          =   4815
      Left            =   960
      TabIndex        =   3
      Top             =   1320
      Width           =   5535
      _ExtentX        =   9763
      _ExtentY        =   8493
      _Version        =   393216
      Rows            =   500
      Cols            =   5
      AllowBigSelection=   0   'False
      Appearance      =   0
   End
   Begin XPLayout.XPButton XPButton1 
      Height          =   375
      Left            =   7800
      TabIndex        =   2
      Top             =   1680
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Show"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "ProductRates.frx":1596D6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSDataListLib.DataCombo DC1 
      Height          =   330
      Left            =   2160
      TabIndex        =   1
      Top             =   720
      Width           =   2535
      _ExtentX        =   4471
      _ExtentY        =   582
      _Version        =   393216
      Appearance      =   0
      BackColor       =   14737632
      ForeColor       =   8388608
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin XPLayout.XPButton Save 
      Height          =   375
      Left            =   2040
      TabIndex        =   7
      Top             =   6600
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Save"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "ProductRates.frx":1596F2
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin XPLayout.XPButton Delet 
      Height          =   375
      Left            =   3360
      TabIndex        =   8
      Top             =   6600
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Delete"
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "ProductRates.frx":15970E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin XPLayout.XPButton PrintE 
      Height          =   375
      Left            =   6000
      TabIndex        =   9
      Top             =   6600
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Print"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "ProductRates.frx":15972A
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin XPLayout.XPButton Updat 
      Height          =   375
      Left            =   4680
      TabIndex        =   10
      Top             =   6600
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Update"
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "ProductRates.frx":159746
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin XPLayout.XPButton Resete 
      Height          =   375
      Left            =   720
      TabIndex        =   11
      Top             =   6600
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "Re&set"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "ProductRates.frx":159762
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSFlexGridLib.MSFlexGrid FG2 
      Height          =   255
      Left            =   960
      TabIndex        =   12
      Top             =   6120
      Visible         =   0   'False
      Width           =   5535
      _ExtentX        =   9763
      _ExtentY        =   450
      _Version        =   393216
      Rows            =   1
      Cols            =   5
      FixedRows       =   0
      FixedCols       =   0
      AllowBigSelection=   0   'False
      Appearance      =   0
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Doc #"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000040&
      Height          =   270
      Index           =   2
      Left            =   1200
      TabIndex        =   13
      Top             =   240
      Width           =   630
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "S.Type"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000040&
      Height          =   270
      Index           =   3
      Left            =   1200
      TabIndex        =   4
      Top             =   720
      Width           =   735
   End
End
Attribute VB_Name = "frmProductRate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Function CL_FG1()
FG1.clear
FG2.clear
FG1.TextMatrix(0, 0) = "SNO"
FG1.TextMatrix(0, 1) = "PRID"
FG1.TextMatrix(0, 2) = "Product"
FG1.TextMatrix(0, 3) = "P RATE"
FG1.TextMatrix(0, 4) = "C RATE"

FG1.ColWidth(0) = 500
FG1.ColWidth(1) = 0
FG1.ColWidth(2) = 3000
FG1.ColWidth(3) = 1000
FG1.ColWidth(4) = 0
''''''''''FG2
FG2.ColWidth(0) = 0
FG2.ColWidth(1) = 500
FG2.ColWidth(2) = 3000
FG2.ColWidth(3) = 1000
FG2.ColWidth(4) = 0

r = 1
Set Rs = New ADODB.Recordset
Rs.Open "SELECT * FROM PRODUCT WHERE CATAGORY='FINISHED' ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
Do Until Rs.EOF
FG1.TextMatrix(r, 0) = r
FG1.TextMatrix(r, 1) = Rs!PRID
FG1.TextMatrix(r, 2) = Rs!Name
r = r + 1
Rs.MoveNext
Loop
End Function

Function FG1_TOT()
FG2.clear
For i = 1 To FG1.Rows - 1
FG2.TextMatrix(0, 1) = Val(FG2.TextMatrix(0, 1)) + Val(FG1.TextMatrix(i, 1))
FG2.TextMatrix(0, 2) = Val(FG2.TextMatrix(0, 2)) + Val(FG1.TextMatrix(i, 2))
FG2.TextMatrix(0, 3) = Val(FG2.TextMatrix(0, 3)) + Val(FG1.TextMatrix(i, 3))
FG2.TextMatrix(0, 4) = Val(FG2.TextMatrix(0, 4)) + Val(FG1.TextMatrix(i, 4))
Next i
End Function

Private Sub DC1_Change()
If CC = False Then
Set RSP = New ADODB.Recordset
If DC1.Text <> "" Then
RSP.Open "SELECT DISTINCT STYPE FROM PARTY WHERE STYPE Like '" & UCase(DC1.Text) & "%' AND STYPE<>'' ORDER BY STYPE", CN, adOpenStatic, adLockOptimistic
Else
RSP.Open "SELECT DISTINCT STYPE FROM PARTY WHERE STYPE<>'' ORDER BY STYPE", CN, adOpenStatic, adLockOptimistic
End If
Set DC1.RowSource = RSP
DC1.ListField = "STYPE"
End If
End Sub

Private Sub DC1_GotFocus()
SendKeys "{F4}"
DC1_Change
End Sub

Function GetS()
Set RSP = New ADODB.Recordset
If DC1.Text <> "" Then
RSP.Open "SELECT PARTY FROM PARTY WHERE PARTY Like '" & UCase(DC1.Text) & "%' AND TYPE='SALESMAN' ORDER BY PARTY", CN, adOpenStatic, adLockOptimistic
Else
RSP.Open "SELECT PARTY FROM PARTY WHERE TYPE='SALESMAN' ORDER BY PARTY", CN, adOpenStatic, adLockOptimistic
End If
Set DC1.RowSource = RSP
DC1.ListField = "PARTY"
End Function

Private Sub DC1_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DC1_Validate(Cancel As Boolean)
'If DC1.Text <> "" Then
'Set RS = New ADODB.Recordset
'RS.Open "SELECT PID FROM PARTY WHERE NAME ='" & DC1.Text & "'", CN, adOpenStatic, adLockOptimistic
'Set TXTPID.DataSource = RS
'TXTPID.DataField = "PID"
'If TXTPID.Text = "" Then
'MsgBox "Select a valid party please"
'Cancel = True
'Else
'Cancel = False
'End If
'End If
End Sub

Private Sub Delet_Click()
CN.Execute "DELETE FROM PRODUCTRATE WHERE DCNO=" & T1.Text & ""
End Sub

Private Sub FG1_Click()
SHoW_CoNT
End Sub

Private Sub FG1_EnterCell()
SHoW_CoNT
End Sub

Private Sub FG1_GotFocus()
SHoW_CoNT
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyF2 Then
FG1.RemoveItem (FG1.Row)
End If
End Sub

Private Sub Form_Load()
''''''''''''''''''' theme
Set OBJ = New clsFunction
OBJ.clear Me
'''''''''''''''''''''''''''''

CL_FG1
T1.Text = MaX_ID("DCNO", "PRODUCTRATE")
End Sub

Function SHoW_CoNT()
If FG1.CoL = 3 Then
Txt1.Text = FG1.TextMatrix(FG1.Row, FG1.CoL)
    Txt1.Left = FG1.Left + FG1.CellLeft
    Txt1.Top = FG1.Top + FG1.CellTop
    Txt1.Height = FG1.CellHeight
    Txt1.Width = FG1.CellWidth
    Txt1.Visible = True
    Txt1.SetFocus
End If
End Function

Private Sub Resete_Click()
CL_FG1
Delet.Enabled = False
Updat.Enabled = False
Save.Enabled = True
T1.Text = MaX_ID("DCNO", "PRODUCTRATE")
Txt1.Visible = False
DC1.Text = ""
End Sub

Private Sub Save_Click()
Set RSD = New ADODB.Recordset
RSD.Open "SELECT * FROM PRODUCTRATE WHERE STYPE ='" & DC1.Text & "'", CN, adOpenStatic, adLockOptimistic
If RSD.RecordCount > 0 Then
MsgBox "ALREADY ENTERED " & RSD!DCNO
Exit Sub
End If

If DC1.Text <> "" Then
r = 1
Set Rs = New ADODB.Recordset
Rs.Open "SELECT * FROM PRODUCTRATE", CN, adOpenStatic, adLockOptimistic
Do Until FG1.TextMatrix(r, 2) = ""
Rs.AddNew
Rs!RPID = MaX_ID("RPID", "PRODUCTRATE")
Rs!PRID = FG1.TextMatrix(r, 1)
Rs!STYPE = DC1.Text
Rs!DCNO = Val(T1.Text)
Rs!SRate = Val(FG1.TextMatrix(r, 3))
r = r + 1
Rs.Update
Loop
Resete_Click
Else
MsgBox "ENTER STYPE"
End If
End Sub

Private Sub T1_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = 38 Then
T1.Text = Val(T1.Text) - 1
T1_KeyPress 13
ElseIf KeyCode = 40 Then
T1.Text = Val(T1.Text) + 1
T1_KeyPress 13
End If
End Sub

Private Sub T1_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
CL_FG1
r = 1
Set RSU = New ADODB.Recordset
RSU.Open "SELECT R.PRID,R.STYPE,R.SRATE,R.DCNO FROM PRODUCTRATE R,PRODUCT P WHERE DCNO=" & T1.Text & " AND P.PRID=R.PRID ORDER BY P.NAME", CN, adOpenStatic, adLockOptimistic
If RSU.RecordCount > 0 Then
DC1.Text = RSU!STYPE
Delet.Enabled = True
Updat.Enabled = True
Save.Enabled = False
Do Until RSU.EOF
r = 1
    Do Until FG1.TextMatrix(r, 2) = ""
If FG1.TextMatrix(r, 1) = RSU!PRID Then
'FG1.TextMatrix(r, 2) = NaME_iD("product", "PRID", RSU!PRID)
FG1.TextMatrix(r, 3) = IIf(RSU!SRate = 0, "", RSU!SRate)
End If
r = r + 1
Loop
RSU.MoveNext
r = r + 1
Loop
End If
End If
End Sub

Private Sub Txt1_Change()
FG1.TextMatrix(FG1.Row, FG1.CoL) = Txt1.Text
End Sub

Private Sub Txt1_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
'If FG1.CoL = 3 Then
'FG1.CoL = FG1.CoL + 1

If FG1.CoL = 3 Then
FG1.Row = FG1.Row + 1
FG1.CoL = 3
End If
SHoW_CoNT
End If
End Sub

Private Sub Updat_Click()
Delet_Click
Save_Click
End Sub
Private Sub XPButton1_Click()
CL_FG1
r = 1
If DC1.Text <> "" And TXTPID.Text <> "" Then
Set RSU = New ADODB.Recordset
RSU.Open "SELECT * FROM PRODUCTRATE WHERE RPID=" & TXTPID.Text & "", CN, adOpenStatic, adLockOptimistic
Do Until RSU.EOF
FG1.TextMatrix(r, 1) = RSU!PRID
Set RSD = New ADODB.Recordset
RSD.Open "SELECT NAME FROM PRODUCT WHERE SNO=" & RSU!PRID & "", CN, adOpenStatic, adLockOptimistic
FG1.TextMatrix(r, 2) = RSD!Name
FG1.TextMatrix(r, 3) = IIf(RSU!PRATE = 0, "", RSU!PRATE)
FG1.TextMatrix(r, 4) = IIf(RSU!CRate = 0, "", RSU!CRate)
RSU.MoveNext
Loop
Else
MsgBox "ENTER PARTY NAME"
CL_FG1
End If
End Sub
Private Sub XPButton2_Click()
Set DEL = New ADODB.Connection
DEL.Open CONN
DEL.Execute "DELETE TEMP"
r = 1
counter = 1
Set Rs = New ADODB.Recordset
Rs.Open "SELECT * FROM TEMP ORDER BY SNO", CN, adOpenStatic, adLockOptimistic
For counter = 1 To FG1.Rows - 1
Rs.AddNew
Rs("T1") = FG1.TextMatrix(r, 0)
Rs("N1") = Val(FG1.TextMatrix(r, 1))
Rs("N2") = Val(FG1.TextMatrix(r, 2))
Rs("N3") = Val(FG1.TextMatrix(r, 3))
Rs("N4") = Val(FG1.TextMatrix(r, 4))
Rs("N5") = Val(FG1.TextMatrix(r, 5))
Rs("N6") = Val(FG1.TextMatrix(r, 6))
Rs("SNO") = Val(FG1.TextMatrix(r, 7))
Rs.Update
r = r + 1
If FG1.TextMatrix(r, 0) = "" Then
counter = FG1.Rows
End If
Next counter
If DE1.rsCTEMP.State = 1 Then
DE1.rsCTEMP.Close
End If
DE1.rsCTEMP.Open "SELECT SNO,T1,N1,N2,N3,N4,ROUND(N5,0) N5,N6 FROM TEMP ORDER BY SNO"
'DE1.RSCTEMP.Open "SELECT * FROM TDSR"
'REDSR.Sections(1).Controls("TV").Caption = FG1.TextMatrix(0, 0)
REDSR.Sections(1).Controls("TN").Caption = DC1.Text
REDSR.Sections(1).Controls("TD").Caption = DT1.Value
REDSR.Show vbModal
End Sub
Private Sub XPButton4_Click()
CL_FG1
DC1.Text = ""
'TXTPID.Text = ""
DT1.SetFocus
End Sub
