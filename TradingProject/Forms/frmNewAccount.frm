VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{D750EAB9-EB7B-450B-B9B6-0A0A17E13DCA}#1.0#0"; "XP.ocx"
Begin VB.Form frmNewAccount 
   BackColor       =   &H00FFFFFF&
   Caption         =   "New Account"
   ClientHeight    =   6765
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   8070
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frmNewAccount.frx":0000
   ScaleHeight     =   6765
   ScaleWidth      =   8070
   StartUpPosition =   2  'CenterScreen
   Visible         =   0   'False
   Begin VB.ComboBox Combo2 
      Height          =   315
      ItemData        =   "frmNewAccount.frx":1596D6
      Left            =   1800
      List            =   "frmNewAccount.frx":1596EF
      TabIndex        =   4
      Top             =   1800
      Width           =   2055
   End
   Begin VB.Frame Frame2 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0C0&
      Caption         =   "Salesman"
      Enabled         =   0   'False
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000008&
      Height          =   1215
      Left            =   4440
      TabIndex        =   33
      Top             =   4080
      Width           =   3375
      Begin VB.ComboBox Combo1 
         Height          =   315
         ItemData        =   "frmNewAccount.frx":159733
         Left            =   1080
         List            =   "frmNewAccount.frx":15973D
         TabIndex        =   16
         Top             =   720
         Width           =   2055
      End
      Begin VB.TextBox Text3 
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         Height          =   285
         Left            =   1080
         TabIndex        =   15
         Top             =   360
         Width           =   1575
      End
      Begin VB.Label Label16 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Working"
         BeginProperty Font 
            Name            =   "Times New Roman"
            Size            =   12.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   -1  'True
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00004080&
         Height          =   300
         Left            =   120
         TabIndex        =   35
         Top             =   720
         Width           =   930
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "SALARY"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00404080&
         Height          =   225
         Index           =   15
         Left            =   120
         TabIndex        =   34
         Top             =   390
         Width           =   690
      End
   End
   Begin VB.TextBox T8 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00004080&
      Height          =   285
      Left            =   1800
      TabIndex        =   3
      Top             =   1440
      Width           =   4095
   End
   Begin VB.TextBox T7 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00004080&
      Height          =   270
      Left            =   1800
      TabIndex        =   13
      Top             =   5160
      Width           =   855
   End
   Begin VB.TextBox T6 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00004080&
      Height          =   270
      Left            =   1800
      TabIndex        =   12
      Top             =   4800
      Width           =   855
   End
   Begin VB.TextBox T5 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00004080&
      Height          =   270
      Left            =   1800
      TabIndex        =   11
      Top             =   4440
      Width           =   855
   End
   Begin VB.TextBox T1 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      Height          =   285
      Left            =   1800
      TabIndex        =   1
      Top             =   720
      Width           =   1215
   End
   Begin VB.TextBox Text1 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00004080&
      Height          =   285
      Left            =   1800
      TabIndex        =   6
      Top             =   2160
      Width           =   5295
   End
   Begin VB.TextBox T2 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00004080&
      Height          =   285
      Left            =   5280
      TabIndex        =   5
      Top             =   1800
      Width           =   2175
   End
   Begin VB.TextBox T4 
      Appearance      =   0  'Flat
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00004080&
      Height          =   285
      Left            =   1800
      TabIndex        =   2
      Top             =   1080
      Width           =   4095
   End
   Begin XPLayout.XPButton cmdSave 
      Height          =   375
      Left            =   2895
      TabIndex        =   17
      Top             =   6120
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Save"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   16711680
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "frmNewAccount.frx":15974A
      PICN            =   "frmNewAccount.frx":159766
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSDataListLib.DataCombo DC1 
      Height          =   360
      Left            =   1800
      TabIndex        =   10
      Top             =   3960
      Width           =   2415
      _ExtentX        =   4260
      _ExtentY        =   635
      _Version        =   393216
      Appearance      =   0
      ForeColor       =   16512
      ListField       =   ""
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin XPLayout.XPButton cmdUpdate 
      Height          =   375
      Left            =   4080
      TabIndex        =   18
      Top             =   6120
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&UPDATE"
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   16711680
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "frmNewAccount.frx":159D02
      PICN            =   "frmNewAccount.frx":159D1E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSDataListLib.DataCombo DC2 
      Height          =   360
      Left            =   1800
      TabIndex        =   7
      Top             =   2520
      Width           =   2415
      _ExtentX        =   4260
      _ExtentY        =   635
      _Version        =   393216
      Appearance      =   0
      ForeColor       =   16512
      ListField       =   ""
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSDataListLib.DataCombo DC3 
      Height          =   360
      Left            =   1800
      TabIndex        =   8
      Top             =   3000
      Width           =   2415
      _ExtentX        =   4260
      _ExtentY        =   635
      _Version        =   393216
      Appearance      =   0
      ForeColor       =   16512
      ListField       =   ""
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSDataListLib.DataCombo DC4 
      Height          =   360
      Left            =   1800
      TabIndex        =   14
      Top             =   5520
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   635
      _Version        =   393216
      Appearance      =   0
      ForeColor       =   16512
      ListField       =   ""
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSDataListLib.DataCombo DCU 
      Height          =   315
      Left            =   1800
      TabIndex        =   0
      Top             =   240
      Width           =   4695
      _ExtentX        =   8281
      _ExtentY        =   582
      _Version        =   393216
      Appearance      =   0
      BackColor       =   16777215
      ForeColor       =   16512
      ListField       =   ""
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   178
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSDataListLib.DataCombo DCS 
      Height          =   360
      Left            =   1800
      TabIndex        =   9
      Top             =   3480
      Width           =   2415
      _ExtentX        =   4260
      _ExtentY        =   635
      _Version        =   393216
      Appearance      =   0
      ForeColor       =   16512
      ListField       =   ""
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin XPLayout.XPButton cmdDELETE 
      Height          =   375
      Left            =   5280
      TabIndex        =   37
      Top             =   6120
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Delete"
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   178
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "frmNewAccount.frx":15A2BA
      PICN            =   "frmNewAccount.frx":15A2D6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin XPLayout.XPButton cmdReset 
      Height          =   375
      Left            =   1680
      TabIndex        =   38
      Top             =   6120
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Reset"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   178
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   16711680
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "frmNewAccount.frx":15A872
      PICN            =   "frmNewAccount.frx":15A88E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label14 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Day"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   300
      Left            =   360
      TabIndex        =   36
      Top             =   1800
      Width           =   420
   End
   Begin VB.Label Label13 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Sub.Sector:"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   300
      Left            =   360
      TabIndex        =   32
      Top             =   3480
      Width           =   1230
   End
   Begin VB.Label Label15 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Update"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   300
      Left            =   360
      TabIndex        =   31
      Top             =   360
      Width           =   780
   End
   Begin VB.Label Label12 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Salesman"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   300
      Left            =   360
      TabIndex        =   30
      Top             =   5520
      Width           =   1035
   End
   Begin VB.Label Label11 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Sector:"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   300
      Left            =   360
      TabIndex        =   29
      Top             =   3000
      Width           =   765
   End
   Begin VB.Label Label10 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Contact.P"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   300
      Left            =   360
      TabIndex        =   28
      Top             =   1440
      Width           =   1035
   End
   Begin VB.Label Label9 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Less.SMS %"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   300
      Left            =   360
      TabIndex        =   27
      Top             =   5160
      Width           =   1305
   End
   Begin VB.Label Label8 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Ext.Com %"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   300
      Left            =   360
      TabIndex        =   26
      Top             =   4800
      Width           =   1185
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Com %"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   300
      Left            =   360
      TabIndex        =   25
      Top             =   4440
      Width           =   750
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "City:"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   300
      Left            =   360
      TabIndex        =   24
      Top             =   2520
      Width           =   510
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Sr.No"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   300
      Left            =   360
      TabIndex        =   23
      Top             =   765
      Width           =   600
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Type: *"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   300
      Left            =   360
      TabIndex        =   22
      Top             =   3960
      Width           =   795
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Address:"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   300
      Left            =   360
      TabIndex        =   21
      Top             =   2160
      Width           =   960
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Phone No."
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   300
      Left            =   4080
      TabIndex        =   20
      Top             =   1800
      Width           =   1080
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Name *"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   300
      Index           =   0
      Left            =   360
      TabIndex        =   19
      Top             =   1080
      Width           =   825
   End
End
Attribute VB_Name = "frmNewAccount"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdDelete_Click()
If MsgBox("Are you sure to delete", vbYesNo) = vbOK Then
CN.Execute "delete from party where pid=" & Val(T1.Text) & ""
MsgBox "Delete Successful"
Call cmdReset_Click
End If
End Sub

Private Sub cmdReset_Click()
cmdUpdate.Enabled = False
cmdSave.Enabled = True
Set OBJ = New clsFunction
OBJ.clear Me
T1.Text = MaX_ID("PID", "PARTY")
End Sub

Private Sub DC2_Change()
If CC = False Then
Set RS3 = New ADODB.Recordset
If DC2.Text <> "" Then
RS3.Open "SELECT DISTINCT CITY FROM PARTY WHERE CITY Like '" & DC2.Text & "%' AND CITY<>'' ORDER BY CITY", CN, adOpenStatic, adLockOptimistic
Else
RS3.Open "SELECT DISTINCT CITY FROM PARTY WHERE CITY<>'' ORDER BY CITY", CN, adOpenStatic, adLockOptimistic
End If
If RS3.RecordCount > 0 Then
Set DC2.RowSource = RS3
DC2.ListField = "CITY"
End If
End If
End Sub

Private Sub DC2_GotFocus()
DC2_Change
SendKeys "{F4}"
End Sub

Private Sub DC2_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DC3_Change()
If CC = False Then
Set RS3 = New ADODB.Recordset
If DC3.Text <> "" Then
RS3.Open "SELECT DISTINCT SECTOR FROM PARTY WHERE SECTOR Like '" & DC3.Text & "%' ORDER BY SECTOR", CN, adOpenStatic, adLockOptimistic
Else
RS3.Open "SELECT DISTINCT SECTOR FROM PARTY ORDER BY SECTOR", CN, adOpenStatic, adLockOptimistic
End If
Set DC3.RowSource = RS3
DC3.ListField = "SECTOR"
End If
End Sub

Private Sub DC3_GotFocus()
DC3_Change
SendKeys "{F4}"
End Sub

Private Sub DC3_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DC4_Change()
If CC = False Then
Set RSP = New ADODB.Recordset
If DC4.Text <> "" Then
RSP.Open "SELECT NAME FROM PARTY WHERE NAME Like '" & DC4.Text & "%' AND TYPE='SALESMAN' ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
Else
RSP.Open "SELECT NAME FROM PARTY WHERE TYPE='SALESMAN' ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
End If
Set DC4.RowSource = RSP
DC4.ListField = "NAME"
End If
End Sub

Private Sub DC4_GotFocus()
DC4_Change
SendKeys "{F4}"
End Sub

Private Sub DC4_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DC4_Validate(Cancel As Boolean)
If DC4.Text <> "" Then
Set Rs = New ADODB.Recordset
Rs.Open "SELECT PID FROM PARTY WHERE NAME='" & DC4.Text & "'", CN, adOpenStatic, adLockOptimistic
If Rs.RecordCount > 0 Then
Cancel = False
Else
MsgBox "Select a valid party please"
Cancel = True
End If
End If
End Sub

Private Sub DCS_Change()
If CC = False Then
Set RS3 = New ADODB.Recordset
If DCS.Text <> "" Then
RS3.Open "SELECT DISTINCT SUBSECTOR FROM PARTY WHERE SUBSECTOR Like '" & DCS.Text & "%' AND SUBSECTOR<>' ' ORDER BY SUBSECTOR", CN, adOpenStatic, adLockOptimistic
Else
RS3.Open "SELECT DISTINCT SUBSECTOR FROM PARTY WHERE SUBSECTOR<>' ' ORDER BY SUBSECTOR", CN, adOpenStatic, adLockOptimistic
End If
If RS3.RecordCount > 0 Then
Set DCS.RowSource = RS3
DCS.ListField = "SUBSECTOR"
End If
End If
End Sub

Private Sub DCS_GotFocus()
DCS_Change
End Sub

Private Sub DCS_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DCU_Change()
If CC = False Then
Set RS3 = New ADODB.Recordset
If DCU.Text <> "" Then
RS3.Open "SELECT NAME FROM PARTY WHERE NAME Like '" & DCU.Text & "%' ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
Else
RS3.Open "SELECT NAME FROM PARTY ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
End If
Set DCU.RowSource = RS3
DCU.ListField = "NAME"
End If
End Sub

Private Sub DCU_GotFocus()
DCU_Change
End Sub

Private Sub DCU_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DCU_KeyUp(KeyCode As Integer, Shift As Integer)
Set Rs = New ADODB.Recordset
Rs.Open "SELECT * FROM PARTY WHERE NAME='" & DCU.Text & "'", CN, adOpenStatic, adLockOptimistic
If Rs.RecordCount > 0 Then
T1.Text = IIf(IsNull(Rs("PID")), "", Rs("PID"))
T1_KeyPress 13
End If
End Sub

Private Sub T1_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = 38 Then
T1.Text = Val(T1.Text) - 1
T1_KeyPress 13
ElseIf KeyCode = 40 Then
T1.Text = Val(T1.Text) + 1
T1_KeyPress 13
End If
End Sub

Private Sub T4_Validate(Cancel As Boolean)
Set RSP = New ADODB.Recordset
RSP.Open "select * from PARTY WHERE NAME='" & T4.Text & "'", CN, adOpenStatic, adLockOptimistic
If RSP.RecordCount > 0 Then
MsgBox "ALREADY EXIST."
Exit Sub
End If
End Sub

Private Sub cmdSave_Click()
Set RSP = New ADODB.Recordset
RSP.Open "select * from PARTY WHERE NAME='" & T4.Text & "'", CN, adOpenStatic, adLockOptimistic
If RSP.RecordCount > 1 Then
MsgBox "ALREADY EXIST."
Exit Sub
End If

If T4.Text <> "" And DC1.Text <> "" Then
Set Rs = New ADODB.Recordset
If cmdUpdate.Enabled = False Then
Rs.Open "select * from party WHERE PID=(SELECT MAX(PID) FROM PARTY)", CN, adOpenStatic, adLockOptimistic
Rs.AddNew
Rs("PID") = T1.Text
Else
Rs.Open "select * from party WHERE PID=" & Val(T1.Text) & "", CN, adOpenStatic, adLockOptimistic
End If

If DC4.Text <> "" Then Rs("SID") = ID_NaME("PARTY", "PID", "NAME", DC4.Text)
Rs("NAME") = T4.Text
'Rs("TID") = V_Tid
Rs("CPERSON") = T8.Text
Rs("PHONE") = T2.Text
Rs("ADDRESS") = Text1.Text
Rs("CITY") = DC2.Text
Rs("SUBSECTOR") = DCS.Text
Rs("SECTOR") = DC3.Text
Rs("TYPE") = DC1.Text
Rs("COMM") = Val(T5.Text)
Rs("ECOMM") = Val(T6.Text)
Rs("SMS") = Val(T7.Text)
Rs("SALARY") = Val(Text1.Text)
Rs("DAY") = Combo2.Text
Rs("WORKING") = Combo1.Text
Rs.Update
cmdReset_Click
T4.SetFocus
Else
MsgBox "Please Enter Into Empty Fields"
End If
End Sub

Private Sub DC1_Change()
If CC = False Then
Set RS3 = New ADODB.Recordset
If DC1.Text <> "" Then
RS3.Open "SELECT DISTINCT TYPE FROM PARTY WHERE TYPE Like '" & DC1.Text & "%' AND TYPE<>' ' ORDER BY TYPE", CN, adOpenStatic, adLockOptimistic
Else
RS3.Open "SELECT DISTINCT TYPE FROM PARTY WHERE TYPE<>' ' ORDER BY TYPE", CN, adOpenStatic, adLockOptimistic
End If
Set DC1.RowSource = RS3
DC1.ListField = "TYPE"
End If
If DC1.Text = "SALESMAN" Then
Frame2.Enabled = True
Else
Frame2.Enabled = False
End If
End Sub

Private Sub DC1_GotFocus()
DC1_Change
SendKeys "{F4}"
End Sub

Private Sub DC1_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Function Getp1()
Set RS3 = New ADODB.Recordset
If DC1.Text <> "" Then
RS3.Open "SELECT DISTINCT TYPE FROM PARTY WHERE TYPE Like '" & DC1.Text & "%' AND TYPE<>' ' ORDER BY TYPE", CN, adOpenStatic, adLockOptimistic
Else
RS3.Open "SELECT DISTINCT TYPE FROM PARTY WHERE TYPE<>' ' ORDER BY TYPE", CN, adOpenStatic, adLockOptimistic
End If
Set DC1.RowSource = RS3
DC1.ListField = "TYPE"
End Function

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyF5 Then
cmdReset_Click
ElseIf KeyCode = vbKeyEscape Then
Unload Me
End If
End Sub

Private Sub Form_Load()
''''''''''''''''''' theme
Set OBJ = New clsFunction
OBJ.clear Me
'''''''''''''''''''''''''''''

T1.Text = MaX_ID("PID", "PARTY")
End Sub

Private Sub T1_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
SHoW_UP
End If
If (KeyAscii > 47 And KeyAscii < 58) Or KeyAscii = vbKeyBack Or KeyAscii = 13 Then
Else
KeyAscii = 0
MsgBox "Integer Only"
End If
End Sub

Function SHoW_UP()
'If Chk_Tid("PARTY", "PID", T1.Text) = False Then
'MsgBox "Change Login"
'Exit Function
'End If

i = T1.Text
Str1 = DCU.Text
cmdReset_Click             '''''cmdReset_ClickT
T1.Text = i
DCU.Text = Str1

Set Rs = New ADODB.Recordset
Rs.Open "SELECT * FROM PARTY WHERE PID=" & Val(T1.Text) & "", CN, adOpenStatic, adLockOptimistic
If Rs.RecordCount > 0 Then

Set RSD = New ADODB.Recordset
RSD.Open "SELECT * FROM stock_main WHERE pid =" & Val(T1.Text) & "", CN, adOpenStatic, adLockOptimistic
Set RsD2 = New ADODB.Recordset
RsD2.Open "SELECT * FROM pledger WHERE pid =" & Val(T1.Text) & "", CN, adOpenStatic, adLockOptimistic
Set RSP = New ADODB.Recordset
RSP.Open "SELECT * FROM product WHERE pid =" & Val(T1.Text) & "", CN, adOpenStatic, adLockOptimistic
If RSD.RecordCount > 0 Or RsD2.RecordCount > 0 Or RSP.RecordCount > 0 Then
Me.cmdDelete.Enabled = False
Else
Me.cmdDelete.Enabled = True
End If

cmdSave.Enabled = False
cmdUpdate.Enabled = True
If Not IsNull(Rs!SID) Then
DC4.Text = NaME_iD("party", "PID", Rs!SID)
End If
T4.Text = IIf(IsNull(Rs("NAME")), "", Rs("NAME"))
T2.Text = IIf(IsNull(Rs("PHONE")), "", Rs("PHONE"))
T5.Text = IIf(IsNull(Rs("COMM")), "", Rs("COMM"))
T6.Text = IIf(IsNull(Rs("ECOMM")), "", Rs("ECOMM"))
T7.Text = IIf(IsNull(Rs("SMS")), "", Rs("SMS"))
T8.Text = IIf(IsNull(Rs("CPERSON")), "", Rs("CPERSON"))
Text1.Text = IIf(IsNull(Rs("ADDRESS")), "", Rs("ADDRESS"))
DC1.Text = IIf(IsNull(Rs("TYPE")), "", Rs("TYPE"))
DCS.Text = IIf(IsNull(Rs("SUBSECTOR")), "", Rs("SUBSECTOR"))
DC2.Text = IIf(IsNull(Rs("CITY")), "", Rs("CITY"))
DC3.Text = IIf(IsNull(Rs("SECTOR")), "", Rs("SECTOR"))
Text3.Text = IIf(IsNull(Rs("SALARY")), "", Rs("SALARY"))
Combo2.Text = IIf(IsNull(Rs("DAY")), "", Rs("DAY"))
Combo1.Text = IIf(IsNull(Rs("WORKING")), "", Rs("WORKING"))
'Else
'cmdReset_Click
End If
End Function

Private Sub cmdUpdate_Click()
If MsgBox("Are you sure to Update", vbYesNo) = vbYes Then
cmdSave_Click
MsgBox "Update Successful"
End If
End Sub

