VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "msdatlst.ocx"
Object = "{D750EAB9-EB7B-450B-B9B6-0A0A17E13DCA}#1.0#0"; "xp.ocx"
Begin VB.Form frmJV 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Jurnal Voucher Entries"
   ClientHeight    =   6390
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   11175
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Palette         =   "JournalVoucher.frx":0000
   Picture         =   "JournalVoucher.frx":11AAA2
   ScaleHeight     =   6390
   ScaleWidth      =   11175
   StartUpPosition =   2  'CenterScreen
   Begin MSDataListLib.DataCombo CMBBABY 
      Height          =   315
      Left            =   1440
      TabIndex        =   4
      Top             =   2400
      Visible         =   0   'False
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   556
      _Version        =   393216
      Appearance      =   0
      BackColor       =   12632319
      Text            =   ""
   End
   Begin VB.TextBox T1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   1080
      MaxLength       =   60
      TabIndex        =   0
      Top             =   120
      Width           =   1455
   End
   Begin VB.TextBox TXTBABY 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      BorderStyle     =   0  'None
      Height          =   285
      Left            =   3000
      TabIndex        =   5
      Top             =   2400
      Visible         =   0   'False
      Width           =   1095
   End
   Begin MSFlexGridLib.MSFlexGrid FG1 
      Height          =   4935
      Left            =   0
      TabIndex        =   3
      Top             =   600
      Width           =   11175
      _ExtentX        =   19711
      _ExtentY        =   8705
      _Version        =   393216
      Cols            =   7
      AllowBigSelection=   0   'False
      Appearance      =   0
   End
   Begin MSComCtl2.DTPicker DT1 
      Height          =   375
      Left            =   3645
      TabIndex        =   1
      Top             =   120
      Width           =   1935
      _ExtentX        =   3413
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   12
         Charset         =   178
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   12648447
      CalendarTitleBackColor=   12640511
      CustomFormat    =   "dd-MMM-yy"
      Format          =   63700993
      CurrentDate     =   37916
   End
   Begin XPLayout.XPButton cmdSave 
      Height          =   375
      Left            =   3285
      TabIndex        =   7
      Top             =   5880
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Save"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   178
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "JournalVoucher.frx":274178
      PICN            =   "JournalVoucher.frx":274194
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin XPLayout.XPButton cmdDelete 
      Height          =   375
      Left            =   4605
      TabIndex        =   8
      Top             =   5880
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Delete"
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   178
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "JournalVoucher.frx":274730
      PICN            =   "JournalVoucher.frx":27474C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin XPLayout.XPButton cmdPrint 
      Height          =   375
      Left            =   7320
      TabIndex        =   10
      Top             =   5880
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Print"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   178
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "JournalVoucher.frx":274CE8
      PICN            =   "JournalVoucher.frx":274D04
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin XPLayout.XPButton cmdUpdate 
      Height          =   375
      Left            =   6000
      TabIndex        =   9
      Top             =   5880
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Update"
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   178
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "JournalVoucher.frx":2752A0
      PICN            =   "JournalVoucher.frx":2752BC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin XPLayout.XPButton cmdReset 
      Height          =   375
      Left            =   1920
      TabIndex        =   6
      Top             =   5880
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Reset"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   178
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "JournalVoucher.frx":275858
      PICN            =   "JournalVoucher.frx":275874
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSFlexGridLib.MSFlexGrid FG2 
      Height          =   255
      Left            =   0
      TabIndex        =   11
      Top             =   5520
      Width           =   11175
      _ExtentX        =   19711
      _ExtentY        =   450
      _Version        =   393216
      Rows            =   1
      Cols            =   7
      FixedRows       =   0
      AllowBigSelection=   0   'False
      Appearance      =   0
   End
   Begin MSDataListLib.DataCombo DC2 
      Height          =   330
      Index           =   0
      Left            =   6960
      TabIndex        =   2
      Top             =   120
      Width           =   3495
      _ExtentX        =   6165
      _ExtentY        =   582
      _Version        =   393216
      Appearance      =   0
      BackColor       =   12632319
      ForeColor       =   8388608
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Saleman"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   225
      Index           =   7
      Left            =   6120
      TabIndex        =   14
      Top             =   120
      Width           =   750
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Sr.No."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000040&
      Height          =   255
      Index           =   2
      Left            =   480
      TabIndex        =   13
      Top             =   120
      Width           =   615
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Date"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000040&
      Height          =   255
      Index           =   0
      Left            =   3120
      TabIndex        =   12
      Top             =   120
      Width           =   465
   End
End
Attribute VB_Name = "frmJV"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub CMBBABY_GotFocus()
SendKeys "{F4}"
CMBBABY_Change
End Sub

Private Sub CMBBABY_Validate(Cancel As Boolean)
If CMBBABY.Text <> "" Then
Set Rs = New ADODB.Recordset
Rs.Open "SELECT PID FROM PARTY WHERE NAME='" & CMBBABY.Text & "'", CN, adOpenStatic, adLockOptimistic
If Rs.RecordCount > 0 Then
FG1.TextMatrix(FG1.Row, 1) = Rs(0)
End If
If Val(FG1.TextMatrix(FG1.Row, 1)) = 0 Then
MsgBox "Select a valid party please"
Cancel = True
Else
Cancel = False
End If
End If
End Sub

Private Sub cmdSave_Click()
If Val(FG2.TextMatrix(0, 5)) = Val(FG2.TextMatrix(0, 6)) Then
r = 1
    Dim RS2 As New ADODB.Recordset
    RS2.Open "SELECT * FROM PLEDGER WHERE PLEDID=(SELECT MAX(PLEDID) FROM PLEDGER) ORDER BY PLEDID", CN, adOpenStatic, adLockOptimistic
    Do Until FG1.TextMatrix(r, 1) = ""
    RS2.AddNew
    RS2("PLEDID") = MaX_ID("PLEDID", "PLEDGER")
    RS2("DCNO") = T1.Text
    RS2("ETYPE") = "JV"
    'RS2("TID") = V_Tid
    RS2("SID") = ID_NaME("PARTY", "PID", "NAME", DC2(0).Text)
    RS2("DATE") = DT1.Value
    RS2("PID") = FG1.TextMatrix(r, 1)
    RS2("DESCRIPTION2") = FG1.TextMatrix(r, 3)
    RS2("DESCRIPTION") = "JV. " & T1.Text & " " & FG1.TextMatrix(r, 3)
    RS2("INVOICE") = Val(FG1.TextMatrix(r, 4))
    RS2("DEBIT") = Val(FG1.TextMatrix(r, 5))
    RS2("CREDIT") = Val(FG1.TextMatrix(r, 6))
    RS2.Update
    r = r + 1
    Loop
Call cmdReset_Click
Else
MsgBox "DEBIT ,CREDIT SHOULD BE EQUALL"
End If
End Sub

Private Sub DC2_Change(Index As Integer)
If CC = False Then
Set RSP = New ADODB.Recordset
If DC2(Index).Text <> "" Then
RSP.Open "SELECT NAME FROM PARTY WHERE NAME Like '" & UCase(DC2(Index).Text) & "%' AND (TYPE='SALESMAN' OR TYPE='SUPPLYMAN')  ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
Else
RSP.Open "SELECT NAME FROM PARTY WHERE (TYPE='SALESMAN' OR TYPE='SUPPLYMAN') ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
End If
Set DC2(Index).RowSource = RSP
DC2(Index).ListField = "NAME"
End If
End Sub

Private Sub DC2_GotFocus(Index As Integer)
DC2_Change Index
SendKeys "{F4}"
End Sub

Private Sub DC2_KeyDown(Index As Integer, KeyCode As Integer, shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DC2_Validate(Index As Integer, Cancel As Boolean)
'Private Sub DC2_Validate(Cancel As Boolean)
If DC2(Index).Text <> "" Then
Set Rs = New ADODB.Recordset
Rs.Open "SELECT * FROM PARTY WHERE NAME='" & DC2(Index).Text & "'", CN, adOpenStatic, adLockOptimistic
If Rs.RecordCount > 0 Then
Cancel = False
Else
MsgBox "Select a valid party please"
Cancel = True
End If
End If
'End Sub
End Sub


Private Sub DT1_Validate(Cancel As Boolean)
FG1.CoL = 2
End Sub

Private Sub FG1_EnterCell()
SHOW_BABY
End Sub

Private Sub FG1_GotFocus()
SHOW_BABY
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, shift As Integer)

If KeyCode = vbKeyF2 Then
FG1.RemoveItem (FG1.Row)
ElseIf KeyCode = vbKeyF3 Then
frmNewAccount.Show vbModal
End If
End Sub

Private Sub Form_Load()
''''''''''''''''''' theme
Set OBJ = New clsFunction
OBJ.clear Me
'''''''''''''''''''''''''''''

T1.Text = MaX_DcN("JV", "PLEDGER")
DT1.Value = Date
Get_head
End Sub

Function Get_head()
FG1.clear
FG2.clear
FG1.TextMatrix(0, 0) = "SNO"
FG1.TextMatrix(0, 1) = "PID"
FG1.TextMatrix(0, 2) = "Party / Account"
FG1.TextMatrix(0, 3) = "Description"
FG1.TextMatrix(0, 4) = "Invoice"
FG1.TextMatrix(0, 5) = "Debit"
FG1.TextMatrix(0, 6) = "Credit"

FG1.ColWidth(0) = 500
FG1.ColWidth(1) = 0
FG1.ColWidth(2) = 4000
FG1.ColWidth(3) = 3000
FG1.ColWidth(4) = 800
FG1.ColWidth(5) = 1000
FG1.ColWidth(6) = 1000
''''''''''''''''''''
FG1.ColAlignment(2) = 0
'''''''''''''''''''
FG2.ColWidth(0) = 500
FG2.ColWidth(1) = 0
FG2.ColWidth(2) = 4000
FG2.ColWidth(3) = 3000
FG1.ColWidth(4) = 800
FG2.ColWidth(5) = 1000
FG2.ColWidth(6) = 1000

For i = 1 To FG1.Rows - 1
FG1.TextMatrix(i, 0) = i
Next i

TXTBABY.Visible = False
TXTBABY.Text = ""
CMBBABY.Visible = False
CMBBABY.Text = ""
End Function

Sub SHOW_BABY()
   If FG1.CoL = 2 Then
        CMBBABY.Text = FG1.TextMatrix(FG1.Row, FG1.CoL)
        CMBBABY.Top = FG1.Top + FG1.CellTop
        CMBBABY.Left = FG1.Left + FG1.CellLeft
        CMBBABY.Visible = True
        CMBBABY.SetFocus
        CMBBABY.Width = FG1.CellWidth
        TXTBABY.Visible = False
    ElseIf FG1.CoL = 3 Or FG1.CoL = 4 Or FG1.CoL = 5 Or FG1.CoL = 6 Then
        TXTBABY.Text = FG1.TextMatrix(FG1.Row, FG1.CoL)
        TXTBABY.Left = FG1.Left + FG1.CellLeft
        TXTBABY.Top = FG1.Top + FG1.CellTop
        TXTBABY.Height = FG1.CellHeight
        TXTBABY.Width = FG1.CellWidth
        TXTBABY.Visible = True
        TXTBABY.SetFocus
        CMBBABY.Visible = False
    End If
End Sub

Private Sub FG1_Click()
SHOW_BABY
End Sub

Private Sub T1_GotFocus()
SendKeys "{home}+{end}"
End Sub

Private Sub T1_KeyDown(KeyCode As Integer, shift As Integer)
If KeyCode = 38 Then
T1.Text = Val(T1.Text) - 1
T1_KeyPress 13
ElseIf KeyCode = 40 Then
T1.Text = Val(T1.Text) + 1
T1_KeyPress 13
End If
End Sub

Private Sub T1_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
Call UP_VIeW
End If
End Sub

Function UP_VIeW()
'If Chk_Tid("PLEDGER", "JV", T1.Text) = False Then
'MsgBox "Change Login"
'Exit Function
'End If

c = Val(T1.Text)
cmdReset_Click
T1.Text = c

r = 1
Set rs1 = New ADODB.Recordset
Set RS2 = New ADODB.Recordset
    RS2.Open "SELECT * FROM PLEDGER WHERE DCNO=" & Val(T1.Text) & " AND ETYPE='JV' ORDER BY PLEDID", CN, adOpenStatic, adLockOptimistic
    If RS2.RecordCount > 0 Then
    DT1.Value = RS2("DATE")
    cmdDelete.Enabled = True
    cmdUpdate.Enabled = True
    cmdSave.Enabled = False
    If Not IsNull(RS2!SID) Then DC2(0).Text = NaME_iD("PARTY", "PID", RS2!SID)
    Do Until RS2.EOF
    'rs1.Open "SELECT NAME FROM PARTY WHERE PID=" & RS2("PID") & "", CN, adOpenStatic, adLockOptimistic
    FG1.TextMatrix(r, 1) = RS2("PID")
    FG1.TextMatrix(r, 2) = NaME_iD("PARTY", "PID", RS2!PID)
    FG1.TextMatrix(r, 3) = IIf(IsNull(RS2("DESCRIPTION2")), "", RS2("DESCRIPTION2"))
    FG1.TextMatrix(r, 4) = IIf(RS2("Invoice") = 0, "", RS2("Invoice"))
    FG1.TextMatrix(r, 5) = IIf(RS2("DEBIT") = 0, "", RS2("DEBIT"))
    FG1.TextMatrix(r, 6) = IIf(RS2("CREDIT") = 0, "", RS2("CREDIT"))
    RS2.MoveNext
    r = r + 1
    FG1.Rows = r + 1
    'rs1.Close
    Loop
FG1_TOT
Else
TV = T1.Text
Call cmdReset_Click
T1.Text = TV
End If
End Function

Private Sub TXTBABY_Change()
FG1.TextMatrix(FG1.Row, FG1.CoL) = TXTBABY.Text
FG1_TOT
End Sub

Private Sub TXTBABY_KeyDown(KeyCode As Integer, shift As Integer)
If KeyCode = vbKeyRight Then
'If FG1.CoL <> FG1.Cols - 1 Then
'FG1.CoL = FG1.CoL + 1
'End If
ElseIf KeyCode = vbKeyLeft Then
'If FG1.CoL <> 0 Then
'FG1.CoL = FG1.CoL - 1
'End If
ElseIf KeyCode = vbKeyUp Then
If FG1.Row <> 1 Then
FG1.Row = FG1.Row - 1
End If
ElseIf KeyCode = vbKeyDown Then
If FG1.Row <> FG1.Rows - 1 Then
FG1.Row = FG1.Row + 1
End If
End If
SHOW_BABY
End Sub

Private Sub txtbaby_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
'''Add new row
If FG1.Rows = FG1.Row + 1 Then
FG1.Rows = FG1.Rows + 1
End If
'''Got ot next row
If FG1.CoL = 6 Then
    FG1.Row = FG1.Row + 1
    FG1.CoL = 2
Else
    FG1.CoL = FG1.CoL + 1
End If
SHOW_BABY
End If
End Sub

Function FG1_TOT()
FG2.clear
For i = 1 To FG1.Rows - 1
    FG2.TextMatrix(0, 5) = Val(FG2.TextMatrix(0, 5)) + Val(FG1.TextMatrix(i, 5))
    FG2.TextMatrix(0, 6) = Val(FG2.TextMatrix(0, 6)) + Val(FG1.TextMatrix(i, 6))
Next i
End Function

Private Sub CMBBABY_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
CMBBABY_Validate False
    If FG1.CoL = 3 Then
        FG1.Row = FG1.Row + 1
        FG1.CoL = 1
    Else
        FG1.CoL = FG1.CoL + 1
    End If
    SHOW_BABY
End If
End Sub

Private Sub CMBBABY_Change()
If CC = False Then
Set RSP = New ADODB.Recordset
If CMBBABY.Text <> "" Then
RSP.Open "SELECT NAME FROM PARTY WHERE NAME Like '" & CMBBABY.Text & "%' ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
Else
RSP.Open "SELECT NAME FROM PARTY ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
End If
Set CMBBABY.RowSource = RSP
CMBBABY.ListField = "NAME"
End If

FG1.TextMatrix(FG1.Row, FG1.CoL) = CMBBABY.Text
End Sub

Private Sub CMBBABY_KeyDown(KeyCode As Integer, shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub cmdDelete_Click()
CN.Execute "DELETE FROM PLEDGER WHERE DCNO=" & Val(T1.Text) & " AND ETYPE='JV'"
End Sub

Private Sub cmdPrint_Click()
Unload rptCashReport
UpdateReportCredentials rptCashReport
With rptCashReport
.txtHead.SetText "Journal Voucher"
.txtDate.SetText "Voucher # " & Me.T1.Text
'.txtDate.Suppress = True
'.txtCredit.Suppress = True
'.DEBIT1.Suppress = True
.RecordSelectionFormula = "{pledger.etype}='JV' AND {pledger.dcno}=" & T1.Text
End With
ReportsViewer.CRViewer1.ReportSource = rptCashReport
ReportsViewer.CRViewer1.ViewReport
ReportsViewer.Show vbModal
End Sub

Private Sub cmdUpdate_Click()
If Val(FG2.TextMatrix(0, 5)) = Val(FG2.TextMatrix(0, 6)) Then
Call cmdDelete_Click
Call cmdSave_Click
Else
MsgBox "N.Credit Should Equal To N.Debit"
End If
End Sub


Private Sub cmdReset_Click()
T1.Text = MaX_DcN("JV", "PLEDGER")
Get_head
cmdSave.Enabled = True
cmdDelete.Enabled = False
cmdUpdate.Enabled = False
End Sub

