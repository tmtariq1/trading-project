@echo on
SET ver=0.1
IF %OS%==Windows_NT Goto WinNT

ECHO Windows Xp Installer Routine by: Shehzad Ashiq Ali
:Win9x
CLS
Echo.
Echo Windows 9x or ME Detected.
Echo Sorry you can only install this file under Windows 2000/Xp
Echo Wassallam :(
Goto End

:WinNT
CLS
Echo Install Keyboard %ver%
Echo.
Echo Windows NT or 2000 Detected.
Echo Windows is in : %windir%

Echo Installing Files...
Echo   + kbdurdu.dll Stage 1 Overwriting Cache
copy Kbd\kbdurdu.dll %windir%\system32\dllcache >nul
rem System32\dllcache\
Echo + kbdurdu.dll Stage 2 Renaming Old Urdu Keyboard Layout File to kbdurdu.bak
ren %windir%\system32\kbdurdu.dll kbdurdu.bak
Echo + kbdurdu.dll Stage 3 Installing File
copy Kbd\kbdurdu.dll %windir%\system32 > nul

Echo Done!
Echo Phonetic Urdu Keyboard Layout has been installed
Echo Reboot your PC to make the changes active.
Goto End

End:
:End
SET ver= 1.0
pause
@ECHO OFF
rem exit