VERSION 5.00
Object = "{CDE57A40-8B86-11D0-B3C6-00A0C90AEA82}#1.0#0"; "MSDATGRD.OCX"
Object = "{0ECD9B60-23AA-11D0-B351-00A0C9055D8E}#6.0#0"; "MSHFLXGD.OCX"
Begin VB.Form frmACCL 
   Caption         =   "CUSTOMER LEDGER"
   ClientHeight    =   7005
   ClientLeft      =   60
   ClientTop       =   750
   ClientWidth     =   11850
   FillStyle       =   0  'Solid
   Icon            =   "Accountledger.frx":0000
   LinkTopic       =   "Form1"
   Picture         =   "Accountledger.frx":030A
   ScaleHeight     =   7005
   ScaleWidth      =   11850
   WindowState     =   2  'Maximized
   Begin VB.PictureBox SB1 
      Align           =   2  'Align Bottom
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      ForeColor       =   &H80000008&
      Height          =   465
      Left            =   0
      Picture         =   "Accountledger.frx":1599E0
      ScaleHeight     =   435
      ScaleWidth      =   11820
      TabIndex        =   5
      Top             =   6540
      Width           =   11850
      Begin VB.TextBox TB 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00000040&
         Height          =   375
         Left            =   9600
         TabIndex        =   15
         Top             =   65
         Width           =   1935
      End
      Begin VB.TextBox TD 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00000040&
         Height          =   375
         Left            =   6600
         TabIndex        =   14
         Top             =   65
         Width           =   1935
      End
      Begin VB.TextBox TC 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00000040&
         Height          =   375
         Left            =   3240
         TabIndex        =   13
         Top             =   65
         Width           =   1935
      End
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid MSHFlexGrid2 
         Height          =   30
         Left            =   0
         TabIndex        =   6
         Top             =   600
         Width           =   7455
         _ExtentX        =   13150
         _ExtentY        =   53
         _Version        =   393216
         _NumberOfBands  =   1
         _Band(0).Cols   =   2
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Debit Total"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   270
         Index           =   1
         Left            =   1920
         TabIndex        =   9
         Top             =   105
         Width           =   1170
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Balance"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   270
         Index           =   3
         Left            =   8640
         TabIndex        =   8
         Top             =   105
         Width           =   855
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Credit Total"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   270
         Index           =   4
         Left            =   5280
         TabIndex        =   7
         Top             =   120
         Width           =   1260
      End
   End
   Begin VB.PictureBox picStatBox 
      Align           =   1  'Align Top
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      ForeColor       =   &H80000008&
      Height          =   585
      Left            =   0
      Picture         =   "Accountledger.frx":784502
      ScaleHeight     =   555
      ScaleWidth      =   11820
      TabIndex        =   1
      Top             =   0
      Width           =   11850
      Begin VB.TextBox TPH 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00000040&
         Height          =   375
         Left            =   5160
         Locked          =   -1  'True
         TabIndex        =   16
         Top             =   120
         Width           =   2415
      End
      Begin VB.TextBox Text4 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00000040&
         Height          =   375
         Left            =   9720
         Locked          =   -1  'True
         TabIndex        =   12
         Top             =   120
         Width           =   2055
      End
      Begin VB.TextBox Text1 
         Alignment       =   2  'Center
         Appearance      =   0  'Flat
         BackColor       =   &H00FFFFFF&
         ForeColor       =   &H00000040&
         Height          =   375
         Left            =   1440
         Locked          =   -1  'True
         TabIndex        =   11
         Top             =   120
         Width           =   2775
      End
      Begin MSHierarchicalFlexGridLib.MSHFlexGrid MSHFlexGrid1 
         Height          =   30
         Left            =   0
         TabIndex        =   2
         Top             =   600
         Width           =   7455
         _ExtentX        =   13150
         _ExtentY        =   53
         _Version        =   393216
         _NumberOfBands  =   1
         _Band(0).Cols   =   2
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Phone#"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   270
         Index           =   6
         Left            =   4320
         TabIndex        =   10
         Top             =   150
         Width           =   810
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Previous Balance"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   270
         Index           =   0
         Left            =   7680
         TabIndex        =   4
         Top             =   150
         Width           =   1875
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Party Name"
         BeginProperty Font 
            Name            =   "Tahoma"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000080&
         Height          =   270
         Index           =   2
         Left            =   0
         TabIndex        =   3
         Top             =   150
         Width           =   1320
      End
   End
   Begin MSDataGridLib.DataGrid DG1 
      Align           =   1  'Align Top
      Height          =   3255
      Left            =   0
      TabIndex        =   0
      Top             =   585
      Width           =   11850
      _ExtentX        =   20902
      _ExtentY        =   5741
      _Version        =   393216
      AllowUpdate     =   0   'False
      BackColor       =   16374736
      HeadLines       =   1
      RowHeight       =   15
      FormatLocked    =   -1  'True
      BeginProperty HeadFont {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   178
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   178
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ColumnCount     =   7
      BeginProperty Column00 
         DataField       =   "DATE"
         Caption         =   "DATE"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   "0"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column01 
         DataField       =   "INVOICE"
         Caption         =   "INVOICE"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column02 
         DataField       =   "DESCRIPTION"
         Caption         =   "DESCRIPTION"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column03 
         DataField       =   "DEBIT"
         Caption         =   "DEBIT"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   "0.00"
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   1
         EndProperty
      EndProperty
      BeginProperty Column04 
         DataField       =   "CREDIT"
         Caption         =   "CREDIT"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column05 
         DataField       =   "FBAL"
         Caption         =   "BALANCE"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      BeginProperty Column06 
         DataField       =   "FCD"
         Caption         =   "D/C"
         BeginProperty DataFormat {6D835690-900B-11D0-9484-00A0C91110ED} 
            Type            =   0
            Format          =   ""
            HaveTrueFalseNull=   0
            FirstDayOfWeek  =   0
            FirstWeekOfYear =   0
            LCID            =   1033
            SubFormatType   =   0
         EndProperty
      EndProperty
      SplitCount      =   1
      BeginProperty Split0 
         BeginProperty Column00 
            ColumnWidth     =   1019.906
         EndProperty
         BeginProperty Column01 
            ColumnWidth     =   780.095
         EndProperty
         BeginProperty Column02 
            ColumnWidth     =   6329.764
         EndProperty
         BeginProperty Column03 
            ColumnWidth     =   900.284
         EndProperty
         BeginProperty Column04 
            ColumnWidth     =   959.811
         EndProperty
         BeginProperty Column05 
            ColumnWidth     =   1065.26
         EndProperty
         BeginProperty Column06 
            ColumnWidth     =   420.095
         EndProperty
      EndProperty
   End
   Begin VB.Menu P 
      Caption         =   "PRINT"
   End
End
Attribute VB_Name = "frmACCL"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Form_Load()
''''''''''''''''''' theme
Set OBJ = New clsFunction
OBJ.clear Me
'''''''''''''''''''''''''''''

If frmACCLD.DC1.Text <> "" Then
Party_Leadger
End If
End Sub

Function Party_Leadger()
CN.Execute "DELETE FROM LEDGER"
Set PD = New ADODB.Recordset
Set PC = New ADODB.Recordset
PD.Open "SELECT SUM(DEBIT) FROM PLEDGER WHERE PID=" & ID_NaME("PARTY", "PID", "NAME", frmACCLD.DC1.Text) & " AND DATE < '" & frmACCLD.DTS.Value & "'", CN, adOpenStatic, adLockOptimistic
PC.Open "SELECT SUM(CREDIT)FROM  PLEDGER WHERE PID=" & ID_NaME("PARTY", "PID", "NAME", frmACCLD.DC1.Text) & " AND DATE < '" & frmACCLD.DTS.Value & "'", CN, adOpenStatic, adLockOptimistic
TT = IIf(IsNull(PD(0)), 0, PD(0)) - IIf(IsNull(PC(0)), 0, PC(0))
''''''''''''''PREVIOUS BALANCE
If TT <> 0 Then
Set UP = New ADODB.Recordset
UP.Open "SELECT * FROM LEDGER ORDER BY SNO", CN, adOpenStatic, adLockOptimistic
UP.AddNew
UP("SNO") = 1
UP("DATE") = frmACCLD.DTS.Value
UP("DESCRIPTION") = "BROUGHT FORWARD"
'''''''''''''''''''''''''''''''''''''''''
If TT < 0 Then
UP("DEBIT") = Abs(TT)
Str1 = "CR"
ElseIf TT > 0 Then
UP("CREDIT") = Abs(TT)
Str1 = "DB"
Else
Str1 = "Nill"
End If
UP("FBAL") = Abs(TT)
UP("FCD") = Str1
UP.Update
UP.Close

''''''''''''''''''''''''''''''
End If
Set Rs = New ADODB.Recordset
Rs.Open "SELECT * FROM PLEDGER WHERE PID=" & ID_NaME("PARTY", "PID", "NAME", frmACCLD.DC1.Text) & " AND DATE BETWEEN '" & frmACCLD.DTS.Value & "' AND '" & frmACCLD.DTE.Value & "' ORDER BY DATE,PLEDID", CN, adOpenStatic, adLockOptimistic
Do Until Rs.EOF
TT = TT + IIf(IsNull(Rs("DEBIT")), 0, Rs("DEBIT")) - IIf(IsNull(Rs("CREDIT")), 0, Rs("CREDIT"))
If TT < 0 Then
Str1 = "CR"
ElseIf TT > 0 Then
Str1 = "DB"
Else
Str1 = "Nill"
End If

Set UP = New ADODB.Recordset
UP.Open "SELECT * FROM LEDGER ORDER BY SNO", CN, adOpenStatic, adLockOptimistic
UP.AddNew
UP("SNO") = MaX_ID("SNO", "LEDGER")
UP("PID") = Rs("PID")
UP("DATE") = Rs("DATE")
UP("DESCRIPTION") = Rs("DESCRIPTION")
UP("INVOICE") = Rs!INVOICE
UP("DEBIT") = IIf(IsNull(Rs("DEBIT")), 0, Rs("DEBIT"))
UP("CREDIT") = IIf(IsNull(Rs("CREDIT")), 0, Rs("CREDIT"))
UP("FBAL") = Abs(TT)
UP("FCD") = Str1
UP.Update
UP.Close
Rs.MoveNext
Loop
Rs.Close

Text1.Text = frmACCLD.DC1.Text
Set Rs = New ADODB.Recordset
Rs.Open "SELECT * FROM LEDGER ORDER BY SNO", CN, adOpenStatic, adLockOptimistic
Set DG1.DataSource = Rs
Me.Caption = "Party ledger from " & frmACCLD.DTS.Value & " To " & frmACCLD.DTE.Value

Set DR = New ADODB.Recordset
Set cr = New ADODB.Recordset
DR.Open "SELECT SUM(DEBIT) FROM PLEDGER WHERE PID=" & ID_NaME("PARTY", "PID", "NAME", frmACCLD.DC1.Text) & " AND DATE BETWEEN '" & frmACCLD.DTS.Value & "' AND '" & frmACCLD.DTE.Value & "'", CN, adOpenStatic, adLockOptimistic
cr.Open "SELECT SUM(CREDIT) FROM PLEDGER WHERE PID=" & ID_NaME("PARTY", "PID", "NAME", frmACCLD.DC1.Text) & " AND DATE BETWEEN '" & frmACCLD.DTS.Value & "' AND '" & frmACCLD.DTE.Value & "'", CN, adOpenStatic, adLockOptimistic

TD.Text = IIf(IsNull(PD(0)), 0, PD(0)) + IIf(IsNull(DR(0)), 0, DR(0))
TC.Text = IIf(IsNull(PC(0)), 0, PC(0)) + IIf(IsNull(cr(0)), 0, cr(0))
TT = Val(TD.Text) - Val(TC.Text)

If TT < 0 Then
Str1 = "CR"
ElseIf TT > 0 Then
Str1 = "DR"
Else
Str1 = "Nill"
End If
TB.Text = Abs(TT) & " " & Str1
End Function

Function Party_Leadger_All()
CN.Execute "DELETE FROM LEDGER"
Set PD = New ADODB.Recordset
Set PC = New ADODB.Recordset
PD.Open "SELECT SUM(DEBIT) FROM PLEDGER WHERE PID=" & ID_NaME("PARTY", "PID", "NAME", frmACCLD.DC1.Text) & " AND DATE < '" & frmACCLD.DTS.Value & "'", CN, adOpenStatic, adLockOptimistic
PC.Open "SELECT SUM(CREDIT)FROM  PLEDGER WHERE PID=" & ID_NaME("PARTY", "PID", "NAME", frmACCLD.DC1.Text) & " AND DATE < '" & frmACCLD.DTS.Value & "'", CN, adOpenStatic, adLockOptimistic
TT = IIf(IsNull(PD(0)), 0, PD(0)) - IIf(IsNull(PC(0)), 0, PC(0))
''''''''''''''PREVIOUS BALANCE
If TT <> 0 Then
Set UP = New ADODB.Recordset
UP.Open "SELECT * FROM LEDGER ORDER BY SNO", CN, adOpenStatic, adLockOptimistic
UP.AddNew
UP("SNO") = 1
UP("DATE") = frmACCLD.DTS.Value
UP("DESCRIPTION") = "BROUGHT FORWARD"
'''''''''''''''''''''''''''''''''''''''''
If TT < 0 Then
UP("DEBIT") = Abs(TT)
Str1 = "CR"
ElseIf TT > 0 Then
UP("CREDIT") = Abs(TT)
Str1 = "DB"
Else
Str1 = "Nill"
End If
UP("FBAL") = Abs(TT)
UP("FCD") = Str1
UP.Update
UP.Close

''''''''''''''''''''''''''''''
End If
Set Rs = New ADODB.Recordset
Rs.Open "SELECT * FROM PLEDGER WHERE PID=" & ID_NaME("PARTY", "PID", "NAME", frmACCLD.DC1.Text) & " AND DATE BETWEEN '" & frmACCLD.DTS.Value & "' AND '" & frmACCLD.DTE.Value & "' ORDER BY DATE,PLEDID", CN, adOpenStatic, adLockOptimistic
Do Until Rs.EOF
TT = TT + IIf(IsNull(Rs("DEBIT")), 0, Rs("DEBIT")) - IIf(IsNull(Rs("CREDIT")), 0, Rs("CREDIT"))
If TT < 0 Then
Str1 = "CR"
ElseIf TT > 0 Then
Str1 = "DB"
Else
Str1 = "Nill"
End If

Set UP = New ADODB.Recordset
UP.Open "SELECT * FROM LEDGER ORDER BY SNO", CN, adOpenStatic, adLockOptimistic
UP.AddNew
UP("SNO") = MaX_ID("SNO", "LEDGER")
UP("PID") = Rs("PID")
UP("DATE") = Rs("DATE")
UP("DESCRIPTION") = Rs("DESCRIPTION")
UP("INVOICE") = Rs!INVOICE
'''''''''''''''''''''''''''''''''''''''''''''''''
UP("DEBIT") = IIf(IsNull(Rs("DEBIT")), 0, Rs("DEBIT"))
UP("CREDIT") = IIf(IsNull(Rs("CREDIT")), 0, Rs("CREDIT"))
UP("FBAL") = Abs(TT)
UP("FCD") = Str1
UP.Update
UP.Close
Rs.MoveNext
Loop
Rs.Close

Text1.Text = frmACCLD.DC1.Text
Set Rs = New ADODB.Recordset
Rs.Open "SELECT * FROM LEDGER ORDER BY SNO", CN, adOpenStatic, adLockOptimistic
Set DG1.DataSource = Rs
Me.Caption = "Party ledger from " & frmACCLD.DTS.Value & " To " & frmACCLD.DTE.Value

Set DR = New ADODB.Recordset
Set cr = New ADODB.Recordset
DR.Open "SELECT SUM(DEBIT) FROM PLEDGER WHERE PID=" & ID_NaME("PARTY", "PID", "NAME", frmACCLD.DC1.Text) & " AND DATE BETWEEN '" & frmACCLD.DTS.Value & "' AND '" & frmACCLD.DTE.Value & "'", CN, adOpenStatic, adLockOptimistic
cr.Open "SELECT SUM(CREDIT) FROM PLEDGER WHERE PID=" & ID_NaME("PARTY", "PID", "NAME", frmACCLD.DC1.Text) & " AND DATE BETWEEN '" & frmACCLD.DTS.Value & "' AND '" & frmACCLD.DTE.Value & "'", CN, adOpenStatic, adLockOptimistic

TD.Text = IIf(IsNull(PD(0)), 0, PD(0)) + IIf(IsNull(DR(0)), 0, DR(0))
TC.Text = IIf(IsNull(PC(0)), 0, PC(0)) + IIf(IsNull(cr(0)), 0, cr(0))
TSBAL = Val(TD.Text) - Val(TC.Text)

If TT < 0 Then
Str1 = "CR"
ElseIf TT > 0 Then
Str1 = "DR"
Else
Str1 = "Nill"
End If
TB.Text = Abs(TT) & " " & Str1
End Function

Private Sub Form_Resize()
If Me.ScaleHeight = 0 Then
Else
DG1.Height = Me.ScaleHeight - 460 - SB1.Height - 130
End If
End Sub

Private Sub P_Click()
Unload REPLED
UpdateReportCredentials REPLED
ReportsViewer.CRViewer1.ReportSource = REPLED
ReportsViewer.CRViewer1.ViewReport
ReportsViewer.Show vbModal
End Sub

