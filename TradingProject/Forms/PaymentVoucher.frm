VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "msdatlst.ocx"
Object = "{D750EAB9-EB7B-450B-B9B6-0A0A17E13DCA}#1.0#0"; "xp.ocx"
Begin VB.Form FRMPTP 
   Appearance      =   0  'Flat
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Cash Payment"
   ClientHeight    =   6540
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   10500
   Icon            =   "PaymentVoucher.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "PaymentVoucher.frx":0442
   ScaleHeight     =   6540
   ScaleWidth      =   10500
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Begin VB.TextBox T1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   960
      MaxLength       =   60
      TabIndex        =   0
      Top             =   120
      Width           =   1455
   End
   Begin VB.TextBox TXT1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      BorderStyle     =   0  'None
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   2040
      MaxLength       =   60
      TabIndex        =   5
      Top             =   2400
      Visible         =   0   'False
      Width           =   975
   End
   Begin MSDataListLib.DataCombo DC1 
      Height          =   330
      Left            =   600
      TabIndex        =   4
      Top             =   2400
      Visible         =   0   'False
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   582
      _Version        =   393216
      Appearance      =   0
      BackColor       =   12632319
      ForeColor       =   8388608
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   178
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid FG1 
      Height          =   5055
      Left            =   120
      TabIndex        =   3
      Top             =   600
      Width           =   10215
      _ExtentX        =   18018
      _ExtentY        =   8916
      _Version        =   393216
      Rows            =   150
      Cols            =   6
      AllowBigSelection=   0   'False
      Appearance      =   0
   End
   Begin MSComCtl2.DTPicker DT1 
      Height          =   375
      Left            =   3240
      TabIndex        =   1
      Top             =   120
      Width           =   1935
      _ExtentX        =   3413
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   12
         Charset         =   178
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   12648447
      CalendarTitleBackColor=   12640511
      CustomFormat    =   "dd-MMM-yy"
      Format          =   64159745
      CurrentDate     =   37916
   End
   Begin XPLayout.XPButton cmdSave 
      Height          =   375
      Left            =   3000
      TabIndex        =   7
      Top             =   6000
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Save"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   178
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "PaymentVoucher.frx":159B18
      PICN            =   "PaymentVoucher.frx":159B34
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin XPLayout.XPButton cmdDelete 
      Height          =   375
      Left            =   4320
      TabIndex        =   8
      Top             =   6000
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Delete"
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   178
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "PaymentVoucher.frx":15A0D0
      PICN            =   "PaymentVoucher.frx":15A0EC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin XPLayout.XPButton cmdPrint 
      Height          =   375
      Left            =   6960
      TabIndex        =   10
      Top             =   6000
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Print"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   178
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "PaymentVoucher.frx":15A688
      PICN            =   "PaymentVoucher.frx":15A6A4
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin XPLayout.XPButton cmdUpdate 
      Height          =   375
      Left            =   5640
      TabIndex        =   9
      Top             =   6000
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Update"
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   178
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "PaymentVoucher.frx":15AC40
      PICN            =   "PaymentVoucher.frx":15AC5C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin XPLayout.XPButton cmdReset 
      Height          =   375
      Left            =   1800
      TabIndex        =   6
      Top             =   6000
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Reset"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   178
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "PaymentVoucher.frx":15B1F8
      PICN            =   "PaymentVoucher.frx":15B214
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSFlexGridLib.MSFlexGrid FG2 
      Height          =   255
      Left            =   120
      TabIndex        =   11
      Top             =   5640
      Width           =   10215
      _ExtentX        =   18018
      _ExtentY        =   450
      _Version        =   393216
      Rows            =   1
      Cols            =   6
      FixedRows       =   0
      AllowBigSelection=   0   'False
      Appearance      =   0
   End
   Begin MSDataListLib.DataCombo DC2 
      Height          =   330
      Left            =   6360
      TabIndex        =   2
      Top             =   120
      Width           =   3495
      _ExtentX        =   6165
      _ExtentY        =   582
      _Version        =   393216
      Appearance      =   0
      BackColor       =   12632319
      ForeColor       =   8388608
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "SupplyMan"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000040&
      Height          =   225
      Index           =   5
      Left            =   5400
      TabIndex        =   14
      Top             =   120
      Width           =   900
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Sr.No."
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000040&
      Height          =   255
      Index           =   2
      Left            =   360
      TabIndex        =   13
      Top             =   120
      Width           =   615
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Date"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   11.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000040&
      Height          =   255
      Index           =   0
      Left            =   2640
      TabIndex        =   12
      Top             =   120
      Width           =   465
   End
End
Attribute VB_Name = "FRMPTP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub DC1_Change()
If CC = False Then
Set RSP = New ADODB.Recordset
If DC1.Text <> "" Then
RSP.Open "SELECT NAME FROM PARTY WHERE NAME Like '" & DC1.Text & "%' ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
Else
RSP.Open "SELECT NAME FROM PARTY ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
End If
Set DC1.RowSource = RSP
DC1.ListField = "NAME"
End If
FG1.TextMatrix(FG1.Row, FG1.CoL) = DC1.Text
End Sub

Private Sub DC1_KeyDown(KeyCode As Integer, shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DC1_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
DC1_Validate False
    If FG1.CoL = 3 Then
        FG1.Row = FG1.Row + 1
        FG1.CoL = 1
    Else
        FG1.CoL = FG1.CoL + 1
    End If
    SHoW_CoNT
End If
End Sub

Private Sub DC1_Validate(Cancel As Boolean)
If DC1.Text <> "" Then
Set Rs = New ADODB.Recordset
Rs.Open "SELECT PID FROM PARTY WHERE NAME='" & DC1.Text & "'", CN, adOpenStatic, adLockOptimistic
If Rs.RecordCount > 0 Then
FG1.TextMatrix(FG1.Row, 1) = Rs(0)
Else
FG1.TextMatrix(FG1.Row, 1) = ""
End If
If Val(FG1.TextMatrix(FG1.Row, 1)) = 0 Then
MsgBox "Select a valid party please"
Cancel = False
Else
Cancel = True
End If
End If
End Sub

Private Sub cmdSave_Click()
r = 1
    Set RS2 = New ADODB.Recordset
    RS2.Open "SELECT * FROM PLEDGER WHERE PLEDID=(SELECT MAX(PLEDID) FROM PLEDGER) ORDER BY PLEDID", CN, adOpenStatic, adLockOptimistic
    Do Until FG1.TextMatrix(r, 1) = ""
    RS2.AddNew
    RS2("PLEDID") = MaX_ID("PLEDID", "PLEDGER")
    If DC2.Text <> "" Then
    RS2("SID") = ID_NaME("PARTY", "PID", "NAME", DC2.Text)
    End If
    'RS2("TID") = V_Tid
    RS2("DCNO") = T1.Text
    RS2("ETYPE") = "PTP"
    RS2("DATE") = DT1.Value
    RS2("PID") = FG1.TextMatrix(r, 1)
    RS2("DESCRIPTION") = "PTP " & T1.Text & " " & FG1.TextMatrix(r, 3)
    RS2("DESCRIPTION2") = FG1.TextMatrix(r, 3)
    RS2("INVOICE") = FG1.TextMatrix(r, 4)
    RS2("DEBIT") = Val(FG1.TextMatrix(r, 5))
    RS2.Update
        '''''''''''''''''''''''CASH
    Set RsL = New ADODB.Recordset
    RsL.Open "SELECT * FROM PLEDGER WHERE PLEDID=(SELECT MAX(PLEDID) FROM PLEDGER)", CN, adOpenStatic, adLockOptimistic
    RsL.AddNew
    RsL("PLEDID") = MaX_ID("PLEDID", "PLEDGER")
    If DC2.Text <> "" Then
    RsL("SID") = ID_NaME("PARTY", "PID", "NAME", DC2.Text)
    End If
    'RsL("TID") = V_Tid
    RsL("PID") = ID_NaME("PARTY", "PID", "NAME", "CASH")
    RsL("DCNO") = T1.Text
    RsL("ETYPE") = "PTP"
    RsL("DATE") = DT1.Value
    RsL("DESCRIPTION") = "PTP " & T1.Text & " (" & FG1.TextMatrix(r, 2) & ") " & FG1.TextMatrix(r, 3)
    RsL("INVOICE") = FG1.TextMatrix(r, 4)
    RsL("CREDIT") = Val(FG1.TextMatrix(r, 5))
    RsL.Update
    RsL.Close
    
    r = r + 1
    Loop
Call cmdReset_Click
End Sub

Private Sub DC1_GotFocus()
DC1_Change
SendKeys "{F4}"
End Sub

Private Sub DC2_Change()
If CC = False Then
Set RSP = New ADODB.Recordset
If DC2.Text <> "" Then
RSP.Open "SELECT NAME FROM PARTY WHERE NAME Like '" & UCase(DC2.Text) & "%' AND TYPE='SALESMAN'  ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
Else
RSP.Open "SELECT NAME FROM PARTY WHERE TYPE='SALESMAN'  ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
End If
Set DC2.RowSource = RSP
DC2.ListField = "NAME"
End If
End Sub

Private Sub DC2_GotFocus()
DC2_Change
End Sub

Private Sub DC2_KeyDown(KeyCode As Integer, shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DC2_Validate(Cancel As Boolean)
If DC2.Text <> "" Then
Set Rs = New ADODB.Recordset
Rs.Open "SELECT * FROM PARTY WHERE NAME='" & DC2.Text & "'", CN, adOpenStatic, adLockOptimistic
If Rs.RecordCount > 0 Then
Cancel = False
Else
MsgBox "Select a valid party please"
Cancel = True
End If
End If
End Sub

Private Sub FG1_GotFocus()
SHoW_CoNT
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, shift As Integer)
If KeyCode = vbKeyF2 Then
FG1.RemoveItem (FG1.Row)
ElseIf KeyCode = vbKeyF3 Then
frmNEW.Show vbModal
ElseIf KeyCode = vbKeyF5 Then
Call cmdReset_Click
End If
End Sub

Private Sub Form_Load()
''''''''''''''''''' theme
Set OBJ = New clsFunction
OBJ.clear Me
'''''''''''''''''''''''''''''

Get_head
DT1.Value = Date
T1.Text = MaX_DcN("PTP", "PLEDGER")
End Sub

Sub SHoW_CoNT()
   If FG1.CoL = 2 Then
        DC1.Text = FG1.TextMatrix(FG1.Row, FG1.CoL)
        DC1.Top = FG1.Top + FG1.CellTop
        DC1.Left = FG1.Left + FG1.CellLeft
        DC1.Visible = True
        DC1.SetFocus
        DC1.Width = FG1.CellWidth
        Txt1.Visible = False
    ElseIf FG1.CoL = 3 Or FG1.CoL = 4 Or FG1.CoL = 5 Then
        Txt1.Text = FG1.TextMatrix(FG1.Row, FG1.CoL)
        Txt1.Left = FG1.Left + FG1.CellLeft
        Txt1.Top = FG1.Top + FG1.CellTop
        Txt1.Height = FG1.CellHeight
        Txt1.Width = FG1.CellWidth
        Txt1.Visible = True
        Txt1.SetFocus
        DC1.Visible = False
    End If
    
End Sub

Function Get_head()
DC1.Visible = False
Txt1.Visible = False
FG1.clear
FG2.clear
FG1.Rows = 2
    FG1.TextMatrix(0, 0) = "SNO"
    FG1.TextMatrix(0, 1) = "PID"
    FG1.TextMatrix(0, 2) = "Account"
    FG1.TextMatrix(0, 3) = "Particulars"
    FG1.TextMatrix(0, 4) = "Invoice#"
    FG1.TextMatrix(0, 5) = "Amount"
    
FG1.ColWidth(0) = 500
FG1.ColWidth(1) = 0
FG1.ColWidth(2) = 4000
FG1.ColWidth(3) = 3000
FG1.ColWidth(4) = 1000
FG1.ColWidth(5) = 1000
'''''''''''''''
FG1.ColAlignment(2) = 0
'''''''''''
FG2.ColWidth(0) = 500
FG2.ColWidth(1) = 0
FG2.ColWidth(2) = 4000
FG2.ColWidth(3) = 3000
FG2.ColWidth(4) = 1000
FG2.ColWidth(5) = 1000

For i = 1 To FG1.Rows - 1
FG1.TextMatrix(i, 0) = i
Next i

End Function

Private Sub FG1_Click()
SHoW_CoNT
End Sub

Private Sub T1_KeyDown(KeyCode As Integer, shift As Integer)
If KeyCode = 38 Then
T1.Text = Val(T1.Text) - 1
T1_KeyPress 13
ElseIf KeyCode = 40 Then
T1.Text = Val(T1.Text) + 1
T1_KeyPress 13
End If
End Sub

Private Sub T1_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
Call UP_VIeW
End If
End Sub

Function UP_VIeW()
'If Chk_Tid("PLEDGER", "PTP", T1.Text) = False Then
'MsgBox "Change Login"
'Exit Function
'End If

c = Val(T1.Text)
cmdReset_Click
T1.Text = c

r = 1
    Set RS2 = New ADODB.Recordset
    RS2.Open "SELECT * FROM PLEDGER WHERE DCNO=" & T1.Text & " AND ETYPE='PTP' AND PID<>" & ID_NaME("PARTY", "PID", "NAME", "CASH") & " ORDER BY PLEDID", CN, adOpenStatic, adLockOptimistic
    If RS2.RecordCount > 0 Then
    cmdUpdate.Enabled = True
    cmdDelete.Enabled = True
    cmdSave.Enabled = False
    'DT1.MinDate = "1-1-2003"
    'DT1.MaxDate = "1-1-2025"
    DT1.Value = RS2("DATE")
    If Not IsNull(RS2!SID) Then
    DC2.Text = NaME_iD("PARTY", "PID", RS2!SID)
    End If
    For counter = 1 To RS2.RecordCount
    Set rs1 = New ADODB.Recordset
    rs1.Open "SELECT NAME FROM PARTY WHERE PID=" & RS2("PID") & "", CN, adOpenStatic, adLockOptimistic
    FG1.TextMatrix(r, 1) = RS2("PID")
    FG1.TextMatrix(r, 2) = rs1("NAME")
    FG1.TextMatrix(r, 3) = IIf(IsNull(RS2("DESCRIPTION2")), "", RS2("DESCRIPTION2"))
    FG1.TextMatrix(r, 4) = IIf(IsNull(RS2("INVOICE")), 0, RS2("INVOICE"))
    FG1.TextMatrix(r, 5) = RS2("DEBIT")
    RS2.MoveNext
    r = r + 1
    FG1.Rows = r + 1
    rs1.Close
    Next counter
    FG1_TOT
End If
End Function

Function FG1_TOT()
FG2.clear
For i = 1 To FG1.Rows - 1
FG2.TextMatrix(0, 5) = Val(FG2.TextMatrix(0, 5)) + Val(FG1.TextMatrix(i, 5))
Next i
End Function

Private Sub Txt1_Change()
FG1.TextMatrix(FG1.Row, FG1.CoL) = Txt1.Text
FG1_TOT
End Sub

Private Sub Txt1_KeyDown(KeyCode As Integer, shift As Integer)
If KeyCode = vbKeyRight Then
'If FG1.CoL <> FG1.Cols - 1 Then
'FG1.CoL = FG1.CoL + 1
'End If
ElseIf KeyCode = vbKeyLeft Then
'If FG1.CoL <> 0 Then
'FG1.CoL = FG1.CoL - 1
'End If
ElseIf KeyCode = vbKeyUp Then
If FG1.Row <> 1 Then
FG1.Row = FG1.Row - 1
End If
ElseIf KeyCode = vbKeyDown Then
If FG1.Row <> FG1.Rows - 1 Then
FG1.Row = FG1.Row + 1
End If
End If
SHoW_CoNT
End Sub

Private Sub Txt1_KeyPress(KeyAscii As Integer)
'''Add new row
If FG1.Rows = FG1.Row + 1 Then
FG1.Rows = FG1.Rows + 1
End If
'''Got ot next row
If KeyAscii = 13 Then
If FG1.CoL = 5 Then
FG1.Row = FG1.Row + 1
FG1.CoL = 2
Else
FG1.CoL = FG1.CoL + 1
End If
SHoW_CoNT
End If
End Sub

Private Sub cmdDelete_Click()
CN.Execute "DELETE FROM PLEDGER WHERE DCNO=" & T1.Text & " AND ETYPE='PTP'"
End Sub

Private Sub cmdPrint_Click()
Unload rptCashReport
UpdateReportCredentials rptCashReport
With rptCashReport
.txtHead.SetText "Cash Payment Voucher"
.txtDate.SetText "Voucher # " & Me.T1.Text
'.txtDate.Suppress = True
.txtCredit.Suppress = True
'.DEBIT1.Suppress = True
.RecordSelectionFormula = "{pledger.etype}='PTP' AND {PARTY.NAME} <> 'CASH' AND {pledger.dcno}=" & T1.Text
End With
ReportsViewer.CRViewer1.ReportSource = rptCashReport
ReportsViewer.CRViewer1.ViewReport
ReportsViewer.Show vbModal
End Sub

Private Sub cmdUpdate_Click()
Call cmdDelete_Click
Call cmdSave_Click
End Sub

Private Sub cmdReset_Click()
Get_head
T1.Text = MaX_DcN("PTP", "PLEDGER")
DC2.Text = ""
cmdSave.Enabled = True
cmdDelete.Enabled = False
cmdUpdate.Enabled = False
End Sub
