VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{D750EAB9-EB7B-450B-B9B6-0A0A17E13DCA}#1.0#0"; "XP.ocx"
Begin VB.Form frmPN 
   BackColor       =   &H00C0C0C0&
   Caption         =   "Product Nevegation"
   ClientHeight    =   6870
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   7815
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frmPN.frx":0000
   ScaleHeight     =   6870
   ScaleWidth      =   7815
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox Txt1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   2160
      MaxLength       =   60
      TabIndex        =   6
      Top             =   2880
      Width           =   975
   End
   Begin MSDataListLib.DataCombo DCPR 
      Height          =   330
      Left            =   480
      TabIndex        =   5
      Top             =   2880
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   582
      _Version        =   393216
      Appearance      =   0
      BackColor       =   12632319
      ForeColor       =   8388608
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.TextBox T1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   375
      Left            =   6000
      MaxLength       =   60
      TabIndex        =   0
      Top             =   240
      Width           =   1095
   End
   Begin MSFlexGridLib.MSFlexGrid FG1 
      Height          =   3495
      Left            =   480
      TabIndex        =   4
      Top             =   2160
      Width           =   6735
      _ExtentX        =   11880
      _ExtentY        =   6165
      _Version        =   393216
      Rows            =   150
      Cols            =   8
      AllowBigSelection=   0   'False
      GridLines       =   3
      Appearance      =   0
   End
   Begin MSComCtl2.DTPicker DT1 
      Height          =   375
      Left            =   5160
      TabIndex        =   1
      Top             =   720
      Width           =   1935
      _ExtentX        =   3413
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   12648447
      CalendarTitleBackColor=   12640511
      CustomFormat    =   "dd-MMM-yy"
      Format          =   20840449
      CurrentDate     =   37916
   End
   Begin MSFlexGridLib.MSFlexGrid FG2 
      Height          =   255
      Left            =   480
      TabIndex        =   13
      Top             =   5640
      Width           =   6735
      _ExtentX        =   11880
      _ExtentY        =   450
      _Version        =   393216
      Rows            =   1
      Cols            =   8
      FixedRows       =   0
      AllowBigSelection=   0   'False
      GridLines       =   3
      Appearance      =   0
   End
   Begin MSDataListLib.DataCombo DC1 
      Height          =   330
      Left            =   2640
      TabIndex        =   2
      Top             =   1200
      Width           =   4575
      _ExtentX        =   8070
      _ExtentY        =   582
      _Version        =   393216
      Appearance      =   0
      BackColor       =   12632319
      ForeColor       =   8388608
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSDataListLib.DataCombo DC2 
      Height          =   330
      Left            =   2640
      TabIndex        =   3
      Top             =   1680
      Width           =   4575
      _ExtentX        =   8070
      _ExtentY        =   582
      _Version        =   393216
      Appearance      =   0
      BackColor       =   12632319
      ForeColor       =   8388608
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin XPLayout.XPButton cmdSave 
      Height          =   375
      Left            =   2040
      TabIndex        =   8
      Top             =   6120
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Save"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "frmPN.frx":1596D6
      PICN            =   "frmPN.frx":1596F2
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin XPLayout.XPButton cmdDelete 
      Height          =   375
      Left            =   3360
      TabIndex        =   9
      Top             =   6120
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Delete"
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "frmPN.frx":159C8E
      PICN            =   "frmPN.frx":159CAA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin XPLayout.XPButton cmdPrint 
      Height          =   375
      Left            =   6000
      TabIndex        =   11
      Top             =   6120
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Print"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "frmPN.frx":15A246
      PICN            =   "frmPN.frx":15A262
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin XPLayout.XPButton cmdUpdate 
      Height          =   375
      Left            =   4680
      TabIndex        =   10
      Top             =   6120
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Update"
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "frmPN.frx":15A7FE
      PICN            =   "frmPN.frx":15A81A
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin XPLayout.XPButton cmdReset 
      Height          =   375
      Left            =   720
      TabIndex        =   7
      Top             =   6120
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Reset"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "frmPN.frx":15ADB6
      PICN            =   "frmPN.frx":15ADD2
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "To"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000040&
      Height          =   285
      Index           =   1
      Left            =   600
      TabIndex        =   16
      Top             =   1680
      Width           =   270
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "From"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000040&
      Height          =   285
      Index           =   3
      Left            =   600
      TabIndex        =   15
      Top             =   1200
      Width           =   555
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "D.NO."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000040&
      Height          =   270
      Index           =   2
      Left            =   600
      TabIndex        =   14
      Top             =   240
      Width           =   660
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Date"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000040&
      Height          =   285
      Index           =   0
      Left            =   600
      TabIndex        =   12
      Top             =   720
      Width           =   480
   End
End
Attribute VB_Name = "frmPN"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Function CL_FG1()
FG1.clear
FG2.clear
FG1.TextMatrix(0, 0) = "SNO"
FG1.TextMatrix(0, 1) = "PRID"
FG1.TextMatrix(0, 2) = "Product"
FG1.TextMatrix(0, 3) = "Qty"
FG1.TextMatrix(0, 4) = "Pieces"
'FG1.TextMatrix(0, 5) = "Description"
'FG1.TextMatrix(0, 5) = "F.D.S.Qty"
'FG1.TextMatrix(0, 6) = "T.D.S.Qty"
'FG1.TextMatrix(0, 7) = "S.Qty"


FG1.ColWidth(0) = 500
FG1.ColWidth(1) = 0
FG1.ColWidth(2) = 4000
FG1.ColWidth(3) = 1000
FG1.ColWidth(4) = 0
FG1.ColWidth(5) = 0
FG1.ColWidth(6) = 0
FG1.ColWidth(7) = 0

'''''''FG2''''''
FG2.ColWidth(0) = 500
FG2.ColWidth(1) = 0
FG2.ColWidth(2) = 4000
FG2.ColWidth(3) = 1000
FG2.ColWidth(4) = 0
FG2.ColWidth(5) = 0
FG2.ColWidth(6) = 0
FG2.ColWidth(7) = 0

For i = 1 To FG1.Rows - 1
FG1.TextMatrix(i, 0) = i
Next i

Txt1.Visible = False
Txt1.Text = ""
DCPR.Text = ""
DCPR.Visible = False
End Function

Private Sub DC1_Change()
If CC = False Then
Set RSP = New ADODB.Recordset
If DC1.Text <> "" Then
RSP.Open "SELECT NAME FROM warehouse WHERE NAME Like '" & DC1.Text & "%' ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
Else
RSP.Open "SELECT NAME FROM warehouse ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
End If
Set DC1.RowSource = RSP
DC1.ListField = "NAME"
End If
End Sub

Private Sub DC1_GotFocus()
DC1_Change
SendKeys "{F4}"
End Sub

Function Getp()
Set RSP = New ADODB.Recordset
If DC1.Text <> "" Then
RSP.Open "SELECT NAME FROM PARTY WHERE NAME Like '" & DC1.Text & "%' AND TYPE3<>'EXPENSE' AND TYPE3<>'BANK'  ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
Else
RSP.Open "SELECT NAME FROM PARTY WHERE TYPE3<>'EXPENSE' AND TYPE3<>'BANK' ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
End If
Set DC1.RowSource = RSP
DC1.ListField = "NAME"
End Function

Private Sub DC1_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DC1_Validate(Cancel As Boolean)
a = ID_NaME("warehouse", "dId", "name", Me.DC1.Text)
If a <> 0 Then Cancel = False Else Cancel = True
End Sub

Private Sub DC2_Change()
If CC = False Then
Set RSP = New ADODB.Recordset
If DC2.Text <> "" Then
RSP.Open "SELECT NAME FROM warehouse WHERE NAME Like '" & DC2.Text & "%' ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
Else
RSP.Open "SELECT NAME FROM warehouse ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
End If
Set DC2.RowSource = RSP
DC2.ListField = "NAME"
End If
End Sub

Private Sub DC2_GotFocus()
SendKeys "{F4}"
DC2_Change
End Sub

Private Sub DC2_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DC2_Validate(Cancel As Boolean)
FG1.CoL = 2
a = ID_NaME("warehouse", "dId", "name", Me.DC2.Text)
If a <> 0 Then Cancel = False Else Cancel = True
End Sub

Private Sub DCD_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
    If FG1.CoL = 4 Then
        FG1.Row = FG1.Row + 1
        FG1.CoL = 2
    Else
        FG1.CoL = FG1.CoL + 1
    End If
    SHoW_CoNT
End If
End Sub

Private Sub DCPR_Change()
If CC = False Then
Set RSP = New ADODB.Recordset
If DCPR.Text <> "" Then
RSP.Open "SELECT NAME FROM PRODUCT WHERE NAME Like '" & DCPR.Text & "%' ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
Else
RSP.Open "SELECT NAME FROM PRODUCT ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
End If
Set DCPR.RowSource = RSP
DCPR.ListField = "NAME"
End If
FG1.TextMatrix(FG1.Row, FG1.CoL) = DCPR.Text
End Sub

Private Sub DCPR_GotFocus()
SendKeys "{F4}"
DCPR_Change
End Sub

Private Sub DCPR_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DCPR_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
DCPR_Validate False
If FG1.CoL = 4 Then
FG1.Row = FG1.Row + 1
FG1.CoL = 1
Else
FG1.CoL = FG1.CoL + 1
End If
SHoW_CoNT
End If
End Sub

Private Sub DCPR_Validate(Cancel As Boolean)
If DCPR.Text <> "" Then
Set Rs = New ADODB.Recordset
Rs.Open "SELECT PRID,PRATE,ppc FROM PRODUCT WHERE NAME='" & DCPR.Text & "'", CN, adOpenStatic, adLockOptimistic
If Rs.RecordCount > 0 Then
FG1.TextMatrix(FG1.Row, 1) = IIf(IsNull(Rs("PRID")), "", Rs("PRID"))
FG1.TextMatrix(FG1.Row, 6) = IIf(IsNull(Rs("PPC")), "", Rs("PPC"))
End If
If Val(FG1.TextMatrix(FG1.Row, 1)) = 0 Then
MsgBox "Select a valid Product please"
Cancel = True
Else
Cancel = False
End If
End If
End Sub

Private Sub FG1_Click()
SHoW_CoNT
End Sub

Function SHoW_CoNT()
If FG1.CoL = 2 Then
        DCPR.Text = FG1.TextMatrix(FG1.Row, FG1.CoL)
        DCPR.Top = FG1.Top + FG1.CellTop
        DCPR.Left = FG1.Left + FG1.CellLeft
        DCPR.Visible = True
        DCPR.SetFocus
        DCPR.Width = FG1.CellWidth
        Txt1.Visible = False
ElseIf FG1.CoL = 3 Then
Txt1.Text = FG1.TextMatrix(FG1.Row, FG1.CoL)
    Txt1.Left = FG1.Left + FG1.CellLeft
    Txt1.Top = FG1.Top + FG1.CellTop
    Txt1.Height = FG1.CellHeight
    Txt1.Width = FG1.CellWidth
    Txt1.Visible = True
    Txt1.SetFocus
    DCPR.Visible = False
ElseIf FG1.CoL = 4 Then
FG1.CoL = 2
FG1.Row = FG1.Row + 1
End If
End Function

Function GetpR()
End Function

Private Sub FG1_EnterCell()
SHoW_CoNT
End Sub

Private Sub FG1_GotFocus()
SHoW_CoNT
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyF3 Then
frmNEW.Show vbModal
ElseIf KeyCode = vbKeyF7 Then
frmNPRODUCT.Show vbModal
ElseIf KeyCode = vbKeyF5 Then
Call cmdReset_Click
ElseIf KeyCode = vbKeyEscape Then
Unload Me
ElseIf KeyCode = vbKeyF6 Then
Y = MsgBox("Are you sure to cmdDeletee ?", vbYesNo)
If Y = vbYes Then
FG1.RemoveItem (FG1.Row)
Else
End If
End If
End Sub

Private Sub Form_Load()
''''''''''''''''''' theme
Set OBJ = New clsFunction
OBJ.clear Me
'''''''''''''''''''''''''''''

FG1.ColAlignment(2) = 0
DT1.Value = Date
cmdReset_Click
End Sub

Private Sub T1_KeyDown(KeyCode As Integer, Shift As Integer)
If Val(T1.Text) > 0 Then
If KeyCode = 38 Then
T1.Text = T1.Text - 1
T1_KeyPress 13
ElseIf KeyCode = 40 Then
T1.Text = T1.Text + 1
T1_KeyPress 13
End If
End If
End Sub

Private Sub T1_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
'If Chk_Tid("STOCK", "TRANSFER", T1.Text) = False Then
'MsgBox "Change Login"
'Exit Sub
'End If

c = Val(T1.Text)
cmdReset_Click
T1.Text = c

    Set Rs = New ADODB.Recordset
    Rs.Open "SELECT * FROM STOCK_main WHERE DCNO=" & Val(T1.Text) & " AND ETYPE='TRANSFER' AND PID IS NOT NULL", CN, adOpenStatic, adLockOptimistic
    If Rs.RecordCount > 0 Then
    cmdSave.Enabled = False
    cmdDelete.Enabled = True
    cmdPrint.Enabled = True
    cmdUpdate.Enabled = True
    DC2.Text = NaME_iD("warehouse", "dID", Rs!PID)
    DC1.Text = NaME_iD("warehouse", "dID", Rs!did)
    DT1.Value = Rs("DATE")
    r = 1
    Set RsS = New ADODB.Recordset
    RsS.Open "SELECT * FROM STOCK WHERE DCNO=" & Val(T1.Text) & " AND ETYPE='TRANSFER' AND tqtyp <0", CN, adOpenStatic, adLockOptimistic
    If RsS.RecordCount > 0 Then
        Do Until RsS.EOF
        FG1.TextMatrix(r, 1) = RsS!PRID
        FG1.TextMatrix(r, 2) = NaME_iD("PRODUCT", "PRID", RsS!PRID)
        FG1.TextMatrix(r, 3) = Abs(RsS("QTYC"))
        FG1.TextMatrix(r, 4) = Abs(RsS("QTYP"))
        FG1.TextMatrix(r, 5) = IIf(IsNull(RsS!tqtyp), 0, Abs(RsS!tqtyp))
        RsS.MoveNext
        r = r + 1
        Loop
    End If
    FG1_TOT
    Else
    'Call cmdReset_Click
    End If
End If
End Sub

Private Sub T2_Validate(Cancel As Boolean)
If T2.Text = "" Then
MsgBox "ENTER INVOICE"
Cancel = True
End If
End Sub

Private Sub Txt1_Change()
FG1.TextMatrix(FG1.Row, FG1.CoL) = Txt1.Text
FG1.TextMatrix(FG1.Row, 5) = Val(FG1.TextMatrix(FG1.Row, 6)) * Val(FG1.TextMatrix(FG1.Row, 3)) + Val(FG1.TextMatrix(FG1.Row, 4))
FG1_TOT
End Sub

Function FG1_TOT()
FG2.clear
For i = 1 To FG1.Rows - 1
FG2.TextMatrix(0, 3) = Val(FG2.TextMatrix(0, 3)) + Val(FG1.TextMatrix(i, 3))
FG2.TextMatrix(0, 4) = Val(FG2.TextMatrix(0, 4)) + Val(FG1.TextMatrix(i, 4))
Next i
End Function

Private Sub Txt1_GotFocus()
SendKeys "{home}+{end}"
End Sub

Private Sub Txt1_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyRight Then
If FG1.CoL <> FG1.Cols - 1 Then
FG1.CoL = FG1.CoL + 1
End If
ElseIf KeyCode = vbKeyLeft Then
If FG1.CoL <> 0 Then
FG1.CoL = FG1.CoL - 1
End If
ElseIf KeyCode = vbKeyUp Then
If FG1.Row <> 1 Then
FG1.Row = FG1.Row - 1
End If
ElseIf KeyCode = vbKeyDown Then
If FG1.Row <> FG1.Rows - 1 Then
FG1.Row = FG1.Row + 1
End If
End If
SHoW_CoNT
End Sub

Private Sub Txt1_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
If FG1.CoL = 4 Then
FG1.Row = FG1.Row + 1
FG1.CoL = 2
Else
FG1.CoL = FG1.CoL + 1
End If
SHoW_CoNT
End If
End Sub

Private Sub cmdSave_Click()
If T1.Text <> "" Then
''''''''''''stock_main
Set RsM = New ADODB.Recordset
RsM.Open "SELECT * FROM STOCK_MAIN", CN, adOpenStatic, adLockOptimistic
RsM.AddNew
Dim v_stmid As Double
v_stmid = MaX_ID("STMID", "STOCK_main")
RsM("STMID") = v_stmid
RsM("DATE") = DT1.Value
RsM("PID") = ID_NaME("warehouse", "dID", "NAME", DC2.Text)
RsM("DID") = ID_NaME("warehouse", "dID", "NAME", DC1.Text)
RsM("DCNO") = T1.Text
RsM("ETYPE") = "TRANSFER"
RsM.Update

''''''''''''''''STOCK''''''''''''''''''''''
r = 1
Set Rs = New ADODB.Recordset
Rs.Open "SELECT * FROM STOCK", CN, adOpenStatic, adLockOptimistic
Do Until FG1.TextMatrix(r, 2) = ""
Rs.AddNew
Rs("STID") = MaX_ID("STID", "STOCK")
Rs("DCNO") = T1.Text
Rs("ETYPE") = "TRANSFER"
Rs("STMID") = v_stmid
Rs("PRID") = Val(FG1.TextMatrix(r, 1))
Rs("QTYC") = -Val(FG1.TextMatrix(r, 3))
Rs("QTYP") = -Val(FG1.TextMatrix(r, 4))
Rs("tqtyp") = -Val(FG1.TextMatrix(r, 5))
Rs.Update
'''''''''''''''''''''''''''''
Rs.AddNew
Rs("STID") = MaX_ID("STID", "STOCK")
Rs("DCNO") = T1.Text
Rs("ETYPE") = "TRANSFER"
Rs("STMID") = v_stmid
Rs("PRID") = Val(FG1.TextMatrix(r, 1))
Rs("QTYC") = Val(FG1.TextMatrix(r, 3))
Rs("QTYP") = Val(FG1.TextMatrix(r, 4))
Rs("tqtyp") = Val(FG1.TextMatrix(r, 5))
Rs.Update

r = r + 1
Loop

Call cmdReset_Click
Else
MsgBox "Enter Into Empty Fields"
End If
End Sub

Private Sub cmdDelete_Click()
CN.Execute "Delete STOCK WHERE DCNO=" & T1.Text & " AND ETYPE='TRANSFER'"
CN.Execute "Delete STOCK_main WHERE DCNO=" & T1.Text & " AND ETYPE='TRANSFER'"
End Sub

Private Sub cmdPrint_Click()
If T1.Text <> "" Then
'If DE1.rsCTEMP.State = 1 Then
'DE1.rsCTEMP.Close
'End If
'''''''''''''''''''''''''''''''''
'''''''''''''''''''''
CN.Execute "cmdDeleteE FROM TEMP"
r = 1
Counter = 1
Set RsT = New ADODB.Recordset
RsT.Open "SELECT * FROM TEMP", CN, adOpenStatic, adLockOptimistic
Do Until FG1.TextMatrix(r, 1) = ""
RsT.AddNew
RsT("SNO") = r
RsT("T1") = FG1.TextMatrix(r, 2)
RsT("N2") = Val(FG1.TextMatrix(r, 3))
RsT("T2") = FG1.TextMatrix(r, 4)
RsT.cmdUpdatee
r = r + 1
Loop
''''''''''''''''''''''
'''''''''''''''''''''''''''''''''
DE1.rsCTEMP.Open "SELECT * FROM TEMP"
rePRDNAV.Sections(1).Controls("TV").Caption = T1.Text
rePRDNAV.Sections(1).Controls("TN").Caption = DC1.Text
rePRDNAV.Sections(1).Controls("label5").Caption = DC2.Text
'rePRDNAV.Sections(1).Controls("TI").Caption = T2.Text
rePRDNAV.Sections(1).Controls("TD").Caption = DT1.Value
rePRDNAV.Show vbModal
End If
End Sub

Private Sub cmdUpdate_Click()
If T1.Text Then
Call cmdDelete_Click
Call cmdSave_Click
Call cmdReset_Click
Else
MsgBox "ENTER INTO EMPTY FIELDS"
End If
End Sub

Private Sub cmdReset_Click()
cmdSave.Enabled = True
cmdDelete.Enabled = False
cmdPrint.Enabled = False
cmdUpdate.Enabled = False
T1.Text = MaX_DcN("TRANSFER", "STOCK")
CL_FG1
DC1.Text = ""
DC2.Text = ""
End Sub
