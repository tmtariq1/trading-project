VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Object = "{D750EAB9-EB7B-450B-B9B6-0A0A17E13DCA}#1.0#0"; "xp.ocx"
Begin VB.Form ProfitLoss 
   Caption         =   "TRIAL BALANCE"
   ClientHeight    =   8400
   ClientLeft      =   165
   ClientTop       =   555
   ClientWidth     =   11040
   Icon            =   "ProfitLoss.frx":0000
   LinkTopic       =   "Form1"
   Picture         =   "ProfitLoss.frx":0442
   ScaleHeight     =   8400
   ScaleWidth      =   11040
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox Text2 
      Height          =   375
      Left            =   11040
      TabIndex        =   3
      Text            =   "Text2"
      Top             =   4440
      Visible         =   0   'False
      Width           =   150
   End
   Begin VB.TextBox Text3 
      Height          =   495
      Left            =   11040
      TabIndex        =   2
      Text            =   "Text1"
      Top             =   3720
      Visible         =   0   'False
      Width           =   255
   End
   Begin MSFlexGridLib.MSFlexGrid MS1 
      Height          =   7455
      Left            =   0
      TabIndex        =   1
      Top             =   720
      Width           =   11025
      _ExtentX        =   19447
      _ExtentY        =   13150
      _Version        =   393216
      Cols            =   4
      FixedCols       =   0
      BackColor       =   16777215
      BackColorBkg    =   14737632
      AllowBigSelection=   0   'False
      HighLight       =   2
      SelectionMode   =   1
      Appearance      =   0
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin XPLayout.XPButton COMMAND1 
      Height          =   375
      Left            =   9720
      TabIndex        =   0
      Top             =   240
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Show"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   16711680
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "ProfitLoss.frx":159B18
      PICN            =   "ProfitLoss.frx":159B34
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSComctlLib.ProgressBar pgb1 
      Height          =   255
      Left            =   0
      TabIndex        =   4
      Top             =   8160
      Visible         =   0   'False
      Width           =   11055
      _ExtentX        =   19500
      _ExtentY        =   450
      _Version        =   393216
      Appearance      =   0
   End
   Begin VB.Menu PR 
      Caption         =   "PRINT"
   End
End
Attribute VB_Name = "ProfitLoss"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Command1_Click()
MS1.clear
pgb1.Visible = True
'MS1.Rows = 3

Dim totDebit As Double, totCredit As Double
'B = 0
totDebit = 0
totCredit = 0
MS1.clear
MS1.Rows = 2

MS1.ColWidth(0) = 600
MS1.ColWidth(1) = 7000
MS1.ColWidth(2) = 1500
MS1.ColWidth(3) = 1500
'MS1.Width = 8500
MS1.TextMatrix(0, 0) = "S.N"
MS1.TextMatrix(0, 1) = "PARTICULARS"
MS1.TextMatrix(0, 2) = "DEBIT"
MS1.TextMatrix(0, 3) = "CREDIT"


r = 1
i = 1
Set rsa = New ADODB.Recordset
rsa.Open "SELECT P.PID,P.[NAME],SUM(L.DEBIT) Debit,SUM(L.CREDIT) Credit,SUM(ISNULL(L.DEBIT, 0)-ISNULL(L.CREDIT, 0)) AS Balance FROM PARTY P,PLEDGER L WHERE P.PID=L.PID AND L.[DATE] <='" & DT1.Value & "' group by P.Pid,P.[Name] ORDER BY [NAME]", CN
pgb1.Max = rsa.RecordCount

Do Until rsa.EOF
MS1.Rows = MS1.Rows + 1
pgb1.Value = r
'i = i + 1
'Set PD = New ADODB.Recordset
'Set PC = New ADODB.Recordset
Dim T As Double
'PD.Open "SELECT SUM(DEBIT),SUM(CREDIT) FROM PLEDGER WHERE PID=" & rsa(0) & " AND DATE<='" & DT1.Value & "'", CN
'PC.Open "SELECT SUM(CREDIT) FROM PLEDGER WHERE PID=" & rsa(0) & " AND DATE<='" & DT1.Value & "'", CN
'T = Round(IIf(Not IsNull(PD(0)), PD(0), 0) - IIf(Not IsNull(PC(0)), PC(0), 0), 0)
T = Round(rsa("Balance"), 2)
If T < 0 Then
'T = T * -1
MS1.TextMatrix(r, 3) = Abs(T)
'r = r + 1
totCredit = totCredit + Abs(T)
ElseIf T > 0 Then
'MS1.Rows = MS1.Rows + 1
MS1.TextMatrix(r, 2) = T
totDebit = totDebit + T
End If

MS1.TextMatrix(r, 0) = r
MS1.TextMatrix(r, 1) = rsa("Name")
rsa.MoveNext
r = r + 1
Loop
rsa.Close
pgb1.Value = pgb1.Max

r = r + 1
MS1.Rows = MS1.Rows + 1
MS1.TextMatrix(r, 1) = "TOTALS"
MS1.TextMatrix(r, 2) = Round(totDebit, 2)
MS1.TextMatrix(r, 3) = Round(totCredit, 2)
MS1.ColAlignment(1) = 0
pgb1.Visible = False
End Sub

Private Sub Form_Load()
''''''''''''''''''' theme
Set OBJ = New clsFunction
OBJ.clear Me
'''''''''''''''''''''''''''''

DT1.Value = Date
MS1.clear
End Sub

Function EXPEN(V_D2 As Date, V_D3 As Date, V_D4 As Date)
Dim V_D1 As Date
Dim V_MM As String
V_MM = Format(V_D2, "MMM")
Dim V_YY As Integer
V_YY = Format(V_D2, "yyyy")
V_D1 = 1 & "-" & V_MM & "-" & V_YY
'''''''''''
Dim V_D11 As Date
V_MM = Format(V_D3, "MMM")
V_YY = Format(V_D3, "yyyy")
V_D11 = 1 & "-" & V_MM & "-" & V_YY
''''''''''''''''''
Dim V_D111 As Date
V_MM = Format(V_D4, "MMM")
V_YY = Format(V_D4, "yyyy")
V_D111 = 1 & "-" & V_MM & "-" & V_YY
''''''''''''''''''''''''''''''''''''''''''''''''
Dim TT As Double
Set RS2 = New ADODB.Recordset
RS2.Open "SELECT DISTINCT PID FROM PLEDGER WHERE PID IN (SELECT SNO FROM PARTY WHERE TYPE='EXPENSE' AND PARTY IS NOT NULL)", CN, adOpenStatic, adLockOptimistic
Do Until RS2.EOF
Set PD = New ADODB.Recordset
Set PC = New ADODB.Recordset
PD.Open "SELECT SUM(DEBIT) FROM PLEDGER WHERE PID=" & RS2(0) & " AND DATE BETWEEN '" & V_D1 & "' AND '" & V_D2 & "'", CN, adOpenStatic, adLockOptimistic
PC.Open "SELECT SUM(CREDIT)FROM  PLEDGER WHERE PID=" & RS2(0) & " AND DATE BETWEEN '" & V_D1 & "' AND '" & V_D2 & "'", CN, adOpenStatic, adLockOptimistic
TT = IIf(IsNull(PD(0)), 0, PD(0)) - IIf(IsNull(PC(0)), 0, PC(0))
MS1.Rows = MS1.Rows + 1
MS1.TextMatrix(r, 2) = TT
Set rs1 = New ADODB.Recordset
rs1.Open "SELECT PARTY FROM PARTY WHERE SNO =" & RS2(0) & "", CN
B = B + TT
'''''''''''''''''
TT = 0
Set PD = New ADODB.Recordset
Set PC = New ADODB.Recordset
PD.Open "SELECT SUM(DEBIT) FROM PLEDGER WHERE PID=" & RS2(0) & " AND DATE BETWEEN '" & V_D11 & "' AND '" & V_D3 & "'", CN, adOpenStatic, adLockOptimistic
PC.Open "SELECT SUM(CREDIT)FROM  PLEDGER WHERE PID=" & RS2(0) & " AND DATE BETWEEN '" & V_D11 & "' AND '" & V_D3 & "'", CN, adOpenStatic, adLockOptimistic
TT = IIf(IsNull(PD(0)), 0, PD(0)) - IIf(IsNull(PC(0)), 0, PC(0))
MS1.TextMatrix(r, 3) = TT
'''''''''''''''
TT = 0
Set PD = New ADODB.Recordset
Set PC = New ADODB.Recordset
PD.Open "SELECT SUM(DEBIT) FROM PLEDGER WHERE PID=" & RS2(0) & " AND DATE BETWEEN '" & V_D111 & "' AND '" & V_D4 & "'", CN, adOpenStatic, adLockOptimistic
PC.Open "SELECT SUM(CREDIT)FROM  PLEDGER WHERE PID=" & RS2(0) & " AND DATE BETWEEN '" & V_D111 & "' AND '" & V_D4 & "'", CN, adOpenStatic, adLockOptimistic
TT = IIf(IsNull(PD(0)), 0, PD(0)) - IIf(IsNull(PC(0)), 0, PC(0))
MS1.TextMatrix(r, 4) = TT
'''''''''''''''''''''''''''''''''
TT = 0
MS1.TextMatrix(r, 1) = "          " & rs1(0)
RS2.MoveNext
r = r + 1
PD.Close
PC.Close
Loop
RS2.Close
End Function

Private Sub SR_Click()
End Sub

Private Sub PR_Click()
CN.Execute "DELETE TEMP"
r = 1
For counter = 1 To MS1.Rows - 3

CN.Execute "Insert into temp(T1,N1,N2) values('" & MS1.TextMatrix(r, 1) & "'," & Val(MS1.TextMatrix(r, 2)) & "," & Val(MS1.TextMatrix(r, 3)) & ")"
'Set RsT = New ADODB.Recordset
'RsT.Open "SELECT * FROM TEMP", CN, adOpenStatic, adLockOptimistic
'RsT.AddNew
'RsT!T1 = MS1.TextMatrix(r, 0)
'RsT!N1 = Val(MS1.TextMatrix(r, 1))
'RsT!N2 = Val(MS1.TextMatrix(r, 2))
'RsT.Update
r = r + 1
Next
Dim crystalApp As CRAXDRT.Application
Dim crystalRep As CRAXDRT.Report
Dim myTextObject As CRAXDRT.TextObject
Set crystalApp = New CRAXDRT.Application
Set crystalRep = crystalApp.OpenReport(App.Path & "\Reports\" & "TrialBalance.rpt") 'Opens the report

'Unload rptTb
UpdateReportCredentials crystalRep
'crystalRep.Sections("RH").ReportObjects("txtDate").SetText Me.DT1.Value

        ReportsViewer.CRViewer1.ReportSource = crystalRep
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
End Sub
