VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsFunction"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'Dim Con As Connection
Dim Rs As Recordset
Dim TB As Recordset
Dim ctrl As Control

Function unload_report()
'For Each Report In Galaxy_Foods
'Report.Unload
'Next
End Function

Public Function FlexTotal(F As MSFlexGrid, FlexCol As Long) As Double
    FlexTotal = 0
    For i = 1 To F.Rows - 1
        FlexTotal = FlexTotal + Val(F.TextMatrix(i, FlexCol))
    Next
End Function

Public Function clear(frm As Form) As Variant
frm.Picture = LoginForm.Picture
frm.BackColor = LoginForm.BackColor
For Each ctrl In frm
    If ctrl.Tag = ClearFalse And (TypeOf ctrl Is TextBox Or TypeOf ctrl Is DataCombo Or TypeOf ctrl Is ComboBox) Then
        ctrl.Text = ""
        ctrl.BackColor = LoginForm.Text1.BackColor
        ctrl.ForeColor = LoginForm.Text1.ForeColor
    ElseIf TypeOf ctrl Is MSHFlexGrid Then
        ctrl.clear
        ctrl.BackColor = LoginForm.FG2.BackColor
    ElseIf TypeOf ctrl Is Label Then
    ctrl.Font = LoginForm.Label1(1).Font
    ctrl.ForeColor = LoginForm.Label1(1).ForeColor
    ctrl.FontSize = LoginForm.Label1(1).FontSize
    End If
Next
End Function

Public Function CheckDate(frm As Form, TB As String) As Boolean

Dim Rs As New Recordset
Rs.Open ("select * from " & TB), Con, adOpenStatic, adLockOptimistic


For Each ctrl In frm

    If ctrl.Tag <> "" Then
     
    If Rs.Fields(Val(ctrl.Tag)).Type = 202 Then
        
        If Len(Trim(ctrl)) = 0 Then
            Set Rs = Nothing
            MsgBox "Enter Correct Text"
            CheckDate = False
            Exit Function
        End If
        
    ElseIf Rs.Fields(Val(ctrl.Tag)).Type = 7 Then
            
        If IsDate(ctrl) = False Then
            Set Rs = Nothing
            MsgBox "Enter Correct Date"
            CheckDate = False
            Exit Function
        End If
    
    Else
    
       If IsNumeric(ctrl) = False Then
            Set Rs = Nothing
            MsgBox "Enter Correct Number"
            CheckDate = False
            Exit Function
        End If
    End If



   End If
Next ctrl


CheckDate = True

Set Rs = Nothing
End Function


Public Function AddTbl(TB As String, frm As Form)
On Error Resume Next
Dim Rs As New Recordset

Rs.Open ("select * from " & TB), Con, adOpenStatic, adLockOptimistic

Rs.AddNew

'-------------------------
For Each ctrl In frm
    If ctrl.Tag <> "" Then

        If TypeOf ctrl Is Label Then
            Rs.Fields(Val(ctrl.Tag)) = ctrl.Caption
        ElseIf TypeOf ctrl Is OptionButton Then
            Rs.Fields(Val(ctrl.Tag)) = ctrl.Value
        ElseIf TypeOf ctrl Is DTPicker Then
             Rs.Fields(Val(ctrl.Tag)) = ctrl.Value
        Else
            Debug.Print ctrl.Tag
            Debug.Print ctrl.Text
            Debug.Print ctrl.Name
            Rs.Fields(Val(ctrl.Tag)) = ctrl.Text
        End If


    End If
Next ctrl

'--------------------------




Rs.Update
Rs.Close

Set Rs = Nothing

End Function
Public Function AddTblFlex(Flex As MSFlexGrid, flexStr As String, tblstr As String, TB As String, frm As Form)
On Error Resume Next
Dim tot_len_flex_str, tot_len_tbl_str  As Integer
Set Rs = New ADODB.Recordset
Rs.Open ("select * from " & TB), Con, adOpenStatic, adLockOptimistic

Dim c
Dim v
c = Split(flexStr, ",")
v = Split(tblstr, ",")



For Y = Flex.FixedRows To Flex.Rows - 1
Rs.AddNew


For Each ctrl In frm
If TypeOf ctrl Is TextBox Or TypeOf ctrl Is ComboBox Then
If ctrl.Tag <> "" Then
Debug.Print ctrl.Tag
Debug.Print ctrl.Text
Rs.Fields(Val(ctrl.Tag)) = ctrl.Text
End If
End If
Next ctrl



Flex.Row = Y
For D = 0 To UBound(c)
Flex.CoL = c(D)
Rs.Fields(Val(v(D))) = Flex.Text
Next D
Next Y
Rs.Update
End Function
Public Function MaxVal(TB As String, FieldName As String)

Dim maxPrNo As New Recordset

maxPrNo.Open ("select max( [" & FieldName & "]) from " & TB), Con, adOpenStatic, adLockOptimistic

If maxPrNo.EOF Or IsNull(maxPrNo.Fields(0)) = True Then
    MaxVal = 1
Else
    MaxVal = 1 + Val(maxPrNo.Fields(0))
End If

Set maxPrNo = Nothing

End Function

Public Function OpenTable(TB As String) As Recordset
Dim Rs As Recordset
Set Rs = New Recordset
Rs.Open ("select * from " & TB), Con, adOpenStatic, adLockOptimistic
Set OpenTable = Rs
End Function

Public Sub CmbFilled(StrQry As String, Cmb As ComboBox)
Dim TB As New Recordset
TB.Open StrQry, Con, adOpenStatic, adLockOptimistic
Cmb.clear
Do While Not TB.EOF = True
Cmb.AddItem TB.Fields(0)
TB.MoveNext
Loop
If Cmb.ListCount > 0 Then Cmb.ListIndex = 0
Set TB = Nothing
End Sub

Public Function FlexAlphabaticIndex(Flex As MSFlexGrid, StrQry As String, FieldNames As String, Optional FlexCaption As String)
Dim TB As New ADODB.Recordset
Dim Field
Dim Captin

TB.Open StrQry, Con, adOpenStatic, adLockOptimistic

Debug.Print StrQry
Field = Split(FieldNames, ",")
Flex.clear
Flex.Cols = UBound(Field) + 1
Flex.Rows = 1

If FlexCaption <> "" Then
 Caption = Split(FlexCaption, ",")
 For i = 0 To UBound(Field)
 Flex.TextMatrix(0, i) = Caption(i)
 Next i
Else
 For i = 0 To UBound(Field)
   Flex.TextMatrix(0, i) = TB.Fields(Val(Field(i))).Name
 Next i
End If
For i = 1 To TB.RecordCount
 Flex.Rows = Flex.Rows + 1
 For j = 0 To UBound(Field)
 
 If IsNull(TB.Fields(Val(Field(j)))) = False Then
 
    If TB.Fields(Val(Field(j))).Type = adDate Then
        Flex.TextMatrix(i, j) = Format(TB.Fields(Val(Field(j))), "d-m-YY")
    Else
        Flex.TextMatrix(i, j) = UCase(TB.Fields(Val(Field(j))))
    End If
 End If
 Next j
TB.MoveNext
Next i
End Function
Public Function AddTblFromListCtrl(TB As String, Lst As ListBox, frm As Form)
Dim ctrl As Control

Dim Rs As New Recordset
Rs.Open ("select * from " & TB), Con, adOpenStatic, adLockOptimistic

For a = 0 To Lst.ListCount - 1
    Rs.AddNew
        Lst.ListIndex = a
        For Each ctrl In frm
                If TypeOf ctrl Is TextBox Or TypeOf ctrl Is ComboBox Or TypeOf ctrl Is DTPicker Or TypeOf ctrl Is ListBox Then
                     If ctrl.Tag <> "" Then
                        Rs.Fields(Val(ctrl.Tag)) = ctrl.Text
                     End If
                End If
        Next ctrl
    Rs.Update
Next a

End Function
Public Function TagClear(frm As Form)
Dim ctrl As Control
For Each ctrl In frm
ctrl.Tag = ""
Next
End Function
Public Function Search(frm As Form, StrQry As String) As Variant
Dim ctrl As Control
Set TB = New Recordset

TB.Open StrQry, Con, adOpenStatic, adLockOptimistic
For Each ctrl In frm
If ctrl.Tag <> "" Then
ctrl.Text = TB.Fields(CInt(ctrl.Tag))
Search = TB.Fields(CInt(ctrl.Tag))
End If
Next ctrl
'tb.Close
End Function
Public Function CheckRepeat(TableName As String, FieldsNo As String, CheckingValues As String) As Boolean
Dim Rs As New Recordset
Rs.Open ("select * from " & TableName), Con, adOpenStatic, adLockOptimistic


FieldsNovrb = Split(FieldsNo, ",")
checkingvaluesvrb = Split(CheckingValues, ",")
Rs.MoveFirst

For a = 0 To UBound(FieldsNovrb)
    Do While Not Rs.EOF = True
    
        If Rs.Fields((Val(FieldsNovrb(a)))) = Val(checkingvaluesvrb(a)) Then
            CheckRepeat = False
            Exit Function
        Else
            Rs.MoveNext
        End If
        
    Loop
    
Next a


End Function
Public Function CmbSettingListIndex(frm As Form)
On Error Resume Next
For Each ctrl In frm
    If TypeOf ctrl Is ComboBox Then

        ctrl.ListIndex = 0
    End If
Next
End Function

