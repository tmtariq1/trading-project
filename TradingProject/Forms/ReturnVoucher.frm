VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{D750EAB9-EB7B-450B-B9B6-0A0A17E13DCA}#1.0#0"; "XP.ocx"
Begin VB.Form frmSRETURN 
   BackColor       =   &H00C0C0C0&
   Caption         =   "SALEMAN RETURN"
   ClientHeight    =   6315
   ClientLeft      =   1770
   ClientTop       =   1905
   ClientWidth     =   7365
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "ReturnVoucher.frx":0000
   ScaleHeight     =   6315
   ScaleWidth      =   7365
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox Txt1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   3000
      MaxLength       =   60
      TabIndex        =   5
      Top             =   2400
      Width           =   975
   End
   Begin MSDataListLib.DataCombo DCPR 
      Height          =   330
      Left            =   1200
      TabIndex        =   4
      Top             =   2400
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   582
      _Version        =   393216
      Appearance      =   0
      BackColor       =   12632319
      ForeColor       =   8388608
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.TextBox T1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   330
      Left            =   2040
      MaxLength       =   60
      TabIndex        =   0
      Top             =   285
      Width           =   1095
   End
   Begin MSFlexGridLib.MSFlexGrid FG1 
      Height          =   3255
      Left            =   240
      TabIndex        =   3
      Top             =   1800
      Width           =   6855
      _ExtentX        =   12091
      _ExtentY        =   5741
      _Version        =   393216
      Rows            =   150
      Cols            =   8
      AllowBigSelection=   0   'False
      Appearance      =   0
   End
   Begin MSComCtl2.DTPicker DT1 
      Height          =   375
      Left            =   2040
      TabIndex        =   2
      Top             =   1200
      Width           =   1935
      _ExtentX        =   3413
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   12648447
      CalendarTitleBackColor=   12640511
      CustomFormat    =   "dd-MMM-yy"
      Format          =   20840451
      CurrentDate     =   37916
   End
   Begin XPLayout.XPButton cmdSave 
      Height          =   375
      Left            =   1800
      TabIndex        =   7
      Top             =   5640
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Save"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "ReturnVoucher.frx":1596D6
      PICN            =   "ReturnVoucher.frx":1596F2
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin XPLayout.XPButton cmdDelete 
      Height          =   375
      Left            =   3120
      TabIndex        =   8
      Top             =   5640
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Delete"
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "ReturnVoucher.frx":159C8E
      PICN            =   "ReturnVoucher.frx":159CAA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin XPLayout.XPButton cmdPrint 
      Height          =   375
      Left            =   5760
      TabIndex        =   10
      Top             =   5640
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "P&rint"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "ReturnVoucher.frx":15A246
      PICN            =   "ReturnVoucher.frx":15A262
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin XPLayout.XPButton cmdUpdate 
      Height          =   375
      Left            =   4440
      TabIndex        =   9
      Top             =   5640
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Update"
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "ReturnVoucher.frx":15A7FE
      PICN            =   "ReturnVoucher.frx":15A81A
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin XPLayout.XPButton cmdReset 
      Height          =   375
      Left            =   480
      TabIndex        =   6
      Top             =   5640
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Reset"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "ReturnVoucher.frx":15ADB6
      PICN            =   "ReturnVoucher.frx":15ADD2
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSDataListLib.DataCombo DC2 
      Height          =   330
      Left            =   2040
      TabIndex        =   1
      Top             =   720
      Width           =   2895
      _ExtentX        =   5106
      _ExtentY        =   582
      _Version        =   393216
      Appearance      =   0
      BackColor       =   12632319
      ForeColor       =   8388608
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid FG2 
      Height          =   255
      Left            =   240
      TabIndex        =   14
      Top             =   5040
      Width           =   6855
      _ExtentX        =   12091
      _ExtentY        =   450
      _Version        =   393216
      Rows            =   1
      Cols            =   6
      FixedRows       =   0
      AllowBigSelection=   0   'False
      Appearance      =   0
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Salesman"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000040&
      Height          =   240
      Index           =   5
      Left            =   720
      TabIndex        =   13
      Top             =   720
      Width           =   900
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Sr.No."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000040&
      Height          =   240
      Index           =   2
      Left            =   720
      TabIndex        =   12
      Top             =   360
      Width           =   585
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000040&
      Height          =   240
      Index           =   0
      Left            =   720
      TabIndex        =   11
      Top             =   1200
      Width           =   405
   End
End
Attribute VB_Name = "frmSRETURN"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Function CL_FG1()
FG1.clear
FG2.clear
FG1.TextMatrix(0, 0) = "SNO"
FG1.TextMatrix(0, 1) = "PRID"
FG1.TextMatrix(0, 2) = "Product"
FG1.TextMatrix(0, 3) = "Carton"
FG1.TextMatrix(0, 4) = "Piece"
FG1.TextMatrix(0, 5) = "T.Piece"
FG1.TextMatrix(0, 6) = "N.Stock"
FG1.TextMatrix(0, 7) = "PPC"

FG1.ColWidth(0) = 500
FG1.ColWidth(1) = 0
FG1.ColWidth(2) = 3000
FG1.ColWidth(3) = 800
FG1.ColWidth(4) = 800
FG1.ColWidth(5) = 0
FG1.ColWidth(6) = 0
FG1.ColWidth(7) = 0
''''''''''FG2
FG2.ColWidth(0) = 500
FG2.ColWidth(1) = 0
FG2.ColWidth(2) = 3000
FG2.ColWidth(3) = 800
FG2.ColWidth(4) = 800
FG2.ColWidth(5) = 1000

For i = 1 To FG1.Rows - 1
FG1.TextMatrix(i, 0) = i
Next i

Txt1.Visible = False
DCPR.Visible = False
Txt1.Text = ""
DCPR.Text = ""
End Function

Private Sub DC1_Change()
If CC = False Then
GetS
End If
End Sub

Private Sub DC1_GotFocus()
If CC = False Then
GetS
End If
SendKeys "{F4}"
End Sub

Function GetS()
Set RSP = New ADODB.Recordset
If DC1.Text <> "" Then
RSP.Open "SELECT PARTY FROM PARTY WHERE PARTY Like '" & UCase(DC1.Text) & "%' AND TYPE='SUPPLYMAN' ORDER BY PARTY", CN, adOpenStatic, adLockOptimistic
Else
RSP.Open "SELECT PARTY FROM PARTY WHERE TYPE='SUPPLYMAN' ORDER BY PARTY", CN, adOpenStatic, adLockOptimistic
End If
Set DC1.RowSource = RSP
DC1.ListField = "PARTY"
End Function

Function GetC()
End Function

Private Sub DC1_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DC1_Validate(Cancel As Boolean)
If DC1.Text <> "" Then
Set Rs = New ADODB.Recordset
Rs.Open "SELECT SNO FROM PARTY WHERE PARTY='" & DC1.Text & "'", CN, adOpenStatic, adLockOptimistic
Set TXTPID.DataSource = Rs
TXTPID.DataField = "SNO"
If TXTPID.Text = "" Then
MsgBox "Select a valid party please"
Cancel = True
Else
Cancel = False
End If
End If
End Sub

Private Sub DC2_Change()
If CC = False Then
Set RSP = New ADODB.Recordset
If DC2.Text <> "" Then
RSP.Open "SELECT NAME FROM PARTY WHERE NAME Like '" & UCase(DC2.Text) & "%' AND TYPE='SALESMAN' ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
Else
RSP.Open "SELECT NAME FROM PARTY WHERE TYPE='SALESMAN' ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
End If
Set DC2.RowSource = RSP
DC2.ListField = "NAME"
End If
End Sub

Private Sub DC2_GotFocus()
DC2_Change
SendKeys "{F4}"
End Sub

Private Sub DC2_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DC2_Validate(Cancel As Boolean)
If DC2.Text <> "" Then
Set Rs = New ADODB.Recordset
Rs.Open "SELECT PID FROM PARTY WHERE NAME='" & DC2.Text & "'", CN, adOpenStatic, adLockOptimistic
If Rs.RecordCount > 0 Then
Cancel = False
Else
MsgBox "Select a valid party please"
Cancel = True
End If
End If
End Sub

Function S_STOCK()
'If DC2.Text <> "" And Val(FG1.TextMatrix(FG1.Row, 1)) <> 0 Then
'Set Rs = New ADODB.Recordset
'Rs.Open "SELECT -SUM(QTYC),-SUM(QTYP) FROM STOCK WHERE SID=" & TXTPID.Text & " AND PRID=" & FG1.TextMatrix(FG1.Row, 1) & "", CN, adOpenStatic, adLockOptimistic
'If Rs.RecordCount > 0 Then
'If Rs(1) > Val(FG1.TextMatrix(FG1.Row, 7)) Then
'Cart1 = Rs(0) + Fix(Rs(1) / Val(FG1.TextMatrix(FG1.Row, 7)))
'Piec1 = Rs(1) Mod Val(FG1.TextMatrix(FG1.Row, 7))
'Else
'Cart1 = Rs(0)
'Piec1 = IIf(IsNull(Rs(1)), 0, Rs(1))
'End If
'FG1.TextMatrix(FG1.Row, 6) = Cart1 & "-" & Piec1
'End If
'End If
End Function
Private Sub DCPR_Change()
FG1.TextMatrix(FG1.Row, FG1.CoL) = DCPR.Text
If CC = False Then
Set RSP = New ADODB.Recordset
If DCPR.Text <> "" Then
RSP.Open "SELECT NAME FROM PRODUCT WHERE NAME Like '" & DCPR.Text & "%' ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
Else
RSP.Open "SELECT NAME FROM PRODUCT ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
End If
Set DCPR.RowSource = RSP
DCPR.ListField = "NAME"
End If
End Sub

Private Sub DCPR_GotFocus()
DCPR_Change
SendKeys "{F4}"
End Sub

Private Sub DCPR_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DCPR_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
DCPR_Validate False
If FG1.CoL = 7 Then
FG1.Row = FG1.Row + 1
FG1.CoL = 2
Else
FG1.CoL = FG1.CoL + 1
End If
SHoW_CoNT
End If
End Sub

Private Sub DCPR_Validate(Cancel As Boolean)
If DCPR.Text <> "" Then
Set Rs = New ADODB.Recordset
Rs.Open "SELECT PRID,PRATE,PPC FROM PRODUCT WHERE NAME='" & DCPR.Text & "'", CN, adOpenStatic, adLockOptimistic
If Rs.RecordCount > 0 Then
FG1.TextMatrix(FG1.Row, 1) = IIf(IsNull(Rs("PRID")), "", Rs("PRID"))
'FG1.TextMatrix(FG1.Row, 4) = IIf(IsNull(RS("PRATE")), "", RS("PRATE"))
FG1.TextMatrix(FG1.Row, 7) = IIf(IsNull(Rs("PPC")), "", Rs("PPC"))
Call S_STOCK
Else
FG1.TextMatrix(FG1.Row, 1) = ""
'FG1.TextMatrix(FG1.Row, 4) = ""
FG1.TextMatrix(FG1.Row, 7) = ""
End If
If Val(FG1.TextMatrix(FG1.Row, 1)) = 0 Then
MsgBox "Select a valid Product please"
Cancel = True
Else
Cancel = False
End If
End If
End Sub

Private Sub FG1_Click()
SHoW_CoNT
End Sub

Function SHoW_CoNT()
If FG1.CoL = 2 Then
        DCPR.Text = FG1.TextMatrix(FG1.Row, FG1.CoL)
        DCPR.Top = FG1.Top + FG1.CellTop
        DCPR.Left = FG1.Left + FG1.CellLeft
        DCPR.Visible = True
        DCPR.SetFocus
        DCPR.Width = FG1.CellWidth
        Txt1.Visible = False
        Call GetpR
ElseIf FG1.CoL = 3 Or FG1.CoL = 4 Or FG1.CoL = 5 Or FG1.CoL = 6 Then
Txt1.Text = FG1.TextMatrix(FG1.Row, FG1.CoL)
    Txt1.Left = FG1.Left + FG1.CellLeft
    Txt1.Top = FG1.Top + FG1.CellTop
    Txt1.Height = FG1.CellHeight
    Txt1.Width = FG1.CellWidth
    Txt1.Visible = True
    Txt1.SetFocus
    DCPR.Visible = False
End If
End Function

Function GetpR()
End Function

Private Sub FG1_EnterCell()
SHoW_CoNT
End Sub

Private Sub FG1_GotFocus()
SHoW_CoNT
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyF3 Then
frmNEW.Show vbModal
ElseIf KeyCode = vbKeyF7 Then
frmPRODUCT.Show vbModal
ElseIf KeyCode = vbKeyF5 Then
Call cmdReset_Click
ElseIf KeyCode = vbKeyEscape Then
Unload Me
ElseIf KeyCode = vbKeyF6 Then
Y = MsgBox("Are you sure to cmdDeletee ?", vbYesNo)
If Y = vbYes Then
FG1.RemoveItem (FG1.Row)
Else
End If
End If
End Sub

Private Sub Form_Load()
''''''''''''''''''' theme
Set OBJ = New clsFunction
OBJ.clear Me
'''''''''''''''''''''''''''''

DT1.Value = Date
T1.Text = MaX_DcN("RETURN", "STOCK")
FG1.ColAlignment(2) = 0
CL_FG1
End Sub

Private Sub T1_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
'If Chk_Tid("STOCK", "SRETURN", T1.Text) = False Then
'MsgBox "Change Login"
'Exit Sub
'End If

c = Val(T1.Text)
cmdReset_Click
T1.Text = c

    Set Rs = New ADODB.Recordset
    Rs.Open "SELECT * FROM STOCK WHERE ETYPE='RETURN' AND DCNO=" & T1.Text & "", CN, adOpenStatic, adLockOptimistic
    If Rs.RecordCount > 0 Then
    cmdSave.Enabled = False
    cmdDelete.Enabled = True
    cmdPrint.Enabled = True
    cmdUpdate.Enabled = True
    Set RSP = New ADODB.Recordset
    RSP.Open "SELECT NAME FROM PARTY WHERE PID=" & Rs("SID") & "", CN, adOpenStatic, adLockOptimistic
    DC2.Text = RSP(0)
    DT1.Value = Rs("DATE")
    'T2.Text = IIf(IsNull(Rs("INVOICE")), "", Rs("INVOICE"))
       r = 1
        Do Until Rs.EOF
        Set RSPR = New ADODB.Recordset
        RSPR.Open "SELECT * FROM PRODUCT WHERE PRID=" & Rs("PRID") & "", CN, adOpenStatic, adLockOptimistic
        FG1.TextMatrix(r, 1) = Rs("PRID")
        FG1.TextMatrix(r, 2) = RSPR!Name
        FG1.TextMatrix(r, 3) = IIf(IsNull(Rs("QTYC")), "", Rs("QTYC"))
        FG1.TextMatrix(r, 4) = IIf(Rs("QTYP") = 0, "", Rs("QTYP"))
        FG1.TextMatrix(r, 7) = Rs("TQTYP")
        Rs.MoveNext
        r = r + 1
        'FG1_TOT
        Loop
     End If
        
    'Else
    'Call cmdReset_Click
    End If
'End If
End Sub

Private Sub T2_LostFocus()
FG1.CoL = 2
FG1.Row = 1
End Sub

Private Sub T2_Validate(Cancel As Boolean)
If T2.Text = "" Then
MsgBox "ENTER INVOICE"
Cancel = True
End If
End Sub

Private Sub T4_Change()
ToT_COM
End Sub

Private Sub T5_Change()
ToT_COM
End Sub

Private Sub T6_Change()
ToT_COM
End Sub

Function ISSUE_VERIFY()
Set ISs = New ADODB.Recordset
ISs.Open "SELECT * FROM ISSUE WHERE ORDN=" & TON.Text & "", CN, adOpenStatic, adLockOptimistic
If ISs.RecordCount > 0 Then
MsgBox "ALREADY ENTERED...ISSUE#" & ISs!SNO
Exit Function
End If
End Function

Private Sub TON_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
CL_FG1
Set ISs = New ADODB.Recordset
ISs.Open "SELECT * FROM ISSUE WHERE ORDN=" & TON.Text & "", CN, adOpenStatic, adLockOptimistic
If ISs.RecordCount > 0 Then
MsgBox "ALREADY ENTERED...ISSUE#" & ISs!SNO
Exit Sub
End If
    Set Rs = New ADODB.Recordset
    Rs.Open "SELECT * FROM ORDERS WHERE SNO=" & TON.Text & "", CN, adOpenStatic, adLockOptimistic
    If Rs.RecordCount > 0 Then
    TXTPID.Text = Rs("SID")
    TXTPID2.Text = Rs("PID")
    Set RSP = New ADODB.Recordset
    RSP.Open "SELECT PARTY FROM PARTY WHERE SNO=" & Rs("SID") & "", CN, adOpenStatic, adLockOptimistic
    Sname.Text = RSP(0)
    Set RSP = New ADODB.Recordset
    RSP.Open "SELECT PARTY FROM PARTY WHERE SNO=" & Rs("PID") & "", CN, adOpenStatic, adLockOptimistic
    DC2.Text = RSP(0)
    DT1.Value = Rs("DATE")
    T2.Text = Rs!SNO & "-" & T1.Text
    T3.Text = IIf(IsNull(Rs("DESCRIPTION")), "", Rs("DESCRIPTION"))
    T4.Text = IIf(IsNull(Rs!COMMISSION), 0, Rs!COMMISSION)
    r = 1
        Set RSST = New ADODB.Recordset
        RSST.Open "SELECT * FROM STOCK WHERE ORDN=" & TON.Text & "", CN, adOpenStatic, adLockOptimistic
        If RSST.RecordCount > 0 Then
        Do Until RSST.EOF
        Set RSPR = New ADODB.Recordset
        RSPR.Open "SELECT NAME,SRATE,SQTY,SCHEME,QTY FROM PRODUCT WHERE SNO=" & RSST("PRID") & "", CN, adOpenStatic, adLockOptimistic
        FG1.TextMatrix(r, 1) = RSST("PRID")
        FG1.TextMatrix(r, 2) = RSPR!Name
        FG1.TextMatrix(r, 3) = IIf(IsNull(RSST("SAQUANTITY")), "", RSST("SAQUANTITY"))
        FG1.TextMatrix(r, 4) = IIf(RSST("SCQUANTITY") = 0, "", RSST("SCQUANTITY"))
        FG1.TextMatrix(r, 5) = RSST("RATE")
        FG1.TextMatrix(r, 6) = IIf(IsNull(RSST("LESS")), "", RSST("LESS"))
        FG1.TextMatrix(r, 7) = Val(RSST("RATE")) * Val(FG1.TextMatrix(r, 3)) - Val(RSST("LESS"))
        FG1.TextMatrix(r, 8) = IIf(IsNull(RSPR!SCHEME), "", RSPR!SCHEME)
        FG1.TextMatrix(r, 9) = RSPR!SQTY
        FG1.TextMatrix(r, 10) = RSPR!Qty
        RSST.MoveNext
        r = r + 1
        'FG1_TOT
        Loop
        End If
        'Set RSL = New ADODB.Recordset
        'RSL.Open "SELECT * FROM PLEDGER WHERE ORDN=" & T1.Text & " AND DESCRIPTION='COMMISSION'", CN, adOpenStatic, adLockOptimistic
        'If RSL.RecordCount > 0 Then
        'T4.Text = RSL("COMM")
        'Set RSL = New ADODB.Recordset
        'RSL.Open "SELECT * FROM PLEDGER WHERE ORDN=" & T1.Text & " AND DESCRIPTION='ADVANCE'", CN, adOpenStatic, adLockOptimistic
        'If RSL.RecordCount > 0 Then
        'T5.Text = IIf(IsNull(RSL!CREDIT), 0, RSL!CREDIT)
        'End If
        'End If
    Else
    Call cmdReset_Click
    End If
End If
End Sub

Private Sub Txt1_Change()
FG1.TextMatrix(FG1.Row, FG1.CoL) = Txt1.Text
FG1.TextMatrix(FG1.Row, 5) = Val(FG1.TextMatrix(FG1.Row, 4)) * Val(FG1.TextMatrix(FG1.Row, 7)) + Val(FG1.TextMatrix(FG1.Row, 3))
'GeT_LESS
'FG1.TextMatrix(FG1.Row, 5) = Cart + Cart1 & "-" & Piec + Piec1
End Sub

Function GeT_LESS()
FG1.TextMatrix(FG1.Row, FG1.CoL) = Txt1.Text
If FG1.CoL = 3 Then
str = FG1.TextMatrix(FG1.Row, 3)
k = InStr(1, str, "-", vbTextCompare)
If k = 1 Then
Cart = 0
Piec = -Val(str)
ElseIf k = 0 Then
Cart = Val(str)
Piec = 0
ElseIf k = Len(str) Then
Cart = Left$(str, k - 1)
Piec = 0
Else
Cart = Left$(str, k - 1)
Piec = Right$(str, Len(str) - Len(Cart) - 1)
End If
If Val(FG1.TextMatrix(FG1.Row, 6)) = 0 Then
MsgBox "Piece/Carton Not Defined Or Zero In New Product Registration"
Exit Function
End If
Qty = (Val(Cart) * Val(FG1.TextMatrix(FG1.Row, 6))) + Val(Piec)
'FG1.TextMatrix(FG1.Row, 5) = (Val(Qty) * (Val(FG1.TextMatrix(FG1.Row, 4)) / Val(FG1.TextMatrix(FG1.Row, 6))))
End If
End Function

Function ToT_COM()
TA.Text = Format(Val(FG2.TextMatrix(0, 7)) - Val(FG2.TextMatrix(0, 6)), "#.##")
TCo.Text = Format(Val(TA.Text) * (Val(T4.Text) / 100), "#.##")
T6.Text = Format(Val(TA.Text) - Val(TCo.Text), "#.##")
'TC.Text = Format(T6.Text, "#.##")
End Function

Private Sub Txt1_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyRight Then
If FG1.CoL <> FG1.Cols - 1 Then
FG1.CoL = FG1.CoL + 1
End If
ElseIf KeyCode = vbKeyLeft Then
If FG1.CoL <> 0 Then
FG1.CoL = FG1.CoL - 1
End If
ElseIf KeyCode = vbKeyUp Then
If FG1.Row <> 1 Then
FG1.Row = FG1.Row - 1
End If
ElseIf KeyCode = vbKeyDown Then
If FG1.Row <> FG1.Rows - 1 Then
FG1.Row = FG1.Row + 1
End If
End If
SHoW_CoNT
End Sub

Private Sub Txt1_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
If FG1.CoL = 4 Then
FG1.Row = FG1.Row + 1
FG1.CoL = 2
Else
FG1.CoL = FG1.CoL + 1
End If
SHoW_CoNT
End If
End Sub
Private Sub cmdSave_Click()
If T1.Text Then
''''''''''''''''STOCK''''''''''''''''''''''
r = 1
Set Rs = New ADODB.Recordset
Rs.Open "SELECT * FROM STOCK", CN, adOpenStatic, adLockOptimistic
Do Until FG1.TextMatrix(r, 1) = ""
Rs.AddNew
Rs("STID") = MaX_ID("STID", "STOCK")
Rs("SID") = ID_NaME("PARTY", "PID", "NAME", DC2.Text)
Rs("TID") = V_Tid
Rs("DCNO") = Val(T1.Text)
Rs("ETYPE") = "RETURN"
Rs("DATE") = DT1.Value
'Rs("INVOICE") = T3.Text
Rs("PRID") = Val(FG1.TextMatrix(r, 1))
Rs("QTYC") = Val(FG1.TextMatrix(r, 3))
Rs("QTYP") = Val(FG1.TextMatrix(r, 4))
Rs("TQTYP") = Val(FG1.TextMatrix(r, 5))
Rs.Update
r = r + 1
Loop

Call cmdReset_Click
Else
MsgBox "Enter Into Empty Fields"
End If
End Sub

Private Sub cmdDelete_Click()
CN.Execute "Delete STOCK WHERE ETYPE='RETURN' AND DCNO='" & T1.Text & "'"
End Sub

Function Pri()
CN.Execute "cmdDeleteE TEMP"
Set SA = New ADODB.Recordset
SA.Open "SELECT * FROM VRETURNS WHERE SNO=" & T1.Text & "", CN
'SA.Open "select * from sale WHERE SNO=" & T1.Text & "", CN
Set TM = New ADODB.Recordset
TM.Open "select * from TEMP", CN, adOpenStatic, adLockOptimistic
Do Until SA.EOF
TM.AddNew
TM!SNO = SA!SNO
TM!Date = SA!Date
TM!T1 = SA!Name
TM!T2 = SA!Description
If Val(SA!SCQUANTITY) <> 0 Then
TM!T3 = SA!SAQUANTITY & " + " & SA!SCQUANTITY
Else
TM!T3 = SA!SAQUANTITY
End If
TM!N2 = SA!Rate
TM!N3 = SA!LESS
TM!N4 = SA!AMOUNT
SA.MoveNext
Loop
TM.cmdUpdatee
End Function

Private Sub cmdPrint_Click()
'''Call Pri
'''If T1.Text <> "" Then
'''If DE1.rsCTEMP.State = 1 Then
'''DE1.rsCTEMP.Close
'''End If
'''DE1.rsCTEMP.Open "SELECT T1,T3,N2,ROUND(N3,0) N3,ROUND(N4,0) N4 FROM TEMP WHERE SNO=" & T1.Text & " ORDER BY T1"
'''REGIN.Sections(1).Controls("TV").Caption = T2.Text
'''REGIN.Sections(1).Controls("TN").Caption = DC2.Text
'''REGIN.Sections(1).Controls("TD").Caption = DT1.Value
'''REGIN.Sections(5).Controls("TNET").Caption = Round(Val(FG2.TextMatrix(0, 7)) - Val(FG2.TextMatrix(0, 6)), 0)
'''REGIN.Sections(5).Controls("TCOM").Caption = Val(T4.Text)
'''REGIN.Sections(5).Controls("Label8").Caption = Val(TCo.Text)
'''REGIN.Sections(5).Controls("Label17").Caption = Round(Val(T6.Text), 0)
'''If Text1.Text <> "" Then
'''REGIN.Sections(1).Controls("Label20").Caption = Text1.Text
'''End If
'''REGIN.Show vbModal
'''End If
''''''''''''
'If T1.Text <> "" Then
'If DE1.rsCISSUE.State = 1 Then
'DE1.rsCISSUE.Close
'End If
'DE1.rsCISSUE.Open "SELECT * FROM VISSUE WHERE SNO=" & T1.Text & ""
'reISSUE.Sections(1).Controls("TV").Caption = T1.Text
'reISSUE.Sections(1).Controls("TN").Caption = DC1.Text
'reISSUE.Sections(1).Controls("TD").Caption = DT1.Value
'reISSUE.Sections(5).Controls("TNET").Caption = Val(T6.Text)
'reISSUE.Sections(5).Controls("TCOM").Caption = Val(T4.Text)
'reISSUE.Show vbModal
'End If
rptSMISSUE.RecordSelectionFormula = "{stock.etype}='RETURN'  AND {PARTY.NAME}='" & DC2.Text & "' AND {STOCK.DATE} IN DATETIME('" & DT1.Value & "')  to  datetime('" & DT1.Value & "') "
rptSMISSUE.txtHead.SetText "Sale Man Return Report"
ViewReports.CRViewer1.ReportSource = rptSMISSUE
ViewReports.CRViewer1.ViewReport
ViewReports.Show vbModal

End Sub

Private Sub cmdUpdate_Click()
If T1.Text Then
Call cmdDelete_Click
Call cmdSave_Click
Call cmdReset_Click
Else
MsgBox "Enter Into Empty Fields"
End If
End Sub

Private Sub cmdReset_Click()
FG1.Row = 1
FG1.CoL = 2
cmdSave.Enabled = True
cmdDelete.Enabled = False
cmdUpdate.Enabled = False
DC2.Text = ""
DT1.Value = Date
CL_FG1
T1.Text = MaX_DcN("RETURN", "STOCK")

'T2.Text = ""
T1.SetFocus
End Sub


