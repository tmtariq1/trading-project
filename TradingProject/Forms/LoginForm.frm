VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "msdatlst.ocx"
Object = "{D750EAB9-EB7B-450B-B9B6-0A0A17E13DCA}#1.0#0"; "xp.ocx"
Begin VB.Form LoginForm 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   1  'Fixed Single
   Caption         =   "Please Login"
   ClientHeight    =   2220
   ClientLeft      =   150
   ClientTop       =   240
   ClientWidth     =   5685
   BeginProperty Font 
      Name            =   "Times New Roman"
      Size            =   21.75
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "LoginForm.frx":0000
   KeyPreview      =   -1  'True
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2220
   ScaleWidth      =   5685
   StartUpPosition =   2  'CenterScreen
   Begin VB.ComboBox Combo1 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   300
      ItemData        =   "LoginForm.frx":27C92
      Left            =   2040
      List            =   "LoginForm.frx":27C9C
      TabIndex        =   0
      Text            =   "Sr_Traders2"
      Top             =   240
      Visible         =   0   'False
      Width           =   3135
   End
   Begin VB.TextBox Text1 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   178
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      IMEMode         =   3  'DISABLE
      Left            =   2040
      PasswordChar    =   "*"
      TabIndex        =   2
      Top             =   960
      Width           =   3135
   End
   Begin VB.TextBox Text2 
      Appearance      =   0  'Flat
      Height          =   285
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Visible         =   0   'False
      Width           =   300
   End
   Begin XPLayout.XPButton XPButton1 
      Height          =   375
      Left            =   1665
      TabIndex        =   3
      Top             =   1680
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Login"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   16711680
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "LoginForm.frx":27CBF
      PICN            =   "LoginForm.frx":27CDB
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin XPLayout.XPButton XPButton2 
      Height          =   375
      Left            =   2865
      TabIndex        =   4
      Top             =   1680
      Width           =   975
      _ExtentX        =   1720
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Exit"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   16711680
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "LoginForm.frx":28277
      PICN            =   "LoginForm.frx":28293
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSDataListLib.DataCombo DC1 
      Height          =   330
      Left            =   2040
      TabIndex        =   1
      Top             =   600
      Width           =   3135
      _ExtentX        =   5530
      _ExtentY        =   582
      _Version        =   393216
      MatchEntry      =   -1  'True
      Appearance      =   0
      BackColor       =   16761024
      ForeColor       =   8388608
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid FG2 
      Height          =   255
      Left            =   0
      TabIndex        =   9
      Top             =   1800
      Visible         =   0   'False
      Width           =   9615
      _ExtentX        =   16960
      _ExtentY        =   450
      _Version        =   393216
      Rows            =   1
      Cols            =   6
      FixedRows       =   0
      BackColor       =   16761024
      AllowBigSelection=   0   'False
      Appearance      =   0
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Company"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00400000&
      Height          =   228
      Index           =   0
      Left            =   360
      TabIndex        =   8
      Top             =   360
      Visible         =   0   'False
      Width           =   792
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Type Password."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00400000&
      Height          =   228
      Index           =   1
      Left            =   360
      TabIndex        =   7
      Top             =   960
      Width           =   1368
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "User Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00400000&
      Height          =   225
      Index           =   3
      Left            =   360
      TabIndex        =   6
      Top             =   720
      Width           =   945
   End
End
Attribute VB_Name = "LoginForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'FIXIT: Use Option Explicit to avoid implicitly creating variables of type Variant         FixIT90210ae-R383-H1984

Private Sub Combo1_Validate(Cancel As Boolean)
V_Tid = Combo1.ListIndex
'If Combo1.ListIndex = 0 Then
'MAIN.Label3.Caption = "KASHIF TRADERS"
'Else
'MAIN.Label3.Caption = "KHARI TRADERS"
'End If
End Sub

Private Sub DC1_Change()
If CC = False Then
Set RSP = New ADODB.Recordset
If DC1.Text <> "" Then
RSP.Open "SELECT UNAME FROM LOGIN WHERE UNAME Like '" & DC1.Text & "%' ORDER BY UNAME", CN, adOpenStatic, adLockOptimistic
Else
RSP.Open "SELECT UNAME FROM LOGIN  ORDER BY UNAME", CN, adOpenStatic, adLockOptimistic
End If
Set DC1.RowSource = RSP
DC1.ListField = "UNAME"
End If
chkA = DC1.Text
End Sub

Private Sub DC1_KeyDown(KeyCode As Integer, shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DC1_Validate(Cancel As Boolean)
If DC1.Text <> "" Then
'CONN = "Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=Galaxy_Foods;Data Source=DS1"
'If CN.State = 1 Then
'CN.Close
'End If
'CN.CommandTimeout = 50
'CN.Open CONN
'CN.CursorLocation = adUseClient
Set Rs = New ADODB.Recordset
Rs.Open "SELECT PASS FROM LOGIN WHERE UNAME='" & DC1.Text & "'", CN
If Rs.RecordCount > 0 Then
Text2.Text = IIf(IsNull(Rs!PASS), "", (Rs!PASS))
Else
MsgBox "select a valid user"
End If
End If
End Sub

'Private Sub DC1_Validate(Cancel As Boolean)
'If DC1.Text <> "" Then
'On Error GoTo er
'Dim st As String
'st = Text3.Text
''CONN = "Provider=SQLOLEDB.1;Password=Rose;Persist Security Info=True;User ID=digital;Initial Catalog=DELIGHT_FUEL;Data Source=" & st
'CONN = "Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=Delight_Fuel;Data Source=SERVER"
'If CN.State = 1 Then
'CN.Close
'End If
'CN.CommandTimeout = 50
'CN.Open CONN
'CN.CursorLocation = adUseClient
'If DE1.Connection1.State = 1 Then
'DE1.Connection1.Close
'End If
'DE1.Connection1.Open CONN, "DIGITAL", "Rose"
'Set RS = New ADODB.Recordset
'RS.Open "SELECT * FROM LOGIN where uname='" & DC1.Text & "' ORDER BY DC1", CN
'Text2.DataField = "PASS"
'Set Text2.DataSource = RS
'
'er:
'If Err.Number <> 0 Then
'MsgBox Err.Description
'Err.Clear
'End If
'End If
'End Sub

Private Sub DC1_Click(Area As Integer)
End Sub

Private Sub DC1_GotFocus()
DC1_Change
End Sub

Private Sub Form_Load()
Call Company_Setup
'Call UpdateAllReportPath
If App.PrevInstance = True Then
MsgBox "Software Already Running", vbCritical
End
End If
CONN = "Provider=SQLOLEDB.1;Persist Security Info=True;Initial Catalog=" & V_DataBase & ";Data Source=" & V_Server & ";User ID=" & V_User & ";Password=" & V_password
If CN.State = 1 Then
CN.Close
End If
CN.CursorLocation = adUseClient
CN.Open CONN
Create_Login

'Export_Report "Select * from party", App.Path & "\Reports\" & "CCSale.rpt", App.Path & "\Reports\" & "CCSale.rpt", "Report Title Here"
End Sub

'FIXIT: Declare 'Create_Login' with an early-bound data type                               FixIT90210ae-R1672-R1B8ZE
Function Create_Login()
Set RsL = New ADODB.Recordset
RsL.Open "SELECT * FROM LOGIN", CN
If RsL.RecordCount > 0 Then
Else
CN.Execute "INSERT INTO LOGIN(UNAME,PASS) VALUES('Administrator','123')"
End If
End Function

Private Sub Text1_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
FLOG
End If
End Sub

Private Sub Text3_KeyDown(KeyCode As Integer, shift As Integer)
If KeyCode = vbKeyF10 Then
Text3.Locked = False
End If
End Sub


Private Sub XPButton1_Click()
If DC1.Text = "" Then
MsgBox "Select A Valid User To login", vbCritical
Exit Sub
Else
FLOG
End If
End Sub

'FIXIT: Declare 'FLOG' with an early-bound data type                                       FixIT90210ae-R1672-R1B8ZE
Function FLOG()
If Text1.Text = Text2.Text And DC1.Text <> "" And Combo1.Text <> "" Then
'V_User = DC1.Text
If DC1.Text = "ADMINISTRATOR" Or DC1.Text = "Administrator" Then
v_Check_Constraints = False
Else
v_Check_Constraints = True
End If
Unload Me
MAIN.Show
'frmSplash.Show vbModeless, Me
Else
MsgBox "Wrong Password", vbCritical
Text1.Text = ""
Text1.SetFocus
End If
End Function

Private Sub XPButton2_Click()
Unload Me
End
End Sub

