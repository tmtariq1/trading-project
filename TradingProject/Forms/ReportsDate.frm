VERSION 5.00
Object = "{BDC217C8-ED16-11CD-956C-0000C04E4C0A}#1.1#0"; "tabctl32.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "msdatlst.ocx"
Object = "{D750EAB9-EB7B-450B-B9B6-0A0A17E13DCA}#1.0#0"; "xp.ocx"
Begin VB.Form ReportDate 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "Reports Date"
   ClientHeight    =   2850
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   7365
   Icon            =   "ReportsDate.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "ReportsDate.frx":000C
   ScaleHeight     =   2850
   ScaleWidth      =   7365
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin TabDlg.SSTab SSTab1 
      Height          =   1815
      Left            =   0
      TabIndex        =   5
      Top             =   0
      Width           =   7365
      _ExtentX        =   12991
      _ExtentY        =   3201
      _Version        =   393216
      Tabs            =   2
      TabsPerRow      =   2
      TabHeight       =   520
      TabCaption(0)   =   "Date"
      TabPicture(0)   =   "ReportsDate.frx":1596E2
      Tab(0).ControlEnabled=   -1  'True
      Tab(0).Control(0)=   "Label1(0)"
      Tab(0).Control(0).Enabled=   0   'False
      Tab(0).Control(1)=   "Label1(5)"
      Tab(0).Control(1).Enabled=   0   'False
      Tab(0).Control(2)=   "DTE"
      Tab(0).Control(2).Enabled=   0   'False
      Tab(0).Control(3)=   "DTS"
      Tab(0).Control(3).Enabled=   0   'False
      Tab(0).Control(4)=   "Frame1"
      Tab(0).Control(4).Enabled=   0   'False
      Tab(0).ControlCount=   5
      TabCaption(1)   =   "Option"
      TabPicture(1)   =   "ReportsDate.frx":1596FE
      Tab(1).ControlEnabled=   0   'False
      Tab(1).Control(0)=   "DC1"
      Tab(1).Control(1)=   "DC2"
      Tab(1).Control(2)=   "DC4"
      Tab(1).Control(3)=   "DC5"
      Tab(1).Control(4)=   "DC3"
      Tab(1).Control(5)=   "DC6"
      Tab(1).Control(6)=   "Label1(8)"
      Tab(1).Control(7)=   "Label1(6)"
      Tab(1).Control(8)=   "Label1(3)"
      Tab(1).Control(9)=   "Label1(2)"
      Tab(1).Control(10)=   "Label1(1)"
      Tab(1).Control(11)=   "Label1(7)"
      Tab(1).ControlCount=   12
      Begin VB.Frame Frame1 
         BackColor       =   &H00800000&
         BorderStyle     =   0  'None
         Caption         =   "Frame1"
         Height          =   255
         Left            =   3840
         TabIndex        =   22
         Top             =   600
         Width           =   2535
         Begin VB.OptionButton Option1 
            BackColor       =   &H00FFFFFF&
            Caption         =   "All"
            ForeColor       =   &H00800000&
            Height          =   225
            Left            =   0
            TabIndex        =   25
            Top             =   0
            Value           =   -1  'True
            Width           =   855
         End
         Begin VB.OptionButton Option3 
            BackColor       =   &H00FFFFFF&
            Caption         =   "Credit"
            ForeColor       =   &H00800000&
            Height          =   225
            Left            =   1680
            TabIndex        =   24
            Top             =   0
            Width           =   855
         End
         Begin VB.OptionButton Option2 
            BackColor       =   &H00FFFFFF&
            Caption         =   "Cash"
            ForeColor       =   &H00800000&
            Height          =   225
            Left            =   840
            TabIndex        =   23
            Top             =   0
            Width           =   855
         End
      End
      Begin MSComCtl2.DTPicker DTS 
         CausesValidation=   0   'False
         Height          =   345
         Left            =   1920
         TabIndex        =   6
         Top             =   600
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   609
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CalendarBackColor=   14737632
         CustomFormat    =   "dd-MMM-yy"
         Format          =   59047937
         CurrentDate     =   38099
      End
      Begin MSComCtl2.DTPicker DTE 
         CausesValidation=   0   'False
         Height          =   345
         Left            =   1920
         TabIndex        =   7
         Top             =   1080
         Width           =   1695
         _ExtentX        =   2990
         _ExtentY        =   609
         _Version        =   393216
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         CalendarBackColor=   14737632
         CustomFormat    =   "dd-MMM-yy"
         Format          =   59047937
         CurrentDate     =   38099
      End
      Begin MSDataListLib.DataCombo DC1 
         Height          =   315
         Left            =   -74160
         TabIndex        =   10
         Top             =   480
         Width           =   2775
         _ExtentX        =   4895
         _ExtentY        =   556
         _Version        =   393216
         Appearance      =   0
         BackColor       =   8454143
         ForeColor       =   8388608
         Text            =   ""
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSDataListLib.DataCombo DC2 
         Height          =   315
         Left            =   -70560
         TabIndex        =   11
         Top             =   480
         Width           =   2775
         _ExtentX        =   4895
         _ExtentY        =   556
         _Version        =   393216
         Appearance      =   0
         BackColor       =   8454143
         Text            =   ""
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSDataListLib.DataCombo DC4 
         Height          =   315
         Left            =   -70560
         TabIndex        =   12
         Top             =   840
         Width           =   2775
         _ExtentX        =   4895
         _ExtentY        =   556
         _Version        =   393216
         Appearance      =   0
         BackColor       =   8454143
         ForeColor       =   8388608
         Text            =   ""
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSDataListLib.DataCombo DC5 
         Height          =   315
         Left            =   -74160
         TabIndex        =   13
         Top             =   1200
         Width           =   2775
         _ExtentX        =   4895
         _ExtentY        =   556
         _Version        =   393216
         Appearance      =   0
         BackColor       =   8454143
         Text            =   ""
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSDataListLib.DataCombo DC3 
         Height          =   315
         Left            =   -74160
         TabIndex        =   14
         Top             =   840
         Width           =   2775
         _ExtentX        =   4895
         _ExtentY        =   556
         _Version        =   393216
         Appearance      =   0
         BackColor       =   8454143
         Text            =   ""
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin MSDataListLib.DataCombo DC6 
         Height          =   315
         Left            =   -70560
         TabIndex        =   15
         Top             =   1200
         Width           =   2775
         _ExtentX        =   4895
         _ExtentY        =   556
         _Version        =   393216
         Appearance      =   0
         BackColor       =   8454143
         Text            =   ""
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Verdana"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Name 1"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0080FFFF&
         Height          =   240
         Index           =   8
         Left            =   -74880
         TabIndex        =   21
         Top             =   480
         Width           =   720
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Name 2"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0080FFFF&
         Height          =   225
         Index           =   6
         Left            =   -71280
         TabIndex        =   20
         Top             =   480
         Width           =   645
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Name 5"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0080FFFF&
         Height          =   225
         Index           =   3
         Left            =   -74880
         TabIndex        =   19
         Top             =   1200
         Width           =   645
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Name 4"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9.75
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0080FFFF&
         Height          =   240
         Index           =   2
         Left            =   -71280
         TabIndex        =   18
         Top             =   840
         Width           =   720
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Name 3"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0080FFFF&
         Height          =   225
         Index           =   1
         Left            =   -74880
         TabIndex        =   17
         Top             =   840
         Width           =   645
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "Name 6"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   9
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H0080FFFF&
         Height          =   225
         Index           =   7
         Left            =   -71280
         TabIndex        =   16
         Top             =   1200
         Width           =   645
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "From"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000040&
         Height          =   270
         Index           =   5
         Left            =   1080
         TabIndex        =   9
         Top             =   600
         Width           =   540
      End
      Begin VB.Label Label1 
         AutoSize        =   -1  'True
         BackStyle       =   0  'Transparent
         Caption         =   "To"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   11.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00000040&
         Height          =   270
         Index           =   0
         Left            =   1080
         TabIndex        =   8
         Top             =   1200
         Width           =   285
      End
   End
   Begin VB.CheckBox Check1 
      BackColor       =   &H00C0C0FF&
      Caption         =   "All"
      Height          =   195
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Visible         =   0   'False
      Width           =   615
   End
   Begin VB.TextBox TY 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   14.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   330
      Left            =   0
      MaxLength       =   60
      TabIndex        =   4
      Top             =   240
      Visible         =   0   'False
      Width           =   735
   End
   Begin MSDataListLib.DataCombo dcPname 
      Height          =   345
      Left            =   2520
      TabIndex        =   1
      Top             =   1920
      Visible         =   0   'False
      Width           =   3735
      _ExtentX        =   6588
      _ExtentY        =   609
      _Version        =   393216
      Appearance      =   0
      BackColor       =   14737632
      ForeColor       =   4210752
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin XPLayout.XPButton COMMAND1 
      Height          =   495
      Left            =   2520
      TabIndex        =   2
      Top             =   2280
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   873
      BTYPE           =   3
      TX              =   "&Show"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   16711680
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "ReportsDate.frx":15971A
      PICN            =   "ReportsDate.frx":159736
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Select Name"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000040&
      Height          =   270
      Index           =   4
      Left            =   480
      TabIndex        =   3
      Top             =   2040
      Visible         =   0   'False
      Width           =   1335
   End
End
Attribute VB_Name = "ReportDate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Dim strSuplymandSelection As String

Private Sub Check1_Click()
MsgBox Check1.Value
End Sub

Private Sub Command1_Click()
    If DTS.Value > DTE.Value Then
        MsgBox "ENDING DATE CANNOT LESS THAN STARTING DATE"
    Else

    strSuplymandSelection = " or {stock_MAIN.SupplymanID}=" & ID_NaME("PARTY", "PID", "NAME", DC1.Text) & " or {stock_MAIN.SupplymanID}=" & ID_NaME("PARTY", "PID", "NAME", DC2.Text) & " or {stock_MAIN.SupplymanID}=" & ID_NaME("PARTY", "PID", "NAME", DC3.Text) & " or {stock_MAIN.SupplymanID}=" & ID_NaME("PARTY", "PID", "NAME", DC4.Text) & " or {stock_MAIN.SupplymanID}=" & ID_NaME("PARTY", "PID", "NAME", DC5.Text) & " or {stock_MAIN.SupplymanID}=" & ID_NaME("PARTY", "PID", "NAME", DC6.Text) & " "
    strSuplymandSelection = Replace(strSuplymandSelection, "= ", "=0 ")
    'All_Reports_Path
    ''''''''''''''''''''''''''''''''''''''''''''Purchase
    If TY.Text = "PURCHASE" Then
        fn_PurchaseOverAll
    ElseIf TY.Text = "CCSale" Then
        Call Load_Report(App.Path & "\Reports\" & "CCSale.rpt")
    ElseIf TY.Text = "SMSCWise" Then
        Call Load_Report1(App.Path & "\Reports\" & "SMSCWise.rpt")
    ElseIf TY.Text = "smCSale" Then
        Call Load_Report(App.Path & "\Reports\" & "smCSale.rpt")
    ElseIf TY.Text = "PurchaseDateWise" Then
        fn_PurDateWise
    ElseIf TY.Text = "PurPartyWise" Then
        fn_PurPartyWise
    ElseIf TY.Text = "PurWareHouseWise" Then
        fn_PurWareHouseWise
    ''''''''''''''''''''''''''''''''''''''''''''SALE
    ElseIf TY.Text = "SALE" Then
        fn_SaleOverAll
    ElseIf TY.Text = "SaleWareHouseWise" Then
        fn_SaleWareHouseWise
    ElseIf TY.Text = "SalePartyWise" Then
        fn_SalePartyWise
    ElseIf TY.Text = "SaleDateWise" Then
        fn_SaleDateWise
    ElseIf TY.Text = "SALE" Then
        fn_SaleOverAll
    ElseIf TY.Text = "GainOverAll" Then
        fn_GainReport 1
    ElseIf TY.Text = "GainDateWise" Then
        fn_GainReport 2
    ElseIf TY.Text = "ItemLedger" Then
        fn_ItemLedger
    ElseIf TY.Text = "SaleManPartyWise" Then
        fn_SaleManPartyWise
    ElseIf TY.Text = "SaleManPartyWise2" Then
        fn_SaleManPartyWise2
    ElseIf TY.Text = "SaleManMonthWise" Then
        fn_SaleManMonthWise
    ElseIf TY.Text = "SaleManProductWise" Then
        fn_SaleManProductWise
    ElseIf TY.Text = "SaleManCompanyProductWise" Or TY.Text = "SaleManCompanyProductWiseTP" Then
        fn_SaleManCompanyProductWise
    ElseIf TY.Text = "SaleManCompanyPartyWise" Then
        fn_SaleManCompanyPartyWise
    ElseIf TY.Text = "LEDGER" Then
        fn_Ledger
    ElseIf TY.Text = "PURCHASE1" Then
        Unload rptSaleManWise
        UpdateReportCredentials rptSaleManWise
        rptSaleManWise.txtHead.SetText "PURCHASE SALESMAN WISE"
        rptSaleManWise.txtDate.SetText "From " & DTS.Value & " To " & DTE.Value
        Call FN_Comm("PURCHASE", "CR", rptSaleManWise) '''''///////////COMM DISSC
        rptSaleManWise.RecordSelectionFormula = "{STOCK_main.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {STOCK.ETYPE}='PURCHASE'"
        ReportsViewer.CRViewer1.ReportSource = rptSaleManWise
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
    ElseIf TY.Text = "RSALE1" Then
        Unload rptSaleManWise
        UpdateReportCredentials rptSaleManWise
        rptSaleManWise.txtHead.SetText "SALE RETURN SALESMAN WISE"
        rptSaleManWise.txtDate.SetText "From " & DTS.Value & " To " & DTE.Value
        Call FN_Comm("RSALE", "CR", rptSaleManWise) '''''///////////COMM DISSC
        rptSaleManWise.RecordSelectionFormula = "{STOCK_main.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {STOCK.ETYPE}='RSALE'"
        ReportsViewer.CRViewer1.ReportSource = rptSaleManWise
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
    ElseIf TY.Text = "RPURCHASE1" Then
        Unload rptSaleManWise
        UpdateReportCredentials rptSaleManWise
        rptSaleManWise.txtHead.SetText "PURCHASE RETURN SALESMAN WISE"
        rptSaleManWise.txtDate.SetText "From " & DTS.Value & " To " & DTE.Value
        Call FN_Comm("RPURCHASE", "DR", rptSaleManWise) '''''///////////COMM DISSC
        rptSaleManWise.RecordSelectionFormula = "{STOCK_main.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {STOCK.ETYPE}='RPURCHASE'"
        ReportsViewer.CRViewer1.ReportSource = rptSaleManWise
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
    ElseIf TY.Text = "RSALE" Then
        Unload rptSaleReport
        UpdateReportCredentials rptSaleReport
        rptSaleReport.txtHead.SetText "Sale Return Report"
        Call FN_Comm("RSALE", "CR", rptSaleReport) '''''///////////COMM DISSC
        rptSaleReport.RecordSelectionFormula = "{STOCK_main.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {STOCK.ETYPE}='RSALE'"
        ReportsViewer.CRViewer1.ReportSource = rptSaleReport
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
    ElseIf TY.Text = "RPURCHASE" Then
        Unload rptSaleReport
        UpdateReportCredentials rptSaleReport
        rptSaleReport.txtHead.SetText "Purchase Return Report"
        Call FN_Comm("RPURCHASE", "DR", rptSaleReport) '''''///////////COMM DISSC
        rptSaleReport.RecordSelectionFormula = "{STOCK_main.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {STOCK.ETYPE}='RPURCHASE'"
         ReportsViewer.CRViewer1.ReportSource = rptSaleReport
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
    ElseIf TY.Text = "RSALESW" Then
        Unload rptSaleReportSalemanWise
        UpdateReportCredentials rptSaleReportSalemanWise
        rptSaleReportSalemanWise.Text1.SetText "Return Sale Product Wise"
        'If Check1.Value = 0 Then
        'rptSaleReportSalemanWise.RecordSelectionFormula = "{STOCK_main.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {STOCK.ETYPE}='RSALE' AND {STOCK.TID}=" & V_Tid & ""
        'Else
        rptSaleReportSalemanWise.RecordSelectionFormula = "{STOCK_main.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {STOCK.ETYPE}='RSALE'"
        'End If
        ReportsViewer.CRViewer1.ReportSource = rptSaleReportSalemanWise
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
    ElseIf TY.Text = "STOCK" Then
        Unload rptStock
        UpdateReportCredentials rptStock
        rptStock.RecordSelectionFormula = "{STOCK_main.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "')"
        rptStock.txtDate.SetText "Date " & DTE.Value
        ReportsViewer.CRViewer1.ReportSource = rptStock
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
    ElseIf TY.Text = "STOCKOVERALL" Then
        Unload rptStockOverAll
        UpdateReportCredentials rptStockOverAll
        'rptStockOverAll.ParameterFields(1).DiscreteOrRangeKind = crDiscreteValue
        rptStockOverAll.ParameterFields(1).EnableMultipleValues = True
        'rptStockOverAll.ParameterFields(1).ClearCurrentValueAndRange
        'rptStockOverAll.ParameterFields(1).DeleteNthDefaultValue (1)
        rptStockOverAll.ParameterFields(1).AddDefaultValue DTE.Value     'RecordSelectionFormula  = "{STOCK_main.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "')"
        rptStockOverAll.ParameterFields(1).AddCurrentValue (DTE.Value)
        rptStockOverAll.txtDate.SetText "Date " & DTE.Value
        ReportsViewer.CRViewer1.ReportSource = rptStockOverAll
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
    ElseIf TY.Text = "StockCompanyWise" Then
        Unload rptStockCompanyWise
        UpdateReportCredentials rptStockCompanyWise
        'rptStockOverAll.ParameterFields(1).DiscreteOrRangeKind = crDiscreteValue
        rptStockCompanyWise.ParameterFields(1).EnableMultipleValues = True
        'rptStockOverAll.ParameterFields(1).ClearCurrentValueAndRange
        'rptStockOverAll.ParameterFields(1).DeleteNthDefaultValue (1)
        rptStockCompanyWise.ParameterFields(1).AddDefaultValue DTE.Value     'RecordSelectionFormula  = "{STOCK_main.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "')"
        rptStockCompanyWise.ParameterFields(1).AddCurrentValue (DTE.Value)
        rptStockCompanyWise.txtDate.SetText "Date " & DTE.Value
        ReportsViewer.CRViewer1.ReportSource = rptStockCompanyWise
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
    ElseIf TY.Text = "STOCKINVENTORY" Then
        Unload rptInventoryReport
        UpdateReportCredentials rptInventoryReport
        rptInventoryReport.RecordSelectionFormula = "{STOCK_main.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "')"
        rptInventoryReport.txtDate.SetText "From " & DTS.Value & " To " & DTE.Value
        ReportsViewer.CRViewer1.ReportSource = rptInventoryReport
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
    ElseIf TY.Text = "SALEPW" Then
        Unload rptSalePartyWise
        UpdateReportCredentials rptSalePartyWise
        'If Check1.Value = 0 Then
        'rptSalenameWise.RecordSelectionFormula = "{STOCK_main.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {STOCK.ETYPE}='SALE' AND {STOCK.TID}=" & V_Tid & ""
        'Else
        rptSalePartyWise.RecordSelectionFormula = "{STOCK_main.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {STOCK_main.ETYPE}='SALE'"
        'End If
        ReportsViewer.CRViewer1.ReportSource = rptSalePartyWise
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
    ElseIf TY.Text = "SALEPSCW" Then
        Unload rptSalePartySectorWise
        UpdateReportCredentials rptSalePartySectorWise
        'If Check1.Value = 0 Then
        'rptSalePartySectorWise.RecordSelectionFormula = "{STOCK_main.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {STOCK.ETYPE}='SALE' AND {STOCK.TID}=" & V_Tid & ""
        'Else
        rptSalePartySectorWise.RecordSelectionFormula = "{STOCK_main.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {STOCK.ETYPE}='SALE'"
        'End If
        ReportsViewer.CRViewer1.ReportSource = rptSalePartySectorWise
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
    ElseIf TY.Text = "RSALEPW" Then
        Unload rptSaleManWise
        UpdateReportCredentials rptSaleManWise
        rptSaleManWise.Text2.SetText "Return Sale Report name Wise"
        'If Check1.Value = 0 Then
        'rptSalenameWise.RecordSelectionFormula = "{STOCK_main.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {STOCK.ETYPE}='RSALE' AND {STOCK.TID}=" & V_Tid & ""
        'Else
        rptSaleManWise.RecordSelectionFormula = "{STOCK_main.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {STOCK_main.ETYPE}='RSALE'"
        'End If
        ReportsViewer.CRViewer1.ReportSource = rptSaleManWise
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
    ElseIf TY.Text = "PURCHASEINW" Then
'        Unload rptPurchaseInvoiceWise
'        rptPurchaseInvoiceWise.Text6.SetText "From " & DTS.Value & " To " & DTE.Value
'        If Option1 Then
'        rptPurchaseInvoiceWise.RecordSelectionFormula = "{PLEDGER.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {PLEDGER.ETYPE}='PURCHASE' AND {PLEDGER.CREDIT}<>0"
'        ElseIf Option2 Then
'        str = View_Dcno("PURCHASE")
'        rptPurchaseInvoiceWise.RecordSelectionFormula = "{PLEDGER.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {PLEDGER.ETYPE}='PURCHASE' AND {PLEDGER.CREDIT}<>0 AND {PLEDGER.DCNO} IN [" & str & "]"
'        ElseIf Option3 Then
'        str = View_Dcno("PURCHASE")
'        rptPurchaseInvoiceWise.RecordSelectionFormula = "{PLEDGER.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {PLEDGER.ETYPE}='PURCHASE' AND {PLEDGER.CREDIT}<>0 AND (NOT({PLEDGER.DCNO} IN [" & str & "]))"
'        End If
'        ReportsViewer.CRViewer1.ReportSource = rptPurchaseInvoiceWise
'        ReportsViewer.CRViewer1.ViewReport
'        ReportsViewer.Show vbModal
    ElseIf TY.Text = "SALEINW" Then
        Unload rptSaleInvoiceWise
        UpdateReportCredentials rptSaleInvoiceWise
        rptSaleInvoiceWise.Text6.SetText "From " & DTS.Value & " To " & DTE.Value
        rptSaleInvoiceWise.Text7.Suppress = True
        'If Option1 Then
        rptSaleInvoiceWise.RecordSelectionFormula = "{PLEDGER.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {PLEDGER.ETYPE}='SALE' AND {PLEDGER.DEBIT}<>0"
        'ElseIf Option2 Then
        'str = View_Dcno("SALE")
        'rptSaleInvoiceWise.RecordSelectionFormula = "{PLEDGER.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {PLEDGER.ETYPE}='SALE' AND {PLEDGER.DEBIT}<>0 AND {PLEDGER.DCNO} IN [" & str & "]"
        'ElseIf Option3 Then
        'str = View_Dcno("SALE")
        'rptSaleInvoiceWise.RecordSelectionFormula = "{PLEDGER.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {PLEDGER.ETYPE}='SALE' AND {PLEDGER.DEBIT}<>0 AND (NOT({PLEDGER.DCNO} IN [" & str & "]))"
        'End If
        ReportsViewer.CRViewer1.ReportSource = rptSaleInvoiceWise
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
    '''''''''rptSaleSectorWise
    ElseIf TY.Text = "SALESCW" Then
        Unload rptSaleSectorWise
        UpdateReportCredentials rptSaleSectorWise
        rptSaleSectorWise.Text6.SetText "From " & DTS.Value & " To " & DTE.Value
        rptSaleSectorWise.Text7.Suppress = True
        If Option1 Then
        rptSaleSectorWise.RecordSelectionFormula = "{PLEDGER.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {PLEDGER.ETYPE}='SALE' AND {PLEDGER.DEBIT}<>0"
        ElseIf Option2 Then
        str = View_Dcno("SALE")
        rptSaleSectorWise.RecordSelectionFormula = "{PLEDGER.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {PLEDGER.ETYPE}='SALE' AND {PLEDGER.DEBIT}<>0 AND {PLEDGER.DCNO} IN [" & str & "]"
        ElseIf Option3 Then
        str = View_Dcno("SALE")
        rptSaleSectorWise.RecordSelectionFormula = "{PLEDGER.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {PLEDGER.ETYPE}='SALE' AND {PLEDGER.DEBIT}<>0 AND (NOT({PLEDGER.DCNO} IN [" & str & "]))"
        End If
        ReportsViewer.CRViewer1.ReportSource = rptSaleSectorWise
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
    ElseIf TY.Text = "RSALEINW" Then
        ''''Salesman Sale Invoic Wise
        Unload rptSaleInvoiceWise
        UpdateReportCredentials rptSaleInvoiceWise
        rptSaleInvoiceWise.Text2.SetText "Salesman Return Sale Invoic Wise "
        rptSaleInvoiceWise.Text6.SetText "From " & DTS.Value & " To " & DTE.Value
        rptSaleInvoiceWise.Text5.Suppress = True
        If Option1 Then
        rptSaleInvoiceWise.RecordSelectionFormula = "{PLEDGER.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {PLEDGER.ETYPE}='RSALE' AND {PLEDGER.CREDIT}<>0 and {Party.TYPE}<>'TAX'"
        ElseIf Option2 Then
        rptSaleInvoiceWise.RecordSelectionFormula = "{PLEDGER.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {PLEDGER.ETYPE}='RSALE' AND {PLEDGER.CREDIT}<>0 AND {PLEDGER.DCNO} IN [" & View_Dcno("RSALE") & "] and {Party.TYPE}<>'TAX'"
        ElseIf Option3 Then
        rptSaleInvoiceWise.RecordSelectionFormula = "{PLEDGER.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {PLEDGER.ETYPE}='RSALE' AND {PLEDGER.CREDIT}<>0 AND (NOT({PLEDGER.DCNO} IN [" & View_Dcno("RSALE") & "])) and {Party.TYPE}='TAX'"
        End If
        ReportsViewer.CRViewer1.ReportSource = rptSaleInvoiceWise
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
    ElseIf TY.Text = "DSR" Then
        Unload rptDailySaleReport
        UpdateReportCredentials rptDailySaleReport
        rptDailySaleReport.Text19.SetText "From " & DTS.Value & " To " & DTE.Value
        rptDailySaleReport.OpenSubreport("rptSaleReport").RecordSelectionFormula = "{STOCK_main.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {STOCK.ETYPE}='RSALE'"
        rptDailySaleReport.RecordSelectionFormula = "{STOCK_main.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {STOCK.ETYPE}='SALE'"
        ReportsViewer.CRViewer1.ReportSource = rptDailySaleReport
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
    ElseIf TY.Text = "SMSALE" Then
        Unload rptSaleComm
        UpdateReportCredentials rptSaleComm
        rptSaleComm.RecordSelectionFormula = "{PLEDGER.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') " 'AND {PLEDGER.ETYPE}='SALE'"
        ReportsViewer.CRViewer1.ReportSource = rptSaleComm
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
    ElseIf TY.Text = "SALEMANISSUE" Then
        Unload rptIssueReturn
        UpdateReportCredentials rptIssueReturn
        rptIssueReturn.Text9.SetText "From " & DTS.Value & " To " & DTE.Value
        rptIssueReturn.Text8.SetText "Issue Report"
        rptIssueReturn.RecordSelectionFormula = "{STOCK_main.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {STOCK.ETYPE}='ISSUE'"
        ReportsViewer.CRViewer1.ReportSource = rptIssueReturn
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
    ElseIf TY.Text = "SALEMANRETURN" Then
        Unload rptIssueReturn
        UpdateReportCredentials rptIssueReturn
        rptIssueReturn.Text9.SetText "From " & DTS.Value & " To " & DTE.Value
        rptIssueReturn.Text8.SetText "Return Report"
        rptIssueReturn.RecordSelectionFormula = "{STOCK_main.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {STOCK.ETYPE}='RETURN'"
        ReportsViewer.CRViewer1.ReportSource = rptIssueReturn
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
    ElseIf TY.Text = "NEVIGATION" Then
    'CN.Execute "Delete STOCK WHERE DCNO=" & T1.Text & " AND ETYPE='TRANSFER'"
    'Unload rptNevigation
        rptNevigation.Text9.SetText "From " & DTS.Value & " To " & DTE.Value
        rptNevigation.Text8.SetText "Stock Nevigation"
        rptNevigation.RecordSelectionFormula = "{STOCK_main.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {STOCK.ETYPE}='NEVEGATION'"
        ReportsViewer.CRViewer1.ReportSource = rptNevigation
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
    ElseIf TY.Text = "DAYBOOK" Then
    Unload rptDayBook
    UpdateReportCredentials rptDayBook
    rptDayBook.txtDate.SetText "From " & DTS.Value & " To " & DTE.Value
    rptDayBook.RecordSelectionFormula = "{PLEDGER.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {party.TYPE}<>'TAX'"
    ReportsViewer.CRViewer1.ReportSource = rptDayBook
    ReportsViewer.CRViewer1.ViewReport
    ReportsViewer.Show vbModal
    ElseIf TY.Text = "EXPENSE" Then     '''''''''''''''''Cash Related
        fn_Expense
    ElseIf TY.Text = "PAYMENT" Then
        fn_Payment
    ElseIf TY.Text = "RECEIVE" Then
        fn_Recieve
    ElseIf TY.Text = "JV" Then
        fn_JournalVoucher
    End If
End If
End Sub

Function Load_Report(sReportName As String)
'Dim dbTable As CRAXDRT.DatabaseTable
Dim crystalApp As CRAXDRT.Application
Dim crystalRep As CRAXDRT.Report

Set crystalApp = New CRAXDRT.Application
Set crystalRep = crystalApp.OpenReport(sReportName) 'Opens the report

'Unload crrep
Call UpdateReportCredentials(crystalRep)
        With crystalRep
        .ParameterFields(1).AddCurrentValue (DTS.Value)
        .ParameterFields(2).AddCurrentValue (DTE.Value)
        '.txtHead.SetText "Expense Report"
        '.txtDate.SetText "Date Between:" & Me.DTS.Value & " " & Me.DTE.Value
        '.RecordSelectionFormula = "{PLEDGER.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {Party.TYPE}='EXPENSE'"
        End With
        ReportsViewer.CRViewer1.ReportSource = crystalRep
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal

End Function

Function Load_Report1(sReportName As String)
'Dim dbTable As CRAXDRT.DatabaseTable
Dim crystalApp As CRAXDRT.Application
Dim crystalRep As CRAXDRT.Report

Set crystalApp = New CRAXDRT.Application
Set crystalRep = crystalApp.OpenReport(sReportName) 'Opens the report

'Unload crrep
Call UpdateReportCredentials(crystalRep)
        With crystalRep
        '.txtHead.SetText "Expense Report"
        '.txtDate.SetText "Date Between:" & Me.DTS.Value & " " & Me.DTE.Value
        '.RecordSelectionFormula = "{SaleMainDetail.date} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "')"
        End With
        ReportsViewer.CRViewer1.ReportSource = crystalRep
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal

End Function


Function fn_Reports(rpt As Report, str As String)
'Unload rpt
With rpt
        .txtHead.SetText Me.Caption
        .txtDate.SetText "Date Between:" & Me.DTS.Value & " " & Me.DTE.Value
        MsgBox "{Stock_main.DATE} in datetime('" & Me.DTS.Value & "')  to  datetime('" & Me.DTE.Value & "') AND {STOCK_MAIN.etype}= '" & str
        .RecordSelectionFormula = "{STOCK_MAIN.etype}= '" & str & "' and {Stock_main.DATE} in datetime('" & Me.DTS.Value & "')  to  datetime('" & Me.DTE.Value & "')"
End With
        ReportsViewer.CRViewer1.ReportSource = rpt
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
End Function

Function fn_Expense()
Unload rptCashReport
UpdateReportCredentials rptCashReport
        With rptCashReport
        .txtHead.SetText "Expense Report"
        '.txtDate.SetText "Voucher # " & Me.T1.Text
        '.txtDate.Suppress = True
        '.txtCredit.Suppress = True
        '.DEBIT1.Suppress = True
        .txtDate.SetText "Date Between:" & Me.DTS.Value & " " & Me.DTE.Value
        .RecordSelectionFormula = "{PLEDGER.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {Party.TYPE}='EXPENSE'"
        End With
        ReportsViewer.CRViewer1.ReportSource = rptCashReport
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
End Function

Function fn_Payment()
Unload rptCashReport
UpdateReportCredentials rptCashReport
        With rptCashReport
        .txtHead.SetText "Payments Report"
        '.txtDate.SetText "Voucher # " & Me.T1.Text
        '.txtDate.Suppress = True
        '.txtCredit.Suppress = True
        '.DEBIT1.Suppress = True
        .txtDate.SetText "Date Between:" & Me.DTS.Value & " " & Me.DTE.Value
        .RecordSelectionFormula = "{PLEDGER.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {PLEDGER.etype}='PTP' AND {Party.NAME} <> 'CASH'"
        End With
        ReportsViewer.CRViewer1.ReportSource = rptCashReport
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
End Function

Function fn_Recieve()
Unload rptCashReport
UpdateReportCredentials rptCashReport
        With rptCashReport
        .txtHead.SetText "Receipts Report"
        '.txtDate.SetText "Voucher # " & Me.T1.Text
        '.txtDate.Suppress = True
        '.txtCredit.Suppress = True
        '.DEBIT1.Suppress = True
        .txtDate.SetText "Date Between:" & Me.DTS.Value & " " & Me.DTE.Value
        .RecordSelectionFormula = "{PLEDGER.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {PLEDGER.etype}='PFP' AND {Party.NAME} <> 'CASH'"
        End With
        ReportsViewer.CRViewer1.ReportSource = rptCashReport
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
End Function

Function fn_JournalVoucher()
Unload rptCashReport
UpdateReportCredentials rptCashReport
        With rptCashReport
        .txtHead.SetText "Journal Voucher Report"
        '.txtDate.SetText "Voucher # " & Me.T1.Text
        '.txtDate.Suppress = True
        '.txtCredit.Suppress = True
        '.DEBIT1.Suppress = True
        .txtDate.SetText "Date Between:" & Me.DTS.Value & " " & Me.DTE.Value
        .RecordSelectionFormula = "{PLEDGER.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {PLEDGER.etype}='JV'"
        End With
        ReportsViewer.CRViewer1.ReportSource = rptCashReport
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
End Function

Function fn_PurchaseOverAll()
Unload rptPurchaseOverAll
UpdateReportCredentials rptPurchaseOverAll
        With rptPurchaseOverAll
        .txtHead.SetText "Purchase Report"
        .txtDate.SetText "Date Between:" & Me.DTS.Value & " " & Me.DTE.Value
        .RecordSelectionFormula = "{trans_MAIN.DATE} in datetime('" & Me.DTS.Value & "')  to  datetime('" & Me.DTE.Value & "') AND {trans_MAIN.Etype}='PURCHASE'"
        End With
        'Report_ID rptPurchaseOverAll, V_DataBase, V_Server, V_User, v_password
        'UpdateReportCredentials rptPurchaseOverAll
        ReportsViewer.CRViewer1.ReportSource = rptPurchaseOverAll
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
End Function

Function fn_PurPartyWise()
Unload rptPurchasePartyWise
UpdateReportCredentials rptPurchaseDatePartyWise
        With rptPurchasePartyWise
        .txtHead.SetText Me.Caption
        .txtDate.SetText "Date Between:" & Me.DTS.Value & " " & Me.DTE.Value
        If DC1.Text = "" Then
        .RecordSelectionFormula = "{trans_MAIN.DATE} in datetime('" & Me.DTS.Value & "')  to  datetime('" & Me.DTE.Value & "') AND {trans_MAIN.etype}='PURCHASE'"
        Else
        .RecordSelectionFormula = "{trans_MAIN.DATE} in datetime('" & Me.DTS.Value & "')  to  datetime('" & Me.DTE.Value & "') AND {trans_MAIN.etype}='PURCHASE' and ({party.name} = '" & DC1.Text & "' or {party.name} = '" & DC2.Text & "' or {party.name} = '" & DC3.Text & "' or {party.name} = '" & DC4.Text & "' or {party.name} = '" & DC5.Text & "' or {party.name} = '" & DC6.Text & "')"
      End If
        End With
        ReportsViewer.CRViewer1.ReportSource = rptPurchasePartyWise
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
End Function

Function fn_PurWareHouseWise()
Unload rptPurchaseWareHouseWise
UpdateReportCredentials rptPurchaseWareHouseWise
        With rptPurchaseWareHouseWise
        .txtHead.SetText Me.Caption
        .txtDate.SetText "Date Between:" & Me.DTS.Value & " " & Me.DTE.Value
        .RecordSelectionFormula = "{trans_MAIN.DATE} in datetime('" & Me.DTS.Value & "')  to  datetime('" & Me.DTE.Value & "') AND {trans_MAIN.etype}='PURCHASE'"
        End With
        ReportsViewer.CRViewer1.ReportSource = rptPurchaseWareHouseWise
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
End Function

Function fn_PurDateWise()
Unload rptPurchaseDatePartyWise
UpdateReportCredentials rptPurchaseDatePartyWise
        With rptPurchaseDatePartyWise
        .txtHead.SetText Me.Caption
        .txtDate.SetText "Date Between:" & Me.DTS.Value & " " & Me.DTE.Value
        .RecordSelectionFormula = "{trans_MAIN.DATE} in datetime('" & Me.DTS.Value & "')  to  datetime('" & Me.DTE.Value & "') AND {trans_MAIN.etype}='PURCHASE'"
        End With
        ReportsViewer.CRViewer1.ReportSource = rptPurchaseDatePartyWise
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
End Function

Function fn_SaleOverAll()
Unload rptOverAllSaleReport
UpdateReportCredentials rptOverAllSaleReport
        With rptOverAllSaleReport
        '.txtHead.SetText "Sale Report"
        '.txtDate.SetText "Date Between:" & Me.DTS.Value & " " & Me.DTE.Value
'        MsgBox "{STOCK_MAIN.DATE} in datetime('" & Me.DTS.Value & "')  to  datetime('" & Me.DTE.Value & "') AND {STOCK.ETYPE}='sale' and {party.name} = '" & DC1.Text
        If DC1.Text = "" Then
        .RecordSelectionFormula = "{STOCK_MAIN.DATE} in datetime('" & Me.DTS.Value & "')  to  datetime('" & Me.DTE.Value & "') AND {STOCK.ETYPE}='SALE'"
        Else
        .RecordSelectionFormula = "{STOCK_MAIN.DATE} in datetime('" & Me.DTS.Value & "')  to  datetime('" & Me.DTE.Value & "') AND {STOCK.ETYPE}='SALE' and ({party_1.name} = '" & DC1.Text & "' OR {party_1.name} = '" & DC2.Text & "' OR {party_1.name} = '" & DC3.Text & "' OR {party_1.name} = '" & DC4.Text & "' OR {party_1.name} = '" & DC5.Text & "' OR {party_1.name} = '" & DC6.Text & "'" & strSuplymandSelection & ")"
        End If
        '.RecordSelectionFormula = "{STOCK_MAIN.DATE} in datetime('" & Me.DTS.Value & "')  to  datetime('" & Me.DTE.Value & "') AND {STOCK.ETYPE}='SALE'"
        End With
        ReportsViewer.CRViewer1.ReportSource = rptOverAllSaleReport
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
End Function

Function fn_SalePartyWise()
Unload rptPurchasePartyWise
UpdateReportCredentials rptPurchasePartyWise
        With rptPurchasePartyWise
        .txtHead.SetText Me.Caption
        .txtDate.SetText "Date Between:" & Me.DTS.Value & " " & Me.DTE.Value
        If DC1.Text = "" Then
        .RecordSelectionFormula = "{trans_MAIN.DATE} in datetime('" & Me.DTS.Value & "')  to  datetime('" & Me.DTE.Value & "') AND {trans_MAIN.etype}='SALE'"
        Else
        .RecordSelectionFormula = "{trans_MAIN.DATE} in datetime('" & Me.DTS.Value & "')  to  datetime('" & Me.DTE.Value & "') AND {trans_MAIN.etype}='SALE' AND ({party.name} = '" & DC1.Text & "' or {party.name} = '" & DC2.Text & "' or {party.name} = '" & DC3.Text & "' or {party.name} = '" & DC4.Text & "' or {party.name} = '" & DC5.Text & "' or {party.name} = '" & DC6.Text & "'" & strSuplymandSelection & ")"
      End If
        End With
        ReportsViewer.CRViewer1.ReportSource = rptPurchasePartyWise
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
End Function

Function fn_SaleWareHouseWise()
Unload rptPurchaseWareHouseWise
UpdateReportCredentials rptPurchaseWareHouseWise
        With rptPurchaseWareHouseWise
        .txtHead.SetText Me.Caption
        .txtDate.SetText "Date Between:" & Me.DTS.Value & " " & Me.DTE.Value
        .RecordSelectionFormula = "{trans_MAIN.DATE} in datetime('" & Me.DTS.Value & "')  to  datetime('" & Me.DTE.Value & "') AND {trans_MAIN.etype}='SALE'"
        End With
        ReportsViewer.CRViewer1.ReportSource = rptPurchaseWareHouseWise
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
End Function

Function fn_SaleDateWise()
Unload rptPurchaseDatePartyWise
UpdateReportCredentials rptPurchaseDatePartyWise
        With rptPurchaseDatePartyWise
        .txtHead.SetText Me.Caption
        .txtDate.SetText "Date Between:" & Me.DTS.Value & " " & Me.DTE.Value
        .RecordSelectionFormula = "{trans_MAIN.DATE} in datetime('" & Me.DTS.Value & "')  to  datetime('" & Me.DTE.Value & "') AND {trans_MAIN.etype}='PURCHASE'"
        End With
        ReportsViewer.CRViewer1.ReportSource = rptPurchaseDatePartyWise
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
End Function

''''''''''''''''''''''''''''''''SaleMan
Function fn_SaleManPartyWise()  ''''name Wise
Unload rptSaleManPartyWise
UpdateReportCredentials rptSaleManPartyWise
        With rptSaleManPartyWise
        .txtHead.SetText Me.Caption
        .txtDate.SetText "Date Between:" & Me.DTS.Value & " " & Me.DTE.Value
        If DC1.Text = "" Then
        .RecordSelectionFormula = "{stock_MAIN.DATE} in datetime('" & Me.DTS.Value & "')  to  datetime('" & Me.DTE.Value & "') AND {stock_Main.etype}='SALE'"
        Else
        .RecordSelectionFormula = "{stock_MAIN.DATE} in datetime('" & Me.DTS.Value & "')  to  datetime('" & Me.DTE.Value & "') AND {stock_Main.etype}='SALE' AND ({party_1.name} = '" & DC1.Text & "' or {party_1.name} = '" & DC2.Text & "' or {party_1.name} = '" & DC3.Text & "' or {party_1.name} = '" & DC4.Text & "' or {party_1.name} = '" & DC5.Text & "' or {party_1.name} = '" & DC6.Text & "'" & strSuplymandSelection & ")"
        End If
        End With
        ReportsViewer.CRViewer1.ReportSource = rptSaleManPartyWise
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
End Function

Function fn_SaleManPartyWise2()  ''''name Wise
Unload rptSaleManPartyWise2
UpdateReportCredentials rptSaleManPartyWise2
        With rptSaleManPartyWise2
        .txtHead.SetText Me.Caption
        .txtDate.SetText "Date Between:" & Me.DTS.Value & " " & Me.DTE.Value
        If DC1.Text = "" Then
        .RecordSelectionFormula = "{stock_MAIN.DATE} in datetime('" & Me.DTS.Value & "')  to  datetime('" & Me.DTE.Value & "') AND {stock_Main.etype}='SALE'"
        Else
        .RecordSelectionFormula = "{stock_MAIN.DATE} in datetime('" & Me.DTS.Value & "')  to  datetime('" & Me.DTE.Value & "') AND {stock_Main.etype}='SALE' AND ({party_1.name} = '" & DC1.Text & "' or {party_1.name} = '" & DC2.Text & "' or {party_1.name} = '" & DC3.Text & "' or {party_1.name} = '" & DC4.Text & "' or {party_1.name} = '" & DC5.Text & "' or {party_1.name} = '" & DC6.Text & "'" & strSuplymandSelection & ")"
        End If
        End With
        ReportsViewer.CRViewer1.ReportSource = rptSaleManPartyWise2
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
End Function

Function fn_SaleManMonthWise()  ''''''''''''Month wise
Unload rptSaleManMonthWise
UpdateReportCredentials rptSaleManMonthWise
        With rptSaleManMonthWise
        .txtHead.SetText Me.Caption
        .txtDate.SetText "Date Between:" & Me.DTS.Value & " " & Me.DTE.Value
        'Dim rsFormula As String
        'rsFormula = "{stock_MAIN.DATE} in datetime('" & Me.DTS.Value & "')  to  datetime('" & Me.DTE.Value & "') AND {stock_Main.etype}='SALE'"
        If DC1.Text = "" Then
        .RecordSelectionFormula = "{stock_MAIN.DATE} in datetime('" & Me.DTS.Value & "')  to  datetime('" & Me.DTE.Value & "') AND {stock_Main.etype}='SALE'"
        Else
        .RecordSelectionFormula = "{stock_MAIN.DATE} in datetime('" & Me.DTS.Value & "')  to  datetime('" & Me.DTE.Value & "') AND {stock_Main.etype}='SALE' and ({party_1.name} = '" & DC1.Text & "' or {party_1.name} = '" & DC2.Text & "' or {party_1.name} = '" & DC3.Text & "' or {party_1.name} = '" & DC4.Text & "' or {party_1.name} = '" & DC5.Text & "' or {party_1.name} = '" & DC6.Text & "'" & strSuplymandSelection & ")"
        End If
        End With
        ReportsViewer.CRViewer1.ReportSource = rptSaleManMonthWise
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
End Function

Function fn_SaleManProductWise()
'Dim strSuplymandSelection As String
Unload rptSaleManSaleProductWise
UpdateReportCredentials rptSaleManSaleProductWise
        With rptSaleManSaleProductWise
        .txtHead.SetText Me.Caption
        .txtDate.SetText "Date From " & Me.DTS.Value & " To " & Me.DTE.Value
        If DC1.Text = "" Then
        .RecordSelectionFormula = "{stock_MAIN.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {stock_Main.etype}='SALE'"
        Else
        'strSuplymandSelection = " or {stock_MAIN.SupplymanID}=" & ID_NaME("PARTY", "PID", "NAME", DC1.Text) & " or {stock_MAIN.SupplymanID}=" & ID_NaME("PARTY", "PID", "NAME", DC2.Text) & " or {stock_MAIN.SupplymanID}=" & ID_NaME("PARTY", "PID", "NAME", DC3.Text) & " or {stock_MAIN.SupplymanID}=" & ID_NaME("PARTY", "PID", "NAME", DC4.Text) & " or {stock_MAIN.SupplymanID}=" & ID_NaME("PARTY", "PID", "NAME", DC5.Text) & " or {stock_MAIN.SupplymanID}=" & ID_NaME("PARTY", "PID", "NAME", DC6.Text) & " "
        'strSuplymandSelection = Replace(strSuplymandSelection, "= ", "=0 ")
        .txtNames.SetText DC1.Text + ", " + DC2.Text + ", " + DC3.Text + ", " + DC4.Text + ", " + DC5.Text + ", " + DC6.Text
        .RecordSelectionFormula = "{stock_MAIN.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {stock_Main.etype}='SALE' and ({party_1.name} = '" & DC1.Text & "' or {party_1.name} = '" & DC2.Text & "' or {party_1.name} = '" & DC3.Text & "' or {party_1.name} = '" & DC4.Text & "' or {party_1.name} = '" & DC5.Text & "' or {party_1.name} = '" & DC6.Text & "'" & strSuplymandSelection & ")"
        End If
        End With
        
        ReportsViewer.CRViewer1.ReportSource = rptSaleManSaleProductWise
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
End Function

Function fn_SaleManCompanyProductWise()
Dim crReport As Report
If TY.Text = "SaleManCompanyProductWise" Then
Unload rptSaleManSaleCompanyProductWise
UpdateReportCredentials rptSaleManSaleCompanyProductWise
    '.RATE1.Set "{Product.SRate}"
    '.AMOUNT1.SetUnboundFieldSource "ABS({STOCK.TQTYP}) * {Product.SRate}"
Set crReport = rptSaleManSaleCompanyProductWise
Else
Unload rptSaleManSaleCompanyProductWiseTP
UpdateReportCredentials rptSaleManSaleCompanyProductWiseTP
Set crReport = rptSaleManSaleCompanyProductWiseTP
End If
      
      
        With crReport
        .txtHead.SetText Me.Caption
        .txtDate.SetText "Date From " & Me.DTS.Value & " TO " & Me.DTE.Value
        'Dim recordSelection As String
        'recordSelection = "{stock_MAIN.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {stock_Main.etype}='SALE'"
       If DC1.Text = "" Then
       .RecordSelectionFormula = "{stock_MAIN.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {stock_Main.etype}='SALE'"
       Else
        .txtNames.SetText DC1.Text + ", " + DC2.Text + ", " + DC3.Text + ", " + DC4.Text + ", " + DC5.Text + ", " + DC6.Text
        .RecordSelectionFormula = "{stock_MAIN.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {stock_Main.etype}='SALE' and ({party_1.name} = '" & DC1.Text & "' or {party_1.name} = '" & DC2.Text & "' or {party_1.name} = '" & DC3.Text & "' or {party_1.name} = '" & DC4.Text & "' or {party_1.name} = '" & DC5.Text & "' or {party_1.name} = '" & DC6.Text & "'" & strSuplymandSelection & ")"
       End If
        End With
        If TY.Text = "SaleManCompanyProductWise" Then
        ReportsViewer.CRViewer1.ReportSource = rptSaleManSaleCompanyProductWise
        Else
        ReportsViewer.CRViewer1.ReportSource = rptSaleManSaleCompanyProductWiseTP
        End If
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
End Function

Function fn_SaleManCompanyPartyWise()  ''''name Wise
Unload rptSaleManCompanyPartyWise
UpdateReportCredentials rptSaleManCompanyPartyWise
        With rptSaleManCompanyPartyWise
        .txtHead.SetText Me.Caption
        .txtDate.SetText "Date From " & Me.DTS.Value & " To " & Me.DTE.Value
        If Me.DC1.Text = "" And Me.DC2.Text = "" And Me.DC3.Text = "" And Me.DC4.Text = "" And Me.DC5.Text = "" And Me.DC6.Text = "" Then
        .RecordSelectionFormula = "{stock_MAIN.DATE} in datetime('" & Me.DTS.Value & "')  to  datetime('" & Me.DTE.Value & "') AND {stock_Main.etype}='SALE'"
        Else
        .RecordSelectionFormula = "{stock_MAIN.DATE} in datetime('" & Me.DTS.Value & "')  to  datetime('" & Me.DTE.Value & "') AND {stock_Main.etype}='SALE' and ({party_1.name} = '" & DC1.Text & "' or {party_1.name} = '" & DC2.Text & "' or {party_1.name} = '" & DC3.Text & "' or {party_1.name} = '" & DC4.Text & "' or {party_1.name} = '" & DC5.Text & "' or {party_1.name} = '" & DC6.Text & "'" & strSuplymandSelection & ")"
        End If
        End With
        ReportsViewer.CRViewer1.ReportSource = rptSaleManCompanyPartyWise
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
End Function

' reportType
' 1 = OverAll Report
' 2 = CompanyWise
Function fn_GainReport(ReportType As Integer)
Dim crystalApp As CRAXDRT.Application
Dim crystalRep As CRAXDRT.Report
Dim myTextObject As CRAXDRT.TextObject
Set crystalApp = New CRAXDRT.Application
If ReportType = 1 Then
Set crystalRep = crystalApp.OpenReport(App.Path & "\Reports\" & "GainReport.rpt") 'Opens the report
Else
Set crystalRep = crystalApp.OpenReport(App.Path & "\Reports\" & "GainCompanyWise.rpt") 'Opens the report
End If
UpdateReportCredentials crystalRep
      
      
        With crystalRep
        .Sections("RH").ReportObjects("txtHead").SetText Me.Caption
        .Sections("RH").ReportObjects("txtDate").SetText "Date Between:" & Me.DTS.Value & " " & Me.DTE.Value
        'Dim recordSelection As String
        'recordSelection = "{stock_MAIN.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {stock_Main.etype}='SALE'"
       If DC1.Text = "" Then
        .RecordSelectionFormula = "{trans_MAIN.DATE} in datetime('" & Me.DTS.Value & "')  to  datetime('" & Me.DTE.Value & "') AND {trans_MAIN.etype}='SALE'"
        Else
        .RecordSelectionFormula = "{trans_MAIN.DATE} in datetime('" & Me.DTS.Value & "')  to  datetime('" & Me.DTE.Value & "') AND {trans_MAIN.etype}='SALE' and ({party_1.name} = '" & DC1.Text & "' OR {party_1.name} = '" & DC2.Text & "' OR {party_1.name} = '" & DC3.Text & "' OR {party_1.name} = '" & DC4.Text & "' OR {party_1.name} = '" & DC5.Text & "' OR {party_1.name} = '" & DC6.Text & "'" & strSuplymandSelection & ")"
        End If
        End With
        ReportsViewer.CRViewer1.ReportSource = crystalRep
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
        
    'Unload rptPurchaseSaleOverAll
    'UpdateReportCredentials rptPurchaseSaleOverAll
    '.With rptPurchaseSaleOverAll
    '.txtHead.SetText Me.Caption
    '.txtDate.SetText "Date Between:" & Me.DTS.Value & " " & Me.DTE.Value
    'If DC1.Text = "" Then
    '.RecordSelectionFormula = "{trans_MAIN.DATE} in datetime('" & Me.DTS.Value & "')  to  datetime('" & Me.DTE.Value & "') AND {trans_MAIN.etype}='SALE'"
    'Else
    '.RecordSelectionFormula = "{trans_MAIN.DATE} in datetime('" & Me.DTS.Value & "')  to  datetime('" & Me.DTE.Value & "') AND {trans_MAIN.etype}='SALE' and ({party_1.name} = '" & DC1.Text & "' OR {party_1.name} = '" & DC2.Text & "' OR {party_1.name} = '" & DC3.Text & "' OR {party_1.name} = '" & DC4.Text & "' OR {party_1.name} = '" & DC5.Text & "' OR {party_1.name} = '" & DC6.Text & "')"
    'End If
    'End With
    'ReportsViewer.CRViewer1.ReportSource = rptPurchaseSaleOverAll
    'ReportsViewer.CRViewer1.ViewReport
    'ReportsViewer.Show vbModal
End Function

Function fn_GainDateWise()
Unload rptPurchaseSaleDateWise
UpdateReportCredentials rptPurchaseSaleDateWise
        With rptPurchaseSaleDateWise
        .txtHead.SetText Me.Caption
        .txtDate.SetText "Date Between:" & Me.DTS.Value & " " & Me.DTE.Value
        .RecordSelectionFormula = "{trans_MAIN.DATE} in datetime('" & Me.DTS.Value & "')  to  datetime('" & Me.DTE.Value & "') AND {trans_MAIN.etype}='SALE'"
        End With
        ReportsViewer.CRViewer1.ReportSource = rptPurchaseSaleDateWise
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
End Function

Function fn_Ledger()
Unload rptLEDGER
UpdateReportCredentials rptLEDGER
        With rptLEDGER
        '.txtHead.SetText "Item Ledger" & dcPname.Text
        '.txtDate.SetText "Date Between:" & Me.DTS.Value & " " & Me.DTE.Value
        .RecordSelectionFormula = "{PLEDGER.DATE} in datetime('" & Me.DTS.Value & "')  to  datetime('" & Me.DTE.Value & "')"
        End With
        ReportsViewer.CRViewer1.ReportSource = rptLEDGER
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
End Function
Function fn_ItemLedger()
Unload rptItemLedger
UpdateReportCredentials rptItemLedger
        With rptItemLedger
        .txtHead.SetText "Item Ledger " & dcPname.Text
        .Text5.SetText dcPname.Text
        .txtDate.SetText "Date Between:" & Me.DTS.Value & " " & Me.DTE.Value
        .RecordSelectionFormula = "{trans_MAIN.DATE} in datetime('" & Me.DTS.Value & "')  to  datetime('" & Me.DTE.Value & "') AND {product.name}= '" & dcPname.Text & "'"
        End With
        ReportsViewer.CRViewer1.ReportSource = rptItemLedger
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal
End Function

Function FN_Comm(Str1 As String, str2 As String, Str3 As Report)
Dim ST
With Str3
.txtC1.SetText "Comm: " & FN_Pid(ID_NaME("Party", "pid", "NAME", "COMMISSION"), "" & Str1 & "", DTS.Value, DTE.Value, "" & str2 & "")
.txtC2.SetText "E.Comm: " & FN_Pid(ID_NaME("Party", "pid", "NAME", "EXTRA COMMISSION"), "" & Str1 & "", DTS.Value, DTE.Value, "" & str2 & "")
.txtC3.SetText "SMS: " & FN_Pid(ID_NaME("Party", "pid", "NAME", "SMS"), "" & Str1 & "", DTS.Value, DTE.Value, "" & str2 & "")
.txtD.SetText "Discount: " & FN_Pid(ID_NaME("Party", "pid", "NAME", "DISCOUNT"), "" & Str1 & "", DTS.Value, DTE.Value, "" & str2 & "")
.txtB.SetText "Builty: " & FN_Pid(ID_NaME("Party", "pid", "NAME", "FREIGHT"), "" & Str1 & "", DTS.Value, DTE.Value, "" & str2 & "")
Set rs1 = New ADODB.Recordset
rs1.Open "SELECT SUM(net_AMOUNT) FROM STOCK_main WHERE ETYPE='" & Str1 & "' AND DATE BETWEEN '" & DTS.Value & "' AND '" & DTE.Value & "'", CN, adOpenStatic, adLockReadOnly
If rs1.RecordCount > 0 Then
.txtNa.SetText "Net " & Str1 & ": " & IIf(IsNull(rs1(0)), 0, rs1(0)) - getBitOfString(.txtC1.Text) - getBitOfString(.txtC2.Text) - getBitOfString(.txtC3.Text) - getBitOfString(.txtD.Text) - getBitOfString(.txtB.Text)
End If
End With
End Function

Private Sub DC1_Change()
If CC = False Then
GetS
End If
End Sub

Private Sub DC1_GotFocus()
If CC = False Then
GetS
End If
SendKeys "{F4}"
End Sub

Function GetS()
Set RSP = New ADODB.Recordset
If DC1.Text <> "" Then
RSP.Open "SELECT name FROM party WHERE name Like '" & UCase(DC1.Text) & "%' AND (TYPE='SALESMAN' OR Type='SUPPLYMAN') ORDER BY name", CN, adOpenStatic, adLockOptimistic
Else
RSP.Open "SELECT name FROM party WHERE (TYPE='SALESMAN' OR Type='SUPPLYMAN') ORDER BY name", CN, adOpenStatic, adLockOptimistic
End If
Set DC1.RowSource = RSP
DC1.ListField = "name"
End Function

Private Sub DC1_KeyDown(KeyCode As Integer, shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DC1_Validate(Cancel As Boolean)
If DC1.Text <> "" Then
Set Rs = New ADODB.Recordset
Rs.Open "SELECT pid FROM party WHERE name='" & DC1.Text & "'", CN, adOpenStatic, adLockOptimistic
If Rs.RecordCount > 0 Then
Cancel = False
Else
MsgBox "Select a valid name please"
Cancel = True
End If
End If
End Sub

Private Sub DC2_Change()
If CC = False Then
Set RSP = New ADODB.Recordset
If DC2.Text <> "" Then
RSP.Open "SELECT name FROM party WHERE name Like '" & DC2.Text & "%' AND (TYPE='SALESMAN' OR Type='SUPPLYMAN') ORDER BY name", CN, adOpenStatic, adLockOptimistic
Else
RSP.Open "SELECT name FROM party WHERE (TYPE='SALESMAN' OR Type='SUPPLYMAN') ORDER BY name", CN, adOpenStatic, adLockOptimistic
End If
Set DC2.RowSource = RSP
DC2.ListField = "name"
End If
End Sub

Private Sub DC2_Click(Area As Integer)
DC2_Change
End Sub

Private Sub DC2_GotFocus()
DC2_Change
SendKeys "{F4}"
End Sub

Private Sub DC2_KeyDown(KeyCode As Integer, shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DC2_Validate(Cancel As Boolean)
If DC2.Text <> "" Then
Set Rs = New ADODB.Recordset
Rs.Open "SELECT pid FROM party WHERE name='" & DC2.Text & "'", CN, adOpenStatic, adLockOptimistic

If Rs.RecordCount > 0 Then
Cancel = False
Else
MsgBox "Select a valid name please"
Cancel = True
End If
End If
End Sub

Private Sub DC3_Change()
If CC = False Then
Set RSP = New ADODB.Recordset
If DC3.Text <> "" Then
RSP.Open "SELECT name FROM party WHERE name Like '" & DC3.Text & "%' AND (TYPE='SALESMAN' OR Type='SUPPLYMAN') ORDER BY name", CN, adOpenStatic, adLockOptimistic
Else
RSP.Open "SELECT name FROM party WHERE (TYPE='SALESMAN' OR Type='SUPPLYMAN') ORDER BY name", CN, adOpenStatic, adLockOptimistic
End If
Set DC3.RowSource = RSP
DC3.ListField = "name"
End If
End Sub

Private Sub DC3_GotFocus()
DC3_Change
End Sub

Private Sub DC3_KeyDown(KeyCode As Integer, shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DC3_Validate(Cancel As Boolean)
If DC3.Text <> "" Then
Set Rs = New ADODB.Recordset
Rs.Open "SELECT pid FROM party WHERE name='" & DC3.Text & "'", CN, adOpenStatic, adLockOptimistic
If Rs.RecordCount > 0 Then
Cancel = False
Else
MsgBox "Select a valid name please"
Cancel = True
End If
End If
End Sub

Private Sub DC4_Change()
If CC = False Then
Set RSP = New ADODB.Recordset
If DC4.Text <> "" Then
RSP.Open "SELECT name FROM party WHERE name Like '" & DC4.Text & "%' AND (TYPE='SALESMAN' OR Type='SUPPLYMAN') ORDER BY name", CN, adOpenStatic, adLockOptimistic
Else
RSP.Open "SELECT name FROM party WHERE (TYPE='SALESMAN' OR Type='SUPPLYMAN') ORDER BY name", CN, adOpenStatic, adLockOptimistic
End If
Set DC4.RowSource = RSP
DC4.ListField = "name"
End If
End Sub

Private Sub DC4_GotFocus()
DC4_Change
End Sub

Private Sub DC4_KeyDown(KeyCode As Integer, shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DC4_Validate(Cancel As Boolean)
If DC4.Text <> "" Then
Set Rs = New ADODB.Recordset
Rs.Open "SELECT pid FROM party WHERE name='" & DC4.Text & "'", CN, adOpenStatic, adLockOptimistic

If Rs.RecordCount > 0 Then
Cancel = False
Else
MsgBox "Select a valid name please"
Cancel = True
End If
End If
End Sub

Private Sub DC5_Change()
If CC = False Then
Set RSP = New ADODB.Recordset
If DC5.Text <> "" Then
RSP.Open "SELECT name FROM party WHERE name Like '" & DC5.Text & "%' AND (TYPE='SALESMAN' OR Type='SUPPLYMAN') ORDER BY name", CN, adOpenStatic, adLockOptimistic
Else
RSP.Open "SELECT name FROM party WHERE (TYPE='SALESMAN' OR Type='SUPPLYMAN') ORDER BY name", CN, adOpenStatic, adLockOptimistic
End If
Set DC5.RowSource = RSP
DC5.ListField = "name"
End If
End Sub

Private Sub DC5_GotFocus()
DC5_Change
End Sub

Private Sub DC5_KeyDown(KeyCode As Integer, shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DC5_Validate(Cancel As Boolean)
If DC5.Text <> "" Then
Set Rs = New ADODB.Recordset
Rs.Open "SELECT pid FROM party WHERE name='" & DC5.Text & "'", CN, adOpenStatic, adLockOptimistic

If Rs.RecordCount > 0 Then
Cancel = False
Else
MsgBox "Select a valid name please"
Cancel = True
End If
End If
End Sub

Private Sub DC6_Change()
If CC = False Then
Set RSP = New ADODB.Recordset
If DC6.Text <> "" Then
RSP.Open "SELECT name FROM party WHERE name Like '" & DC6.Text & "%' AND (TYPE='SALESMAN' OR Type='SUPPLYMAN') ORDER BY name", CN, adOpenStatic, adLockOptimistic
Else
RSP.Open "SELECT name FROM party WHERE (TYPE='SALESMAN' OR Type='SUPPLYMAN') ORDER BY name", CN, adOpenStatic, adLockOptimistic
End If
Set DC6.RowSource = RSP
DC6.ListField = "name"
End If
End Sub

Private Sub DC6_GotFocus()
DC6_Change
End Sub

Private Sub DC6_KeyDown(KeyCode As Integer, shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DC6_Validate(Cancel As Boolean)
If DC6.Text <> "" Then
Set Rs = New ADODB.Recordset
Rs.Open "SELECT pid FROM party WHERE name='" & DC6.Text & "'", CN, adOpenStatic, adLockOptimistic

If Rs.RecordCount > 0 Then
Cancel = False
Else
MsgBox "Select a valid name please"
Cancel = True
End If
End If
End Sub

Private Sub dcPname_Change()
If CC = False Then
Set RS3 = New ADODB.Recordset
If dcPname.Text <> "" Then
RS3.Open "SELECT name FROM product WHERE name Like '" & UCase(dcPname.Text) & "%' ORDER BY name", CN, adOpenStatic, adLockReadOnly
Else
RS3.Open "SELECT name FROM product ORDER BY name", CN, adOpenStatic, adLockReadOnly
End If
Set dcPname.RowSource = RS3
dcPname.ListField = "name"
End If
End Sub

Private Sub dcPname_GotFocus()
dcPname_Change
SendKeys "{F4}"
End Sub

Private Sub dcPname_KeyDown(KeyCode As Integer, shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub dcPname_Validate(Cancel As Boolean)
a = ID_NaME("product", "prid", "name", dcPname.Text)
If a = "" Then
Cancel = True
End If
End Sub

Private Sub Form_Load()
''''''''''''''''''' theme
Set OBJ = New clsFunction
OBJ.clear Me
'''''''''''''''''''''''''''''

If Me.TY.Text = "STOCKOVERALL" Or Me.TY.Text = "STOCK" Then
DTE.Value = "01-01-2002"
Me.SSTab1.TabEnabled(1) = False
Else: DTE.Value = Date
DTS.Value = Date
End If
End Sub

