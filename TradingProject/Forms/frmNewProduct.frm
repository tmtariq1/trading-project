VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "msdatlst.ocx"
Object = "{D750EAB9-EB7B-450B-B9B6-0A0A17E13DCA}#1.0#0"; "xp.ocx"
Begin VB.Form frmNewProduct 
   BackColor       =   &H00C0C0C0&
   Caption         =   "New Product"
   ClientHeight    =   6270
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   7545
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frmNewProduct.frx":0000
   ScaleHeight     =   6270
   ScaleWidth      =   7545
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtBarcode 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      Height          =   285
      Left            =   4680
      TabIndex        =   2
      Top             =   720
      Width           =   2775
   End
   Begin VB.TextBox T9 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00004080&
      Height          =   285
      Left            =   2400
      TabIndex        =   10
      Top             =   4080
      Width           =   1095
   End
   Begin VB.TextBox T8 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00004080&
      Height          =   285
      Left            =   5760
      TabIndex        =   11
      Text            =   "1"
      Top             =   4440
      Width           =   1095
   End
   Begin VB.TextBox T7 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00004080&
      Height          =   285
      Left            =   2400
      TabIndex        =   8
      Top             =   3240
      Width           =   1095
   End
   Begin VB.TextBox T6 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00004080&
      Height          =   285
      Left            =   2400
      TabIndex        =   7
      Top             =   2880
      Width           =   1095
   End
   Begin VB.TextBox T5 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00004080&
      Height          =   285
      Left            =   2400
      Locked          =   -1  'True
      TabIndex        =   14
      Top             =   5160
      Width           =   1215
   End
   Begin VB.TextBox T1 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      Height          =   285
      Left            =   2400
      TabIndex        =   1
      Top             =   720
      Width           =   1215
   End
   Begin VB.TextBox T3 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00004080&
      Height          =   285
      Left            =   2400
      TabIndex        =   13
      Top             =   4800
      Width           =   1095
   End
   Begin VB.TextBox T2 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00004080&
      Height          =   285
      Left            =   2400
      TabIndex        =   6
      Top             =   2520
      Width           =   2895
   End
   Begin VB.TextBox T4 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00004080&
      Height          =   285
      Left            =   2400
      TabIndex        =   12
      Top             =   4440
      Width           =   1095
   End
   Begin XPLayout.XPButton cmdSave 
      Height          =   375
      Left            =   2400
      TabIndex        =   15
      Top             =   5640
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Save"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   16711680
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "frmNewProduct.frx":1596D6
      PICN            =   "frmNewProduct.frx":1596F2
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin XPLayout.XPButton cmdUpdate 
      Height          =   375
      Left            =   3600
      TabIndex        =   16
      Top             =   5640
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&UPDATE"
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   16711680
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "frmNewProduct.frx":159C8E
      PICN            =   "frmNewProduct.frx":159CAA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSDataListLib.DataCombo DC1 
      Height          =   330
      Left            =   2400
      TabIndex        =   3
      Top             =   1080
      Width           =   3375
      _ExtentX        =   5953
      _ExtentY        =   582
      _Version        =   393216
      Appearance      =   0
      BackColor       =   16777215
      ForeColor       =   16512
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSDataListLib.DataCombo DC2 
      Height          =   330
      Left            =   2400
      TabIndex        =   4
      Top             =   1560
      Width           =   3375
      _ExtentX        =   5953
      _ExtentY        =   582
      _Version        =   393216
      Appearance      =   0
      BackColor       =   16777215
      ForeColor       =   16512
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSDataListLib.DataCombo DC3 
      Height          =   330
      Left            =   2400
      TabIndex        =   9
      Top             =   3600
      Width           =   3375
      _ExtentX        =   5953
      _ExtentY        =   582
      _Version        =   393216
      Appearance      =   0
      BackColor       =   16777215
      ForeColor       =   16512
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSDataListLib.DataCombo DCU 
      Height          =   315
      Left            =   2400
      TabIndex        =   0
      Top             =   240
      Width           =   4695
      _ExtentX        =   8281
      _ExtentY        =   582
      _Version        =   393216
      Appearance      =   0
      BackColor       =   16777215
      ForeColor       =   16512
      ListField       =   ""
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   178
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSDataListLib.DataCombo DC4 
      Height          =   330
      Left            =   2400
      TabIndex        =   5
      Top             =   2040
      Width           =   3375
      _ExtentX        =   5953
      _ExtentY        =   582
      _Version        =   393216
      Appearance      =   0
      BackColor       =   16777215
      ForeColor       =   16512
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin XPLayout.XPButton cmdReset 
      Height          =   375
      Left            =   1200
      TabIndex        =   31
      Top             =   5640
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Reset"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   178
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   16711680
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "frmNewProduct.frx":15A246
      PICN            =   "frmNewProduct.frx":15A262
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin XPLayout.XPButton cmdDELETE 
      Height          =   375
      Left            =   4800
      TabIndex        =   32
      Top             =   5640
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Delete"
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   178
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "frmNewProduct.frx":15A7FC
      PICN            =   "frmNewProduct.frx":15A818
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Barcode 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Barcode"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000040&
      Height          =   300
      Left            =   3720
      TabIndex        =   33
      Top             =   720
      Width           =   900
   End
   Begin VB.Label Label12 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Sub Catagory"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000040&
      Height          =   300
      Left            =   720
      TabIndex        =   30
      Top             =   2040
      Width           =   1440
   End
   Begin VB.Label Label15 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Update"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000040&
      Height          =   300
      Left            =   720
      TabIndex        =   29
      Top             =   360
      Width           =   780
   End
   Begin VB.Label Label11 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "U.O.M"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000040&
      Height          =   300
      Left            =   720
      TabIndex        =   28
      Top             =   3600
      Width           =   705
   End
   Begin VB.Label Label10 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Weight"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000040&
      Height          =   300
      Left            =   720
      TabIndex        =   27
      Top             =   4080
      Width           =   765
   End
   Begin VB.Label Label9 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Piece / Carton"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000040&
      Height          =   300
      Left            =   4200
      TabIndex        =   26
      Top             =   4440
      Width           =   1515
   End
   Begin VB.Label Label8 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Catagory"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000040&
      Height          =   300
      Left            =   720
      TabIndex        =   25
      Top             =   1560
      Width           =   975
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "*Supplier"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000040&
      Height          =   300
      Index           =   3
      Left            =   720
      TabIndex        =   24
      Top             =   1080
      Width           =   1050
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Sale.Rate"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000040&
      Height          =   300
      Left            =   720
      TabIndex        =   23
      Top             =   3240
      Width           =   1020
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Pur.Rate"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000040&
      Height          =   300
      Left            =   720
      TabIndex        =   22
      Top             =   2880
      Width           =   945
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "*Sr.No"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000040&
      Height          =   300
      Left            =   720
      TabIndex        =   21
      Top             =   765
      Width           =   735
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Scheme.Piece"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000040&
      Height          =   300
      Left            =   720
      TabIndex        =   20
      Top             =   4800
      Width           =   1470
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Scheme.Qty"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000040&
      Height          =   300
      Left            =   720
      TabIndex        =   19
      Top             =   4440
      Width           =   1245
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Scheme"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000040&
      Height          =   300
      Left            =   720
      TabIndex        =   18
      Top             =   5160
      Width           =   825
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "*Product"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000040&
      Height          =   300
      Index           =   0
      Left            =   720
      TabIndex        =   17
      Top             =   2520
      Width           =   975
   End
End
Attribute VB_Name = "frmNewProduct"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub cmdDelete_Click()
If MsgBox("Are you sure to delete", vbYesNo) = vbOK Then
On Error GoTo ErrO
CN.BeginTrans
CN.Execute "delete from product where prid=" & Val(T1.Text) & ""
CN.CommitTrans
MsgBox "Delete Successful"
Call cmdReset_Click
ErrO:
If Err.Number <> 0 Then MsgBox Err.Description
End If
End Sub

Private Sub cmdReset_Click()
cmdUpdate.Enabled = False
cmdSave.Enabled = True
Set OBJ = New clsFunction
OBJ.clear Me
T1.Text = MaX_ID("PRID", "PRODUCT")

End Sub

Private Sub DC1_Change()
If CC = False Then
Set RS3 = New ADODB.Recordset
If DC1.Text <> "" Then
RS3.Open "SELECT NAME FROM PARTY WHERE type='purchase' and NAME Like '" & DC1.Text & "%' ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
Else
RS3.Open "SELECT NAME FROM PARTY where type='purchase' ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
End If
Set DC1.RowSource = RS3
DC1.ListField = "NAME"
End If
End Sub

Private Sub DC1_GotFocus()
DC1_Change
End Sub

Private Sub DC1_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DC1_Validate(Cancel As Boolean)
If DC1.Text <> "" Then
Set Rs = New ADODB.Recordset
Rs.Open "SELECT PID FROM PARTY WHERE NAME='" & DC1.Text & "'", CN, adOpenStatic, adLockOptimistic
If Rs.RecordCount > 0 Then
Cancel = False
Else
MsgBox "Select a valid party please"
Cancel = True
End If
End If
End Sub

Private Sub DC2_Change()
If CC = FASLE Then
Set Rsp = New ADODB.Recordset
If DC2.Text <> "" Then
Rsp.Open "SELECT DISTINCT CATAGORY FROM PRODUCT WHERE CATAGORY Like '" & DC2.Text & "%' AND CATAGORY<>'' ORDER BY CATAGORY", CN, adOpenStatic, adLockOptimistic
Else
Rsp.Open "SELECT DISTINCT CATAGORY FROM PRODUCT WHERE CATAGORY<>'' ORDER BY CATAGORY", CN, adOpenStatic, adLockOptimistic
End If
If Rsp.RecordCount > 0 Then
Set DC2.RowSource = Rsp
DC2.ListField = "CATAGORY"
End If
End If
End Sub

Private Sub DC2_GotFocus()
DC2_Change
End Sub

Private Sub DC2_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DC3_Change()
If CC = FASLE Then
Set Rsp = New ADODB.Recordset
If DC3.Text <> "" Then
Rsp.Open "SELECT DISTINCT UOM FROM PRODUCT WHERE UOM Like '" & DC3.Text & "%' AND UOM<>'' ORDER BY UOM", CN, adOpenStatic, adLockOptimistic
Else
Rsp.Open "SELECT DISTINCT UOM FROM PRODUCT WHERE UOM<>'' ORDER BY UOM", CN, adOpenStatic, adLockOptimistic
End If
If Rsp.RecordCount > 0 Then
Set DC3.RowSource = Rsp
DC3.ListField = "UOM"
End If
End If
End Sub

Private Sub DC3_GotFocus()
DC3_Change
End Sub

Private Sub DC3_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DC4_Change()
If CC = False Then
Set RS3 = New ADODB.Recordset
If DC4.Text <> "" Then
RS3.Open "SELECT DISTINCT SUBCATAGORY FROM PRODUCT WHERE SUBCATAGORY Like '" & DC4.Text & "%' ORDER BY SUBCATAGORY", CN, adOpenStatic, adLockOptimistic
Else
RS3.Open "SELECT DISTINCT SUBCATAGORY FROM PRODUCT ORDER BY SUBCATAGORY", CN, adOpenStatic, adLockOptimistic
End If
If RS3.RecordCount > 0 Then
Set DC4.RowSource = RS3
DC4.ListField = "SUBCATAGORY"
End If
End If
End Sub

Private Sub DC4_GotFocus()
DC4_Change
End Sub

Private Sub DC4_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DCU_Change()
If CC = False Then
Set RS3 = New ADODB.Recordset
If DCU.Text <> "" Then
RS3.Open "SELECT NAME FROM PRODUCT WHERE NAME Like '" & DCU.Text & "%' ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
Else
RS3.Open "SELECT NAME FROM PRODUCT ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
End If
Set DCU.RowSource = RS3
DCU.ListField = "NAME"
End If
'DCU_KeyUp 23, 1
End Sub

Private Sub DCU_GotFocus()
DCU_Change
End Sub

Private Sub DCU_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DCU_KeyUp(KeyCode As Integer, Shift As Integer)
'If KeyAscii = 13 Then
Set Rs = New ADODB.Recordset
Rs.Open "SELECT * FROM PRODUCT WHERE NAME='" & DCU.Text & "'", CN, adOpenStatic, adLockOptimistic
If Rs.RecordCount > 0 Then
T1.Text = Rs!prid
'txtBarcode.Text = Rs!Barcode
T1_KeyPress 13
End If
'End If

End Sub

Private Sub T1_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = 38 Then
T1.Text = Val(T1.Text) - 1
T1_KeyPress 13
ElseIf KeyCode = 40 Then
T1.Text = Val(T1.Text) + 1
T1_KeyPress 13
End If
End Sub

Private Sub T2_Validate(Cancel As Boolean)
Set Rsp = New ADODB.Recordset
Rsp.Open "select * from PRODUCT WHERE NAME='" & T2.Text & "'", CN, adOpenStatic, adLockOptimistic
If Rsp.RecordCount > 0 Then
MsgBox "ALREADY EXIST."
Exit Sub
End If
End Sub

Private Sub T3_Change()
T5.Text = Val(T4.Text) & " + " & Val(T3.Text)
End Sub

Private Sub T4_Change()
T5.Text = Val(T4.Text) & " + " & Val(T3.Text)
End Sub

Private Sub cmdSave_Click()
Set Rsp = New ADODB.Recordset
Rsp.Open "select * from PRODUCT WHERE NAME='" & T2.Text & "'", CN, adOpenStatic, adLockOptimistic
If Rsp.RecordCount > 1 Then
MsgBox "ALREADY EXIST."
Exit Sub
End If

If T1.Text <> "" And T2.Text <> "" And DC1.Text <> "" Then
Set Rs = New ADODB.Recordset
If cmdUpdate.Enabled = False Then
Rs.Open "select * from PRODUCT order by PRID", CN, adOpenStatic, adLockOptimistic
Rs.AddNew
Rs("PRID") = T1.Text
Else
Rs.Open "select * from PRODUCT WHERE PRID=" & Val(T1.Text) & "", CN, adOpenStatic, adLockOptimistic
End If
Rs("NAME") = T2.Text
If DC1.Text <> "" Then Rs("PID") = ID_NaME("PARTY", "PID", "NAME", DC1.Text)
'Rs("TID") = V_Tid
Rs("CATAGORY") = DC2.Text
Rs("SUBCATAGORY") = DC4.Text
Rs("UOM") = DC3.Text
Rs("QTY") = Val(T3.Text)
Rs("SQTY") = Val(T4.Text)
Rs("SCHEME") = T5.Text
Rs("PRATE") = Val(T6.Text)
Rs("SRATE") = Val(T7.Text)
If Val(T8.Text) = 0 Then Rs("PPC") = 1 Else Rs("PPC") = Val(T8.Text)
Rs("WEIGHT") = Val(T9.Text)
Rs("BARCODE") = txtBarcode.Text
Rs.Update
cmdReset_Click
DC2.SetFocus
Else
MsgBox "Please Enter Into Empty Fields"
End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyF5 Then
cmdReset_Click
ElseIf KeyCode = vbKeyEscape Then
Unload Me
End If
End Sub

Private Sub Form_Load()
''''''''''''''''''' theme
Set OBJ = New clsFunction
OBJ.clear Me
'''''''''''''''''''''''''''''

T1.Text = MaX_ID("PRID", "PRODUCT")
End Sub

Private Sub T1_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
SHoW_UP
End If
'If (KeyAscii > 47 And KeyAscii < 58) Or KeyAscii = vbKeyBack Or KeyAscii = 13 Then
'Else
'KeyAscii = 0
'MsgBox "Integer Only"
'End If
End Sub

Function SHoW_UP()
'If Chk_Tid("PRODUCT", "PRID", T1.Text) = False Then
'MsgBox "Change Login"
'Exit Function
'End If
prid = T1.Text
prName = DCU.Text
prBarcode = txtBarcode.Text
cmdReset_Click
T1.Text = prid
DCU.Text = prName
txtBarcode.Text = prBarcode

If Val(T1.Text) <> 0 Then
Set Rs = New ADODB.Recordset
'Rs.Open "SELECT top 1 * FROM PRODUCT WHERE PRID=" & Val(T1.Text) & " or barcode =" & Val(txtBarcode.Text) & "", CN, adOpenStatic, adLockOptimistic
Rs.Open "SELECT top 1 * FROM PRODUCT WHERE PRID=" & Val(T1.Text), CN, adOpenStatic, adLockOptimistic
If Rs.RecordCount > 0 Then

Set RSD = New ADODB.Recordset
RSD.Open "SELECT count(*) FROM stock WHERE prid =" & Rs!prid & "", CN, adOpenStatic, adLockOptimistic
If RSD.RecordCount > 0 And RSD(0) > 0 Then
Me.cmdDelete.Enabled = False
Else
Me.cmdDelete.Enabled = True
End If

cmdUpdate.Enabled = True
cmdSave.Enabled = False
T1.Text = IIf(IsNull(Rs("prid")), "", Rs("prid"))
T2.Text = IIf(IsNull(Rs("NAME")), "", Rs("NAME"))
If Not IsNull(Rs!PID) Then
DC1.Text = NaME_iD("PARTY", "PID", Rs!PID)
End If
DC2.Text = IIf(IsNull(Rs("CATAGORY")), "", Rs("CATAGORY"))
DC4.Text = IIf(IsNull(Rs("SUBCATAGORY")), "", Rs("SUBCATAGORY"))
DC3.Text = IIf(IsNull(Rs("UOM")), "", Rs("UOM"))
T3.Text = IIf(IsNull(Rs!Qty), "", Rs!Qty)
T5.Text = IIf(IsNull(Rs!SCHEME), "", Rs!SCHEME)
T4.Text = IIf(IsNull(Rs!SQTY), "", Rs!SQTY)
T6.Text = IIf(IsNull(Rs("PRATE")), "", Rs("PRATE"))
T7.Text = IIf(IsNull(Rs("SRATE")), "", Rs("SRATE"))
T8.Text = IIf(IsNull(Rs("PPC")), "", Rs("PPC"))
T9.Text = IIf(IsNull(Rs("WEIGHT")), "", Rs("WEIGHT"))
txtBarcode.Text = IIf(IsNull(Rs("barcode")), "", Rs("barcode"))
'txtBarcode.Text = IIf(IsNull(Rs("barcode")), IIf(prBarcode > 0, prBarcode, ""), Rs("barcode"))
'Else
'cmdReset_Click
End If
End If
End Function

Function DeLT()
CN.Execute "UPDATE PRODUCT SET NAME='" & T2.Text & "',SNAME='" & DC1.Text & "',PRATE=" & Val(T6.Text) & ",SRATE=" & Val(T7.Text) & ",SCHEME='" & T5.Text & "',SQTY=" & Val(T4.Text) & ",QTY=" & Val(T3.Text) & " WHERE SNO=" & T1.Text & ""
End Function

Private Sub cmdUpdate_Click()
If MsgBox("Are you sure to Update", vbYesNo) = vbYes Then
cmdSave_Click
MsgBox "Update Successful"
End If
End Sub

Private Sub XPButton3_Click()
cmdReset_Click
End Sub

Private Sub txtBarcode_KeyPress(KeyAscii As Integer)
'T1_KeyPress KeyAscii
End Sub
