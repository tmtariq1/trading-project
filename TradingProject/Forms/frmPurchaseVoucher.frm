VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{D750EAB9-EB7B-450B-B9B6-0A0A17E13DCA}#1.0#0"; "XP.ocx"
Begin VB.Form frmPUR 
   Caption         =   "Purchase Voucher"
   ClientHeight    =   7575
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11940
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frmPurchaseVoucher.frx":0000
   ScaleHeight     =   8791.103
   ScaleMode       =   0  'User
   ScaleWidth      =   12060.61
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox T2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   375
      Left            =   7680
      MaxLength       =   60
      TabIndex        =   17
      Top             =   1080
      Width           =   1455
   End
   Begin VB.TextBox T1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   375
      Left            =   1680
      MaxLength       =   60
      TabIndex        =   16
      Top             =   120
      Width           =   1095
   End
   Begin VB.TextBox T3 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF00FF&
      Height          =   375
      Left            =   1680
      MaxLength       =   60
      TabIndex        =   15
      Top             =   1560
      Width           =   4695
   End
   Begin VB.TextBox Txt1 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   255
      Left            =   2640
      MaxLength       =   60
      TabIndex        =   13
      Top             =   3840
      Width           =   975
   End
   Begin VB.Frame FR1 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   7680
      TabIndex        =   10
      Top             =   240
      Width           =   1695
      Begin VB.OptionButton Option3 
         BackColor       =   &H00C0C0FF&
         Caption         =   "Credit"
         ForeColor       =   &H00800000&
         Height          =   225
         Left            =   0
         TabIndex        =   12
         Top             =   0
         Value           =   -1  'True
         Width           =   855
      End
      Begin VB.OptionButton Option4 
         BackColor       =   &H00C0C0FF&
         Caption         =   "Cash"
         ForeColor       =   &H00800000&
         Height          =   225
         Left            =   840
         TabIndex        =   11
         Top             =   0
         Width           =   855
      End
   End
   Begin VB.TextBox T6 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   3000
      MaxLength       =   60
      TabIndex        =   9
      Top             =   6600
      Width           =   615
   End
   Begin VB.TextBox TC1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   3720
      MaxLength       =   60
      TabIndex        =   8
      Top             =   5880
      Width           =   975
   End
   Begin VB.TextBox T5 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   3000
      MaxLength       =   60
      TabIndex        =   7
      Top             =   6240
      Width           =   615
   End
   Begin VB.TextBox TC3 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   3720
      MaxLength       =   60
      TabIndex        =   6
      Top             =   6600
      Width           =   975
   End
   Begin VB.TextBox TC2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   3720
      MaxLength       =   60
      TabIndex        =   5
      Top             =   6240
      Width           =   975
   End
   Begin VB.TextBox TB1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   7200
      MaxLength       =   60
      TabIndex        =   4
      Top             =   6240
      Width           =   1215
   End
   Begin VB.TextBox TNA 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   6960
      MaxLength       =   60
      TabIndex        =   3
      Top             =   6600
      Width           =   1455
   End
   Begin VB.TextBox T4 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   3000
      MaxLength       =   60
      TabIndex        =   2
      Top             =   5880
      Width           =   615
   End
   Begin VB.TextBox txtD 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   7200
      MaxLength       =   60
      TabIndex        =   1
      Top             =   5880
      Width           =   1215
   End
   Begin VB.TextBox txtDLES 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   9000
      MaxLength       =   60
      TabIndex        =   0
      Top             =   6120
      Visible         =   0   'False
      Width           =   1455
   End
   Begin MSDataListLib.DataCombo DCPR 
      Height          =   330
      Left            =   1320
      TabIndex        =   14
      Top             =   3840
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   582
      _Version        =   393216
      Appearance      =   0
      BackColor       =   12632319
      ForeColor       =   8388608
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid FG1 
      Height          =   3495
      Left            =   120
      TabIndex        =   18
      Top             =   2040
      Width           =   11775
      _ExtentX        =   20770
      _ExtentY        =   6165
      _Version        =   393216
      Rows            =   150
      Cols            =   16
      AllowBigSelection=   0   'False
      GridLines       =   3
      Appearance      =   0
   End
   Begin MSComCtl2.DTPicker DT1 
      Height          =   375
      Left            =   4440
      TabIndex        =   19
      Top             =   120
      Width           =   1935
      _ExtentX        =   3413
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   12648447
      CalendarTitleBackColor=   12640511
      CustomFormat    =   "dd-MMM-yy"
      Format          =   92405761
      CurrentDate     =   37916
   End
   Begin XPLayout.XPButton cmdSave 
      Height          =   375
      Left            =   3600
      TabIndex        =   20
      Top             =   7080
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Save"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "frmPurchaseVoucher.frx":1596D6
      PICN            =   "frmPurchaseVoucher.frx":1596F2
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin XPLayout.XPButton cmdDelete 
      Height          =   375
      Left            =   4920
      TabIndex        =   21
      Top             =   7080
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Delete"
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "frmPurchaseVoucher.frx":159C8E
      PICN            =   "frmPurchaseVoucher.frx":159CAA
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin XPLayout.XPButton cmdPrint 
      Height          =   375
      Left            =   7560
      TabIndex        =   22
      Top             =   7080
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Print"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "frmPurchaseVoucher.frx":15A246
      PICN            =   "frmPurchaseVoucher.frx":15A262
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin XPLayout.XPButton cmdUpdate 
      Height          =   375
      Left            =   6240
      TabIndex        =   23
      Top             =   7080
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Update"
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "frmPurchaseVoucher.frx":15A7FE
      PICN            =   "frmPurchaseVoucher.frx":15A81A
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin XPLayout.XPButton cmdReset 
      Height          =   375
      Left            =   2280
      TabIndex        =   24
      Top             =   7080
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Reset"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "frmPurchaseVoucher.frx":15ADB6
      PICN            =   "frmPurchaseVoucher.frx":15ADD2
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSDataListLib.DataCombo DC1 
      Height          =   330
      Left            =   1680
      TabIndex        =   25
      Top             =   615
      Width           =   4695
      _ExtentX        =   8281
      _ExtentY        =   582
      _Version        =   393216
      Appearance      =   0
      BackColor       =   12632319
      ForeColor       =   8388608
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSDataListLib.DataCombo DC2 
      Height          =   330
      Left            =   7680
      TabIndex        =   26
      Top             =   600
      Width           =   3495
      _ExtentX        =   6165
      _ExtentY        =   582
      _Version        =   393216
      Appearance      =   0
      BackColor       =   12632319
      ForeColor       =   8388608
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid FG2 
      Height          =   255
      Left            =   120
      TabIndex        =   27
      Top             =   5520
      Width           =   11775
      _ExtentX        =   20770
      _ExtentY        =   450
      _Version        =   393216
      Rows            =   1
      Cols            =   16
      FixedRows       =   0
      AllowBigSelection=   0   'False
      GridLines       =   3
      Appearance      =   0
   End
   Begin MSDataListLib.DataCombo DCS 
      Height          =   330
      Left            =   1680
      TabIndex        =   28
      Top             =   1080
      Width           =   4695
      _ExtentX        =   8281
      _ExtentY        =   582
      _Version        =   393216
      Appearance      =   0
      BackColor       =   12632319
      ForeColor       =   8388608
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin XPLayout.XPButton XPButton1 
      Height          =   375
      Left            =   10080
      TabIndex        =   29
      Top             =   0
      Visible         =   0   'False
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&MUPDATE"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "frmPurchaseVoucher.frx":15B36C
      PICN            =   "frmPurchaseVoucher.frx":15B388
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Invoice #"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   225
      Index           =   4
      Left            =   6480
      TabIndex        =   43
      Top             =   1200
      Width           =   720
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   225
      Index           =   0
      Left            =   3720
      TabIndex        =   42
      Top             =   240
      Width           =   390
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Doc #"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   225
      Index           =   2
      Left            =   600
      TabIndex        =   41
      Top             =   240
      Width           =   480
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Description"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   225
      Index           =   1
      Left            =   600
      TabIndex        =   40
      Top             =   1680
      Width           =   945
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Party"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   225
      Index           =   3
      Left            =   600
      TabIndex        =   39
      Top             =   720
      Width           =   405
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "SupplyMan"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   225
      Index           =   5
      Left            =   6480
      TabIndex        =   38
      Top             =   600
      Width           =   900
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Com %"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   240
      Index           =   6
      Left            =   1200
      TabIndex        =   37
      Top             =   5880
      Width           =   645
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Ext.Com %"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   240
      Index           =   10
      Left            =   1200
      TabIndex        =   36
      Top             =   6240
      Width           =   1005
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Less SMS %"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   240
      Index           =   11
      Left            =   1200
      TabIndex        =   35
      Top             =   6600
      Width           =   1155
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Bilty expense"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   240
      Index           =   12
      Left            =   4920
      TabIndex        =   34
      Top             =   6240
      Width           =   1185
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "N.Amount"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   240
      Index           =   16
      Left            =   4920
      TabIndex        =   33
      Top             =   6600
      Width           =   870
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "From"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   225
      Index           =   14
      Left            =   600
      TabIndex        =   32
      Top             =   1200
      Width           =   435
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Discount"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   240
      Index           =   17
      Left            =   4920
      TabIndex        =   31
      Top             =   5880
      Width           =   765
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Disc+Less"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   240
      Index           =   18
      Left            =   9000
      TabIndex        =   30
      Top             =   5880
      Visible         =   0   'False
      Width           =   930
   End
End
Attribute VB_Name = "frmPUR"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Function CL_FG1()
FG1.clear
FG2.clear
FG1.TextMatrix(0, 0) = "SNO"
FG1.TextMatrix(0, 1) = "PRID"
FG1.TextMatrix(0, 2) = "Product"
FG1.TextMatrix(0, 3) = "Carton"
FG1.TextMatrix(0, 4) = "Piece"
FG1.TextMatrix(0, 5) = "Scheme"
FG1.TextMatrix(0, 6) = "Rate"
FG1.TextMatrix(0, 7) = "Less"
FG1.TextMatrix(0, 8) = "Amount"
FG1.TextMatrix(0, 9) = "Com%"
FG1.TextMatrix(0, 10) = "Com Rs"
FG1.TextMatrix(0, 11) = "Net Rs"
FG1.TextMatrix(0, 12) = "SQTY"
FG1.TextMatrix(0, 13) = "Qty"
FG1.TextMatrix(0, 14) = "PPC"
FG1.TextMatrix(0, 15) = "Sc.Amount"

FG1.ColWidth(0) = 500
FG1.ColWidth(1) = 0
FG1.ColWidth(2) = 3000
FG1.ColWidth(3) = 900
FG1.ColWidth(4) = 900
FG1.ColWidth(5) = 800
FG1.ColWidth(6) = 800
FG1.ColWidth(7) = 800
FG1.ColWidth(8) = 900
FG1.ColWidth(9) = 600
FG1.ColWidth(10) = 900
FG1.ColWidth(11) = 1000
FG1.ColWidth(12) = 0
FG1.ColWidth(13) = 0
FG1.ColWidth(14) = 0
FG1.ColWidth(15) = 0
''''''''''FG2
FG2.ColWidth(0) = 500
FG2.ColWidth(1) = 0
FG2.ColWidth(2) = 3000
FG2.ColWidth(3) = 900
FG2.ColWidth(4) = 900
FG2.ColWidth(5) = 800
FG2.ColWidth(6) = 800
FG2.ColWidth(7) = 800
FG2.ColWidth(8) = 900
FG2.ColWidth(9) = 600
FG2.ColWidth(10) = 900
FG2.ColWidth(11) = 1000
FG2.ColWidth(12) = 0
FG2.ColWidth(13) = 0
FG2.ColWidth(14) = 0
FG2.ColWidth(15) = 0

For i = 1 To FG1.Rows - 1
FG1.TextMatrix(i, 0) = i
Next i

Txt1.Visible = False
DCPR.Visible = False
Txt1.Text = ""
DCPR.Text = ""
End Function

Private Sub DC1_Change()
If CC = False Then
Set RsP = New ADODB.Recordset
If DC1.Text <> "" Then
RsP.Open "SELECT NAME FROM PARTY WHERE NAME Like '" & UCase(DC1.Text) & "%' ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
Else
RsP.Open "SELECT NAME FROM PARTY ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
End If
Set DC1.RowSource = RsP
DC1.ListField = "NAME"
End If
End Sub

Private Sub DC1_GotFocus()
DC1_Change
SendKeys "{F4}"
End Sub

Function FG1_TOT()
c = 0
FG2.clear
For i = 1 To FG1.Rows - 1
FG2.TextMatrix(0, 3) = Val(FG2.TextMatrix(0, 3)) + Val(FG1.TextMatrix(i, 3))
FG2.TextMatrix(0, 4) = Val(FG2.TextMatrix(0, 4)) + Val(FG1.TextMatrix(i, 4))
FG2.TextMatrix(0, 5) = Val(FG2.TextMatrix(0, 5)) + Val(FG1.TextMatrix(i, 5))
FG2.TextMatrix(0, 7) = Val(FG2.TextMatrix(0, 7)) + Val(FG1.TextMatrix(i, 7))
FG2.TextMatrix(0, 8) = Val(FG2.TextMatrix(0, 8)) + Val(FG1.TextMatrix(i, 8))
FG2.TextMatrix(0, 10) = Val(FG2.TextMatrix(0, 10)) + Val(FG1.TextMatrix(i, 10))
FG2.TextMatrix(0, 11) = Val(FG2.TextMatrix(0, 11)) + Val(FG1.TextMatrix(i, 11))
FG2.TextMatrix(0, 15) = Val(FG2.TextMatrix(0, 15)) + Val(FG1.TextMatrix(i, 15))
Next i
End Function

Private Sub DC1_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DC1_Validate(Cancel As Boolean)
    If DC1.Text <> "" Then
    Set Rs = New ADODB.Recordset
    Rs.Open "SELECT * FROM PARTY WHERE NAME='" & DC1.Text & "'", CN, adOpenStatic, adLockOptimistic
    If Rs.RecordCount > 0 Then
    Set T4.DataSource = Rs
    T4.DataField = "COMM"
    Set T5.DataSource = Rs
    T5.DataField = "ECOMM"
    Set T6.DataSource = Rs
    T6.DataField = "SMS"
    Call Get_Inv
    Cancel = False
    Else
    MsgBox "Select a valid party please"
    Cancel = True
    End If
    End If
End Sub

Private Sub DC2_Validate(Cancel As Boolean)
If DC2.Text <> "" Then
Set Rs = New ADODB.Recordset
Rs.Open "SELECT * FROM PARTY WHERE NAME='" & DC2.Text & "'", CN, adOpenStatic, adLockOptimistic
If Rs.RecordCount > 0 Then
Cancel = False
Else
MsgBox "Select a valid party please"
Cancel = True
End If
End If
End Sub

Private Sub DCPR_Change()
If CC = False Then
Set RsP = New ADODB.Recordset
If DCPR.Text <> "" Then
RsP.Open "SELECT NAME FROM PRODUCT WHERE NAME Like '" & DCPR.Text & "%' ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
Else
RsP.Open "SELECT NAME FROM PRODUCT ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
End If
Set DCPR.RowSource = RsP
DCPR.ListField = "NAME"
End If
FG1.TextMatrix(FG1.Row, FG1.CoL) = DCPR.Text
End Sub

Private Sub DCPR_GotFocus()
DCPR_Change
SendKeys "{F4}"
End Sub

Private Sub DCPR_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DCPR_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
DCPR_Validate False
If FG1.CoL = 6 Then
FG1.Row = FG1.Row + 1
FG1.CoL = 2
Else
FG1.CoL = FG1.CoL + 1
End If
SHoW_CoNT
End If
End Sub

Private Sub DCPR_Validate(Cancel As Boolean)
If DCPR.Text <> "" Then
FG1.TextMatrix(FG1.Row, 5) = ""
FG1.TextMatrix(FG1.Row, 7) = ""
Set Rs = New ADODB.Recordset
Rs.Open "SELECT * FROM PRODUCT WHERE NAME='" & DCPR.Text & "'", CN, adOpenStatic, adLockOptimistic
If Rs.RecordCount > 0 Then
FG1.TextMatrix(FG1.Row, 1) = IIf(IsNull(Rs("PRID")), "", Rs("PRID"))
FG1.TextMatrix(FG1.Row, 6) = IIf(IsNull(Rs("SRATE")), "", Rs("SRATE"))
FG1.TextMatrix(FG1.Row, 12) = IIf(IsNull(Rs("SQTY")), "", Rs("SQTY"))
FG1.TextMatrix(FG1.Row, 13) = IIf(IsNull(Rs("QTY")), "", Rs("QTY"))
FG1.TextMatrix(FG1.Row, 14) = IIf(IsNull(Rs("PPC")), "", Rs("PPC"))
Cancel = False
Else
MsgBox "Select a valid Product please"
Cancel = True
End If
End If
End Sub

Function Get_Inv()
If DC1.Text <> "" And ID_NaME("PARTY", "PID", "NAME", DC1.Text) <> "" Then
Set RSI = New ADODB.Recordset
RSI.Open "SELECT MAX(INVOICEO) FROM STOCK WHERE PID=" & ID_NaME("PARTY", "PID", "NAME", DC1.Text) & " AND ETYPE='PURCHASE'", CN, adOpenStatic, adLockOptimistic
If RSI.RecordCount > 0 Then
T2.Text = IIf(IsNull(RSI(0)), 1, RSI(0) + 1)
End If
End If
End Function

Private Sub DC2_Change()
If CC = False Then
Set RsP = New ADODB.Recordset
If DC2.Text <> "" Then
RsP.Open "SELECT NAME FROM PARTY WHERE NAME Like '" & UCase(DC2.Text) & "%' AND TYPE='SALESMAN'  ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
Else
RsP.Open "SELECT NAME FROM PARTY WHERE TYPE='SALESMAN' ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
End If
Set DC2.RowSource = RsP
DC2.ListField = "NAME"
End If
End Sub

Private Sub DC2_GotFocus()
DC2_Change
SendKeys "{F4}"
End Sub

Private Sub DC2_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DCS_Change()
If CC = False Then
Set RsP = New ADODB.Recordset
If DCS.Text <> "" Then
RsP.Open "SELECT NAME FROM PARTY WHERE (TYPE='DEPARTMENT' OR TYPE='SALESMAN') AND NAME Like '" & DCS.Text & "%' ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
Else
RsP.Open "SELECT NAME FROM PARTY WHERE (TYPE='DEPARTMENT' OR TYPE='SALESMAN') ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
End If
Set DCS.RowSource = RsP
DCS.ListField = "NAME"
End If
End Sub

Private Sub DCS_GotFocus()
DCS_Change
End Sub

Private Sub DCS_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DCS_Validate(Cancel As Boolean)
If DCS.Text <> "" Then
Set Rs = New ADODB.Recordset
Rs.Open "SELECT PID FROM PARTY WHERE NAME='" & DCS.Text & "'", CN, adOpenStatic, adLockOptimistic
If Rs.RecordCount > 0 Then
Cancel = False
Else
MsgBox "Select a valid Department please"
Cancel = True
End If
End If
End Sub

Private Sub FG1_Click()
SHoW_CoNT
End Sub

Function SHoW_CoNT()
If FG1.CoL = 2 Then
        DCPR.Text = FG1.TextMatrix(FG1.Row, FG1.CoL)
        DCPR.Top = FG1.Top + FG1.CellTop
        DCPR.Left = FG1.Left + FG1.CellLeft
        DCPR.Visible = True
        DCPR.SetFocus
        DCPR.Width = FG1.CellWidth
        Txt1.Visible = False
ElseIf FG1.CoL = 3 Or FG1.CoL = 4 Or FG1.CoL = 5 Or FG1.CoL = 6 Or FG1.CoL = 7 Or FG1.CoL = 9 Then
Txt1.Text = FG1.TextMatrix(FG1.Row, FG1.CoL)
    Txt1.Left = FG1.Left + FG1.CellLeft
    Txt1.Top = FG1.Top + FG1.CellTop
    Txt1.Height = FG1.CellHeight
    Txt1.Width = FG1.CellWidth
    Txt1.Visible = True
    Txt1.SetFocus
    DCPR.Visible = False
End If
End Function

Private Sub FG1_EnterCell()
SHoW_CoNT
End Sub

Private Sub FG1_GotFocus()
SHoW_CoNT
'MsgBox FG1.Row & FG1.Col
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyF2 Then
FG1.RemoveItem (FG1.Row)
ElseIf KeyCode = vbKeyF3 Then
frmNewAccount.Show vbModal
ElseIf KeyCode = vbKeyF7 Then
frmNewProduct.Show vbModal
ElseIf KeyCode = vbKeyF5 Then
Call cmdReset_Click
ElseIf KeyCode = vbKeyEscape Then
Unload Me
ElseIf KeyCode = vbKeyF6 Then
Y = MsgBox("Are you sure to delete ?", vbYesNo)
If Y = vbYes Then
FG1.RemoveItem (FG1.Row)
Else
End If
End If
End Sub

Private Sub Form_Load()
Set OBJ = New clsFunction
OBJ.CHECK_date Me
T1.Text = MaX_DcN("PURCHASE", "PLEDGER")
DT1.Value = Date
CL_FG1
End Sub

Private Sub cmdPrint_KeyPress(KeyAscii As Integer)
If KeyAscii = 9 Then
T1.SetFocus
End If
End Sub

Private Sub MSFlexGrid1_Click()

End Sub

Private Sub T1_GotFocus()
SendKeys "{home}+{end}"
End Sub

Private Sub T1_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = 38 Then
T1.Text = Val(T1.Text) - 1
T1_KeyPress 13
ElseIf KeyCode = 40 Then
T1.Text = Val(T1.Text) + 1
T1_KeyPress 13
End If
End Sub

Private Sub T1_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
If Chk_Tid("STOCK", "PURCHASE", T1.Text) = False Then
MsgBox "Change Login"
Exit Sub
End If

c = Val(T1.Text)
cmdReset_Click
T1.Text = c

    Set Rs = New ADODB.Recordset
    Rs.Open "SELECT * FROM STOCK WHERE DCNO=" & Val(T1.Text) & " AND ETYPE='PURCHASE'", CN, adOpenStatic, adLockOptimistic
    If Rs.RecordCount > 0 Then
    cmdSave.Enabled = False
    cmdDelete.Enabled = True
    cmdUpdate.Enabled = True
    If Not IsNull(Rs!SID) Then
    DC2.Text = NaME_iD("PARTY", "PID", Rs!SID)
    End If
    DC1.Text = NaME_iD("PARTY", "PID", Rs!PID)
    DCS.Text = NaME_iD("PARTY", "PID", Rs!DID)
    DT1.Value = Rs("DATE")
    T2.Text = IIf(IsNull(Rs("INVOICE")), "", Rs("INVOICE"))
    T3.Text = IIf(IsNull(Rs("DESCRIPTION")), "", Rs("DESCRIPTION"))
    r = 1
        Do Until Rs.EOF
        Set RSPR = New ADODB.Recordset
        RSPR.Open "SELECT * FROM PRODUCT WHERE PRID=" & Rs("PRID") & "", CN, adOpenStatic, adLockOptimistic
        FG1.TextMatrix(r, 1) = Rs("PRID")
        FG1.TextMatrix(r, 2) = RSPR!Name
        FG1.TextMatrix(r, 3) = Abs(Rs("QTYC"))
        FG1.TextMatrix(r, 4) = Abs(Rs("QTYP"))
        FG1.TextMatrix(r, 5) = Rs("SQTYP")
        FG1.TextMatrix(r, 6) = Rs("RATE")
        FG1.TextMatrix(r, 7) = Rs("LESS")
        FG1.TextMatrix(r, 8) = IIf(IsNull(Rs!AMOUNT), "", Rs!AMOUNT)
        FG1.TextMatrix(r, 9) = IIf(IsNull(Rs!COMM), "", Rs!COMM)
        FG1.TextMatrix(r, 10) = IIf(IsNull(Rs!CAMOUNT), "", Rs!CAMOUNT)
        FG1.TextMatrix(r, 11) = IIf(IsNull(Rs!NAMOUNT), "", Rs!NAMOUNT)
        FG1.TextMatrix(r, 12) = RSPR!SQTY
        FG1.TextMatrix(r, 13) = RSPR!Qty
        FG1.TextMatrix(r, 14) = RSPR!PPC
        FG1.TextMatrix(r, 15) = IIf(IsNull(Rs!SCAMOUNT), "", Rs!SCAMOUNT)
        Rs.MoveNext
        r = r + 1
        Loop
        '''''''''''''''''''''''''''''''''''''
        FG1_TOT
    Set RsL = New ADODB.Recordset
    RsL.Open "SELECT * FROM PLEDGER WHERE DCNO=" & Val(T1.Text) & " AND ETYPE='PURCHASE' AND PID=" & ID_NaME("PARTY", "PID", "NAME", "CASH") & " ", CN, adOpenStatic, adLockOptimistic
    If RsL.RecordCount > 0 Then
    Option4.Value = True
    Else
    Option3.Value = True
    End If
    '''''''''''''''''''''''''''''COMMISSTION CREDIT
    Set RsL = New ADODB.Recordset
    RsL.Open "SELECT * FROM PLEDGER WHERE DCNO=" & Val(T1.Text) & " AND ETYPE='PURCHASE' AND PID=" & ID_NaME("PARTY", "PID", "NAME", "COMMISSION") & " ", CN, adOpenStatic, adLockOptimistic
    If RsL.RecordCount > 0 Then
    'TC1.Text = RsL!CREDIT
    c = RsL!CREDIT * 100 / Val(FG2.TextMatrix(0, 11))
    T4.Text = Round(c, 0)
    End If
    '''''''''''''''''''''''''''''EXTRA COMMISSTION CREDIT
    Set RsL = New ADODB.Recordset
    RsL.Open "SELECT * FROM PLEDGER WHERE DCNO=" & Val(T1.Text) & " AND ETYPE='PURCHASE' AND PID=" & ID_NaME("PARTY", "PID", "NAME", "EXTRA COMMISSION") & " ", CN, adOpenStatic, adLockOptimistic
    If RsL.RecordCount > 0 Then
    'TC2.Text = RsL!CREDIT
    c = Val(RsL!CREDIT) * 100 / (Val(FG2.TextMatrix(0, 11)) - Val(TC1.Text))
    T5.Text = Round(c, 0)
    End If
    '''''''''''''''''''''''''''''SMS CREDIT
    Set RsL = New ADODB.Recordset
    RsL.Open "SELECT * FROM PLEDGER WHERE DCNO=" & Val(T1.Text) & " AND ETYPE='PURCHASE' AND PID=" & ID_NaME("PARTY", "PID", "NAME", "SMS") & " ", CN, adOpenStatic, adLockOptimistic
    If RsL.RecordCount > 0 Then
    'TC3.Text = RsL!CREDIT
    c = Val(RsL!CREDIT) * 100 / (Val(FG2.TextMatrix(0, 11)) - Val(TC1.Text) - Val(TC2.Text))
    T6.Text = Round(c, 0)
    End If
        '''''''''''''''''''''''''''''DISCOUNT CREDIT
    Set RsL = New ADODB.Recordset
    RsL.Open "SELECT * FROM PLEDGER WHERE DCNO=" & Val(T1.Text) & " AND ETYPE='PURCHASE' AND PID=" & ID_NaME("PARTY", "PID", "NAME", "DISCOUNT") & " ", CN, adOpenStatic, adLockOptimistic
    If RsL.RecordCount > 0 Then
    txtD.Text = RsL!CREDIT
    End If
    '''''''''''''''''''''''''''''''FREIGHT CREDIT
    Set RsL = New ADODB.Recordset
    RsL.Open "SELECT * FROM PLEDGER WHERE DCNO=" & Val(T1.Text) & " AND ETYPE='PURCHASE' AND PID=" & ID_NaME("PARTY", "PID", "NAME", "FREIGHT OUTWORD") & " ", CN, adOpenStatic, adLockOptimistic
    If RsL.RecordCount > 0 Then
    TB1.Text = RsL!CREDIT
    End If
    '''''''''''''''''''''''''''''''''''
    Else
    'Call cmdReset_Click
    End If
End If
End Sub

Private Sub T3_Validate(Cancel As Boolean)
FG1.CoL = 2
FG1.Row = 1
End Sub

Private Sub T4_Change()
TA = Round(Val(FG2.TextMatrix(0, 11)) - Val(FG2.TextMatrix(0, 7)), 0)
TC1.Text = Round(Val(FG2.TextMatrix(0, 11)) * Val(T4.Text) / 100, 0)
TC2.Text = Round((Val(FG2.TextMatrix(0, 11)) - Val(TC1.Text)) * Val(T5.Text) / 100, 0)
TC3.Text = Round((Val(FG2.TextMatrix(0, 11)) - Val(TC1.Text) - Val(TC2.Text)) * Val(T6.Text) / 100, 0)
TNA.Text = Val(FG2.TextMatrix(0, 11)) - Val(TB1.Text) - Val(TC1.Text) - Val(TC2.Text) - Val(TC3.Text) - Val(txtD.Text)
txtDLES.Text = Val(FG2.TextMatrix(0, 15)) + Val(FG2.TextMatrix(0, 7)) + Val(TC1.Text) + Val(TC2.Text) + Val(TC3.Text) + Val(txtD.Text) + Val(FG2.TextMatrix(0, 10))
End Sub

Private Sub T5_Change()
T4_Change
End Sub

Private Sub T6_Change()
T4_Change
End Sub

Private Sub TB1_Change()
T4_Change
End Sub

Private Sub Txt1_Change()
FG1.TextMatrix(FG1.Row, FG1.CoL) = Txt1.Text
Qty1 = (Val(FG1.TextMatrix(FG1.Row, 3)) * Val(FG1.TextMatrix(FG1.Row, 14))) + Val(FG1.TextMatrix(FG1.Row, 4))
FG1.TextMatrix(FG1.Row, 8) = Qty1 * Val(FG1.TextMatrix(FG1.Row, 6))
FG1.TextMatrix(FG1.Row, 15) = Round(Val(FG1.TextMatrix(FG1.Row, 5)) * Val(FG1.TextMatrix(FG1.Row, 6)), 0)    ''''Sc Amount
If Val(FG1.TextMatrix(FG1.Row, 8)) * Val(FG1.TextMatrix(FG1.Row, 9)) > 0 Then
FG1.TextMatrix(FG1.Row, 10) = Round(Val(FG1.TextMatrix(FG1.Row, 8)) * Val(FG1.TextMatrix(FG1.Row, 9)) / 100, 0)
Else
FG1.TextMatrix(FG1.Row, 10) = ""
End If
FG1.TextMatrix(FG1.Row, 11) = Val(FG1.TextMatrix(FG1.Row, 8)) - Val(FG1.TextMatrix(FG1.Row, 10)) - Val(FG1.TextMatrix(FG1.Row, 7))
GeT_LESS
FG1_TOT
T4_Change
End Sub

Function GeT_LESS()
If FG1.CoL = 3 Or FG1.CoL = 4 Then
If Val(Qty1) <> 0 And Val(FG1.TextMatrix(FG1.Row, 12)) <> 0 Then
FG1.TextMatrix(FG1.Row, 5) = Fix((Val(Qty1) / Val(FG1.TextMatrix(FG1.Row, 12))) * Val(FG1.TextMatrix(FG1.Row, 13)))
FG1.TextMatrix(FG1.Row, 7) = Round((Val(Qty1) Mod Val(FG1.TextMatrix(FG1.Row, 12))) * Val(FG1.TextMatrix(FG1.Row, 6)) * 1 / (Val(FG1.TextMatrix(FG1.Row, 12)) + 1), 0)
End If
If Val(Qty1) = 0 Then
FG1.TextMatrix(FG1.Row, 5) = ""
FG1.TextMatrix(FG1.Row, 7) = ""
End If
End If
End Function

Private Sub Txt1_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyRight Then
If FG1.CoL <> FG1.Cols - 1 Then
FG1.CoL = FG1.CoL + 1
End If
ElseIf KeyCode = vbKeyLeft Then
If FG1.CoL <> 0 Then
FG1.CoL = FG1.CoL - 1
End If
ElseIf KeyCode = vbKeyUp Then
If FG1.Row <> 1 Then
FG1.Row = FG1.Row - 1
End If
ElseIf KeyCode = vbKeyDown Then
If FG1.Row <> FG1.Rows - 1 Then
FG1.Row = FG1.Row + 1
End If
End If
SHoW_CoNT
End Sub

Private Sub Txt1_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
If FG1.CoL = 9 Then
FG1.Row = FG1.Row + 1
FG1.CoL = 2
ElseIf FG1.CoL = 7 Then
FG1.CoL = FG1.CoL + 2
Else
FG1.CoL = FG1.CoL + 1
End If
SHoW_CoNT
End If
End Sub

Private Sub cmdSave_Click()
If T1.Text And T2.Text <> "" Then
''''''''''''''''STOCK''''''''''''''''''''''
r = 1
Set Rs = New ADODB.Recordset
Rs.Open "SELECT * FROM STOCK WHERE STID=(SELECT MAX(STID) FROM STOCK)", CN, adOpenStatic, adLockOptimistic
Do Until FG1.TextMatrix(r, 1) = ""
Rs.AddNew
Rs("STID") = MaX_ID("STID", "STOCK")
If DC2.Text <> "" Then
Rs("SID") = ID_NaME("PARTY", "PID", "NAME", DC2.Text)
End If
Rs("TID") = V_Tid
Rs("PID") = ID_NaME("PARTY", "PID", "NAME", DC1.Text)
Rs("DID") = ID_NaME("PARTY", "PID", "NAME", DCS.Text)
Rs("DCNO") = Val(T1.Text)
Rs("ETYPE") = "PURCHASE"
Rs("DATE") = DT1.Value
Rs("INVOICE") = T2.Text
Rs("INVOICEO") = Val(T2.Text)
Rs("DESCRIPTION") = T3.Text
Rs("PRID") = Val(FG1.TextMatrix(r, 1))
Rs("QTYC") = -Val(FG1.TextMatrix(r, 3))
Rs("QTYP") = -Val(FG1.TextMatrix(r, 4))
Rs("TQTYP") = -(Val(FG1.TextMatrix(r, 14)) * Val(FG1.TextMatrix(r, 3)) + Val(FG1.TextMatrix(r, 4)) + Val(FG1.TextMatrix(r, 5)))
Rs("SQTYP") = Val(FG1.TextMatrix(r, 5))
Rs("RATE") = Val(FG1.TextMatrix(r, 6))
Rs("LESS") = Val(FG1.TextMatrix(r, 7))
Rs("SLESS") = Round(Val(FG1.TextMatrix(r, 7)) + (Val(FG1.TextMatrix(r, 6)) * Val(FG1.TextMatrix(r, 5))), 0)
Rs("AMOUNT") = Val(FG1.TextMatrix(r, 8))
Rs("COMM") = Val(FG1.TextMatrix(r, 9))
Rs("CAMOUNT") = Val(FG1.TextMatrix(r, 10))
Rs("NAMOUNT") = Val(FG1.TextMatrix(r, 11))
Rs("SCAMOUNT") = Val(FG1.TextMatrix(r, 15))
Rs.Update
r = r + 1
Loop
'''''''''''''''PLEDGER---PURCHASE DEBIT'''''''''''''
Set RsL = New ADODB.Recordset
RsL.Open "SELECT * FROM PLEDGER WHERE PLEDID=(SELECT MAX(PLEDID) FROM PLEDGER)", CN, adOpenStatic, adLockOptimistic
RsL.AddNew
RsL("PLEDID") = MaX_ID("PLEDID", "PLEDGER")
RsL("PID") = ID_NaME("PARTY", "PID", "NAME", "PURCHASE")
If DC2.Text <> "" Then
RsL("SID") = ID_NaME("PARTY", "PID", "NAME", DC2.Text)
End If
RsL("TID") = V_Tid
RsL("DCNO") = Val(T1.Text)
RsL("ETYPE") = "PURCHASE"
RsL("DATE") = DT1.Value
RsL("INVOICE") = T2.Text
RsL("INVOICEO") = Val(T2.Text)
RsL("DESCRIPTION2") = T3.Text
RsL("DESCRIPTION") = "PURCHASE " & T1.Text & " (" & DC1.Text & ")" '{" & T3.Text & "}"
RsL("DEBIT") = Round(Val(FG2.TextMatrix(0, 8)) + Val(FG2.TextMatrix(0, 15)), 0)
RsL.Update
RsL.Close
'''''''''''''''PLEDGER---scheme CREDIT'''''''''''''
If Round(Val(FG2.TextMatrix(0, 15)), 0) <> 0 Then
Set RsL = New ADODB.Recordset
RsL.Open "SELECT * FROM PLEDGER WHERE PLEDID=(SELECT MAX(PLEDID) FROM PLEDGER)", CN, adOpenStatic, adLockOptimistic
RsL.AddNew
RsL("PLEDID") = MaX_ID("PLEDID", "PLEDGER")
RsL("PID") = ID_NaME("PARTY", "PID", "NAME", "Scheme")
If DC2.Text <> "" Then
RsL("SID") = ID_NaME("PARTY", "PID", "NAME", DC2.Text)
End If
RsL("TID") = V_Tid
RsL("DCNO") = Val(T1.Text)
RsL("ETYPE") = "PURCHASE"
RsL("DATE") = DT1.Value
RsL("INVOICE") = T2.Text
RsL("INVOICEO") = Val(T2.Text)
RsL("DESCRIPTION2") = T3.Text
RsL("DESCRIPTION") = "PURCHASE " & T1.Text & " (" & DC1.Text & ")" '{" & T3.Text & "}"
RsL("CREDIT") = Round(Val(FG2.TextMatrix(0, 15)), 0)
RsL.Update
RsL.Close
End If
'''''''''''''''LESS'''''''''''''
If Val(FG2.TextMatrix(0, 7)) <> 0 Or Val(FG2.TextMatrix(0, 5)) <> 0 Then
Set RsL = New ADODB.Recordset
RsL.Open "SELECT * FROM PLEDGER WHERE PLEDID=(SELECT MAX(PLEDID) FROM PLEDGER)", CN, adOpenStatic, adLockOptimistic
RsL.AddNew
RsL("PLEDID") = MaX_ID("PLEDID", "PLEDGER")
RsL("PID") = ID_NaME("PARTY", "PID", "NAME", "Scheme Less")
If DC2.Text <> "" Then
RsL("SID") = ID_NaME("PARTY", "PID", "NAME", DC2.Text)
End If
RsL("TID") = V_Tid
RsL("DCNO") = Val(T1.Text)
RsL("ETYPE") = "PURCHASE"
RsL("DATE") = DT1.Value
RsL("INVOICE") = T2.Text
RsL("INVOICEO") = Val(T2.Text)
RsL("DESCRIPTION2") = T3.Text
RsL("DESCRIPTION") = "PURCHASE " & T1.Text & " (" & DC1.Text & ")"
RsL("CREDIT") = Round(Val(FG2.TextMatrix(0, 7)), 0)
RsL.Update
RsL.Close
End If
'''''''''''''''Comm'''''''''''''
If Val(FG2.TextMatrix(0, 7)) <> 0 Or Val(FG2.TextMatrix(0, 5)) <> 0 Then
Set RsL = New ADODB.Recordset
RsL.Open "SELECT * FROM PLEDGER WHERE PLEDID=(SELECT MAX(PLEDID) FROM PLEDGER)", CN, adOpenStatic, adLockOptimistic
RsL.AddNew
RsL("PLEDID") = MaX_ID("PLEDID", "PLEDGER")
RsL("PID") = ID_NaME("PARTY", "PID", "NAME", "Scheme Commission")
If DC2.Text <> "" Then
RsL("SID") = ID_NaME("PARTY", "PID", "NAME", DC2.Text)
End If
RsL("TID") = V_Tid
RsL("DCNO") = Val(T1.Text)
RsL("ETYPE") = "PURCHASE"
RsL("DATE") = DT1.Value
RsL("INVOICE") = T2.Text
RsL("INVOICEO") = Val(T2.Text)
RsL("DESCRIPTION2") = T3.Text
RsL("DESCRIPTION") = "PURCHASE " & T1.Text & " (" & DC1.Text & ")"
RsL("CREDIT") = Round(Val(FG2.TextMatrix(0, 10)), 0)
RsL.Update
RsL.Close
End If
'''''''''''''''PLEDGER---COMMISSION'''''''''''''
If Val(TC1.Text) <> 0 Then
Set RsL = New ADODB.Recordset
RsL.Open "SELECT * FROM PLEDGER WHERE PLEDID=(SELECT MAX(PLEDID) FROM PLEDGER)", CN, adOpenStatic, adLockOptimistic
RsL.AddNew
RsL("PLEDID") = MaX_ID("PLEDID", "PLEDGER")
RsL("PID") = ID_NaME("PARTY", "PID", "NAME", "COMMISSION")
If DC2.Text <> "" Then
RsL("SID") = ID_NaME("PARTY", "PID", "NAME", DC2.Text)
End If
RsL("TID") = V_Tid
RsL("DCNO") = Val(T1.Text)
RsL("ETYPE") = "PURCHASE"
RsL("DATE") = DT1.Value
RsL("INVOICE") = T2.Text
RsL("INVOICEO") = Val(T2.Text)
RsL("DESCRIPTION") = "PURCHASE " & T1.Text & " (" & DC1.Text & ")"
RsL("CREDIT") = Val(TC1.Text)
RsL.Update
RsL.Close
End If
'''''''''''''''PLEDGER---EXTRA COMMISSION'''''''''''''
If Val(TC2.Text) <> 0 Then
Set RsL = New ADODB.Recordset
RsL.Open "SELECT * FROM PLEDGER WHERE PLEDID=(SELECT MAX(PLEDID) FROM PLEDGER)", CN, adOpenStatic, adLockOptimistic
RsL.AddNew
RsL("PLEDID") = MaX_ID("PLEDID", "PLEDGER")
RsL("PID") = ID_NaME("PARTY", "PID", "NAME", "EXTRA COMMISSION")
If DC2.Text <> "" Then
RsL("SID") = ID_NaME("PARTY", "PID", "NAME", DC2.Text)
End If
Rs("TID") = V_Tid
RsL("DCNO") = Val(T1.Text)
RsL("ETYPE") = "PURCHASE"
RsL("DATE") = DT1.Value
RsL("INVOICE") = T2.Text
RsL("INVOICEO") = Val(T2.Text)
RsL("DESCRIPTION") = "PURCHASE " & T1.Text & " (" & DC1.Text & ")"
RsL("CREDIT") = Val(TC2.Text)
RsL.Update
RsL.Close
End If
'''''''''''''''PLEDGER---SMS'''''''''''''
If Val(TC3.Text) <> 0 Then
Set RsL = New ADODB.Recordset
RsL.Open "SELECT * FROM PLEDGER WHERE PLEDID=(SELECT MAX(PLEDID) FROM PLEDGER)", CN, adOpenStatic, adLockOptimistic
RsL.AddNew
RsL("PLEDID") = MaX_ID("PLEDID", "PLEDGER")
RsL("PID") = ID_NaME("PARTY", "PID", "NAME", "SMS")
If DC2.Text <> "" Then
RsL("SID") = ID_NaME("PARTY", "PID", "NAME", DC2.Text)
End If
RsL("TID") = V_Tid
RsL("DCNO") = Val(T1.Text)
RsL("ETYPE") = "PURCHASE"
RsL("DATE") = DT1.Value
RsL("INVOICE") = T2.Text
RsL("INVOICEO") = Val(T2.Text)
RsL("DESCRIPTION") = "PURCHASE " & T1.Text & " (" & DC1.Text & ")"
RsL("CREDIT") = Val(TC3.Text)
RsL.Update
RsL.Close
End If
'''''''''''''''DISCOUNT'''''''''''''
If Val(txtD.Text) <> 0 Then
Set RsL = New ADODB.Recordset
RsL.Open "SELECT * FROM PLEDGER WHERE PLEDID=(SELECT MAX(PLEDID) FROM PLEDGER)", CN, adOpenStatic, adLockOptimistic
RsL.AddNew
RsL("PLEDID") = MaX_ID("PLEDID", "PLEDGER")
RsL("PID") = ID_NaME("PARTY", "PID", "NAME", "DISCOUNT")
If DC2.Text <> "" Then
RsL("SID") = ID_NaME("PARTY", "PID", "NAME", DC2.Text)
End If
RsL("TID") = V_Tid
RsL("DCNO") = Val(T1.Text)
RsL("ETYPE") = "PURCHASE"
RsL("DATE") = DT1.Value
RsL("INVOICE") = T2.Text
RsL("INVOICEO") = Val(T2.Text)
RsL("DESCRIPTION2") = T3.Text
RsL("DESCRIPTION") = "PURCHASE " & T1.Text & " (" & DC1.Text & ")"
RsL("CREDIT") = Val(txtD.Text)
RsL.Update
RsL.Close
End If
'''''''''''''''PLEDGER---FRAIGHT'''''''''''''
If Val(TB1.Text) <> 0 Then
Set RsL = New ADODB.Recordset
RsL.Open "SELECT * FROM PLEDGER WHERE PLEDID=(SELECT MAX(PLEDID) FROM PLEDGER)", CN, adOpenStatic, adLockOptimistic
RsL.AddNew
RsL("PLEDID") = MaX_ID("PLEDID", "PLEDGER")
RsL("PID") = ID_NaME("PARTY", "PID", "NAME", "FREIGHT OUTWORD")
If DC2.Text <> "" Then
RsL("SID") = ID_NaME("PARTY", "PID", "NAME", DC2.Text)
End If
RsL("TID") = V_Tid
RsL("DCNO") = Val(T1.Text)
RsL("ETYPE") = "PURCHASE"
RsL("DATE") = DT1.Value
RsL("INVOICE") = T2.Text
RsL("INVOICEO") = Val(T2.Text)
RsL("DESCRIPTION") = "PURCHASE " & T1.Text & " (" & DC1.Text & ")"
RsL("CREDIT") = Val(TB1.Text)
RsL.Update
RsL.Close
End If
'''''''''''''''PLEDGER--PARTY CREDIT'''''''''''''
Set RsL = New ADODB.Recordset
RsL.Open "SELECT * FROM PLEDGER WHERE PLEDID=(SELECT MAX(PLEDID) FROM PLEDGER)", CN, adOpenStatic, adLockOptimistic
RsL.AddNew
RsL("PLEDID") = MaX_ID("PLEDID", "PLEDGER")
If DC2.Text <> "" Then
RsL("SID") = ID_NaME("PARTY", "PID", "NAME", DC2.Text)
End If
RsL("TID") = V_Tid
RsL("PID") = ID_NaME("PARTY", "PID", "NAME", DC1.Text)
RsL("DCNO") = Val(T1.Text)
RsL("ETYPE") = "PURCHASE"
RsL("DATE") = DT1.Value
RsL("INVOICE") = T2.Text
RsL("INVOICEO") = Val(T2.Text)
RsL("DESCRIPTION") = "PURCHASE " & T1.Text & " (" & T3.Text & ")"
RsL("CREDIT") = Val(TNA.Text)
RsL.Update
RsL.Close
''''''''PLEDGER---CASH'''''''''''''
If Option4 Then
Set RsL = New ADODB.Recordset
RsL.Open "SELECT * FROM PLEDGER WHERE PLEDID=(SELECT MAX(PLEDID) FROM PLEDGER)", CN, adOpenStatic, adLockOptimistic
RsL.AddNew
RsL("PLEDID") = MaX_ID("PLEDID", "PLEDGER")
RsL("PID") = ID_NaME("PARTY", "PID", "NAME", "CASH")
If DC2.Text <> "" Then
RsL("SID") = ID_NaME("PARTY", "PID", "NAME", DC2.Text)
End If
Rs("TID") = V_Tid
RsL("DCNO") = Val(T1.Text)
RsL("ETYPE") = "PURCHASE"
RsL("DATE") = DT1.Value
RsL("INVOICE") = T2.Text
RsL("INVOICEO") = Val(T2.Text)
RsL("DESCRIPTION") = "PURCHASE " & T1.Text & " (" & DC1.Text & ")"
RsL("CREDIT") = Val(TNA.Text)
RsL.Update
RsL.Close
''''''''''''''''''''''''
Set RsL = New ADODB.Recordset
RsL.Open "SELECT * FROM PLEDGER WHERE PLEDID=(SELECT MAX(PLEDID) FROM PLEDGER)", CN, adOpenStatic, adLockOptimistic
RsL.AddNew
RsL("PLEDID") = MaX_ID("PLEDID", "PLEDGER")
If DC2.Text <> "" Then
RsL("SID") = ID_NaME("PARTY", "PID", "NAME", DC2.Text)
End If
RsL("TID") = V_Tid
RsL("PID") = ID_NaME("PARTY", "PID", "NAME", DC1.Text)
RsL("DCNO") = Val(T1.Text)
RsL("ETYPE") = "PURCHASE"
RsL("DATE") = DT1.Value
RsL("INVOICE") = T2.Text
RsL("INVOICEO") = Val(T2.Text)
RsL("DESCRIPTION") = "PURCHASE " & T1.Text & " (" & T3.Text & ") Cash Receive"
RsL("DEBIT") = Val(TNA.Text)
RsL.Update
RsL.Close
End If
'''''''''''''''''''''''''''

Call cmdReset_Click
Else
MsgBox "Enter Into Empty Fields"
End If
End Sub

Private Sub cmdDelete_Click()
CN.Execute "DELETE FROM STOCK WHERE ETYPE='PURCHASE' AND DCNO='" & T1.Text & "'"
CN.Execute "DELETE FROM PLEDGER WHERE ETYPE='PURCHASE' AND DCNO='" & T1.Text & "'"
End Sub

Private Sub cmdPrint_Click()
Unload rptSaleVoucher
With frmPUR
'If .Option3.Value = True Then rptSaleVoucher.txtType.SetText "Credit" Else rptSaleVoucher.txtType.SetText "Cash"
rptSaleVoucher.txtHead.SetText "Purchase Voucher "
rptSaleVoucher.txtSno.SetText "Invoice# " & .T1.Text
rptSaleVoucher.txtParty.SetText "Party Name: " & .DC1.Text
rptSaleVoucher.txtSM.SetText "SalesMan Name: " & .DC2.Text
rptSaleVoucher.txtDes.SetText "Description: " & .T3.Text
rptSaleVoucher.txtCompanyName.SetText V_Company
rptSaleVoucher.txtDate.SetText "Date: " & .DT1.Value
rptSaleVoucher.txtC1.SetText "Comm@ " & .T4.Text & " Rs: " & .TC1.Text
rptSaleVoucher.txtC2.SetText "E.Comm@ " & .T5.Text & " Rs: " & .TC2.Text
rptSaleVoucher.txtC3.SetText "SMS@ " & .T5.Text & " Rs: " & .TC3.Text
rptSaleVoucher.txtC4.SetText "Disscount " & .txtD.Text
rptSaleVoucher.txtC5.SetText "Builty Expense " & .TB1.Text
rptSaleVoucher.txta5.SetText "Net Amount " & .TNA.Text
End With
rptSaleVoucher.RecordSelectionFormula = "{STOCK.ETYPE}='PURCHASE' AND {STOCK.dcno}=" & T1.Text
ReportsView.CRViewer1.ReportSource = rptSaleVoucher
ReportsView.CRViewer1.ViewReport
ReportsView.Show vbModal
End Sub

Private Sub txtD_Change()
T4_Change
End Sub

Private Sub cmdUpdate_Click()
If T1.Text And T2.Text <> "" Then
Call cmdDelete_Click  ''''''''''''DELETE
Call cmdSave_Click  ''''''''''''cmdSave
Call cmdReset_Click  ''''''''''''REFRESH
End If
End Sub

Private Sub cmdReset_Click()
cmdSave.Enabled = True
cmdDelete.Enabled = False
cmdUpdate.Enabled = False
CL_FG1
Set OBJ = New clsFunction
OBJ.clear Me
T1.Text = MaX_DcN("PURCHASE", "PLEDGER")
T1.SetFocus
End Sub

Private Sub XPButton1_Click()
Set RSM = New ADODB.Recordset
RSM.Open "SELECT DISTINCT DCNO FROM PLEDGER WHERE ETYPE='PURCHASE' ORDER BY DCNO", CN, adOpenStatic, adLockOptimistic
If RSM.RecordCount > 0 Then
Do Until RSM.EOF
T1.Text = RSM!DCNO
T1_KeyPress 13
cmdUpdate_Click
RSM.MoveNext
Loop
End If
End Sub

