VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Begin VB.Form Form1 
   Caption         =   "Sale Reports"
   ClientHeight    =   2745
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   5490
   LinkTopic       =   "Form1"
   ScaleHeight     =   2745
   ScaleWidth      =   5490
   StartUpPosition =   3  'Windows Default
   Begin VB.CommandButton Command2 
      Caption         =   "Company Without Saleman"
      Height          =   492
      Left            =   1440
      TabIndex        =   5
      Top             =   1920
      Width           =   2292
   End
   Begin MSComCtl2.DTPicker DTPicker2 
      Height          =   255
      Left            =   2280
      TabIndex        =   2
      Top             =   600
      Width           =   2175
      _ExtentX        =   3836
      _ExtentY        =   450
      _Version        =   393216
      Format          =   7733249
      CurrentDate     =   39493
   End
   Begin MSComCtl2.DTPicker DTPicker1 
      Height          =   255
      Left            =   2280
      TabIndex        =   1
      Top             =   240
      Width           =   2175
      _ExtentX        =   3836
      _ExtentY        =   450
      _Version        =   393216
      Format          =   7733249
      CurrentDate     =   39493
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Company SalemanWise"
      Height          =   492
      Left            =   1440
      TabIndex        =   0
      Top             =   1200
      Width           =   2292
   End
   Begin VB.Label Label1 
      Caption         =   "To"
      Height          =   255
      Index           =   1
      Left            =   1080
      TabIndex        =   4
      Top             =   600
      Width           =   615
   End
   Begin VB.Label Label1 
      Caption         =   "From"
      Height          =   255
      Index           =   0
      Left            =   1080
      TabIndex        =   3
      Top             =   240
      Width           =   615
   End
End
Attribute VB_Name = "Form1"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click()
Unload CrystalReport1
Call UpdateReportCredentials(CrystalReport1)
With CrystalReport1
.DiscardSavedData
'Set CrtParameters = CRReport.ParameterFields
        '.ParameterFields.GetItemByName("@dt1").AddDefaultValue DTPicker1.Value
        '.ParameterFields.GetItemByName("@dt2").AddDefaultValue DTPicker2.Value
        '.ParameterFields(0) = "@dt1;" & DTPicker1.Value & ";TRUE"
        '.ParameterFields(0) = "@dt2;" & DTPicker1.Value
        .ParameterFields(1).AddCurrentValue (DTPicker1.Value)
        .ParameterFields(2).AddCurrentValue (DTPicker2.Value)
'.txtHead.SetText "Expense Report"
'.txtDate.SetText "Date Between:" & Me.DTS.Value & " " & Me.DTE.Value
'.RecordSelectionFormula = "{smSaleCW.DATE} in datetime('" & DTPicker1.Value & "')  to  datetime('" & DTPicker2.Value & "')"
End With
Screen.MousePointer = vbHourglass
Form2.CRViewer91.ReportSource = CrystalReport1
Form2.CRViewer91.ViewReport
Screen.MousePointer = vbDefault
Form2.Show vbModal
End Sub

Private Sub Command2_Click()
Unload CrystalReport2
Screen.MousePointer = vbHourglass
Form2.CRViewer91.ReportSource = CrystalReport2
Form2.CRViewer91.ViewReport
Screen.MousePointer = vbDefault
Form2.Show vbModal
End Sub

Private Sub Form_Load()
DTPicker2.Value = Date
End Sub
