VERSION 5.00
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{D750EAB9-EB7B-450B-B9B6-0A0A17E13DCA}#1.0#0"; "XP.ocx"
Begin VB.Form WareHouse 
   BackColor       =   &H00C0C0C0&
   Caption         =   "WareHouse"
   ClientHeight    =   4755
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   6060
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "WareHouse.frx":0000
   ScaleHeight     =   4755
   ScaleWidth      =   6060
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox txtEmail 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      ForeColor       =   &H00004080&
      Height          =   285
      Left            =   1560
      TabIndex        =   5
      Top             =   2880
      Width           =   2655
   End
   Begin VB.TextBox txtFax 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      ForeColor       =   &H00004080&
      Height          =   285
      Left            =   1560
      TabIndex        =   4
      Top             =   2520
      Width           =   2655
   End
   Begin VB.TextBox txtCell 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      ForeColor       =   &H00004080&
      Height          =   285
      Left            =   1560
      TabIndex        =   3
      Top             =   2160
      Width           =   2655
   End
   Begin VB.TextBox txtContactPerson 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      ForeColor       =   &H00004080&
      Height          =   285
      Left            =   1560
      TabIndex        =   1
      Top             =   1440
      Width           =   4095
   End
   Begin VB.TextBox txtSno 
      Appearance      =   0  'Flat
      BackColor       =   &H00008080&
      Height          =   285
      Left            =   1560
      TabIndex        =   11
      Top             =   720
      Width           =   1215
   End
   Begin VB.TextBox txtPhone 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      ForeColor       =   &H00004080&
      Height          =   285
      Left            =   1560
      TabIndex        =   2
      Top             =   1800
      Width           =   2655
   End
   Begin VB.TextBox txtName 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      ForeColor       =   &H00004080&
      Height          =   285
      Left            =   1560
      TabIndex        =   0
      Top             =   1080
      Width           =   4095
   End
   Begin VB.TextBox txtAddress 
      Appearance      =   0  'Flat
      BackColor       =   &H00FFFFFF&
      ForeColor       =   &H00004080&
      Height          =   285
      Left            =   1560
      TabIndex        =   7
      Top             =   3720
      Width           =   4215
   End
   Begin XPLayout.XPButton cmdSave 
      Height          =   375
      Left            =   2175
      TabIndex        =   9
      Top             =   4200
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Save"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   16711680
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "WareHouse.frx":15F942
      PICN            =   "WareHouse.frx":15F95E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin XPLayout.XPButton cmdUpdate 
      Height          =   375
      Left            =   3360
      TabIndex        =   10
      Top             =   4200
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&UPDATE"
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   16711680
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "WareHouse.frx":15FEFA
      PICN            =   "WareHouse.frx":15FF16
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSDataListLib.DataCombo dcUpdate 
      Height          =   360
      Left            =   1560
      TabIndex        =   12
      Top             =   240
      Width           =   4215
      _ExtentX        =   7435
      _ExtentY        =   635
      _Version        =   393216
      Appearance      =   0
      BackColor       =   14737632
      ForeColor       =   16512
      ListField       =   ""
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSDataListLib.DataCombo dcCity 
      Height          =   360
      Left            =   1560
      TabIndex        =   6
      Top             =   3240
      Width           =   2655
      _ExtentX        =   4683
      _ExtentY        =   635
      _Version        =   393216
      Appearance      =   0
      ForeColor       =   16512
      ListField       =   ""
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   9.75
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin XPLayout.XPButton cmdReset 
      Height          =   375
      Left            =   960
      TabIndex        =   8
      Top             =   4200
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Reset"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   178
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   16711680
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "WareHouse.frx":1604B2
      PICN            =   "WareHouse.frx":1604CE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label8 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "E-Mail"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00004080&
      Height          =   240
      Left            =   825
      TabIndex        =   22
      Top             =   2880
      Width           =   600
   End
   Begin VB.Label Label7 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Fax #"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00004080&
      Height          =   240
      Left            =   945
      TabIndex        =   21
      Top             =   2520
      Width           =   480
   End
   Begin VB.Label Label5 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Cell #"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00004080&
      Height          =   240
      Left            =   945
      TabIndex        =   20
      Top             =   2160
      Width           =   480
   End
   Begin VB.Label Label12 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Contact Person"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00004080&
      Height          =   240
      Left            =   150
      TabIndex        =   19
      Top             =   1440
      Width           =   1275
   End
   Begin VB.Label Label10 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Update"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00004080&
      Height          =   240
      Left            =   825
      TabIndex        =   18
      Top             =   360
      Width           =   600
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "City"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00004080&
      Height          =   240
      Left            =   1080
      TabIndex        =   17
      Top             =   3360
      Width           =   345
   End
   Begin VB.Label Label3 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Sr.No"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00004080&
      Height          =   240
      Left            =   960
      TabIndex        =   16
      Top             =   765
      Width           =   465
   End
   Begin VB.Label Label4 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Address"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00004080&
      Height          =   240
      Left            =   765
      TabIndex        =   15
      Top             =   3720
      Width           =   660
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Phone#"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00004080&
      Height          =   240
      Left            =   810
      TabIndex        =   14
      Top             =   1800
      Width           =   615
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Name*"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00004080&
      Height          =   240
      Left            =   840
      TabIndex        =   13
      Top             =   1080
      Width           =   585
   End
End
Attribute VB_Name = "WareHouse"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub dccity_Change()
If CC = False Then
Set RS3 = New ADODB.Recordset
If dcCity.Text <> "" Then
RS3.Open "SELECT DISTINCT CITY FROM warehouse WHERE CITY Like '" & dcCity.Text & "%' ORDER BY CITY", CN, adOpenStatic, adLockOptimistic
Else
RS3.Open "SELECT DISTINCT CITY FROM warehouse ORDER BY CITY", CN, adOpenStatic, adLockOptimistic
End If
If RS3.RecordCount > 0 Then
Set dcCity.RowSource = RS3
dcCity.ListField = "CITY"
End If
End If
End Sub

Private Sub dccity_GotFocus()
dccity_Change
End Sub

Private Sub dccity_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub dcupdate_Change()
If CC = False Then
Set RS3 = New ADODB.Recordset
If dcUpdate.Text <> "" Then
RS3.Open "SELECT NAME FROM warehouse WHERE NAME Like '" & dcUpdate.Text & "%' ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
Else
RS3.Open "SELECT NAME FROM warehouse ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
End If
Set dcUpdate.RowSource = RS3
dcUpdate.ListField = "NAME"
End If
End Sub

Private Sub dcupdate_GotFocus()
dcupdate_Change
End Sub

Private Sub dcupdate_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub dcupdate_KeyUp(KeyCode As Integer, Shift As Integer)
If dcUpdate.Text <> "" Then
Set Rs = New ADODB.Recordset
Rs.Open "SELECT did FROM warehouse WHERE NAME='" & dcUpdate.Text & "'", CN, adOpenStatic, adLockOptimistic
If Rs.RecordCount > 0 Then
cmdSave.Enabled = False
cmdUpdate.Enabled = True
txtSno.Text = Rs!did
txtsno_KeyPress 13
End If
End If
End Sub

Private Sub Label1_Click()
'frmwarehouse.Show vbModal
End Sub

Private Sub txtsno_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = 38 Then
txtSno.Text = Val(txtSno.Text) - 1
txtsno_KeyPress 13
ElseIf KeyCode = 40 Then
txtSno.Text = Val(txtSno.Text) + 1
txtsno_KeyPress 13
End If
End Sub

Private Sub txtname_Validate(Cancel As Boolean)
Set RSP = New ADODB.Recordset
RSP.Open "select * from warehouse WHERE NAME='" & txtName.Text & "' AND did <>" & Val(txtSno.Text) & "", CN, adOpenStatic, adLockOptimistic
If RSP.RecordCount > 0 Then
MsgBox "ALREADY EXIST."
Exit Sub
End If
End Sub


Private Sub cmdReset_Click()
Set OBJ = New clsFunction
OBJ.clear Me
Me.txtSno.Text = MaX_ID("did", "warehouse")
Me.cmdUpdate.Enabled = False
Me.cmdSave.Enabled = True
'Me.dcUpdate = ""
'Me.txtName.Text = ""
'Me.txtContactPerson.Text = ""
'Me.txtPhone.Text = ""
'Me.txtCell.Text = ""
'Me.txtFax.Text = ""
'Me.txtEmail.Text = ""
'Me.txtAddress.Text = ""
'Me.dcCity.Text = ""
End Sub

Private Sub cmdSave_Click()
If txtName.Text <> "" Then
Set Rs = New ADODB.Recordset
If cmdSave.Enabled = False Then
Rs.Open "SELECT * FROM warehouse WHERE did =" & Val(txtSno.Text) & "", CN, adOpenStatic, adLockOptimistic
Else
Rs.Open "SELECT * FROM warehouse WHERE did =(SELECT MAX(did)FROM warehouse)", CN, adOpenStatic, adLockOptimistic
Rs.AddNew
End If
Rs("NAME") = Me.txtName.Text
Rs!Contact_person = Me.txtContactPerson.Text
Rs("PHONE") = Me.txtPhone.Text
Rs("cell") = Me.txtCell.Text
Rs("fax") = Me.txtFax.Text
Rs("Email") = Me.txtEmail.Text
Rs("ADDRESS") = Me.txtAddress.Text
Rs("CITY") = Me.dcCity.Text
Rs.Update
cmdReset_Click
Me.txtName.SetFocus
Else
MsgBox "Please Enter Into Empty Fields"
End If
End Sub

Private Sub dctype_Change()
If CC = False Then
Set RS3 = New ADODB.Recordset
If dcType.Text <> "" Then
RS3.Open "SELECT DISTINCT type FROM warehouse WHERE type Like '" & dcType.Text & "%' AND type<>' ' ORDER BY type", CN, adOpenStatic, adLockOptimistic
Else
RS3.Open "SELECT DISTINCT type FROM warehouse ORDER BY type", CN, adOpenStatic, adLockOptimistic
End If
If RS3.RecordCount > 0 Then
Set dcType.RowSource = RS3
dcType.ListField = "type"
End If
End If
End Sub

Private Sub dctype_GotFocus()
dctype_Change
'SendKeys "{F4}"
End Sub

Private Sub dctype_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = vbKeyF5 Then
cmdReset_Click
ElseIf KeyCode = vbKeyEscape Then
Unload Me
End If
End Sub

Private Sub Form_Load()
''''''''''''''''''''''''''''''
Set OBJ = New clsFunction
OBJ.clear Me
'''''''''''''''''''''''''''''
cmdReset_Click
End Sub

Private Sub txtsno_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
SHoW_UP
End If
If (KeyAscii > 47 And KeyAscii < 58) Or KeyAscii = vbKeyBack Or KeyAscii = 13 Then
Else
KeyAscii = 0
MsgBox "Integer Only"
End If
End Sub

Function SHoW_UP()
Set Rs = New ADODB.Recordset
Rs.Open "SELECT * FROM warehouse WHERE did =" & Val(txtSno.Text) & "", CN, adOpenStatic, adLockOptimistic
If Rs.RecordCount > 0 Then
Me.cmdSave.Enabled = False
Me.cmdUpdate.Enabled = True
Me.txtName.Text = IIf(IsNull(Rs("NAME")), "", Rs("NAME"))
Me.txtPhone.Text = IIf(IsNull(Rs("PHONE")), "", Rs("PHONE"))
Me.txtCell.Text = IIf(IsNull(Rs("cell")), "", Rs("cell"))
Me.txtFax.Text = IIf(IsNull(Rs("fax")), "", Rs("fax"))
Me.txtEmail.Text = IIf(IsNull(Rs("email")), "", Rs("email"))
Me.txtAddress.Text = IIf(IsNull(Rs("address")), "", Rs("address"))
'Me.txtReference.Text = IIf(IsNull(Rs("reference")), "", Rs("reference"))
Me.txtContactPerson.Text = IIf(IsNull(Rs("contact_person")), "", Rs("contact_person"))
dcCity.Text = IIf(IsNull(Rs("CITY")), "", Rs("CITY"))
'dcType.Text = IIf(IsNull(Rs("TYPE")), "", Rs("TYPE"))
Else
cmdReset_Click
End If
End Function

Private Sub cmdUpdate_Click()
cmdSave_Click
End Sub
