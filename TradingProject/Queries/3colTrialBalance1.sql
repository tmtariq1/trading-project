-- create function fn_serial(@index numeric)
-- returns numeric
-- as
-- begin
-- set @index=@index+1
-- return (@index)
-- end



declare @dt1 smalldatetime,
 	@dt2 smalldatetime,
	@ind numeric

set @dt1 = '01-feb-2007'
set @dt2 = '01-jan-2008'
set @ind=0
set @ind=sr_traders2.dbo.fn_serial(@ind)

(select @ind=sr_traders2.dbo.fn_serial(@ind),p.pid,p.name, (select isnull(sum(l.debit),0)- isnull(sum(l.credit),0) from pledger l where date < @dt1 and l.pid=p.pid) as "Opening", isnull(sum(l.debit),0)- isnull(sum(l.credit),0) as 'BetweenDates', (select isnull(sum(l.debit),0)- isnull(sum(l.credit),0) from pledger l where date <= @dt2 and l.pid=p.pid) as "Clossing"  from pledger l,party p where l.pid=p.pid and date between @dt1 and @dt2 group by p.pid,p.name having isnull(sum(l.debit),0)- isnull(sum(l.credit),0)<>0) 
