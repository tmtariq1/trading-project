VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "MSFLXGRD.OCX"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{D750EAB9-EB7B-450B-B9B6-0A0A17E13DCA}#1.0#0"; "XP.ocx"
Begin VB.Form frmSAL 
   BackColor       =   &H00C0C0C0&
   Caption         =   "Sale Voucher"
   ClientHeight    =   8235
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   11955
   DrawMode        =   12  'Nop
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "frmSaleVoucher.frx":0000
   ScaleHeight     =   8235
   ScaleWidth      =   11955
   StartUpPosition =   2  'CenterScreen
   Begin VB.TextBox Txt1 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   3720
      MaxLength       =   60
      TabIndex        =   44
      Top             =   3600
      Width           =   1215
   End
   Begin VB.CheckBox chkPrintOption 
      Caption         =   "Direct Print"
      Height          =   375
      Left            =   8745
      Style           =   1  'Graphical
      TabIndex        =   42
      Top             =   7320
      Value           =   1  'Checked
      Width           =   1095
   End
   Begin VB.TextBox txtDLES 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00FFC0C0&
      BorderStyle     =   0  'None
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   9240
      Locked          =   -1  'True
      MaxLength       =   60
      TabIndex        =   37
      Top             =   6600
      Visible         =   0   'False
      Width           =   2138
   End
   Begin VB.TextBox txtD 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   7200
      MaxLength       =   60
      TabIndex        =   12
      Top             =   6120
      Width           =   1455
   End
   Begin VB.TextBox T4 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   3000
      MaxLength       =   60
      TabIndex        =   9
      Top             =   6120
      Width           =   615
   End
   Begin VB.TextBox TNA 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   7200
      MaxLength       =   60
      TabIndex        =   34
      Top             =   6840
      Width           =   1455
   End
   Begin VB.TextBox TB1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   7200
      MaxLength       =   60
      TabIndex        =   13
      Top             =   6480
      Width           =   1455
   End
   Begin VB.TextBox TC2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   3720
      MaxLength       =   60
      TabIndex        =   31
      Top             =   6480
      Width           =   975
   End
   Begin VB.TextBox TC3 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   3720
      MaxLength       =   60
      TabIndex        =   30
      Top             =   6840
      Width           =   975
   End
   Begin VB.TextBox T5 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   3000
      MaxLength       =   60
      TabIndex        =   10
      Top             =   6480
      Width           =   615
   End
   Begin VB.TextBox TC1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   3720
      MaxLength       =   60
      TabIndex        =   27
      Top             =   6120
      Width           =   975
   End
   Begin VB.TextBox T6 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   315
      Left            =   3000
      MaxLength       =   60
      TabIndex        =   11
      Top             =   6840
      Width           =   615
   End
   Begin VB.Frame FR1 
      Appearance      =   0  'Flat
      BackColor       =   &H00808080&
      BorderStyle     =   0  'None
      Caption         =   "Frame1"
      ForeColor       =   &H80000008&
      Height          =   255
      Left            =   8280
      TabIndex        =   25
      Top             =   240
      Width           =   1695
      Begin VB.OptionButton Option4 
         BackColor       =   &H00808080&
         Caption         =   "Cash"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   178
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   225
         Left            =   840
         TabIndex        =   6
         Top             =   0
         Width           =   735
      End
      Begin VB.OptionButton Option3 
         BackColor       =   &H00808080&
         Caption         =   "Credit"
         BeginProperty Font 
            Name            =   "Arial"
            Size            =   8.25
            Charset         =   178
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         ForeColor       =   &H00FFFFFF&
         Height          =   225
         Left            =   0
         TabIndex        =   5
         Top             =   0
         Value           =   -1  'True
         Width           =   855
      End
   End
   Begin VB.TextBox T3 
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00FF00FF&
      Height          =   375
      Left            =   1200
      MaxLength       =   60
      TabIndex        =   4
      Top             =   1800
      Width           =   10575
   End
   Begin VB.TextBox T1 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   375
      Left            =   1200
      MaxLength       =   60
      TabIndex        =   0
      Top             =   120
      Width           =   1095
   End
   Begin VB.TextBox T2 
      Alignment       =   2  'Center
      Appearance      =   0  'Flat
      BackColor       =   &H00C0C0FF&
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000000&
      Height          =   375
      Left            =   8280
      MaxLength       =   60
      TabIndex        =   8
      Top             =   1080
      Width           =   1455
   End
   Begin MSComCtl2.DTPicker DT1 
      Height          =   375
      Left            =   3960
      TabIndex        =   1
      Top             =   120
      Width           =   1935
      _ExtentX        =   3413
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   12
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   12648447
      CalendarTitleBackColor=   12640511
      CustomFormat    =   "dd-MMM-yy"
      Format          =   91291649
      CurrentDate     =   37916
   End
   Begin XPLayout.XPButton cmdSave 
      Height          =   375
      Left            =   3435
      TabIndex        =   15
      Top             =   7320
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Save"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "frmSaleVoucher.frx":2A2AA
      PICN            =   "frmSaleVoucher.frx":2A2C6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin XPLayout.XPButton cmdDelete 
      Height          =   375
      Left            =   4755
      TabIndex        =   16
      Top             =   7320
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Delete"
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "frmSaleVoucher.frx":2A862
      PICN            =   "frmSaleVoucher.frx":2A87E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin XPLayout.XPButton cmdPrint 
      Height          =   375
      Left            =   7395
      TabIndex        =   18
      Top             =   7320
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Print"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "frmSaleVoucher.frx":2AE1A
      PICN            =   "frmSaleVoucher.frx":2AE36
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin XPLayout.XPButton cmdUpdate 
      Height          =   375
      Left            =   6075
      TabIndex        =   17
      Top             =   7320
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Update"
      ENAB            =   0   'False
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "frmSaleVoucher.frx":2B3D2
      PICN            =   "frmSaleVoucher.frx":2B3EE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin XPLayout.XPButton cmdReset 
      Height          =   375
      Left            =   2115
      TabIndex        =   14
      Top             =   7320
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Reset"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "frmSaleVoucher.frx":2B98A
      PICN            =   "frmSaleVoucher.frx":2B9A6
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSDataListLib.DataCombo DC1 
      Height          =   330
      Left            =   1200
      TabIndex        =   2
      Top             =   615
      Width           =   4695
      _ExtentX        =   8281
      _ExtentY        =   582
      _Version        =   393216
      Appearance      =   0
      BackColor       =   12632319
      ForeColor       =   8388608
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSDataListLib.DataCombo DC2 
      Height          =   330
      Left            =   8280
      TabIndex        =   7
      Top             =   600
      Width           =   3495
      _ExtentX        =   6165
      _ExtentY        =   582
      _Version        =   393216
      Appearance      =   0
      BackColor       =   12632319
      ForeColor       =   8388608
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSDataListLib.DataCombo DCS 
      Height          =   330
      Left            =   1200
      TabIndex        =   3
      Top             =   1080
      Width           =   4695
      _ExtentX        =   8281
      _ExtentY        =   582
      _Version        =   393216
      Appearance      =   0
      BackColor       =   12632319
      ForeColor       =   8388608
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin XPLayout.XPButton XPButton1 
      Height          =   375
      Left            =   10080
      TabIndex        =   39
      Top             =   7080
      Visible         =   0   'False
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&MUPDATE"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   178
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "frmSaleVoucher.frx":2BF40
      PICN            =   "frmSaleVoucher.frx":2BF5C
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin XPLayout.XPButton cmdAddAccount 
      Height          =   375
      Left            =   5880
      TabIndex        =   40
      Top             =   600
      Width           =   1095
      _ExtentX        =   1931
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&New(F3)"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   16576
      FCOLO           =   16576
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "frmSaleVoucher.frx":2C4F6
      PICN            =   "frmSaleVoucher.frx":2C512
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSDataListLib.DataCombo DCPR 
      Height          =   330
      Left            =   1080
      TabIndex        =   45
      Top             =   3960
      Width           =   1935
      _ExtentX        =   3413
      _ExtentY        =   582
      _Version        =   393216
      Appearance      =   0
      BackColor       =   12632319
      ForeColor       =   8388608
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid FG1 
      Height          =   3375
      Left            =   240
      TabIndex        =   46
      Top             =   2280
      Width           =   11535
      _ExtentX        =   20346
      _ExtentY        =   5953
      _Version        =   393216
      Cols            =   16
      AllowBigSelection=   0   'False
      GridLines       =   3
      Appearance      =   0
   End
   Begin MSFlexGridLib.MSFlexGrid FG2 
      Height          =   255
      Left            =   240
      TabIndex        =   47
      Top             =   5640
      Width           =   11535
      _ExtentX        =   20346
      _ExtentY        =   450
      _Version        =   393216
      Rows            =   1
      Cols            =   16
      FixedRows       =   0
      AllowBigSelection=   0   'False
      GridLines       =   3
      Appearance      =   0
   End
   Begin VB.Label lblPartyBalance 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   12
         Charset         =   0
         Weight          =   900
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   345
      Left            =   6000
      TabIndex        =   43
      Top             =   1440
      Width           =   75
   End
   Begin VB.Label Label2 
      BackColor       =   &H80000001&
      BackStyle       =   0  'Transparent
      Caption         =   "F2 = New Store/Warehouse, F3 = New Account, F5 = Reset/Refresh,  F6 = Remove Row, F7 = New Product (Admin Only)"
      Height          =   255
      Left            =   120
      TabIndex        =   41
      Top             =   7920
      Width           =   11655
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Total Discount && Less"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   240
      Index           =   18
      Left            =   9240
      TabIndex        =   38
      Top             =   6360
      Visible         =   0   'False
      Width           =   2025
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Discount"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   240
      Index           =   17
      Left            =   6360
      TabIndex        =   36
      Top             =   6120
      Width           =   765
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Store"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   225
      Index           =   14
      Left            =   720
      TabIndex        =   35
      Top             =   1200
      Width           =   435
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Net Amount"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   240
      Index           =   16
      Left            =   6090
      TabIndex        =   33
      Top             =   6840
      Width           =   1035
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Bilty Expense"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   240
      Index           =   12
      Left            =   5910
      TabIndex        =   32
      Top             =   6480
      Width           =   1215
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Less SMS %"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   240
      Index           =   11
      Left            =   1770
      TabIndex        =   29
      Top             =   6840
      Width           =   1155
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Extra Commission%"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   240
      Index           =   10
      Left            =   1140
      TabIndex        =   28
      Top             =   6480
      Width           =   1785
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Commission%"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   240
      Index           =   6
      Left            =   1665
      TabIndex        =   26
      Top             =   6120
      Width           =   1260
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "SupplyMan"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   225
      Index           =   5
      Left            =   7260
      TabIndex        =   24
      Top             =   720
      Width           =   900
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Party"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   225
      Index           =   3
      Left            =   750
      TabIndex        =   23
      Top             =   720
      Width           =   405
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Description"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   225
      Index           =   1
      Left            =   210
      TabIndex        =   22
      Top             =   1920
      Width           =   945
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Doc #"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   225
      Index           =   2
      Left            =   675
      TabIndex        =   21
      Top             =   240
      Width           =   480
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   225
      Index           =   0
      Left            =   3480
      TabIndex        =   20
      Top             =   240
      Width           =   390
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Invoice #"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   225
      Index           =   4
      Left            =   7440
      TabIndex        =   19
      Top             =   1200
      Width           =   720
   End
End
Attribute VB_Name = "frmSAL"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'On Error Resume Next

Private Sub chkPrintOption_Click()
If chkPrintOption Then
chkPrintOption.Caption = "Direct Print"
Else
chkPrintOption.Caption = "Preview"
End If
End Sub

Private Sub DC1_Change()
If CC = False Then
Set RSP = New ADODB.Recordset
If DC1.Text <> "" Then
RSP.Open "SELECT NAME FROM PARTY WHERE type='sale' and NAME Like '" & UCase(DC1.Text) & "%' ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
Else
RSP.Open "SELECT NAME FROM PARTY where type='sale' ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
End If
Set DC1.RowSource = RSP
DC1.ListField = "NAME"
End If
'Call Get_PartyBalance
End Sub

Private Sub DC1_GotFocus()
DC1_Change
SendKeys "{F4}"
End Sub

Function FG1_TOT()
c = 0
FG2.clear
For i = 1 To FG1.Rows - 1
FG1.TextMatrix(i, 0) = i
FG2.TextMatrix(0, 3) = Val(FG2.TextMatrix(0, 3)) + Val(FG1.TextMatrix(i, 3))
FG2.TextMatrix(0, 4) = Val(FG2.TextMatrix(0, 4)) + Val(FG1.TextMatrix(i, 4))
FG2.TextMatrix(0, 5) = Val(FG2.TextMatrix(0, 5)) + Val(FG1.TextMatrix(i, 5))
FG2.TextMatrix(0, 7) = Val(FG2.TextMatrix(0, 7)) + Val(FG1.TextMatrix(i, 7))
FG2.TextMatrix(0, 8) = Val(FG2.TextMatrix(0, 8)) + Val(FG1.TextMatrix(i, 8))
FG2.TextMatrix(0, 10) = Val(FG2.TextMatrix(0, 10)) + Val(FG1.TextMatrix(i, 10))
FG2.TextMatrix(0, 11) = Val(FG2.TextMatrix(0, 11)) + Val(FG1.TextMatrix(i, 11))
FG2.TextMatrix(0, 15) = Val(FG2.TextMatrix(0, 15)) + Val(FG1.TextMatrix(i, 15))
Next i
End Function

Private Sub DC1_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DC1_Validate(Cancel As Boolean)
    If DC1.Text <> "" Then
    Set Rs = New ADODB.Recordset
    Rs.Open "SELECT * FROM PARTY WHERE NAME='" & DC1.Text & "'", CN, adOpenStatic, adLockOptimistic
    If Rs.RecordCount > 0 Then
    Set T4.DataSource = Rs
    T4.DataField = "COMM"
    Set T5.DataSource = Rs
    T5.DataField = "ECOMM"
    Set T6.DataSource = Rs
    T6.DataField = "SMS"
    Call Get_Inv
    Cancel = False
    Else
    MsgBox "Select a valid party please"
    Cancel = True
    End If
    End If
End Sub

Private Sub DC2_Validate(Cancel As Boolean)
If DC2.Text <> "" Then
Set Rs = New ADODB.Recordset
Rs.Open "SELECT * FROM PARTY WHERE NAME='" & DC2.Text & "'", CN, adOpenStatic, adLockOptimistic
If Rs.RecordCount > 0 Then
Cancel = False
Else
MsgBox "Select a valid party please"
Cancel = True
End If
End If
End Sub

Private Sub DCPR_Change()
If CC = False Then
Set RSP = New ADODB.Recordset
If DCPR.Text <> "" Then
RSP.Open "SELECT NAME FROM PRODUCT WHERE NAME Like '" & DCPR.Text & "%' ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
Else
RSP.Open "SELECT NAME FROM PRODUCT ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
End If
Set DCPR.RowSource = RSP
DCPR.ListField = "NAME"
End If
FG1.TextMatrix(FG1.Row, FG1.CoL) = DCPR.Text
End Sub

Private Sub DCPR_GotFocus()
DCPR_Change
SendKeys "{F4}"
End Sub

Private Sub DCPR_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DCPR_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
DCPR_Validate False
If FG1.CoL = 6 Then
FG1.Row = FG1.Row + 1
FG1.CoL = 2
Else
FG1.CoL = FG1.CoL + 1
End If
'SHoW_CoNT Me
End If
End Sub

Private Sub DCPR_Validate(Cancel As Boolean)
If DCPR.Text <> "" Then
FG1.TextMatrix(FG1.Row, 5) = ""
FG1.TextMatrix(FG1.Row, 7) = ""
Set Rs = New ADODB.Recordset
Rs.Open "SELECT * FROM PRODUCT WHERE NAME='" & DCPR.Text & "'", CN, adOpenStatic, adLockOptimistic
If Rs.RecordCount > 0 Then
Check_Stock Rs("prid")
FG1.TextMatrix(FG1.Row, 1) = IIf(IsNull(Rs("PRID")), "", Rs("PRID"))
FG1.TextMatrix(FG1.Row, 6) = IIf(IsNull(Rs("SRATE")), "", Rs("SRATE"))
FG1.TextMatrix(FG1.Row, 9) = 16
FG1.TextMatrix(FG1.Row, 12) = IIf(IsNull(Rs("SQTY")), "", Rs("SQTY"))
FG1.TextMatrix(FG1.Row, 13) = IIf(IsNull(Rs("QTY")), "", Rs("QTY"))
FG1.TextMatrix(FG1.Row, 14) = IIf(IsNull(Rs("PPC")), "", Rs("PPC"))
Cancel = False
Else
MsgBox "Select a valid Product please"
Cancel = True
End If
End If
End Sub

Function Get_Inv()
If DC1.Text <> "" And ID_NaME("PARTY", "PID", "NAME", DC1.Text) <> "" Then
'''//// Get Invoice Number
Set RSI = New ADODB.Recordset
RSI.Open "SELECT MAX(INVOICEO) FROM STOCK_main WHERE PID=" & ID_NaME("PARTY", "PID", "NAME", DC1.Text) & " AND ETYPE='SALE'", CN, adOpenStatic, adLockOptimistic
If RSI.RecordCount > 0 Then
T2.Text = IIf(IsNull(RSI(0)), 1, RSI(0) + 1)
End If
End If
End Function

Private Sub Get_PartyBalance()
Dim partyId As String
partyId = ID_NaME("PARTY", "PID", "NAME", DC1.Text)
If DC1.Text <> "" And partyId <> "" Then
'''Get Previouse Balance
Set RSI = New ADODB.Recordset
If cmdSave.Enabled Then
RSI.Open "SELECT sum(Debit),sum(credit),sum(Debit)-sum(credit) pbal FROM pledger WHERE PID=" & partyId, CN, adOpenStatic, adLockOptimistic
Else
RSI.Open "SELECT sum(Debit),sum(credit),sum(Debit)-sum(credit) pbal FROM pledger WHERE PID=" & partyId & " and DCNO <>" & T1.Text, CN, adOpenStatic, adLockOptimistic
End If
If RSI.RecordCount > 0 Then
Dim preBal, currentBal As Double
preBal = IIf(IsNull(RSI(0)), 0, RSI(0)) - IIf(IsNull(RSI(1)), 0, RSI(1))
currentBal = IIf(Option3, preBal + Val(TNA.Text), preBal)
lblPartyBalance.Caption = "Previous Balance: " & preBal & IIf(preBal > 0, " Dr", " Cr") & ", Current Balance: " & currentBal & IIf(currentBal > 0, " Dr", " Cr")
End If
End If
End Sub

Private Sub DC2_Change()
If CC = False Then
Set RSP = New ADODB.Recordset
If DC2.Text <> "" Then
RSP.Open "SELECT NAME FROM PARTY WHERE NAME Like '" & UCase(DC2.Text) & "%' AND TYPE='SALESMAN'  ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
Else
RSP.Open "SELECT NAME FROM PARTY WHERE TYPE='SALESMAN' ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
End If
Set DC2.RowSource = RSP
DC2.ListField = "NAME"
End If
End Sub

Private Sub DC2_GotFocus()
DC2_Change
SendKeys "{F4}"
End Sub

Private Sub DC2_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DCS_Change()
If CC = False Then
Set RSP = New ADODB.Recordset
If DCS.Text <> "" Then
RSP.Open "SELECT NAME FROM warehouse WHERE NAME Like '" & DCS.Text & "%' ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
Else
RSP.Open "SELECT NAME FROM warehouse ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
End If
Set DCS.RowSource = RSP
DCS.ListField = "NAME"
End If
End Sub

Private Sub DCS_GotFocus()
DCS_Change
SendKeys "{F4}"
End Sub

Private Sub DCS_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DCS_Validate(Cancel As Boolean)
a = ID_NaME("warehouse", "dId", "name", Me.DCS.Text)
If a <> 0 Then Cancel = False Else Cancel = True
End Sub

Private Sub FG1_Click()
SHoW_CoNT Me
End Sub


Private Sub FG1_EnterCell()
SHoW_CoNT Me
End Sub

Private Sub FG1_GotFocus()
SHoW_CoNT Me
'MsgBox FG1.Row & FG1.Col
End Sub

Private Sub Form_KeyDown(KeyCode As Integer, Shift As Integer)
If v_Check_Constraints = True Then
cmdDelete.Visible = False
cmdUpdate.Visible = False
Else
cmdDelete.Visible = True
cmdUpdate.Visible = True
End If

If KeyCode = vbKeyF2 Then
WareHouse.Show vbModal
ElseIf KeyCode = vbKeyF3 Then
frmNewAccount.Show vbModal
ElseIf KeyCode = vbKeyF7 And Not v_Check_Constraints Then
frmNewProduct.Show vbModal
ElseIf KeyCode = vbKeyF5 Then
Call cmdReset_Click
ElseIf KeyCode = vbKeyEscape Then
Unload Me
ElseIf KeyCode = vbKeyF6 Then
Y = MsgBox("Are you sure to delete ?", vbYesNo)
If Y = vbYes Then
FG1.RemoveItem (FG1.Row)
Else
End If
End If
End Sub

Private Sub Form_Load()
''''''''''''''''''' theme
Set OBJ = New clsFunction
OBJ.clear Me
'''''''''''''''''''''''''''''

T1.Text = MaX_DcN("SALE", "PLEDGER")
DT1.Value = Date
CL_FG1 Me
End Sub

Private Sub cmdPrint_KeyPress(KeyAscii As Integer)
If KeyAscii = 9 Then
T1.SetFocus
End If
End Sub

Private Sub Option3_Click()
'Call Get_PartyBalance
End Sub

Private Sub Option4_Click()
'Call Get_PartyBalance
End Sub

Private Sub T1_GotFocus()
SendKeys "{home}+{end}"
End Sub

Private Sub T1_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode = 38 Then
T1.Text = Val(T1.Text) - 1
T1_KeyPress 13
ElseIf KeyCode = 40 Then
T1.Text = Val(T1.Text) + 1
T1_KeyPress 13
End If
End Sub

Private Sub T1_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
'If Chk_Tid("STOCK", "SALE", T1.Text) = False Then
'MsgBox "Change Login"
'Exit Sub
'End If

c = Val(T1.Text)
cmdReset_Click
T1.Text = c

    Set RsM = New ADODB.Recordset
    RsM.Open "SELECT * FROM STOCK_main WHERE DCNO=" & Val(T1.Text) & " AND ETYPE='SALE'", CN, adOpenStatic, adLockOptimistic
    If RsM.RecordCount > 0 Then
    cmdSave.Enabled = False
    cmdDelete.Enabled = True
    cmdUpdate.Enabled = True
    If Not IsNull(RsM!SID) Then DC2.Text = NaME_iD("PARTY", "PID", RsM!SID)
    DC1.Text = NaME_iD("PARTY", "PID", RsM!PID)
    DCS.Text = NaME_iD("WAREHOUSE", "DID", RsM!did)
    DT1.Value = RsM("DATE")
    T2.Text = IIf(IsNull(RsM("INVOICE")), "", RsM("INVOICE"))
    T3.Text = IIf(IsNull(RsM("DESCRIPTION")), "", RsM("DESCRIPTION"))
    r = 1
    Set Rs = New ADODB.Recordset
    Rs.Open "SELECT * FROM STOCK WHERE DCNO=" & Val(T1.Text) & " AND ETYPE='SALE'", CN, adOpenStatic, adLockOptimistic
    If Rs.RecordCount > 0 Then
        Do Until Rs.EOF
        Set RSPR = New ADODB.Recordset
        RSPR.Open "SELECT * FROM PRODUCT WHERE PRID=" & Rs("PRID") & "", CN, adOpenStatic, adLockOptimistic
        FG1.TextMatrix(r, 1) = Rs("PRID")
        FG1.TextMatrix(r, 2) = NaME_iD("PRODUCT", "PRID", Rs!PRID)
        FG1.TextMatrix(r, 3) = Abs(Rs("QTYC"))
        FG1.TextMatrix(r, 4) = Abs(Rs("QTYP"))
        FG1.TextMatrix(r, 5) = Abs(Rs("SQTYP"))
        FG1.TextMatrix(r, 6) = Rs("RATE")
        FG1.TextMatrix(r, 7) = Rs("LESS")
        FG1.TextMatrix(r, 8) = IIf(IsNull(Rs!AMOUNT), "", Rs!AMOUNT)
        FG1.TextMatrix(r, 9) = IIf(IsNull(Rs!Comm), "", Rs!Comm)
        FG1.TextMatrix(r, 10) = IIf(IsNull(Rs!CAMOUNT), "", Rs!CAMOUNT)
        FG1.TextMatrix(r, 11) = IIf(IsNull(Rs!NAMOUNT), "", Rs!NAMOUNT)
        FG1.TextMatrix(r, 12) = RSPR!SQTY
        FG1.TextMatrix(r, 13) = RSPR!Qty
        FG1.TextMatrix(r, 14) = RSPR!PPC
        FG1.TextMatrix(r, 15) = IIf(IsNull(Rs!SCAMOUNT), "", Rs!SCAMOUNT)
        Rs.MoveNext
        r = r + 1
        FG1.Rows = r + 1
        Loop
        '''''''''''''''''''''''''''''''''''''
        FG1_TOT
    '''''''''''payment term
    If RsM!payment_term = "DEBIT" Then Option3.Value = True Else Option4.Value = True
    '''''''''''''''''''''''''''''COMMISSTION DEBIT
    T4.Text = RsM!Comm
    '''''''''''''''''''''''''''''EXTRA COMMISSTION DEBIT
    T5.Text = RsM!extComm
    '''''''''''''''''''''''''''''SMS DEBIT
    T6.Text = RsM!sms
    '''''''''''''''''''''''''''''DISCOUNT DEBIT
    txtD.Text = RsM!Discount
    '''''''''''''''''''''''''''''''FREIGHT DEBIT
    TB1.Text = RsM!Builty_expense
    '''''''''''''''''''''''''''''''''''
    CalculateTotal  '''''''''''''''''reset values
    'Else
    'Call cmdReset_Click
    End If
    End If
End If
End Sub

Private Sub T3_Validate(Cancel As Boolean)
FG1.CoL = 2
FG1.Row = 1
End Sub

Private Sub CalculateTotal()
TA = Round(Val(FG2.TextMatrix(0, 11)) - Val(FG2.TextMatrix(0, 7)), 2)
TC1.Text = Round(Val(FG2.TextMatrix(0, 8)) * Val(T4.Text) / 100, 2)
TC2.Text = Round((Val(FG2.TextMatrix(0, 8)) - Val(TC1.Text)) * Val(T5.Text) / 100, 2)
TC3.Text = Round((Val(FG2.TextMatrix(0, 8)) - Val(TC1.Text) - Val(TC2.Text)) * Val(T6.Text) / 100, 2)
TNA.Text = Round(Val(FG2.TextMatrix(0, 11)) - Val(TB1.Text) - Val(TC1.Text) - Val(TC2.Text) - Val(TC3.Text) - Val(txtD.Text), 2)
txtDLES.Text = Round(Val(FG2.TextMatrix(0, 15)) + Val(FG2.TextMatrix(0, 7)) + Val(TB1.Text) + Val(TC1.Text) + Val(TC2.Text) + Val(TC3.Text) + Val(txtD.Text), 2)
'Call Get_PartyBalance
End Sub

Private Sub T4_Change()
CalculateTotal
End Sub

Private Sub T5_Change()
CalculateTotal
End Sub

Private Sub T6_Change()
CalculateTotal
End Sub

Private Sub TB1_Change()
CalculateTotal
End Sub

Private Sub Txt1_Change()
FG1.TextMatrix(FG1.Row, FG1.CoL) = Txt1.Text
If Val(FG1.TextMatrix(FG1.Row, 4)) <> 0 Then
Qty1 = (Val(FG1.TextMatrix(FG1.Row, 4)) / Val(FG1.TextMatrix(FG1.Row, 14))) + Val(FG1.TextMatrix(FG1.Row, 3))
Else
Qty1 = Val(FG1.TextMatrix(FG1.Row, 3))
End If
FG1.TextMatrix(FG1.Row, 8) = Round(Qty1 * Val(FG1.TextMatrix(FG1.Row, 6)), 2)
FG1.TextMatrix(FG1.Row, 15) = Round(Val(FG1.TextMatrix(FG1.Row, 5)) * Val(FG1.TextMatrix(FG1.Row, 14)) * Val(FG1.TextMatrix(FG1.Row, 6)), 2)  ''''Sc Amount
If Val(FG1.TextMatrix(FG1.Row, 8)) * Val(FG1.TextMatrix(FG1.Row, 9)) > 0 Then
FG1.TextMatrix(FG1.Row, 10) = Round(Val(FG1.TextMatrix(FG1.Row, 8)) * Val(FG1.TextMatrix(FG1.Row, 9)) / 100, 2)
Else
FG1.TextMatrix(FG1.Row, 10) = ""
End If
GeT_LESS
FG1.TextMatrix(FG1.Row, 11) = Round(Val(FG1.TextMatrix(FG1.Row, 8)) + Val(FG1.TextMatrix(FG1.Row, 10)) - Val(FG1.TextMatrix(FG1.Row, 7)), 2)
FG1_TOT
CalculateTotal
End Sub

Function GeT_LESS()
If FG1.CoL = 3 Or FG1.CoL = 4 Then
If Val(Qty1) <> 0 And Val(FG1.TextMatrix(FG1.Row, 12)) <> 0 Then
FG1.TextMatrix(FG1.Row, 5) = Fix((Val(Qty1) * Val(FG1.TextMatrix(FG1.Row, 14)) / Val(FG1.TextMatrix(FG1.Row, 12))) * Val(FG1.TextMatrix(FG1.Row, 13)))
FG1.TextMatrix(FG1.Row, 7) = Round(((Val(Qty1) * Val(FG1.TextMatrix(FG1.Row, 14))) Mod Val(FG1.TextMatrix(FG1.Row, 12))) * Val(FG1.TextMatrix(FG1.Row, 6)) * 1 / (Val(FG1.TextMatrix(FG1.Row, 12)) + 1), 0)
End If
If Val(Qty1) = 0 Then
FG1.TextMatrix(FG1.Row, 5) = ""
FG1.TextMatrix(FG1.Row, 7) = ""
End If
End If
End Function

Private Sub Txt1_KeyDown(KeyCode As Integer, Shift As Integer)
Call txt_KeyDown(KeyCode, Shift, Me)
End Sub

Private Sub Txt1_KeyPress(KeyAscii As Integer)
If KeyAscii = 13 Then
If FG1.Rows = FG1.Row + 1 Then
FG1.Rows = FG1.Rows + 1
End If
If FG1.CoL = 9 Then
FG1.Row = FG1.Row + 1
FG1.CoL = 2
ElseIf FG1.CoL = 7 Then
FG1.CoL = FG1.CoL + 2
Else
FG1.CoL = FG1.CoL + 1
End If
'SHoW_CoNT Me
End If
End Sub

Private Sub cmdSave_Click()
If T1.Text And T2.Text <> "" And DC2.Text <> "" And DC1.Text <> "" And DCS.Text <> "" Then
'''''''''''''stock main
Set RsM = New ADODB.Recordset
RsM.Open "SELECT * FROM STOCK_Main WHERE STMID=(SELECT MAX(STmID) FROM STOCK_main)", CN, adOpenStatic, adLockOptimistic
RsM.AddNew
Dim v_stmid As Double
v_stmid = MaX_ID("STMID", "STOCK_main")
RsM("STMID") = v_stmid
RsM("SID") = ID_NaME("PARTY", "PID", "NAME", DC2.Text)
'RsM("TID") = V_Tid
RsM("PID") = ID_NaME("PARTY", "PID", "NAME", DC1.Text)
RsM("DID") = ID_NaME("WAREHOUSE", "DID", "NAME", DCS.Text)
RsM("DCNO") = Val(T1.Text)
RsM("ETYPE") = "SALE"
RsM("DATE") = DT1.Value
RsM("INVOICE") = T2.Text
RsM("net_less") = Val(Me.txtDLES.Text)
RsM("INVOICEO") = Val(T2.Text)
RsM("DESCRIPTION") = T3.Text
If Option3.Value = True Then RsM!payment_term = "DEBIT" Else RsM!payment_term = "Cash"
RsM!Comm = Val(Me.T4.Text)
RsM!COMM_amount = Val(Me.TC1.Text)
RsM!extComm = Val(Me.T5.Text)
RsM!extCOMM_amount = Val(Me.TC2.Text)
RsM!sms = Val(Me.T6.Text)
RsM!sms_amount = Val(Me.TC3.Text)

RsM!Discount = Val(Me.txtD.Text)
RsM!Builty_expense = Val(Me.TB1.Text)
RsM!net_amount = Val(Me.TNA.Text)
RsM.Update

''''''''''''''''STOCK''''''''''''''''''''''
r = 1
Set Rs = New ADODB.Recordset
Rs.Open "SELECT * FROM STOCK WHERE STID=(SELECT MAX(STID) FROM STOCK)", CN, adOpenStatic, adLockOptimistic
Do Until FG1.TextMatrix(r, 1) = ""
Rs.AddNew
Rs("STMID") = v_stmid
Rs("DCNO") = Val(T1.Text)
Rs("ETYPE") = "SALE"
Rs("STID") = MaX_ID("STID", "STOCK")
Rs("PRID") = Val(FG1.TextMatrix(r, 1))
Rs("QTYC") = -Abs(Val(FG1.TextMatrix(r, 3)))
Rs("QTYP") = -Abs(Val(FG1.TextMatrix(r, 4)))
Rs("TQTYP") = -Abs((Val(FG1.TextMatrix(r, 14)) * Val(FG1.TextMatrix(r, 3)) + Val(FG1.TextMatrix(r, 4)) + Val(FG1.TextMatrix(r, 5))))
Rs("SQTYP") = -Abs(Val(FG1.TextMatrix(r, 5)))
Rs("RATE") = Val(FG1.TextMatrix(r, 6))
Rs("LESS") = Val(FG1.TextMatrix(r, 7))
Rs("SLESS") = Round(Val(FG1.TextMatrix(r, 7)) + (Val(FG1.TextMatrix(r, 6)) * Val(FG1.TextMatrix(r, 5))), 2)
Rs("AMOUNT") = Val(FG1.TextMatrix(r, 8))
Rs("COMM") = Val(FG1.TextMatrix(r, 9))
Rs("CAMOUNT") = Val(FG1.TextMatrix(r, 10))
Rs("NAMOUNT") = Val(FG1.TextMatrix(r, 11))
Rs("SCAMOUNT") = Val(FG1.TextMatrix(r, 15))
Rs.Update
r = r + 1
Loop
'''''''''''''''PLEDGER---SALE CREDIT'''''''''''''
Set RsL = New ADODB.Recordset
RsL.Open "SELECT * FROM PLEDGER WHERE PLEDID=(SELECT MAX(PLEDID) FROM PLEDGER)", CN, adOpenStatic, adLockOptimistic
RsL.AddNew
RsL("PLEDID") = MaX_ID("PLEDID", "PLEDGER")
RsL("PID") = ID_NaME("PARTY", "PID", "NAME", "SALE")
RsL("SID") = ID_NaME("PARTY", "PID", "NAME", DC2.Text)
'RsL("TID") = V_Tid
RsL("DCNO") = Val(T1.Text)
RsL("ETYPE") = "SALE"
RsL("DATE") = DT1.Value
RsL("INVOICE") = T2.Text
RsL("INVOICEO") = Val(T2.Text)
RsL("DESCRIPTION2") = T3.Text
RsL("DESCRIPTION") = "SALE " & T1.Text & " (" & DC1.Text & ")" '{" & T3.Text & "}"
RsL("CREDIT") = Round(Val(FG2.TextMatrix(0, 8)) + Val(FG2.TextMatrix(0, 15)), 2)
RsL.Update
RsL.Close
'''''''''''''''PLEDGER---scheme DEBIT'''''''''''''
If Round(Val(FG2.TextMatrix(0, 15)), 0) <> 0 Then
Set RsL = New ADODB.Recordset
RsL.Open "SELECT * FROM PLEDGER WHERE PLEDID=(SELECT MAX(PLEDID) FROM PLEDGER)", CN, adOpenStatic, adLockOptimistic
RsL.AddNew
RsL("PLEDID") = MaX_ID("PLEDID", "PLEDGER")
RsL("PID") = ID_NaME("PARTY", "PID", "NAME", "Scheme")
If DC2.Text <> "" Then
RsL("SID") = ID_NaME("PARTY", "PID", "NAME", DC2.Text)
End If
'RsL("TID") = V_Tid
RsL("DCNO") = Val(T1.Text)
RsL("ETYPE") = "SALE"
RsL("DATE") = DT1.Value
RsL("INVOICE") = T2.Text
RsL("INVOICEO") = Val(T2.Text)
RsL("DESCRIPTION2") = T3.Text
RsL("DESCRIPTION") = "SALE " & T1.Text & " (" & DC1.Text & ")" '{" & T3.Text & "}"
RsL("DEBIT") = Round(Val(FG2.TextMatrix(0, 15)), 2)
RsL.Update
RsL.Close
End If
'''''''''''''''LESS'''''''''''''
If Val(FG2.TextMatrix(0, 7)) <> 0 Or Val(FG2.TextMatrix(0, 5)) <> 0 Then
Set RsL = New ADODB.Recordset
RsL.Open "SELECT * FROM PLEDGER WHERE PLEDID=(SELECT MAX(PLEDID) FROM PLEDGER)", CN, adOpenStatic, adLockOptimistic
RsL.AddNew
RsL("PLEDID") = MaX_ID("PLEDID", "PLEDGER")
RsL("PID") = ID_NaME("PARTY", "PID", "NAME", "Scheme Less")
If DC2.Text <> "" Then
RsL("SID") = ID_NaME("PARTY", "PID", "NAME", DC2.Text)
End If
'RsL("TID") = V_Tid
RsL("DCNO") = Val(T1.Text)
RsL("ETYPE") = "SALE"
RsL("DATE") = DT1.Value
RsL("INVOICE") = T2.Text
RsL("INVOICEO") = Val(T2.Text)
RsL("DESCRIPTION2") = T3.Text
RsL("DESCRIPTION") = "SALE " & T1.Text & " (" & DC1.Text & ")"
RsL("DEBIT") = Round(Val(FG2.TextMatrix(0, 7)), 2)
RsL.Update
RsL.Close
End If
'''''''''''''''Comm'''''''''''''
If Val(FG2.TextMatrix(0, 7)) <> 0 Or Val(FG2.TextMatrix(0, 5)) <> 0 Then
Set RsL = New ADODB.Recordset
RsL.Open "SELECT * FROM PLEDGER WHERE PLEDID=(SELECT MAX(PLEDID) FROM PLEDGER)", CN, adOpenStatic, adLockOptimistic
RsL.AddNew
RsL("PLEDID") = MaX_ID("PLEDID", "PLEDGER")
RsL("PID") = ID_NaME("PARTY", "PID", "NAME", "Scheme Commission")
If DC2.Text <> "" Then
RsL("SID") = ID_NaME("PARTY", "PID", "NAME", DC2.Text)
End If
'RsL("TID") = V_Tid
RsL("DCNO") = Val(T1.Text)
RsL("ETYPE") = "SALE"
RsL("DATE") = DT1.Value
RsL("INVOICE") = T2.Text
RsL("INVOICEO") = Val(T2.Text)
RsL("DESCRIPTION2") = T3.Text
RsL("DESCRIPTION") = "SALE " & T1.Text & " (" & DC1.Text & ")"
RsL("DEBIT") = Round(Val(FG2.TextMatrix(0, 10)), 2)
RsL.Update
RsL.Close
End If
'''''''''''''''PLEDGER---COMMISSION'''''''''''''
If Val(TC1.Text) <> 0 Then
Set RsL = New ADODB.Recordset
RsL.Open "SELECT * FROM PLEDGER WHERE PLEDID=(SELECT MAX(PLEDID) FROM PLEDGER)", CN, adOpenStatic, adLockOptimistic
RsL.AddNew
RsL("PLEDID") = MaX_ID("PLEDID", "PLEDGER")
RsL("PID") = ID_NaME("PARTY", "PID", "NAME", "COMMISSION")
If DC2.Text <> "" Then
RsL("SID") = ID_NaME("PARTY", "PID", "NAME", DC2.Text)
End If
'RsL("TID") = V_Tid
RsL("DCNO") = Val(T1.Text)
RsL("ETYPE") = "SALE"
RsL("DATE") = DT1.Value
RsL("INVOICE") = T2.Text
RsL("INVOICEO") = Val(T2.Text)
RsL("DESCRIPTION") = "SALE " & T1.Text & " (" & DC1.Text & ")"
RsL("DEBIT") = Val(TC1.Text)
RsL.Update
RsL.Close
End If
'''''''''''''''PLEDGER---EXTRA COMMISSION'''''''''''''
If Val(TC2.Text) <> 0 Then
Set RsL = New ADODB.Recordset
RsL.Open "SELECT * FROM PLEDGER WHERE PLEDID=(SELECT MAX(PLEDID) FROM PLEDGER)", CN, adOpenStatic, adLockOptimistic
RsL.AddNew
RsL("PLEDID") = MaX_ID("PLEDID", "PLEDGER")
RsL("PID") = ID_NaME("PARTY", "PID", "NAME", "EXTRA COMMISSION")
If DC2.Text <> "" Then
RsL("SID") = ID_NaME("PARTY", "PID", "NAME", DC2.Text)
End If
'RsL("TID") = V_Tid
RsL("DCNO") = Val(T1.Text)
RsL("ETYPE") = "SALE"
RsL("DATE") = DT1.Value
RsL("INVOICE") = T2.Text
RsL("INVOICEO") = Val(T2.Text)
RsL("DESCRIPTION") = "SALE " & T1.Text & " (" & DC1.Text & ")"
RsL("DEBIT") = Val(TC2.Text)
RsL.Update
RsL.Close
End If
'''''''''''''''PLEDGER---SMS'''''''''''''
If Val(TC3.Text) <> 0 Then
Set RsL = New ADODB.Recordset
RsL.Open "SELECT * FROM PLEDGER WHERE PLEDID=(SELECT MAX(PLEDID) FROM PLEDGER)", CN, adOpenStatic, adLockOptimistic
RsL.AddNew
RsL("PLEDID") = MaX_ID("PLEDID", "PLEDGER")
RsL("PID") = ID_NaME("PARTY", "PID", "NAME", "SMS")
If DC2.Text <> "" Then
RsL("SID") = ID_NaME("PARTY", "PID", "NAME", DC2.Text)
End If
'RsL("TID") = V_Tid
RsL("DCNO") = Val(T1.Text)
RsL("ETYPE") = "SALE"
RsL("DATE") = DT1.Value
RsL("INVOICE") = T2.Text
RsL("INVOICEO") = Val(T2.Text)
RsL("DESCRIPTION") = "SALE " & T1.Text & " (" & DC1.Text & ")"
RsL("DEBIT") = Val(TC3.Text)
RsL.Update
RsL.Close
End If
'''''''''''''''DISCOUNT'''''''''''''
If Val(txtD.Text) <> 0 Then
Set RsL = New ADODB.Recordset
RsL.Open "SELECT * FROM PLEDGER WHERE PLEDID=(SELECT MAX(PLEDID) FROM PLEDGER)", CN, adOpenStatic, adLockOptimistic
RsL.AddNew
RsL("PLEDID") = MaX_ID("PLEDID", "PLEDGER")
RsL("PID") = ID_NaME("PARTY", "PID", "NAME", "DISCOUNT")
If DC2.Text <> "" Then
RsL("SID") = ID_NaME("PARTY", "PID", "NAME", DC2.Text)
End If
'RsL("TID") = V_Tid
RsL("DCNO") = Val(T1.Text)
RsL("ETYPE") = "SALE"
RsL("DATE") = DT1.Value
RsL("INVOICE") = T2.Text
RsL("INVOICEO") = Val(T2.Text)
RsL("DESCRIPTION2") = T3.Text
RsL("DESCRIPTION") = "SALE " & T1.Text & " (" & DC1.Text & ")"
RsL("DEBIT") = Val(txtD.Text)
RsL.Update
RsL.Close
End If
'''''''''''''''PLEDGER---FRAIGHT'''''''''''''
If Val(TB1.Text) <> 0 Then
Set RsL = New ADODB.Recordset
RsL.Open "SELECT * FROM PLEDGER WHERE PLEDID=(SELECT MAX(PLEDID) FROM PLEDGER)", CN, adOpenStatic, adLockOptimistic
RsL.AddNew
RsL("PLEDID") = MaX_ID("PLEDID", "PLEDGER")
RsL("PID") = ID_NaME("PARTY", "PID", "NAME", "FREIGHT")
If DC2.Text <> "" Then
RsL("SID") = ID_NaME("PARTY", "PID", "NAME", DC2.Text)
End If
'RsL("TID") = V_Tid
RsL("DCNO") = Val(T1.Text)
RsL("ETYPE") = "SALE"
RsL("DATE") = DT1.Value
RsL("INVOICE") = T2.Text
RsL("INVOICEO") = Val(T2.Text)
RsL("DESCRIPTION") = "SALE " & T1.Text & " (" & DC1.Text & ")"
RsL("DEBIT") = Val(TB1.Text)
RsL.Update
RsL.Close
End If
'''''''''''''''PLEDGER--PARTY DEBIT'''''''''''''
Set RsL = New ADODB.Recordset
RsL.Open "SELECT * FROM PLEDGER WHERE PLEDID=(SELECT MAX(PLEDID) FROM PLEDGER)", CN, adOpenStatic, adLockOptimistic
RsL.AddNew
RsL("PLEDID") = MaX_ID("PLEDID", "PLEDGER")
If DC2.Text <> "" Then
RsL("SID") = ID_NaME("PARTY", "PID", "NAME", DC2.Text)
End If
'RsL("TID") = V_Tid
RsL("PID") = ID_NaME("PARTY", "PID", "NAME", DC1.Text)
RsL("DCNO") = Val(T1.Text)
RsL("ETYPE") = "SALE"
RsL("DATE") = DT1.Value
RsL("INVOICE") = T2.Text
RsL("INVOICEO") = Val(T2.Text)
RsL("DESCRIPTION") = "SALE " & T1.Text & " (" & T3.Text & ")"
RsL("DEBIT") = Val(TNA.Text)
RsL.Update
RsL.Close
''''''''PLEDGER---CASH'''''''''''''
If Option4 Then
Set RsL = New ADODB.Recordset
RsL.Open "SELECT * FROM PLEDGER WHERE PLEDID=(SELECT MAX(PLEDID) FROM PLEDGER)", CN, adOpenStatic, adLockOptimistic
RsL.AddNew
RsL("PLEDID") = MaX_ID("PLEDID", "PLEDGER")
RsL("PID") = ID_NaME("PARTY", "PID", "NAME", "CASH")
If DC2.Text <> "" Then
RsL("SID") = ID_NaME("PARTY", "PID", "NAME", DC2.Text)
End If
'RsL("TID") = V_Tid
RsL("DCNO") = Val(T1.Text)
RsL("ETYPE") = "SALE"
RsL("DATE") = DT1.Value
RsL("INVOICE") = T2.Text
RsL("INVOICEO") = Val(T2.Text)
RsL("DESCRIPTION") = "SALE " & T1.Text & " (" & DC1.Text & ")"
RsL("DEBIT") = Val(TNA.Text)
RsL.Update
RsL.Close
''''''''''''''''''''''''
Set RsL = New ADODB.Recordset
RsL.Open "SELECT * FROM PLEDGER WHERE PLEDID=(SELECT MAX(PLEDID) FROM PLEDGER)", CN, adOpenStatic, adLockOptimistic
RsL.AddNew
RsL("PLEDID") = MaX_ID("PLEDID", "PLEDGER")
If DC2.Text <> "" Then
RsL("SID") = ID_NaME("PARTY", "PID", "NAME", DC2.Text)
End If
'RsL("TID") = V_Tid
RsL("PID") = ID_NaME("PARTY", "PID", "NAME", DC1.Text)
RsL("DCNO") = Val(T1.Text)
RsL("ETYPE") = "SALE"
RsL("DATE") = DT1.Value
RsL("INVOICE") = T2.Text
RsL("INVOICEO") = Val(T2.Text)
RsL("DESCRIPTION") = "SALE " & T1.Text & " (" & T3.Text & ") Cash Receive"
RsL("CREDIT") = Val(TNA.Text)
RsL.Update
RsL.Close
End If
''''''''''''''''''''''''''' PRINT
If MsgBox("Do You Want To Print This Document...?", vbYesNo) = vbYes Then cmdPrint_Click

''''''''''''''''''''''''''
Dim strStock, strSupplyMan As String
strStock = DCS.Text
strSupplyMan = DC2.Text
Call cmdReset_Click
 DCS.Text = strStock
 DC2.Text = strSupplyMan
 DC1.SetFocus
Else
MsgBox "Enter Into Empty Fields"
End If
End Sub

Private Sub cmdDelete_Click()
If MsgBox("Are you sure you want to Delete this document", vbOKCancel) = vbOK Then
    Delete_Data "SALE", Val(T1.Text)
    cmdReset_Click
End If
End Sub

Private Sub cmdPrint_Click()
Dim crystalApp As CRAXDRT.Application
Dim crystalRep As CRAXDRT.Report
Dim myTextObject As CRAXDRT.TextObject
Set crystalApp = New CRAXDRT.Application
Set crystalRep = crystalApp.OpenReport(App.Path & "\Reports\" & "SaleVoucher.rpt") 'Opens the report

'Unload rptSaleVoucher
UpdateReportCredentials crystalRep
With Me
Set myTextObject = crystalRep.Sections("RH").ReportObjects("txtHead")
myTextObject.SetText "SALE Voucher "
Set myTextObject = crystalRep.Sections("RH").ReportObjects("txtCompanyName")
myTextObject.SetText V_Company
Set myTextObject = crystalRep.Sections("RH").ReportObjects("txtCompanyAddress")
myTextObject.SetText V_CompanyAddress
crystalRep.Sections("PH").ReportObjects("txtPartyBalance").SetText Me.lblPartyBalance.Caption
If Val(.T4.Text) = 0 Then crystalRep.Sections("RF").ReportObjects("txtC1").Suppress = True 'Else rptSaleVoucher.txtC1.SetText "Comm@ " & .T4.Text & " Rs: " & .TC1.Text
If Val(.T5.Text) = 0 Then crystalRep.Sections("RF").ReportObjects("txtC2").Suppress = True 'Else rptSaleVoucher.txtC2.SetText "E.Comm@ " & .T5.Text & " Rs: " & .TC2.Text
If Val(.T6.Text) = 0 Then crystalRep.Sections("RF").ReportObjects("txtC3").Suppress = True 'Else rptSaleVoucher.txtC3.SetText "SMS@ " & .T6.Text & " Rs: " & .TC3.Text
If Val(.txtD.Text) = 0 Then crystalRep.Sections("RF").ReportObjects("txtC4").Suppress = True 'Else rptSaleVoucher.txtc4.SetText "Disscount " & .txtD.Text
If Val(.TB1.Text) = 0 Then crystalRep.Sections("RF").ReportObjects("txtC5").Suppress = True 'Else rptSaleVoucher.txtc5.SetText "Builty Expense " & .TB1.Text
End With
crystalRep.RecordSelectionFormula = "{STOCK.ETYPE}='SALE' AND {STOCK.dcno}=" & T1.Text

If chkPrintOption.Value Then
crystalRep.PrintOut
Else
ReportsViewer.CRViewer1.ReportSource = crystalRep
ReportsViewer.CRViewer1.ViewReport
ReportsViewer.Show vbModal
End If
End Sub

Private Sub txtD_Change()
CalculateTotal
End Sub

Private Sub cmdUpdate_Click()
If MsgBox("Are you sure you want to update this document", vbOKCancel) = vbOK Then
    If T1.Text And T2.Text <> "" And DC2.Text <> "" And DC1.Text <> "" And DCS.Text <> "" Then
    Delete_Data "SALE", Val(T1.Text)  ''''''''''''DELETE
    Call cmdSave_Click  ''''''''''''cmdSave
    Call cmdReset_Click  ''''''''''''REFRESH
    End If
End If
End Sub

Private Sub cmdReset_Click()
cmdSave.Enabled = True
cmdDelete.Enabled = False
cmdUpdate.Enabled = False
Set OBJ = New clsFunction
OBJ.clear Me
CL_FG1 Me
T1.Text = MaX_DcN("SALE", "PLEDGER")
T1.SetFocus
End Sub

Private Sub XPButton1_Click()
Set RsM = New ADODB.Recordset
RsM.Open "SELECT DISTINCT DCNO FROM PLEDGER WHERE ETYPE='SALE' ORDER BY DCNO", CN, adOpenStatic, adLockOptimistic
If RsM.RecordCount > 0 Then
Do Until RsM.EOF
T1.Text = RsM!DCNO
T1_KeyPress 13
cmdUpdate_Click
RsM.MoveNext
Loop
End If
End Sub
