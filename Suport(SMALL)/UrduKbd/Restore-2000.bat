@echo on
SET ver=0.1
IF %OS%==Windows_NT Goto WinNT

ECHO Windows 2000 Restoration Routine by: Shehzad Ashiq Ali
:Win9x
CLS
Echo.
Echo Windows 9x or ME Detected.
Echo Sorry you can only install this file under Windows 2000/Xp
Echo Wassallam :(
Goto End

:WinNT
CLS
Echo Restore Keyboard %ver%
Echo.
IF EXIST Backup\shehzad.txt Goto NTDumpOK

:NoNTDUMP
Echo You do not have a Backed Up Keyboard Layout to restore.
GOTO End

:NTDumpOK
Echo Found a backed-Up Keyboard Layout.
Echo Windows NT or 2000 Detected.
Echo Windows is in : %windir%
Echo Restoring Files...

Echo   * kbdfa.dll

IF EXIST Backup\ kbdfa.dll (
  copy Backup\kbdfa.dll %windir%\system32 > nul
  Echo     [ restored ]
) ELSE (
  del %windir%\system\kbdfa.dll > nul
  Echo     [ no backup, therefore killed ]
)

Echo Done!
Echo.
Echo Your Dumped Keyboard Layout has been restored.
Echo Reboot you PC to make the changes active.
Goto End

End:
:End
SET ver=
pause
rem exit
