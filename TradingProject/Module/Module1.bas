Attribute VB_Name = "Module1"
Public CN As New ADODB.Connection
Public Rs As New ADODB.Recordset
Public CONN, Str1, V_DataBase, V_Company, V_Server, V_User, V_password, V_BackupFoler, V_CompanyAddress, V_CGEnable, V_GSTRate, V_PrinterDriver, V_PrinterName, V_PrinterPort As String
Public r, i, c, V_Tid As Long
Public CC, v_Check_Constraints As Boolean
Public TT, Qty1 As Double
Public Report, chkA   As String
Public str As Variant
Public OBJ As clsFunction

Function Company_Setup()
Dim strServer, strDatabase, strCompany, strUser, strPassword, strBackupPath, strCompanyAddress, strCGEnable, strPrinter As String
Dim counter As Integer

Open App.Path & "\" & "config.dat" For Input As #1

'Do Until EOF(1)
Input #1, strServer, strDatabase, strCompany, strUser, strPassword, strBackupPath, strCompanyAddress, strCGEnable, strPrinter
''Print server; Tab(25); database; Tab(25); company; Tab(25); user; Tab(25); password
'Loop
Close #1

V_DataBase = strDatabase
V_Company = strCompany
V_User = strUser
V_password = strPassword
V_Server = strServer
V_BackupFoler = strBackupPath
V_CompanyAddress = strCompanyAddress
strCGWithRate = Split(strCGEnable, "|")
V_CGEnable = strCGWithRate(0)
V_GSTRate = strCGWithRate(1)
strPrinterDetail = Split(strPrinter, "|")
V_PrinterDriver = strPrinterDetail(0)
V_PrinterName = strPrinterDetail(1)
V_PrinterPort = strPrinterDetail(2)

'V_DataBase = "Sr_Traders2"
'V_Company = "S.R.T"
'V_User = "sa"
'v_password = ""
'V_Server = "DS1"

'Call All_Reports_Path
End Function

Function MaX_ID(FD As String, TB As String) As Long
Set PS = New ADODB.Recordset
PS.Open "SELECT MAX(" & FD & ") FROM " & TB & "", CN, adOpenStatic, adLockOptimistic
MaX_ID = IIf(IsNull(PS(0)), 1, PS(0) + 1)
PS.Close
End Function

Function MaX_DcN(E2 As String, TB As String) As Long
Set PS = New ADODB.Recordset
PS.Open "SELECT MAX(DCNO) FROM " & TB & " WHERE ETYPE='" & E2 & "'", CN, adOpenStatic, adLockOptimistic
MaX_DcN = IIf(IsNull(PS(0)), 1, PS(0) + 1)
End Function

Function ID_NaME(TB As String, CoL As String, Tcom As String, Eqt As Variant) As String
Set PS = New ADODB.Recordset
PS.Open "SELECT " & CoL & " FROM " & TB & " WHERE " & Tcom & "='" & Eqt & "'", CN, adOpenStatic, adLockOptimistic
If PS.RecordCount > 0 Then
ID_NaME = IIf(IsNull(PS(0)), "", PS(0))
Else
ID_NaME = ""
'MsgBox "UNKNOWN NAME"
End If
End Function

Function NaME_iD(TB As String, Tcom As String, Eqt As Integer) As String
Set PS = New ADODB.Recordset
PS.Open "SELECT NAME FROM " & TB & " WHERE " & Tcom & "=" & Eqt & "", CN, adOpenStatic, adLockOptimistic
NaME_iD = IIf(IsNull(PS!Name), "", PS!Name)
End Function

Function NEW_ACC(NAME1 As String)
Set RSP = New ADODB.Recordset
RSP.Open "SELECT * FROM PARTY WHERE NAME='" & NAME1 & "'", CN, adOpenStatic, adLockOptimistic
If RSP.RecordCount > 0 Then
Else
RSP.AddNew
RSP!PID = MaX_ID("PID", "PARTY")
RSP!Name = NAME1
RSP!Type = "TAX"
RSP.Update
RSP.Close
End If
End Function

Function Check_Stock(v_Prid As String) As Variant
Set RSP = New ADODB.Recordset
RSP.Open "select sum(tqtyp) from stock where prid=" & v_Prid & "", CN
If RSP.RecordCount > 0 Then
    If RSP(0) <= 12 Then
    MsgBox "Stock Qty Of " & RSP(0)
    End If
End If
End Function

Function View_Dcno(ET As String) As Variant
str = ""
Set Rs = New ADODB.Recordset
Rs.Open "SELECT DISTINCT DCNO FROM PLEDGER WHERE ETYPE='" & ET & "' AND PID=1 AND DATE BETWEEN '" & ReportDate.DTS.Value & "' AND '" & ReportDate.DTE.Value & "'", CN
str = 0
If Rs.RecordCount > 0 Then
str = str & "," & Rs!DCNO
Do Until Rs.EOF
str = str & "," & Rs!DCNO
Rs.MoveNext
Loop
End If
View_Dcno = str
End Function

Function FN_Pid(V_Pid As Integer, V_etype As String, V_DateS As Date, V_DateE As Date, v_DrCr As String) As Double
Set rs1 = New ADODB.Recordset
rs1.Open "select sum(debit),sum(credit) from pledger where pid=" & V_Pid & " and date between '" & V_DateS & "' AND '" & V_DateE & "' and etype='" & V_etype & "'", CN
If rs1.RecordCount > 0 Then
If v_DrCr = "DR" Then
FN_Pid = IIf(IsNull(rs1(0)), 0, rs1(0))
ElseIf v_DrCr = "CR" Then
FN_Pid = IIf(IsNull(rs1(1)), 0, rs1(1))
ElseIf v_DrCr = "BAL" Then
FN_Pid = IIf(IsNull(rs1(0)), 0, rs1(0)) - IIf(IsNull(rs1(1)), 0, rs1(1))
End If
End If
End Function

Public Function getBitOfString(s As String) As Variant
    For i = 1 To Len(s)
        If IsNumeric(Mid(s, i, 1)) Then
            Exit For
        End If
    Next
        getBitOfString = CLng(Right(s, Len(s) - i + 1))
End Function
Function UpdateReportCredentials(ByRef V_Report As Report)
'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog='" & V_DataBase & "';Data Source=" & V_Server
'Provider=SQLOLEDB.1;Persist Security Info=False;User ID=sa;Initial Catalog=Sr_Traders2;Data Source=DS1
'Unload V_Report
Dim CPProperties As CRAXDRT.ConnectionProperties
Dim DBTable As CRAXDRT.DatabaseTable
For Each DBTable In V_Report.Database.Tables
Set CPProperties = DBTable.ConnectionProperties
'DBTable.DllName = "crdb_ado.dll"
CPProperties.DeleteAll
CPProperties.Add "Provider", "SQLOLEDB.1"
CPProperties.Add "Data Source", V_Server
CPProperties.Add "Initial Catalog", V_DataBase
CPProperties.Add "Integrated Security", IIf(IsEmpty(V_User), True, False)
CPProperties.Add "User ID", V_User
CPProperties.Add "Password", IIf(IsEmpty(V_password), "", V_password)
On Error Resume Next
DBTable.Location = DBTable.Name
Next
End Function
Function Report_ID(V_Report As Report, V_DataBase As String, V_Server As String, V_User As String, V_Pass As String)
'Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog='" & V_DataBase & "';Data Source=" & V_Server
Dim CPProperties As CRAXDRT.ConnectionProperties
Dim DBTable As CRAXDRT.DatabaseTable
For Each DBTable In V_Report.Database.Tables
Set CPProperties = DBTable.ConnectionProperties
'DBTable.DllName = "crdb_ado.dll"
CPProperties.DeleteAll
CPProperties.Add "Provider", "SQLOLEDB.1"
CPProperties.Add "Data Source", V_Server
CPProperties.Add "Initial Catalog", V_DataBase
CPProperties.Add "Integrated Security", IIf(IsEmpty(V_User), True, False)
CPProperties.Add "User ID", V_User
CPProperties.Add "Password", V_Pass
On Error Resume Next
DBTable.Location = DBTable.Name
Next
End Function
Function Report_Server(V_Report As Report, ByVal V_Srv As String)
Dim CPProperties As CRAXDRT.ConnectionProperties
Dim DBTable As CRAXDRT.DatabaseTable
For Each DBTable In V_Report.Database.Tables
Set CPProperties = DBTable.ConnectionProperties
'CPProperties.Delete "Data Source"
CPProperties.DeleteAll
CPProperties.Add "Provider", "SQLOLEDB.1"
CPProperties.Add "Data Source", V_Server
CPProperties.Add "Initial Catalog", V_DataBase
CPProperties.Add "Integrated Security", IIf(IsEmpty(V_User), True, False)
CPProperties.Add "User ID", V_User
CPProperties.Add "Password", IIf(IsEmpty(V_password), "", V_password)
On Error Resume Next
DBTable.Location = DBTable.Name
Next
End Function
Function UnloadAllReports()
Unload ProductView
'Unload reTB
'Unload rptAccountLedger
'Unload rptCashPayment
Unload rptCashReport
Unload rptDailySaleReport
Unload rptDayBook
'Unload rptGeneralVocher
Unload rptInventoryReport
'Unload rptIssueReturn
Unload rptItemLedger
Unload rptLEDGER
Unload REPLED
'Unload rptLEDGER1
Unload rptNevigation
Unload rptOpeningTrial
Unload rptOverAllSaleReport
Unload rptPaRe
Unload rptPartyView
Unload rptPartyViewDayWise
'Unload rptPayRec
Unload rptPurchaseDatePartyWise
'Unload rptPurchaseInvoiceWise
Unload rptPurchaseOverAll
Unload rptPurchasePartyWise
'Unload rptPurchaseReport
Unload rptPurchaseSaleDateWise
Unload rptPurchaseSaleOverAll
Unload rptPurchaseWareHouseWise
'Unload rptPurchseVoucher
'Unload rptReceivable
'Unload rptReturnSaleReport
'Unload rptRSaleVoucher
'Unload rptSale
'Unload rptSale1
'Unload rptSaleComm
'Unload rptSaleCompanyWise
Unload rptSaleInvoiceWise
Unload rptSaleManMonthWise
Unload rptSaleManPartyWise
Unload rptSaleManPartyWise2
Unload rptSaleManCompanyPartyWise
Unload rptSaleManSaleCompanyProductWise
Unload rptSaleManSaleProductWise
Unload rptSaleManWise
Unload rptSalePartySectorWise
'Unload rptSalePartyWise
Unload rptSaleReport
'Unload rptSaleReportSalemanWise
Unload rptSaleSectorWise
'Unload rptSaleVoucher
'Unload rptSaleVoucher1
'Unload rptSLPwise
Unload rptStock
Unload rptStockCompanyWise
Unload rptStockOverAll
Unload rptTb
'Unload rptTRBAL
'Unload rptTrialBalance
'Unload rptTrialBalance1
'Unload rptTrlBal
End Function
Function UpdateAllReportPath()
UpdateReportCredentials ProductView
'UpdateReportCredentials reTB
'UpdateReportCredentials rptAccountLedger
'UpdateReportCredentials rptCashPayment
UpdateReportCredentials rptCashReport
UpdateReportCredentials rptDailySaleReport
UpdateReportCredentials rptDayBook
'UpdateReportCredentials rptGeneralVocher
UpdateReportCredentials rptInventoryReport
'UpdateReportCredentials rptIssueReturn
UpdateReportCredentials rptItemLedger
UpdateReportCredentials rptLEDGER
UpdateReportCredentials REPLED
'UpdateReportCredentials rptLEDGER1
UpdateReportCredentials rptNevigation
UpdateReportCredentials rptOpeningTrial
UpdateReportCredentials rptOverAllSaleReport
UpdateReportCredentials rptPaRe
UpdateReportCredentials rptPartyView
UpdateReportCredentials rptPartyViewDayWise
'UpdateReportCredentials rptPayRec
UpdateReportCredentials rptPurchaseDatePartyWise
'UpdateReportCredentials rptPurchaseInvoiceWise
UpdateReportCredentials rptPurchaseOverAll
UpdateReportCredentials rptPurchasePartyWise
'UpdateReportCredentials rptPurchaseReport
UpdateReportCredentials rptPurchaseSaleDateWise
UpdateReportCredentials rptPurchaseSaleOverAll
UpdateReportCredentials rptPurchaseWareHouseWise
'UpdateReportCredentials rptPurchseVoucher
'UpdateReportCredentials rptReceivable
'UpdateReportCredentials rptReturnSaleReport
'UpdateReportCredentials rptRSaleVoucher
'UpdateReportCredentials rptSale
'UpdateReportCredentials rptSale1
'UpdateReportCredentials rptSaleComm
'UpdateReportCredentials rptSaleCompanyWise
UpdateReportCredentials rptSaleInvoiceWise
UpdateReportCredentials rptSaleManMonthWise
UpdateReportCredentials rptSaleManPartyWise
UpdateReportCredentials rptSaleManPartyWise2
UpdateReportCredentials rptSaleManCompanyPartyWise
UpdateReportCredentials rptSaleManSaleCompanyProductWise
UpdateReportCredentials rptSaleManSaleProductWise
UpdateReportCredentials rptSaleManWise
UpdateReportCredentials rptSalePartySectorWise
'UpdateReportCredentials rptSalePartyWise
UpdateReportCredentials rptSaleReport
'UpdateReportCredentials rptSaleReportSalemanWise
UpdateReportCredentials rptSaleSectorWise
UpdateReportCredentials rptSaleVoucher
'UpdateReportCredentials rptSaleVoucher1
'UpdateReportCredentials rptSLPwise
UpdateReportCredentials rptStock
UpdateReportCredentials rptStockCompanyWise
UpdateReportCredentials rptStockOverAll
'UpdateReportCredentials rptTb
'UpdateReportCredentials rptTRBAL
'UpdateReportCredentials rptTrialBalance
'UpdateReportCredentials rptTrialBalance1
'UpdateReportCredentials rptTrlBal
End Function
Function All_Reports_Path()
Report_Server ProductView, V_Server
Report_Server REPLED, V_Server
Report_Server rptCashReport, V_Server
Report_Server rptDailySaleReport, V_Server
Report_Server rptDayBook, V_Server
Report_Server rptInventoryReport, V_Server
Report_Server rptItemLedger, V_Server
Report_Server rptNevigation, V_Server
Report_Server rptOpeningTrial, V_Server
Report_Server rptOverAllSaleReport, V_Server
Report_Server rptPaRe, V_Server
Report_Server rptPartyView, V_Server
Report_Server rptPartyViewDayWise, V_Server
Report_Server rptPurchaseDatePartyWise, V_Server
Report_Server rptPurchaseOverAll, V_Server
Report_Server rptPurchasePartyWise, V_Server
Report_Server rptPurchaseSaleDateWise, V_Server
Report_Server rptPurchaseSaleOverAll, V_Server
Report_Server rptPurchaseWareHouseWise, V_Server
Report_Server rptSaleInvoiceWise, V_Server
Report_Server rptSaleManCompanyPartyWise, V_Server
Report_Server rptSaleManMonthWise, V_Server
Report_Server rptSaleManPartyWise, V_Server
Report_Server rptSaleManPartyWise2, V_Server
Report_Server rptSaleManSaleCompanyProductWise, V_Server
Report_Server rptSaleManSaleProductWise, V_Server
Report_Server rptSaleManWise, V_Server
Report_Server rptSalePartySectorWise, V_Server
Report_Server rptSaleReport, V_Server
Report_Server rptSaleSectorWise, V_Server
'Report_Server rptSaleVoucher, V_Server
Report_Server rptStock, V_Server
Report_Server rptStockCompanyWise, V_Server
Report_Server rptStockOverAll, V_Server
'Report_Server rptTb, V_Server
End Function

Public Function CL_FG1(frm As Form) As Variant
With frm
.FG1.clear
.FG2.clear
.FG1.TextMatrix(0, 0) = "SNO"
.FG1.TextMatrix(0, 1) = "PRID"
.FG1.TextMatrix(0, 2) = "Product"
.FG1.TextMatrix(0, 3) = "Carton"
.FG1.TextMatrix(0, 4) = "Qty"
.FG1.TextMatrix(0, 5) = "Scheme"
.FG1.TextMatrix(0, 6) = "TP Rate"
.FG1.TextMatrix(0, 7) = "Less"
.FG1.TextMatrix(0, 8) = "Amount"
If V_CGEnable = "1" Then
.FG1.TextMatrix(0, 9) = "%GST"
.FG1.TextMatrix(0, 10) = "GST Amount"
Else
.FG1.TextMatrix(0, 9) = "%"
.FG1.TextMatrix(0, 10) = "% Amount"
End If
.FG1.TextMatrix(0, 11) = "Net Amount"
.FG1.TextMatrix(0, 12) = "SQTY"
.FG1.TextMatrix(0, 13) = "Qty"
.FG1.TextMatrix(0, 14) = "PPC"
.FG1.TextMatrix(0, 15) = "Sc.Amount"

.FG1.ColAlignment(2) = 0
.FG1.ColWidth(0) = 500
.FG1.ColWidth(1) = 0
.FG1.ColWidth(2) = 3000
.FG1.ColWidth(3) = 0
.FG1.ColWidth(4) = 800
.FG1.ColWidth(5) = 800
.FG1.ColWidth(6) = 800
.FG1.ColWidth(7) = 800
.FG1.ColWidth(8) = 900
If V_CGEnable = "0" Then
.FG1.ColWidth(9) = 0
.FG1.ColWidth(10) = 0
.FG1.ColWidth(11) = 0
Else
.FG1.ColWidth(9) = 600
.FG1.ColWidth(10) = 1000
.FG1.ColWidth(11) = 1000
End If
.FG1.ColWidth(12) = 0
.FG1.ColWidth(13) = 0
.FG1.ColWidth(14) = 0
.FG1.ColWidth(15) = 0
''''''''''FG2
.FG2.ColWidth(0) = .FG1.ColWidth(0)
.FG2.ColWidth(1) = .FG1.ColWidth(1)
.FG2.ColWidth(2) = .FG1.ColWidth(2)
.FG2.ColWidth(3) = .FG1.ColWidth(3)
.FG2.ColWidth(4) = .FG1.ColWidth(4)
.FG2.ColWidth(5) = .FG1.ColWidth(5)
.FG2.ColWidth(6) = .FG1.ColWidth(6)
.FG2.ColWidth(7) = .FG1.ColWidth(7)
.FG2.ColWidth(8) = .FG1.ColWidth(8)
.FG2.ColWidth(9) = .FG1.ColWidth(9)
.FG2.ColWidth(10) = .FG1.ColWidth(10)
.FG2.ColWidth(11) = .FG1.ColWidth(11)
.FG2.ColWidth(12) = .FG1.ColWidth(12)
.FG2.ColWidth(13) = .FG1.ColWidth(13)
.FG2.ColWidth(14) = .FG1.ColWidth(14)
.FG2.ColWidth(15) = .FG1.ColWidth(15)

For i = 1 To .FG1.Rows - 1
.FG1.TextMatrix(i, 0) = i
Next i

.Txt1.Visible = False
.DCPR.Visible = False
.Txt1.Text = ""
.DCPR.Text = ""
End With
End Function

Public Function SHoW_CoNT(frm As Form) As Variant
On Error Resume Next
With frm
.Txt1.Visible = False
.DCPR.Visible = False
If .FG1.CoL = 2 Then
        .DCPR.Text = .FG1.TextMatrix(.FG1.Row, .FG1.CoL)
        .DCPR.Top = .FG1.Top + .FG1.CellTop
        .DCPR.Left = .FG1.Left + .FG1.CellLeft
        .DCPR.Visible = True
        .DCPR.SetFocus
        .DCPR.Width = .FG1.CellWidth
        .DCPR.Height = .FG1.CellHeight
ElseIf .FG1.CoL = 4 Or .FG1.CoL = 5 Or .FG1.CoL = 6 Or .FG1.CoL = 7 Or (.FG1.CoL = 9 And V_CGEnable <> "0") Then
.Txt1.Text = .FG1.TextMatrix(.FG1.Row, .FG1.CoL)
    .Txt1.Left = .FG1.Left + .FG1.CellLeft
    .Txt1.Top = .FG1.Top + .FG1.CellTop
    .Txt1.Height = .FG1.CellHeight
    .Txt1.Width = .FG1.CellWidth
    .Txt1.Visible = True
    .Txt1.SetFocus
ElseIf .FG1.CoL = 3 Then
.FG1.CoL = .FG1.CoL + 1
Else
'MsgBox .FG1.Row & .FG1.CoL
.FG1.CoL = 2
.FG1.Row = .FG1.Row + 1
End If
End With
End Function

Public Function txt_KeyDown(KeyCode As Integer, shift As Integer, frm As Form)
With frm
If KeyCode = vbKeyRight Then
If .FG1.CoL <> .FG1.Cols - 1 Then
.FG1.CoL = .FG1.CoL + 1
End If
ElseIf KeyCode = vbKeyLeft Then
If .FG1.CoL <> 0 Then
If .FG1.CoL = 4 Then
.FG1.CoL = 2
Else
.FG1.CoL = .FG1.CoL - 1
End If
End If
ElseIf KeyCode = vbKeyUp Then
If .FG1.Row <> 1 Then
.FG1.Row = .FG1.Row - 1
End If
ElseIf KeyCode = vbKeyDown Then
If .FG1.Row <> .FG1.Rows - 1 Then
.FG1.Row = .FG1.Row + 1
End If
End If
SHoW_CoNT frm
End With
End Function

Function Delete_Data(V_etype As String, V_dcno As Long)
CN.Execute "DELETE FROM STOCK WHERE ETYPE='" & V_etype & "' AND DCNO='" & V_dcno & "'"
CN.Execute "DELETE FROM STOCK_main WHERE ETYPE='" & V_etype & "' AND DCNO='" & V_dcno & "'"
CN.Execute "DELETE FROM PLEDGER WHERE ETYPE='" & V_etype & "' AND DCNO='" & V_dcno & "'"
End Function

Public Sub Export_Report(strSQL As String, sReportName As String, sDestFileName As String, ReportTitle As String)

On Error GoTo Err
Dim CrAppl As CRAXDRT.Application
Dim crrep As CRAXDRT.Report

Dim rsCrystal As ADODB.Recordset
 
    Set CrAppl = New CRAXDRT.Application
    Set crrep = CrAppl.OpenReport(sReportName) 'Opens the report
    
   Set rsCrystal = New ADODB.Recordset
  
   
    rsCrystal.Open strSQL, CN, adOpenStatic, adLockReadOnly
    rsCrystal.Requery
    
    
     With crrep
     .DiscardSavedData
     .Database.LogOnServer "C:\Program Files\Common Files\Crystal Decisions\2.0\bin\crdb_dao.dll", V_Server, "SR_Traders", V_User, V_password
     .ReportTitle = ReportTitle
     .Database.SetDataSource rsCrystal, 3 'LINK REPORT TO RECORDSET
     .VerifyOnEveryPrint = True
     End With
 
   With crrep.ExportOptions
    .DestinationType = crEDTDiskFile
    .FormatType = crEFTCrystalReport
    .DiskFileName = sDestFileName
   End With
   
     
   crrep.Export (False)
    Set crrep = Nothing
    Set CrAppl = Nothing
    Exit Sub
Err:
 MsgBox Err.Description

End Sub
