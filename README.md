# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary: General Trading Software project
- Accounts
-- Cash Receive
-- Cash Payment
-- General Journal
-- Account Ledger
-- Accounts Payable/Receivable
-- Trial Balance
- Stock & Inventory
-- Purchase/Purchase Return
-- Sale/Sale Return
- Stores
-- Manage Stores
-- Stock Transfer
- Salesman Sale
- And countless Reports, see screenshots in repo for more details.

* Version: v 1.0

* [DigitalSofts](http://www.digitalsofts.com/)
  [Adroit](http://www.adroit.pk/)

### How do I get set up? ###

* Summary of set up: Prerequisites:
- Microsoft Visual Basic 6
- Microsoft SQL Server 2000
- Crystal Reports 9+

* Configuration:
There is no special configuration. Just install above tools and run project 

* Dependencies
Run 'Click Me First.bat', it will register required OCX and Dlls.
(You can manually register XP.ocx)

* Database configuration
Install sql server 2000 and attach db from Database folder. Modify config.dat for db connection.

### Who do I talk to? ###

* tm.tariq@hotmail.com