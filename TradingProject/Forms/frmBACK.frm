VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Object = "{D750EAB9-EB7B-450B-B9B6-0A0A17E13DCA}#1.0#0"; "xp.ocx"
Begin VB.Form frmBACKUP 
   Caption         =   "Backup Or Restore Database"
   ClientHeight    =   5715
   ClientLeft      =   60
   ClientTop       =   450
   ClientWidth     =   7890
   Icon            =   "frmBACK.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Moveable        =   0   'False
   Picture         =   "frmBACK.frx":058C
   ScaleHeight     =   5715
   ScaleWidth      =   7890
   StartUpPosition =   1  'CenterOwner
   Begin XPLayout.XPButton cmdBrowse 
      Height          =   375
      Left            =   5640
      TabIndex        =   0
      Top             =   1080
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "B&rowse"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   16711680
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "frmBACK.frx":159C62
      PICN            =   "frmBACK.frx":159C7E
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.TextBox txtStatus 
      Height          =   3135
      Left            =   360
      Locked          =   -1  'True
      MultiLine       =   -1  'True
      ScrollBars      =   3  'Both
      TabIndex        =   3
      Top             =   2280
      Width           =   7095
   End
   Begin VB.TextBox txtDataFileName 
      Appearance      =   0  'Flat
      Height          =   288
      Left            =   360
      TabIndex        =   5
      Top             =   1080
      Width           =   4935
   End
   Begin VB.ComboBox cmbDatabaseName 
      Height          =   315
      Left            =   3000
      TabIndex        =   4
      Top             =   5640
      Visible         =   0   'False
      Width           =   3495
   End
   Begin MSComDlg.CommonDialog CommonDialog1 
      Left            =   0
      Top             =   0
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
      DialogTitle     =   "Data File Name:"
   End
   Begin XPLayout.XPButton cmdbackup 
      Height          =   375
      Left            =   600
      TabIndex        =   1
      Top             =   1680
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&BackUp"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   16711680
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "frmBACK.frx":15A218
      PICN            =   "frmBACK.frx":15A234
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin XPLayout.XPButton cmdRestore 
      Height          =   375
      Left            =   2040
      TabIndex        =   2
      Top             =   1680
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Restore"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   16711680
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "frmBACK.frx":15A7D0
      PICN            =   "frmBACK.frx":15A7EC
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label3 
      BackStyle       =   0  'Transparent
      Caption         =   "Backup/Restore File Name:"
      ForeColor       =   &H00000040&
      Height          =   255
      Left            =   360
      TabIndex        =   6
      Top             =   840
      Width           =   2175
   End
End
Attribute VB_Name = "frmBACKUP"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit
Dim gSQLServer As SQLDMO.SQLServer
Dim WithEvents oBackupEvent As SQLDMO.Backup
Attribute oBackupEvent.VB_VarHelpID = -1
Dim WithEvents oRestoreEvent As SQLDMO.Restore
Attribute oRestoreEvent.VB_VarHelpID = -1
Dim gbConnected As Boolean
Dim gDatabaseName As String
Dim gBkupRstrFileName As String
Dim gBkupRstrFilePath As String
Const gTitle = "Server Connection"

Private Sub Form_Load()
''''''''''''''''''' theme
Set OBJ = New clsFunction
OBJ.clear Me
'''''''''''''''''''''''''''''

If CN.State = 1 Then
CN.Close
End If
    Set gSQLServer = Nothing
    gbConnected = False
    Dim ServerName As String
    Dim UserName As String
    Dim password As String
    On Error GoTo ErrHandler:
    If gSQLServer Is Nothing Then
        Set gSQLServer = New SQLDMO.SQLServer
    End If
    gSQLServer.LoginTimeout = 15
    '    gSQLServer.LoginSecure = True
    Screen.MousePointer = vbHourglass
    
    gSQLServer.Connect V_Server, V_User, v_password
    gbConnected = True
    FillDatabaseList
    Screen.MousePointer = vbDefault
    buttonsConnectOpen
    Exit Sub

ErrHandler:
    MsgBox "Error " & Err.Description
    ' Change mousepointer back if it's hourglass.
    If Screen.MousePointer = vbHourglass Then
        Screen.MousePointer = vbDefault
    End If

    'txtDataFileName.Text = "E:\DatabaseBackUp\" & Date & ".bak"
'gBkupRstrFileName = "E:\DatabaseBackUp\" & Format(Date, "dd-MMM-yy") & ".bak"

End Sub

Private Sub Form_Unload(Cancel As Integer)
    If gbConnected = True Then
        Call gSQLServer.Disconnect
    End If
    If Not gSQLServer Is Nothing Then
        Set gSQLServer = Nothing
    End If
    If CN.State = 1 Then
CN.Close
End If
CN.Open CONN
End Sub
Private Sub cmdBackup_Click()
'On Error GoTo Err
txtStatus.Text = ""
    On Error GoTo ErrHandler:
    
    Dim oBackup As SQLDMO.Backup
    cmbDatabaseName.Text = V_DataBase
    
    gDatabaseName = cmbDatabaseName.Text
    Set oBackup = New SQLDMO.Backup
    Set oBackupEvent = oBackup ' enable events
    
    oBackup.database = gDatabaseName
    gBkupRstrFileName = txtDataFileName.Text
    oBackup.Files = gBkupRstrFileName
     
    If Len(Dir(gBkupRstrFileName)) > 0 Then
        Kill (gBkupRstrFileName)
    End If
       
    Screen.MousePointer = vbHourglass
    
    oBackup.SQLBackup gSQLServer
    
    Screen.MousePointer = vbDefault
   
    Set oBackupEvent = Nothing ' disable events
    Set oBackup = Nothing
    
    Exit Sub

ErrHandler:
    MsgBox "Error " & Err.Description
    Resume Next
    
End Sub

Private Sub cmdRestore_Click()
txtStatus.Text = ""
    On Error GoTo ErrHandler:
    
    Dim oRestore As SQLDMO.Restore
    
    Dim Msg As String
    Dim Response As String

'    Msg = "You must choose the right database name according to the data file name selected. Do you want to continue?"
'    Response = MsgBox(Msg, vbYesNo, gTitle)
'    If Response = vbNo Then
'        Exit Sub
'    End If
        
    gDatabaseName = "Hussnain_Traders"
    Set oRestore = New SQLDMO.Restore
    Set oRestoreEvent = oRestore        ' enable events
    
    oRestore.database = gDatabaseName
    gBkupRstrFileName = txtDataFileName.Text
    oRestore.Files = gBkupRstrFileName
    
    ' Change mousepointer while trying to connect.
    Screen.MousePointer = vbHourglass
    
    ' Restore the database.
    oRestore.SQLRestore gSQLServer
    
    ' Change mousepointer back to the default after connect.
    Screen.MousePointer = vbDefault
   
    Set oRestoreEvent = Nothing         ' disable events
    Set oRestore = Nothing
    
    Exit Sub

ErrHandler:
    MsgBox "Error " & Err.Description
    Resume Next
End Sub

Private Sub cmdBrowse_Click()
    On Error GoTo ErrHandler:
    
    CommonDialog1.CancelError = True
    CommonDialog1.Filter = "Backup Files (*.bak)|*.bak"
    CommonDialog1.FilterIndex = 2
    ''CommonDialog1.InitDir = "E:\DatabaseBackUp\" & Format(Date, "dd-MMM-yy") & "FC.bak"
    CommonDialog1.InitDir = V_BackupFoler & Format(Date, "dd-MMM-yy") & "FC.bak"
    CommonDialog1.DefaultExt = "bak"
    CommonDialog1.DialogTitle = "Data File Name:"
    CommonDialog1.Action = 1
    txtDataFileName.Text = CommonDialog1.FileName
    Exit Sub
    
ErrHandler:
    'User pressed the Cancel button
    Exit Sub
End Sub



' VB will create the right prototypes for you, if you select the oBackupEvent in
' the drop down listbox of your editor
Private Sub oBackupEvent_Complete(ByVal Message As String)
    PrintStat "oBackupEvent_Complete -- " & Message
End Sub

Private Sub oBackupEvent_NextMedia(ByVal Message As String)
    PrintStat "oBackupEvent_NextMedia -- " & Message
End Sub

Private Sub oBackupEvent_PercentComplete(ByVal Message As String, ByVal Percent As Long)
    PrintStat "oBackupEvent_PercentComplete -- " & Message & " " & Percent
End Sub

Private Sub oRestoreEvent_Complete(ByVal Message As String)
    PrintStat "oRestoreEvent_Complete -- " & Message
End Sub

Private Sub oRestoreEvent_NextMedia(ByVal Message As String)
    PrintStat "oRestoreEvent_NextMedia -- " & Message
End Sub

Private Sub oRestoreEvent_PercentComplete(ByVal Message As String, ByVal Percent As Long)
    PrintStat "oRestoreEvent_PercentComplete -- " & Message & " " & Percent
End Sub

Private Sub PrintStat(ByRef Message As String)
    txtStatus.Text = txtStatus.Text + Message + vbCrLf
End Sub







Private Sub buttonsConnectOpen()
    'cmdConnect.Enabled = False
    'cmdbackup.Enabled = True
    'cmdRestore.Enabled = True
    'cmdDisconnect.Enabled = True
    
    'cmdBrowse.Enabled = True
    'cmbDatabaseName.Enabled = True
    'txtDataFileName.Enabled = True
    
    ' Disable the Authorization stuff.
    'optWinNTAuth.Enabled = False
    'optSSAuth.Enabled = False
    'txtServerName.Enabled = False
    'lblServer.Enabled = False
    'lblUserName.Enabled = False
    'lblPassword.Enabled = False
    'txtUserName.Enabled = False
    't'xtPassword.Enabled = False
End Sub





Private Sub FillDatabaseList()
'    cmbDatabaseName.Clear
'
'    Dim oDB As SQLDMO.Database
'    For Each oDB In gSQLServer.Databases
'        If oDB.SystemObject = False Then
'            cmbDatabaseName.AddItem oDB.Name
'        End If
'    Next oDB
'
'    ' Take care of the assignment of gBkupRstrFilePath.
'    Dim MyPos As Integer
'    gBkupRstrFilePath = CurDir
'    MyPos = InStr(1, CurDir, "DevTools", 1)
'    If MyPos > 0 Then
'        gBkupRstrFilePath = Left(gBkupRstrFilePath, MyPos - 1)
'        If Len(Dir(gBkupRstrFilePath + "backup", vbDirectory)) Then
'            gBkupRstrFilePath = gBkupRstrFilePath + "backup\"
'        Else
'            gBkupRstrFilePath = "c:\"
'        End If
'    Else
'        gBkupRstrFilePath = "c:\"
'    End If
'
'    ' Select the first database in the list.
'    If cmbDatabaseName.ListCount > 0 Then
'        cmbDatabaseName.ListIndex = 0
'        ' Assign the default backup/restore file name.
'        If Len(cmbDatabaseName.Text) > 0 Then
'            txtDataFileName.Text = gBkupRstrFilePath + cmbDatabaseName.Text + ".bak"
'        End If
'    End If
'gBkupRstrFileName = "E:\DigitalSoft\DatabaseBackups\" & Format(Date, "dd-MMM-yy") & "FC.bak"
gBkupRstrFileName = V_BackupFoler & Format(Now, "dd-MMM-yy(hh-mm-ss)") & ".bak"
txtDataFileName.Text = gBkupRstrFileName
    
End Sub


Private Sub cmbDatabaseName_Click()
    ' Assign the default backup/restore file name.
    If Len(cmbDatabaseName.Text) > 0 Then
        txtDataFileName.Text = gBkupRstrFilePath + cmbDatabaseName.Text + ".bak"
    End If
End Sub


Function DIS()
    'On Error GoTo ErrHandler:
    
    Dim Msg As String
    Dim Response As String

    ' Disconnect from a connected server.
    If gbConnected = True Then
        Msg = "Disconnect from Server?"
        Response = MsgBox(Msg, vbOKCancel, gTitle)
        If Response = vbOK Then
            Call gSQLServer.Disconnect
            Set gSQLServer = Nothing
            cmbDatabaseName.clear
            txtDataFileName.Text = ""
            txtStatus.Text = ""
            gbConnected = False
           ' buttonsConnectClosed
        End If
    End If
    
  '  Exit Sub
    
'ErrHandler:
 '   MsgBox "Error " & Err.Description
'    Resume Next

End Function

