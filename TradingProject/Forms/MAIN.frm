VERSION 5.00
Begin VB.Form MAIN 
   BackColor       =   &H00C0C0C0&
   Caption         =   "Main"
   ClientHeight    =   8760
   ClientLeft      =   165
   ClientTop       =   210
   ClientWidth     =   13950
   BeginProperty Font 
      Name            =   "Times New Roman"
      Size            =   21.75
      Charset         =   0
      Weight          =   700
      Underline       =   0   'False
      Italic          =   0   'False
      Strikethrough   =   0   'False
   EndProperty
   Icon            =   "MAIN.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   8760
   ScaleWidth      =   13950
   StartUpPosition =   2  'CenterScreen
   WindowState     =   2  'Maximized
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Height          =   495
      Left            =   6000
      TabIndex        =   3
      Top             =   4440
      Visible         =   0   'False
      Width           =   1215
   End
   Begin VB.Timer Tm1 
      Interval        =   1000
      Left            =   120
      Top             =   600
   End
   Begin VB.Label lblAddress 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H00C0E0FF&
      BackStyle       =   0  'Transparent
      Caption         =   "Sr Traders"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   14.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   405
      Left            =   6060
      TabIndex        =   2
      Top             =   7200
      Width           =   1650
   End
   Begin VB.Label lblDate 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "date"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   21.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00C0C000&
      Height          =   495
      Left            =   120
      TabIndex        =   1
      Top             =   6000
      Width           =   13755
   End
   Begin VB.Label lblCompany 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H00C0E0FF&
      BackStyle       =   0  'Transparent
      Caption         =   "Sr Traders"
      BeginProperty Font 
         Name            =   "Arial Black"
         Size            =   30
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H000080FF&
      Height          =   840
      Left            =   5265
      TabIndex        =   0
      Top             =   6360
      Width           =   3450
   End
   Begin VB.Menu F 
      Caption         =   "New"
      Begin VB.Menu NewAccount 
         Caption         =   "Account"
      End
      Begin VB.Menu NewProduct 
         Caption         =   "Product"
      End
      Begin VB.Menu NewWareHouse 
         Caption         =   "Store"
      End
      Begin VB.Menu LOGOFF 
         Caption         =   "Logout"
      End
      Begin VB.Menu BR3 
         Caption         =   "-"
      End
      Begin VB.Menu EX 
         Caption         =   "EXIT"
      End
   End
   Begin VB.Menu vouchers 
      Caption         =   "Vouchers"
      Begin VB.Menu VouchersSale 
         Caption         =   "Sale"
      End
      Begin VB.Menu vouchersVouchers 
         Caption         =   "vouchers"
         Begin VB.Menu VouchersPurchase 
            Caption         =   "Purchase"
         End
         Begin VB.Menu BR12 
            Caption         =   "-"
         End
         Begin VB.Menu VoucherSaleman 
            Caption         =   "SaleMan"
            Visible         =   0   'False
            Begin VB.Menu VoucherSalemanIssue 
               Caption         =   "Issue"
            End
            Begin VB.Menu VoucherSalemanReturn 
               Caption         =   "Return"
            End
         End
         Begin VB.Menu VouchersProudctNevegation 
            Caption         =   "Stock Transfer"
         End
         Begin VB.Menu BR1 
            Caption         =   "-"
         End
         Begin VB.Menu VouchersPurchaseReturn 
            Caption         =   "Purchase Return"
         End
         Begin VB.Menu VouchersSaleReturn 
            Caption         =   "Sale Return"
         End
         Begin VB.Menu BR5 
            Caption         =   "-"
         End
         Begin VB.Menu VouchersReceipt 
            Caption         =   "Receipt"
         End
         Begin VB.Menu VouchersPayment 
            Caption         =   "Payment"
         End
         Begin VB.Menu SDFBR4 
            Caption         =   "-"
         End
         Begin VB.Menu VouchersJournalVoucher 
            Caption         =   "Journal Voucher"
         End
      End
   End
   Begin VB.Menu ReportsView 
      Caption         =   "Reports"
      Begin VB.Menu ReportS 
         Caption         =   "Sale"
         Begin VB.Menu reportsSale 
            Caption         =   "Over All"
         End
         Begin VB.Menu OtherSaleReports 
            Caption         =   "Other Sale Reports"
         End
         Begin VB.Menu ReportSaleCCSale 
            Caption         =   "CCSale"
         End
         Begin VB.Menu ReportSaleSMCSale 
            Caption         =   "smCSale"
         End
         Begin VB.Menu ReportSaleSMSCWise 
            Caption         =   "SMSCWise"
         End
         Begin VB.Menu ReportsSaleWareHouseWise 
            Caption         =   "WareHouse Wise"
         End
         Begin VB.Menu ReportSaleReport 
            Caption         =   "Product Wise"
         End
         Begin VB.Menu ReportsSalePartyWise 
            Caption         =   "Party Wise"
         End
         Begin VB.Menu ReportsSaleInvoiceWise 
            Caption         =   "Invoice Wise"
         End
         Begin VB.Menu ReportsSaleSectorWise 
            Caption         =   "Sector Wise"
         End
         Begin VB.Menu reportsSaleDatewise 
            Caption         =   "Sale Datewise"
         End
         Begin VB.Menu reportsSaleman 
            Caption         =   "SaleMan"
            Begin VB.Menu ReportSaleReportproductwisesalesman 
               Caption         =   "Product Wise"
            End
            Begin VB.Menu ReportsSaleManPartyWise 
               Caption         =   "Party Wise"
            End
            Begin VB.Menu ReportsSaleManPartyWise2 
               Caption         =   "Party Wise2"
            End
            Begin VB.Menu reportsSalemanMothWise 
               Caption         =   "Month Wise"
            End
            Begin VB.Menu salemanSaleMonthWise2 
               Caption         =   "Month Wise2"
            End
            Begin VB.Menu reportsSalemanCompanyPartyWise 
               Caption         =   "Company Party Wise"
            End
            Begin VB.Menu reportsSalemanCompanyProductWiseTP 
               Caption         =   "Company Product Wise (TP)"
            End
            Begin VB.Menu reportsSalemanCompanyProductWise 
               Caption         =   "Company Product Wise"
            End
         End
      End
      Begin VB.Menu ReportsReports 
         Caption         =   "Reports"
         Begin VB.Menu Purchase 
            Caption         =   "Purchase"
            Begin VB.Menu ReportsPurchase 
               Caption         =   "Over All"
            End
            Begin VB.Menu ReportsPurWareHouseWise 
               Caption         =   "WareHouse Wise"
            End
            Begin VB.Menu ReportsPurchaseReport 
               Caption         =   "Product Wise"
            End
            Begin VB.Menu ReportsPurchasePartyWise 
               Caption         =   "Party Wise"
            End
            Begin VB.Menu ReportsPurchaseInvoiceWise 
               Caption         =   "Invoice Wise"
            End
            Begin VB.Menu reportspurchaseDatewise 
               Caption         =   "Date Wise"
            End
         End
         Begin VB.Menu ReportsPurchaseReturn 
            Caption         =   "Purhcase Return"
            Begin VB.Menu ReportsRpurchaseProductWise 
               Caption         =   "Proudct wise"
            End
            Begin VB.Menu ReportsRpurchasePartyWise 
               Caption         =   "Party Wise"
            End
            Begin VB.Menu ReportsRpurchaseInvoiceWise 
               Caption         =   "Invoice Wise"
            End
         End
         Begin VB.Menu ReportsReturn 
            Caption         =   "Sale Return"
            Begin VB.Menu ReportsReturntSaleProductWiseSalesman 
               Caption         =   "Salesman Wise"
            End
            Begin VB.Menu ReportsReturnSaleProductWise 
               Caption         =   "Product Wise"
            End
            Begin VB.Menu ReportsReturnSalePartyWise 
               Caption         =   "Party Wise"
            End
            Begin VB.Menu ReportsReturnSaleInvoiceWise 
               Caption         =   "Invoice Wise"
            End
         End
         Begin VB.Menu ReportsBr6 
            Caption         =   "-"
         End
         Begin VB.Menu ReportsNevigation 
            Caption         =   "Stock Transfer"
         End
         Begin VB.Menu BRR4 
            Caption         =   "-"
         End
         Begin VB.Menu ReportsDayBook 
            Caption         =   "Day Book"
         End
         Begin VB.Menu ReportsDailySaleReport 
            Caption         =   "Daily Sale Report"
         End
         Begin VB.Menu reportsbr7 
            Caption         =   "-"
         End
         Begin VB.Menu reportsItemLedger 
            Caption         =   "Item Ledger"
         End
         Begin VB.Menu ReportsBr4 
            Caption         =   "-"
         End
         Begin VB.Menu ReportsGain 
            Caption         =   "Gain"
            Begin VB.Menu ReportsGainOverAll 
               Caption         =   "Over All"
            End
            Begin VB.Menu ReportsGainDateWise 
               Caption         =   "Company Wise"
            End
         End
         Begin VB.Menu BR6 
            Caption         =   "-"
         End
         Begin VB.Menu ReportsStockReports 
            Caption         =   "Stock Reports"
            Begin VB.Menu ReportsStockOverAll 
               Caption         =   "Over All"
            End
            Begin VB.Menu StockReportCompanyWise 
               Caption         =   "Company Wise"
            End
            Begin VB.Menu ReportsStockReport 
               Caption         =   "Report"
            End
            Begin VB.Menu ReportsStockInventory 
               Caption         =   "Inventory"
            End
         End
         Begin VB.Menu DFBR2 
            Caption         =   "-"
         End
         Begin VB.Menu PAYREC 
            Caption         =   "PAY / REC"
         End
         Begin VB.Menu ReportsExpenses 
            Caption         =   "Expenses"
         End
         Begin VB.Menu ReportCashReceive 
            Caption         =   "Cash Receive"
         End
         Begin VB.Menu ReportCashPayment 
            Caption         =   "Cash Payment"
         End
         Begin VB.Menu ReportJournalVoucher 
            Caption         =   "Journal Voucher"
         End
         Begin VB.Menu DFSBR4 
            Caption         =   "-"
         End
         Begin VB.Menu VIEWS 
            Caption         =   "Views"
            Begin VB.Menu ReportsPartyDayWise 
               Caption         =   "Party Day Wise"
            End
            Begin VB.Menu ReportsParty 
               Caption         =   "Party"
            End
            Begin VB.Menu ReportsProduct 
               Caption         =   "Product"
            End
         End
      End
   End
   Begin VB.Menu Ledgers 
      Caption         =   "Ledgers"
      Begin VB.Menu LedgersParty 
         Caption         =   "Account"
      End
      Begin VB.Menu LedgerLedger 
         Caption         =   "Ledger"
      End
      Begin VB.Menu FDGB4 
         Caption         =   "-"
      End
   End
   Begin VB.Menu FinalAccount 
      Caption         =   "Final Account"
      Begin VB.Menu Trialbalance 
         Caption         =   "Trialbalance"
      End
   End
   Begin VB.Menu Utility 
      Caption         =   "Utility"
      Begin VB.Menu DSFB3 
         Caption         =   "-"
      End
      Begin VB.Menu UtilityCalculator 
         Caption         =   "Calculator"
      End
      Begin VB.Menu UtilityChangePassword 
         Caption         =   "Change Password"
      End
      Begin VB.Menu UtilityBackup 
         Caption         =   "Backup"
      End
      Begin VB.Menu UtilityClearDataBase 
         Caption         =   "Clear DataBase"
      End
   End
End
Attribute VB_Name = "MAIN"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub AOPEN_Click()
'frmOPB.Show vbModal
End Sub

Private Sub BANTRAN_Click()
'FRMTRA.Show vbModal
End Sub

Private Sub BR1_Click()
frmPRODUCT.Show vbModal
End Sub

Private Sub BR4_Click()
frmNPRODUCT.Show vbModal
End Sub

Private Sub CALC_Click()
Shell "C:\WINDOWS\system32\calc.exe", vbNormalFocus
End Sub

Private Sub CLED_Click()
FRMCMLD2.Show vbModal
End Sub

Private Sub CLEDABA_Click()
Y = MsgBox("Are U Sure U Want To Clear DataBase", vbYesNo)
If Y = vbYes Then
CN.Execute "DELETE FROM STOCK"
CN.Execute "DELETE FROM PLEDGER"
CN.Execute "DELETE FROM ORDERS"
CN.Execute "DELETE FROM GIN"
CN.Execute "DELETE FROM GRN"
CN.Execute "DELETE FROM ISSUE"
CN.Execute "DELETE FROM RECEIVES"
CN.Execute "DELETE FROM SRETURN"
CN.Execute "DELETE FROM PRETURN"
'CN.Execute "DELETE LEDGER"
'CN.Execute "DELETE PARTY"
'CN.Execute "DELETE PRODUCT"
MsgBox "DataBase Is Cleared"
Else
End If
End Sub

Private Sub DSAL_Click()
frmDSR.Show vbModal
End Sub

Private Sub Command1_Click()
frmNewUser.Show vbModeless, Me
frmUsers.Show vbModeless, Me
'Dim DT1, dt2 As Date
'DT1 = "01-jan-2007"
'dt2 = "01-jan-2007"
'
'Set Rs = New ADODB.Recordset
'Rs.Open "select p.pid,p.name, (select isnull(sum(l.debit),0)- isnull(sum(l.credit),0) from pledger l where date < @dt1 and l.pid=p.pid) as Opening, isnull(sum(l.debit),0)- isnull(sum(l.credit),0) as 'BetweenDates', (select isnull(sum(l.debit),0)- isnull(sum(l.credit),0) from pledger l where date <= @dt2 and l.pid=p.pid) as Clossing  from pledger l,party p where l.pid=p.pid and date between @dt1 and @dt2 group by p.pid,p.name having isnull(sum(l.debit),0)- isnull(sum(l.credit),0)<>0", CN, adOpenStatic, adLockOptimistic
'rptOpeningTrial.Database.SetDataSource = "01-jan-2007" 'And "01-jan-2007"
'ReportsViewer.CRViewer1.ReportSource = rptOpeningTrial
'ReportsViewer.CRViewer1.ViewReport
'ReportsViewer.Show vbModal
End Sub

Private Sub DCPR_Change()
Set Rsp = New ADODB.Recordset
If DCPR.Text <> "" Then
Rsp.Open "SELECT prid,name,(name + ', ' + name) as CNAME FROM PRODUCT WHERE NAME Like '" & DCPR.Text & "%' ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
Else
Rsp.Open "SELECT prid,name,(cast(prid as varchar) + ', ' + name) as CNAME FROM PRODUCT ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
End If
Set DCPR.DataSource = Rsp
DCPR.DataMember = Rsp.DataMember
'DCPR.DataField = Rs.datafie
DCPR.clear

Do While Not Rsp.EOF
    DCPR.AddItem Rsp("prid") & " - " & Rsp("name")
    Rsp.MoveNext
Loop
Rsp.Close
End Sub

Private Sub DCPR_KeyPress(KeyAscii As Integer)
DCPR_Change
End Sub

Private Sub EX_Click()
Un_LOAd
End
End Sub

Private Sub Form_Activate()
check_constraints
lblCompany.Caption = V_Company
lblAddress.Caption = V_CompanyAddress
'UnloadAllReports
'All_Reports_Path

'''''' DEL ME
'Set Rsp = New ADODB.Recordset
'If DCPR.Text <> "" Then
'Rsp.Open "SELECT prid,name,(name + ', ' + name) as CNAME FROM PRODUCT WHERE NAME Like '" & DCPR.Text & "%' ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
'Else
'Rsp.Open "SELECT prid,name,(cast(prid as varchar) + ', ' + name) as CNAME FROM PRODUCT ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
'End If
'Set DCPR.DataSource = Rsp
'DCPR.DataMember = Rsp.DataMember
''DCPR.DataField = Rs.datafie
'
'Do While Not Rsp.EOF
'    DCPR.AddItem Rsp("name")
'    Rsp.MoveNext
'Loop
'Rsp.Close

End Sub

Private Sub Form_Load()
''''''''''''''''''' theme
'Set OBJ = New clsFunction
'OBJ.clear Me
'''''''''''''''''''''''''''''
'CONN = "Provider=SQLOLEDB.1;Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=Kashif_Traders2;Data Source=DSM"
'If CN.State = 1 Then
'CN.Close
'End If
'CN.CursorLocation = adUseClient
'CN.Open CONN
'V_DataBase=""
'Me.Picture.Width = Me.Width
'Me.Picture.Render  = Me.Height
check_constraints
'lblDate.Top = ScaleHeight - 150
'lblDate.Left = 0
'lblDate.Width = ScaleWidth

'lblCompany.Top = ScaleHeight - 100
'lblCompany.Left = 0
'lblCompany.Width = ScaleWidth

'CRViewer1.Height = ScaleHeight
'V_Company = "Sr_Traders"
Call Builtin_Account
End Sub

Function check_constraints()
If v_Check_Constraints = True Then
ReportsReports.Visible = False
vouchersVouchers.Visible = False
Ledgers.Visible = False
NewProduct.Visible = False
FinalAccount.Visible = False
UtilityClearDataBase.Visible = False
ReportsView.Visible = False
Else
ReportsReports.Visible = True
vouchersVouchers.Visible = True
Ledgers.Visible = True
NewProduct.Visible = True
FinalAccount.Visible = True
UtilityClearDataBase.Visible = True
ReportsView.Visible = True
End If
End Function

Function Builtin_Account()
NEW_ACC ("CAPITAL")
NEW_ACC ("CASH")
NEW_ACC ("PURCHASE")
NEW_ACC ("SALE")
NEW_ACC ("FREIGHT")
NEW_ACC ("Scheme Less")
NEW_ACC ("DISCOUNT")
NEW_ACC ("COMMISSION")
NEW_ACC ("EXTRA COMMISSION")
NEW_ACC ("SMS")
NEW_ACC ("MISC EXPENSES")
NEW_ACC ("SALE TAX")
NEW_ACC ("DEPARTMENT ISSUE")
NEW_ACC ("INCOME TAX")
NEW_ACC ("Scheme")
NEW_ACC ("Scheme Commission")
End Function

Private Sub JRNV_Click()
FRMGV.Show vbModal
End Sub

Private Sub Form_Unload(Cancel As Integer)
Dim U
U = MsgBox("Are You Sure You Want To Exit", vbYesNo + vbCritical, "Confirm........?")
If U = vbYes Then
If MsgBox("Do You Want To Take Backup", vbYesNo, "Backup........?") = vbYes Then frmBACKUP.Show vbModal
    Cancel = 0
    End
Else
Cancel = 1
End If
End Sub

'Private Sub Label3(3)_Click()
'ReportsViewer.CRViewer1.ReportSource = rptTRBAL
'ReportsViewer.CRViewer1.ViewReport
'ReportsViewer.Show vbModal
'End Sub

Private Sub Label2_Click()
        'rptSaleReport.RecordSelectionFormula = "{STOCK.ETYPE} ='SALE'"
        'ReportsViewer.CRViewer1.ReportSource = rptDailySaleReport
        'ReportsViewer.CRViewer1.ViewReport
        'ReportsViewer.Show vbModal
End Sub

Private Sub Label3_Click()
'        ReportsViewer.CRViewer1.ReportSource = rptSale1
 '       ReportsViewer.CRViewer1.ViewReport
 '       ReportsViewer.Show vbModal
End Sub

Private Sub Label4_Click()
'rptDailySaleReport.OpenSubreport ("rptSaleReport")
'rptDailySaleReport.Text1.SetText "Daily Sale Report"
'rptDailySaleReport.OpenSubreport("rptSaleReport").RecordSelectionFormula = "{STOCK.ETYPE}='RSALE'"
'rptDailySaleReport.RecordSelectionFormula = "{STOCK.ETYPE}='SALE'"
'rptDailySaleReport.Subreport1_SumofAMOUNT4.Value
'rptDailySaleReport.UnboundNumber11.PmString = rptDailySaleReport.Subreport1_SumofAMOUNT4.Value
'rptSaleReport.RecordSelectionFormula = "{STOCK.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {STOCK.ETYPE}='RSALE'"
'rptDayBook.RecordSelectionFormula = "{PARTY.TYPE}<>'TAX'"
'ReportsViewer.CRViewer1.ReportSource = rptDayBook 'rptDailySaleReport 'rptLEDGER
'ReportsViewer.CRViewer1.ViewReport
'ReportsViewer.Show vbModal
End Sub

Private Sub Label5_Click()
'ReportsViewer.CRViewer1.ReportSource = rptTb
'ReportsViewer.CRViewer1.ViewReport
'ReportsViewer.Show vbModal
'ReportsViewer.CRViewer1.ReportSource = rptTRLBAL
'ReportsViewer.CRViewer1.ViewReport
'ReportsViewer.Show vbModal
End Sub

Private Sub Label6_Click()
'ReportsViewer.CRViewer1.ReportSource = rptTRBAL
'ReportsViewer.CRViewer1.ViewReport
'ReportsViewer.Show vbModal
End Sub

Private Sub LedgerLedger_Click()
ReportDate.Caption = "Ledger Report"
ReportDate.TY.Text = "LEDGER"
ReportDate.Show vbModal
End Sub

Private Sub LedgersParty_Click()
frmACCLD.Show vbModal
End Sub

Private Sub LOGOFF_Click()
Me.Hide
LoginForm.Show
End Sub

Private Sub MSR_Click()
frmDSRM.Show vbModal
End Sub

Private Sub NCH_Click()
frmNEW.Show vbModal
End Sub

Private Sub NORDER_Click()
frmORDER.Show vbModal
End Sub

Private Sub ORDERSVIEW_Click()
frmOVD.Show vbModal
End Sub

Private Sub PARVIEW_Click()
rePRV.Show vbModal
End Sub

Private Sub NewAccount_Click()
frmNewAccount.Show vbModal
End Sub

Private Sub NewProduct_Click()
frmNewProduct.Show vbModal
End Sub

Private Sub NewProductRates_Click()
frmProductRate.Show vbModal
End Sub

Private Sub NewWareHouse_Click()
WareHouse.Show vbModal
End Sub

Private Sub OtherSaleReports_Click()
Form1.Show vbModal
End Sub

Private Sub PAYREC_Click()
frmPARE.Show vbModal
End Sub

Private Sub PFP_Click()
FRMPFP.Show vbModal
End Sub

Private Sub PLEDG_Click()
FRMCMLD.Show vbModal
End Sub

Private Sub PRETURN_Click()
frmPRET.Show vbModal
End Sub

Private Sub PTP_Click()
FRMPTP.Show vbModal
End Sub

Private Sub PUR_Click()
FRMGR1.Show vbModal
End Sub

Private Sub PURCHASEREP_Click()
frmPRD.Show vbModal
End Sub

Private Sub PVIEW_Click()
frmPV.Show vbModal
End Sub

Private Sub rd_Click()
frmPrdRate.Show vbModal
End Sub

Private Sub RECEIVE_Click()
frmRECEIVE.Show vbModal
End Sub

Private Sub SALE_Click()
frmGIN1.Show vbModal
End Sub

Private Sub SALESMANORDERS_Click()
frmSORDERS.Show vbModal
End Sub

Private Sub SALESMANSTOCK_Click()
frmSREP.Show vbModal
End Sub

Private Sub SRETURN_Click()
frmSRET.Show vbModal
End Sub

Private Sub STCK_Click()
FRMCSTD.Show vbModal
End Sub

Private Sub ReportRsPurchaseSaleManWise_Click()
ReportDate.Caption = "Return Purchase SalesMan Wise"
ReportDate.TY.Text = "RPURCHASE1"
ReportDate.Show vbModal
End Sub

Private Sub ReportSaleCCSale_Click()
ReportDate.Caption = "Sale Report"
ReportDate.TY.Text = "CCSale"
ReportDate.Show vbModal
End Sub

Private Sub ReportSaleReport_Click()
ReportDate.Caption = "Sale Report"
ReportDate.TY.Text = "SALE"
ReportDate.Show vbModal
End Sub

Private Sub ReportSaleSMCSale_Click()
ReportDate.Caption = "Sale Report"
ReportDate.TY.Text = "smCSale"
ReportDate.Show vbModal
End Sub

Private Sub ReportSaleSMSCWise_Click()
ReportDate.Caption = "Sale Report"
ReportDate.TY.Text = "SMSCWise"
ReportDate.Show vbModal
End Sub

Private Sub ReportsGainDateWise_Click()
ReportDate.Caption = "Company Wise Gain Report"
ReportDate.TY.Text = "GainDateWise"
ReportDate.Show vbModal
End Sub

Private Sub ReportsGainOverAll_Click()
ReportDate.Caption = "Over All Gain Report"
ReportDate.TY.Text = "GainOverAll"
ReportDate.Show vbModal
End Sub

Private Sub reportsItemLedger_Click()
ReportDate.Caption = "Item Ledger"
ReportDate.TY.Text = "ItemLedger"
ReportDate.dcPname.Visible = True
ReportDate.Label1(4).Visible = True
ReportDate.Show vbModal
End Sub

Private Sub ReportsSale_Click()
ReportDate.Caption = "Sale Report"
ReportDate.TY.Text = "SALE"
ReportDate.Show vbModal
End Sub

Private Sub ReportSaleReportproductwisesalesman_Click()
ReportDate.Caption = "Sale Report"
ReportDate.TY.Text = "SaleManProductWise"
ReportDate.Show vbModal
End Sub

Private Sub ReportsDailySaleReport_Click()
ReportDate.Caption = "Daily Sale Report"
ReportDate.TY.Text = "DSR"
ReportDate.Show vbModal
End Sub

Private Sub ReportsDayBook_Click()
ReportDate.Caption = "Day Book"
ReportDate.TY.Text = "DAYBOOK"
ReportDate.Show vbModal
End Sub

Private Sub ReportsExpenses_Click()
ReportDate.Caption = "Expense Report"
ReportDate.TY.Text = "EXPENSE"
ReportDate.Show vbModal
End Sub

Private Sub ReportCashPayment_Click()
ReportDate.Caption = "Cash Payment Report"
ReportDate.TY.Text = "PAYMENT"
ReportDate.Show vbModal
End Sub

Private Sub ReportCashReceive_Click()
ReportDate.Caption = "Cash Receive Report"
ReportDate.TY.Text = "RECEIVE"
ReportDate.Show vbModal
End Sub

Private Sub ReportJournalVoucher_Click()
ReportDate.Caption = "Journal Voucher Report"
ReportDate.TY.Text = "JV"
ReportDate.Show vbModal
End Sub

Private Sub ReportsNevigation_Click()
ReportDate.Caption = "Nevigation Report"
ReportDate.TY.Text = "NEVIGATION"
ReportDate.Show vbModal
End Sub

Private Sub ReportsParty_Click()
ReportsViewer.CRViewer1.ReportSource = rptPartyView
ReportsViewer.CRViewer1.ViewReport
ReportsViewer.Show vbModal
End Sub

Private Sub ReportsPartyDayWise_Click()
'Unload rptPartyViewDayWise
ReportsViewer.CRViewer1.ReportSource = rptPartyViewDayWise
ReportsViewer.CRViewer1.ViewReport
ReportsViewer.Show vbModal
End Sub

Private Sub ReportsProduct_Click()
'rptSaleVoucher.RecordSelectionFormula = "{STOCK.ETYPE}='SALE' AND {STOCK.dcno}=" & T1.Text
ReportsViewer.CRViewer1.ReportSource = ProductView
ReportsViewer.CRViewer1.ViewReport
ReportsViewer.Show vbModal
End Sub

Private Sub STCK2_Click()
FRMCSTRD.Show vbModal
End Sub

Private Sub STOPEN_Click()
'FRMOPS.Show vbModal
End Sub

Private Sub TBSLANC_Click()
frmTBD.Show vbModal
End Sub

Private Sub USERS1_Click()
'Users.Show vbModal
End Sub

Private Sub ReportsPurchaseInvoiceWise_Click()
ReportDate.Caption = "Purchase Report"
ReportDate.TY.Text = "PURCHASEINW"
ReportDate.Show vbModal
End Sub

Private Sub reportspurchaseDatewise_Click()
ReportDate.Caption = "Purchase Report Date Wise"
ReportDate.TY.Text = "PurchaseDateWise"
ReportDate.Show vbModal
End Sub

Private Sub ReportsPurWareHouseWise_Click()
ReportDate.Caption = "Purchase Report WareHouse Wise"
ReportDate.TY.Text = "PurWareHouseWise"
ReportDate.Show vbModal
End Sub

Private Sub ReportsPurchasePartyWise_Click()
ReportDate.Caption = "Purchase Party Wise Report"
ReportDate.TY.Text = "PurPartyWise"
ReportDate.Show vbModal
End Sub

Private Sub reportsSalemanCompanyPartyWise_Click()
ReportDate.Caption = "Sale Report Company Party Wise"
ReportDate.TY.Text = "SaleManCompanyPartyWise"
ReportDate.Show vbModal
End Sub

Private Sub reportsSalemanCompanyProductWise_Click()
ReportDate.Caption = "Sale Report Company Product Wise"
ReportDate.TY.Text = "SaleManCompanyProductWise"
ReportDate.Show vbModal
End Sub

Private Sub reportsSalemanCompanyProductWiseTP_Click()
ReportDate.Caption = "Sale Report Company Product Wise (TP)"
ReportDate.TY.Text = "SaleManCompanyProductWiseTP"
ReportDate.Show vbModal
End Sub

Private Sub reportsSalemanMothWise_Click()
ReportDate.Caption = "SaleMan Sale Month Wise"
ReportDate.TY.Text = "SaleManMonthWise"
ReportDate.Show vbModal
End Sub

Private Sub ReportsSaleManPartyWise2_Click()
ReportDate.Caption = "Sale Report"
ReportDate.TY.Text = "SaleManPartyWise2"
ReportDate.Show vbModal
End Sub

Private Sub ReportsSaleWareHouseWise_Click()
ReportDate.Caption = "Sale Report WareHouse Wise"
ReportDate.TY.Text = "SaleWareHouseWise"
ReportDate.Show vbModal
End Sub

Private Sub ReportsPurchaseReport_Click()
ReportDate.Caption = "Purchase Report"
ReportDate.TY.Text = "PURCHASE"
ReportDate.Show vbModal
End Sub

Private Sub ReportsPurchase_Click()
ReportDate.Caption = "Purchase Report OverAll"
ReportDate.TY.Text = "PURCHASE"
ReportDate.Show vbModal
End Sub

Private Sub ReportsPurchaseSaleManWise_Click()
ReportDate.Caption = "Purchase Report Salesman Wise"
ReportDate.TY.Text = "PURCHASE1"
ReportDate.Show vbModal
End Sub

Private Sub ReportsReturnSaleInvoiceWise_Click()
ReportDate.Caption = "Sale Report"
ReportDate.TY.Text = "RSALEINW"
ReportDate.Show vbModal
End Sub

Private Sub ReportsReturnSalePartyWise_Click()
ReportDate.Caption = "Sale Report"
ReportDate.TY.Text = "RSALEPW"
ReportDate.Show vbModal
End Sub

Private Sub ReportsReturnSaleProductWise_Click()
ReportDate.Caption = "Sale Report"
ReportDate.TY.Text = "RSALE"
ReportDate.Show vbModal
End Sub

Private Sub ReportsReturntSaleProductWiseSalesman_Click()
ReportDate.Caption = "Sale Return Report SalesMan Wise"
ReportDate.TY.Text = "RSALE1"
ReportDate.Show vbModal
End Sub

Private Sub ReportsRpurchaseProductWise_Click()
ReportDate.Caption = "Purchase Return Report"
ReportDate.TY.Text = "RPURCHASE"
ReportDate.Show vbModal
End Sub

Private Sub ReportsSaleInvoiceWise_Click()
ReportDate.Caption = "Sale Report"
ReportDate.TY.Text = "SALEINW"
ReportDate.Show vbModal
End Sub

Private Sub ReportsSaleManIssue_Click()
ReportDate.Caption = "SALEMAN ISSUE Report"
ReportDate.TY.Text = "SALEMANISSUE"
ReportDate.Show vbModal
End Sub

Private Sub ReportsSaleManReturn_Click()
ReportDate.Caption = "SALEMAN RETURN Report"
ReportDate.TY.Text = "SALEMANRETURN"
ReportDate.Show vbModal
End Sub

Private Sub ReportsSaleManPartyWise_Click()
ReportDate.Caption = "Sale Report"
ReportDate.TY.Text = "SaleManPartyWise"
ReportDate.Show vbModal
End Sub

Private Sub ReportsSalePartyWise_Click()
ReportDate.Caption = "Sale Report Party Wise"
ReportDate.TY.Text = "SalePartyWise"
ReportDate.Show vbModal
End Sub

Private Sub ReportsSaleSectorWise_Click()
ReportDate.Caption = "Sale Report"
ReportDate.TY.Text = "SALESCW"
ReportDate.Show vbModal
End Sub

Private Sub reportsSaleDatewise_Click()
ReportDate.Caption = "Sale Report Date Wise"
ReportDate.TY.Text = "SaleDateWise"
ReportDate.Show vbModal
End Sub

Private Sub ReportsSalesmanSaleReport_Click()
ReportDate.Caption = "Daily Sale Report"
ReportDate.TY.Text = "SMSALE"
ReportDate.Show vbModal
End Sub

Private Sub ReportsStockInventory_Click()
ReportDate.Caption = "Stock Report"
ReportDate.TY.Text = "STOCKINVENTORY"
ReportDate.Show vbModal
End Sub

Private Sub ReportsStockOverAll_Click()
ReportDate.TY.Text = "STOCKOVERALL"
ReportDate.Caption = "Stock Report"
ReportDate.Show vbModal
End Sub

Private Sub ReportsStockReport_Click()
ReportDate.TY.Text = "STOCK"
ReportDate.Caption = "Stock Report"
ReportDate.Show vbModal
End Sub

Private Sub salemanSaleMonthWise2_Click()
Dim crystalApp As CRAXDRT.Application
Dim crystalRep As CRAXDRT.Report
Dim myTextObject As CRAXDRT.TextObject
Set crystalApp = New CRAXDRT.Application

Set crystalRep = crystalApp.OpenReport(App.Path & "\Reports\" & "MonthWiseSalemanSale.rpt") 'Opens the report
UpdateReportCredentials crystalRep
      
      
'        With crystalRep
'        .Sections("RH").ReportObjects("txtHead").SetText Me.Caption
'        .Sections("RH").ReportObjects("txtDate").SetText "Date Between:" & Me.DTS.Value & " " & Me.DTE.Value
'        'Dim recordSelection As String
'        'recordSelection = "{stock_MAIN.DATE} in datetime('" & DTS.Value & "')  to  datetime('" & DTE.Value & "') AND {stock_Main.etype}='SALE'"
'       If DC1.Text = "" Then
'        .RecordSelectionFormula = "{trans_MAIN.DATE} in datetime('" & Me.DTS.Value & "')  to  datetime('" & Me.DTE.Value & "') AND {trans_MAIN.etype}='SALE'"
'        Else
'        .RecordSelectionFormula = "{trans_MAIN.DATE} in datetime('" & Me.DTS.Value & "')  to  datetime('" & Me.DTE.Value & "') AND {trans_MAIN.etype}='SALE' and ({party_1.name} = '" & DC1.Text & "' OR {party_1.name} = '" & DC2.Text & "' OR {party_1.name} = '" & DC3.Text & "' OR {party_1.name} = '" & DC4.Text & "' OR {party_1.name} = '" & DC5.Text & "' OR {party_1.name} = '" & DC6.Text & "')"
'        End If
'        End With
        ReportsViewer.CRViewer1.ReportSource = crystalRep
        ReportsViewer.CRViewer1.ViewReport
        ReportsViewer.Show vbModal

End Sub

Private Sub StockReportCompanyWise_Click()
ReportDate.TY.Text = "StockCompanyWise"
ReportDate.Caption = "Stock Report Company Wise"
ReportDate.Show vbModal
End Sub

Private Sub Tm1_Timer()
lblDate.Caption = Format(Date, "dd - MMM - yy") & " " & Format(Date, "DDDD") & " " & Time
End Sub

Private Sub Trialbalance_Click()
frmTB.Show
End Sub

Private Sub UtilityBackup_Click()
frmBACKUP.Show vbModal
End Sub

Private Sub UtilityCalculator_Click()
Shell "C:\WINDOWS\system32\calc.exe"
End Sub

Private Sub UtilityChangePassword_Click()
ChangPW.Show vbModal
End Sub

Private Sub UtilityClearDataBase_Click()
If MsgBox("Remove All Data", vbOKCancel) = vbOK Then
    If MsgBox("You Are About To Remove All your Data in Database Are you Sure To Remove All Data...? ", vbYesNo) = vbYes Then
        CN.Execute "Delete from Stock"
        CN.Execute "Delete from Stock_main"
        CN.Execute "Delete from pledger"
    End If
End If
End Sub

Private Sub VoucherSalemanIssue_Click()
frmSISSUE.Show vbModal
End Sub

Private Sub VoucherSalemanReturn_Click()
frmSRETURN.Show vbModal
End Sub

Private Sub VouchersJournalVoucher_Click()
frmJV.Show vbModal
End Sub

Private Sub VouchersPayment_Click()
FRMPTP.Show vbModal
End Sub

Private Sub VouchersProudctNevegation_Click()
frmPN.Show vbModal
End Sub

Private Sub VouchersPurchase_Click()
frmPUR.Show vbModal
End Sub

Private Sub VouchersPurchaseReturn_Click()
frmRPUR.Show vbModal
End Sub

Private Sub VouchersReceipt_Click()
FRMPFP.Show vbModal
End Sub

Private Sub VouchersSale_Click()
frmSAL1.Show vbModal
End Sub

Private Sub VouchersSaleReturn_Click()
frmRSAL.Show vbModal
End Sub
Function Un_LOAd()
End Function
