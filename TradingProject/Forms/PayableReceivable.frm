VERSION 5.00
Object = "{5E9E78A0-531B-11CF-91F6-C2863C385E30}#1.0#0"; "msflxgrd.ocx"
Object = "{831FDD16-0C5C-11D2-A9FC-0000F8754DA1}#2.1#0"; "mscomctl.ocx"
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "mscomct2.ocx"
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "msdatlst.ocx"
Object = "{D750EAB9-EB7B-450B-B9B6-0A0A17E13DCA}#1.0#0"; "xp.ocx"
Begin VB.Form frmPARE 
   BackColor       =   &H00C0C0C0&
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "PAYABLE / RECEIVEABLE"
   ClientHeight    =   8475
   ClientLeft      =   45
   ClientTop       =   735
   ClientWidth     =   12225
   ForeColor       =   &H000040C0&
   Icon            =   "PayableReceivable.frx":0000
   KeyPreview      =   -1  'True
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "PayableReceivable.frx":0442
   ScaleHeight     =   8475
   ScaleWidth      =   12225
   ShowInTaskbar   =   0   'False
   StartUpPosition =   1  'CenterOwner
   Visible         =   0   'False
   Begin MSComCtl2.DTPicker DT1 
      Height          =   375
      Left            =   840
      TabIndex        =   0
      Top             =   120
      Width           =   1815
      _ExtentX        =   3201
      _ExtentY        =   661
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   178
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   12640511
      CalendarTitleBackColor=   16777215
      CalendarTrailingForeColor=   0
      CustomFormat    =   "dd-MMM-yy"
      Format          =   60424193
      CurrentDate     =   37474
   End
   Begin XPLayout.XPButton COMMAND1 
      Height          =   375
      Left            =   9480
      TabIndex        =   4
      Top             =   600
      Width           =   1215
      _ExtentX        =   2143
      _ExtentY        =   661
      BTYPE           =   3
      TX              =   "&Show"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   178
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   16711680
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "PayableReceivable.frx":159B18
      PICN            =   "PayableReceivable.frx":159B34
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin MSDataListLib.DataCombo dcCity 
      Height          =   360
      Left            =   840
      TabIndex        =   2
      Top             =   600
      Width           =   3615
      _ExtentX        =   6376
      _ExtentY        =   635
      _Version        =   393216
      Appearance      =   0
      ForeColor       =   16512
      ListField       =   ""
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSFlexGridLib.MSFlexGrid FG1 
      Height          =   6495
      Left            =   120
      TabIndex        =   5
      Top             =   1080
      Width           =   12015
      _ExtentX        =   21193
      _ExtentY        =   11456
      _Version        =   393216
      Cols            =   6
      AllowBigSelection=   0   'False
      Appearance      =   0
   End
   Begin MSFlexGridLib.MSFlexGrid FG2 
      Height          =   255
      Left            =   120
      TabIndex        =   8
      Top             =   7560
      Width           =   12015
      _ExtentX        =   21193
      _ExtentY        =   450
      _Version        =   393216
      Rows            =   1
      Cols            =   6
      FixedRows       =   0
      AllowBigSelection=   0   'False
      Appearance      =   0
   End
   Begin MSDataListLib.DataCombo dcSector 
      Height          =   360
      Left            =   6120
      TabIndex        =   3
      Top             =   600
      Width           =   2415
      _ExtentX        =   4260
      _ExtentY        =   635
      _Version        =   393216
      Appearance      =   0
      ForeColor       =   16512
      ListField       =   ""
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   178
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComctlLib.ProgressBar pgb1 
      Height          =   255
      Left            =   0
      TabIndex        =   10
      Top             =   7920
      Visible         =   0   'False
      Width           =   11295
      _ExtentX        =   19923
      _ExtentY        =   450
      _Version        =   393216
      Appearance      =   0
   End
   Begin MSDataListLib.DataCombo DC2 
      Height          =   330
      Index           =   0
      Left            =   5100
      TabIndex        =   1
      Top             =   120
      Width           =   3495
      _ExtentX        =   6165
      _ExtentY        =   582
      _Version        =   393216
      Appearance      =   0
      BackColor       =   12632319
      ForeColor       =   8388608
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Saleman"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   225
      Index           =   7
      Left            =   4320
      TabIndex        =   11
      Top             =   240
      Width           =   750
   End
   Begin VB.Label Label11 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Sector"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   300
      Left            =   5400
      TabIndex        =   9
      Top             =   600
      Width           =   675
   End
   Begin VB.Label Label6 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "City"
      BeginProperty Font 
         Name            =   "Times New Roman"
         Size            =   12.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   -1  'True
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000040&
      Height          =   300
      Left            =   360
      TabIndex        =   7
      Top             =   600
      Width           =   420
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000040&
      Height          =   240
      Index           =   2
      Left            =   360
      TabIndex        =   6
      Top             =   120
      Width           =   435
   End
   Begin VB.Menu PAYBLE 
      Caption         =   "Print All"
   End
   Begin VB.Menu PrintPayable 
      Caption         =   "Print Payable"
   End
   Begin VB.Menu PrintReceivable 
      Caption         =   "Print Receivable"
   End
End
Attribute VB_Name = "frmPARE"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Function CL_FG1()
FG1.clear
FG1.TextMatrix(0, 1) = "Payalbe Party"
FG1.TextMatrix(0, 2) = "Amount"
FG1.TextMatrix(0, 3) = "Receivalbe Party"
FG1.TextMatrix(0, 4) = "Amount"

FG1.ColWidth(0) = 0
FG1.ColWidth(1) = 4300
FG1.ColWidth(2) = 1500
FG1.ColWidth(3) = 4300
FG1.ColWidth(4) = 1500
FG1.ColWidth(5) = 0

FG2.ColWidth(0) = FG1.ColWidth(0)
FG2.ColWidth(1) = FG1.ColWidth(1)
FG2.ColWidth(2) = FG1.ColWidth(2)
FG2.ColWidth(3) = FG1.ColWidth(3)
FG2.ColWidth(4) = FG1.ColWidth(4)
FG2.ColWidth(5) = FG1.ColWidth(5)

r = 1
Do Until FG1.Rows - 1
FG1.TextMatrix(r, 0) = r
r = r + 1
Loop
End Function

Private Sub Command1_Click()
Dim strWhere As String
CL_FG1
r = 1
C1 = 1
Set rsB = New ADODB.Recordset
strWhere = "WHERE P.PID=L.PID AND L.[DATE] <='" & DT1.Value & "'"
pgb1.Visible = True

'Dim totDebit As Double, totCredit As Double
'totDebit = 0
'totCredit = 0
'pgb1.Max = rsa.RecordCount
'Do Until rsa.EOF
'MS1.Rows = MS1.Rows + 1
'pgb1.Value = r
'Dim T As Double
'T = Round(rsa("Balance"), 2)
'If T < 0 Then
'MS1.TextMatrix(r, 3) = Abs(T)
'totCredit = totCredit + Abs(T)
'ElseIf T > 0 Then
'MS1.TextMatrix(r, 2) = T
'totDebit = totDebit + T
'End If
'
'MS1.TextMatrix(r, 0) = r
'MS1.TextMatrix(r, 1) = rsa("Name")
'rsa.MoveNext
'r = r + 1
'Loop
'rsa.Close
'r = r + 1
'MS1.Rows = MS1.Rows + 1
'MS1.TextMatrix(r, 1) = "TOTALS"
'MS1.TextMatrix(r, 2) = Round(totDebit, 2)
'MS1.TextMatrix(r, 3) = Round(totCredit, 2)
'MS1.ColAlignment(1) = 0
''''''''''''''''''

    If dcCity.Text <> "" Then strWhere = strWhere & " AND P.CITY='" & dcCity.Text & "'"
    'rs1.Open "SELECT DISTINCT PID FROM PLEDGER WHERE PID IS NOT NULL AND PID IN(SELECT PID FROM PARTY WHERE CITY='" & dcCity.Text & "' AND SECTOR='" & dcSector.Text & "')", CN, adOpenStatic, adLockOptimistic
    If dcSector.Text <> "" Then strWhere = strWhere & " AND P.SECTOR='" & dcSector.Text & "'"
    If DC2(0).Text <> "" Then strWhere = strWhere & " AND L.SID=" & ID_NaME("PARTY", "PID", "NAME", DC2(0).Text)
    'If DC2(1).Text <> "" Then strWhere = strWhere & " AND L.SupplymanID=" & ID_NaME("PARTY", "PID", "NAME", DC2(1).Text)
    'rs1.Open "SELECT DISTINCT PID FROM PLEDGER WHERE PID IS NOT NULL AND PID IN(SELECT PID FROM PARTY WHERE SECTOR='" & dcSector.Text & "')", CN, adOpenStatic, adLockOptimistic
    'rs1.Open "SELECT DISTINCT PID FROM PLEDGER WHERE PID IS NOT NULL ORDER BY PID", CN, adOpenStatic, adLockOptimistic
    strWhere = strWhere & " AND P.PID IS NOT NULL AND P.TYPE<>'TAX'"
    rsB.Open "SELECT P.PID,P.[NAME],SUM(L.DEBIT) Debit,SUM(L.CREDIT) Credit,SUM(ISNULL(L.DEBIT, 0)-ISNULL(L.CREDIT, 0)) AS Balance FROM PARTY P,PLEDGER L " & strWhere & " group by P.Pid,P.[Name] ORDER BY [NAME]", CN
FG1.Rows = 1 + IIf(rsB.RecordCount > 0, rsB.RecordCount, 1)
Do Until rsB.EOF
'Set Rs = New ADODB.Recordset
'Rs.Open "SELECT NAME,TYPE FROM PARTY WHERE PID=" & rs1("PID") & "", CN, adOpenStatic, adLockOptimistic
'If Rs("TYPE") <> "TAX" Then
'Set RSB = New ADODB.Recordset
'RSB.Open "SELECT SUM(BAL) FROM BALAN WHERE PID=" & rs1!PID & " and date<='" & DT1.Value & "'", CN, adOpenStatic, adLockOptimistic
TT = Round(rsB("Balance"), 2)  'IIf(IsNull(rsB(0)), 0, rsB(0)) ' - IIf(IsNull(PC(0)), 0, PC(0))
If TT < 0 Then
'TT = TT * -1
FG1.TextMatrix(r, 1) = rsB!Name
FG1.TextMatrix(r, 2) = Abs(TT)
r = r + 1
Else 'If TT > 0 Then
FG1.TextMatrix(C1, 3) = rsB!Name
FG1.TextMatrix(C1, 4) = TT
C1 = C1 + 1
End If
'End If
rsB.MoveNext
Loop
rsB.Close
pgb1.Value = pgb1.Max
pgb1.Visible = False
FG1_TOT
End Sub

Private Sub DC2_Change(Index As Integer)
If CC = False Then
Set Rsp = New ADODB.Recordset
If DC2(Index).Text <> "" Then
Rsp.Open "SELECT NAME FROM PARTY WHERE NAME Like '" & UCase(DC2(Index).Text) & "%' AND (TYPE='SALESMAN' OR TYPE='SUPPLYMAN')  ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
Else
Rsp.Open "SELECT NAME FROM PARTY WHERE (TYPE='SALESMAN' OR TYPE='SUPPLYMAN') ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
End If
Set DC2(Index).RowSource = Rsp
DC2(Index).ListField = "NAME"
End If
End Sub

Private Sub DC2_GotFocus(Index As Integer)
DC2_Change Index
SendKeys "{F4}"
End Sub

Private Sub DC2_KeyDown(Index As Integer, KeyCode As Integer, Shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DCS_Change()
If CC = False Then
Set Rsp = New ADODB.Recordset
If DCS.Text <> "" Then
Rsp.Open "SELECT NAME FROM warehouse WHERE NAME Like '" & DCS.Text & "%' ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
Else
Rsp.Open "SELECT NAME FROM warehouse ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
End If
Set DCS.RowSource = Rsp
DCS.ListField = "NAME"
End If
End Sub

Private Sub dcCity_Change()
If CC = False Then
Set RS3 = New ADODB.Recordset
If dcCity.Text <> "" Then
RS3.Open "SELECT DISTINCT CITY FROM PARTY WHERE CITY Like '" & dcCity.Text & "%' AND CITY<>'' ORDER BY CITY", CN, adOpenStatic, adLockOptimistic
Else
RS3.Open "SELECT DISTINCT CITY FROM PARTY WHERE CITY<>' ' ORDER BY CITY", CN, adOpenStatic, adLockOptimistic
End If
Set dcCity.RowSource = RS3
dcCity.ListField = "CITY"
End If
End Sub

Private Sub dcCity_GotFocus()
dcCity_Change
End Sub

Private Sub dcCity_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Function FG1_TOT()
FG2.clear
'FG2.TextMatrix(0, 1) = "Total"
'FG2.TextMatrix(0, 3) = "Total"
For i = 1 To FG1.Rows - 1
FG2.TextMatrix(0, 2) = Val(FG2.TextMatrix(0, 2)) + Val(FG1.TextMatrix(i, 2))
FG2.TextMatrix(0, 4) = Val(FG2.TextMatrix(0, 4)) + Val(FG1.TextMatrix(i, 4))
Next i
FG2.TextMatrix(0, 0) = "Total"
End Function

Private Sub dcSector_Change()
If CC = False Then
Set RS3 = New ADODB.Recordset
If dcSector.Text <> "" Then
RS3.Open "SELECT DISTINCT SECTOR FROM PARTY WHERE SECTOR Like '" & dcSector.Text & "%' ORDER BY SECTOR", CN, adOpenStatic, adLockOptimistic
Else
RS3.Open "SELECT DISTINCT SECTOR FROM PARTY ORDER BY SECTOR", CN, adOpenStatic, adLockOptimistic
End If
Set dcSector.RowSource = RS3
dcSector.ListField = "SECTOR"
End If
End Sub

Private Sub dcSector_GotFocus()
dcSector_Change
SendKeys "{F4}"
End Sub

Private Sub dcSector_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub Form_Load()
''''''''''''''''''' theme
Set OBJ = New clsFunction
OBJ.clear Me
'''''''''''''''''''''''''''''

DT1.Value = Date
CL_FG1
FG1.ColAlignment(1) = Left
FG1.ColAlignment(3) = Left
'Set OBJ = New clsFunction
'OBJ.CHECK_date Me
End Sub

Private Sub PAYBLE_Click()
CN.Execute "DELETE TEMP"
r = 1
Do Until (FG1.TextMatrix(r, 1) = "" And FG1.TextMatrix(r, 3) = "")
'Set RsT = New ADODB.Recordset
'RsT.Open "SELECT * FROM TEMP", CN, adOpenStatic, adLockOptimistic
'RsT.AddNew
CN.Execute "Insert into temp (SNO, T1,N1,T2,N2) values(" & r & ",'" & FG1.TextMatrix(r, 1) & "'," & Val(FG1.TextMatrix(r, 2)) & ",'" & FG1.TextMatrix(r, 3) & "'," & Val(FG1.TextMatrix(r, 4)) & ")"
'RsT!T1 = FG1.TextMatrix(r, 1)
'RsT!N1 = Val(FG1.TextMatrix(r, 2))
'RsT!T2 = FG1.TextMatrix(r, 3)
'RsT!N2 = Val(FG1.TextMatrix(r, 4))
'RsT.Update
r = r + 1
Loop
Unload rptPaRe
UpdateReportCredentials rptPaRe
ReportsViewer.CRViewer1.ReportSource = rptPaRe
ReportsViewer.CRViewer1.ViewReport
ReportsViewer.Show vbModal
End Sub

Private Sub PrintPayable_Click()
PrintReport 1
End Sub

Private Sub PrintReceivable_Click()
PrintReport 0
End Sub

'ReportType 0 = Detors(Receivable) else Creditors(Payable)
Private Sub PrintReport(ReportType As Integer)
Dim crystalApp As CRAXDRT.Application
Dim crystalRep As CRAXDRT.Report
Dim myTextObject As CRAXDRT.TextObject
Set crystalApp = New CRAXDRT.Application
Set crystalRep = crystalApp.OpenReport(App.Path & "\Reports\" & "AccountBalanceReport.rpt") 'Opens the report

'Unload rptSaleVoucher
UpdateReportCredentials crystalRep
With Me
Set myTextObject = crystalRep.Sections("RH").ReportObjects("ReportTitle")
myTextObject.SetText "Balance Report "
'Set myTextObject = crystalRep.Sections("RH").ReportObjects("txtCompanyName")
'myTextObject.SetText V_Company
'Set myTextObject = crystalRep.Sections("RH").ReportObjects("txtCompanyAddress")
'myTextObject.SetText V_CompanyAddress
'crystalRep.Sections("PH").ReportObjects("txtPartyBalance").SetText Me.lblPartyBalance.Caption
End With

Dim strCriteria As String
strCriteria = "{PLEDGER.DATE} <= datetime('" & DT1.Value & "')"
If dcCity.Text <> "" Then strCriteria = strCriteria & " AND {PARTY.CITY}='" & dcCity.Text & "'"
If dcSector.Text <> "" Then strCriteria = strCriteria & " AND {PARTY.SECTOR}='" & dcSector.Text & "'"
strCriteria = strCriteria & " AND {PARTY.TYPE}<>'TAX'"
'rsB.Open "SELECT P.PID,P.[NAME],SUM(L.DEBIT) Debit,SUM(L.CREDIT) Credit,SUM(ISNULL(L.DEBIT, 0)-ISNULL(L.CREDIT, 0)) AS Balance FROM PARTY P,PLEDGER L " & strWhere & " group by P.Pid,P.[Name] ORDER BY [NAME]", CN

'crystalRep.AddGroup 2, "{party.city}", crGCAnyValue, crAscendingOrder
'crystalRep.DeleteGroup 0
crystalRep.RecordSelectionFormula = strCriteria '" AND {PLEDGER.ETYPE}='SALE' AND {PLEDGER.DEBIT}<>0"
'crystalRep.GroupSelectionFormula = "if isnull(Sum ({PLEDGER.DEBIT}, {PARTY.PID})) then 0 else Sum ({PLEDGER.DEBIT}, {PARTY.PID}) " & IIf(ReportType = 0, ">=", "<=") & " if isnull(Sum ({PLEDGER.CREDIT}, {PARTY.PID})) then 0 else Sum ({PLEDGER.CREDIT}, {PARTY.PID})"
crystalRep.GroupSelectionFormula = "(if isnull(Sum ({PLEDGER.DEBIT}, {PARTY.NAME})) then 0 else Sum ({PLEDGER.DEBIT}, {PARTY.NAME})) " & IIf(ReportType = 0, ">", "<") & " (if isnull(Sum ({PLEDGER.CREDIT}, {PARTY.NAME})) then 0 else Sum ({PLEDGER.CREDIT}, {PARTY.NAME}))"
'crystalRep.GroupSelectionFormula = "{@GroupBalance} > 0" '& IIf(ReportType = 0, ">=", "<=") & " 0 "
'If chkPrintOption.Value Then
'crystalRep.PrintOut
'Else
ReportsViewer.CRViewer1.ReportSource = crystalRep
ReportsViewer.CRViewer1.ViewReport
ReportsViewer.Show vbModal
'End If
End Sub
