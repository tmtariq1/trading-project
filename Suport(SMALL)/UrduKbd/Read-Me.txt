Urdu Phonetic Keyboard Installation Instructions

To install this file succesfully you must restart your computer in safe mode. To do so press the key F8 just as Windows begins to load up. The easiest way to get everything running is if you let the install program install to C:\Urdukbd

1) Press F8 to enter startup menu
2) Choose Safe-Mode with command-prompt
3) Choose to login as the Administrator
4) Enter the following commands. (Press Enter after each line)
cd \
c:
cd urdukbd
backup
install
5) If all is successfully done then the command-prompt window will close.
6) Re-start your system and start the computer as normal
7) Open up notepad and select the Urdu Keyboard layout. If Alif appears when you press A on the keyboard then the file was installed properly. Have fun :)
 Remember me in your prayers please :) I need Dua's lots of them ;)

Wassallam
Shehzad Ashiq Ali

Modifications:
14 May 2003:
. Modified the Installation Batch file so that it renamed
  the exisiting kbdurdu.dll file because otherwise Windows XP
  did not allow the file to be overwritten.

29 August 2003:
. Modified the keyboards layout. Y had incorrectly been mapped to the Arabic letter Alef-Maksurah (U+  0649). It has now been amended to correctly reference the Farsi Yeh (U+06CC) Glyph.
   Sorry for any inconveniences this may have caused.

//--------------------------
File Purposes
Backup - Backs up your existing Urdu Keyboard layout
Install - Installs the Phonetic Keyboard Layout
Restore - Restores the default Windows Keyboard Layout which you backed up (You did back up didn't you?)

Last Updated:
02:06 29/08/2003 Friday (Juma-tul-Mubarak)

http://go.to/shehzad
shehzadashiq@hotmail.com