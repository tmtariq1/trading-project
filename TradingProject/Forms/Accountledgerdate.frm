VERSION 5.00
Object = "{86CF1D34-0C5F-11D2-A9FC-0000F8754DA1}#2.0#0"; "MSCOMCT2.OCX"
Object = "{F0D2F211-CCB0-11D0-A316-00AA00688B10}#1.0#0"; "MSDATLST.OCX"
Object = "{D750EAB9-EB7B-450B-B9B6-0A0A17E13DCA}#1.0#0"; "XP.ocx"
Begin VB.Form frmACCLD 
   BorderStyle     =   3  'Fixed Dialog
   Caption         =   "PARTY LEDGER"
   ClientHeight    =   2700
   ClientLeft      =   45
   ClientTop       =   435
   ClientWidth     =   6585
   Icon            =   "Accountledgerdate.frx":0000
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   Picture         =   "Accountledgerdate.frx":000C
   ScaleHeight     =   2700
   ScaleWidth      =   6585
   ShowInTaskbar   =   0   'False
   StartUpPosition =   2  'CenterScreen
   Begin VB.CheckBox Check1 
      BackColor       =   &H00C0C0FF&
      Caption         =   "All"
      Height          =   195
      Left            =   2400
      TabIndex        =   0
      Top             =   120
      Visible         =   0   'False
      Width           =   615
   End
   Begin MSDataListLib.DataCombo DC1 
      Height          =   345
      Left            =   2400
      TabIndex        =   3
      Top             =   1560
      Width           =   3735
      _ExtentX        =   6588
      _ExtentY        =   609
      _Version        =   393216
      Appearance      =   0
      BackColor       =   16777215
      ForeColor       =   4210752
      Text            =   ""
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
   End
   Begin MSComCtl2.DTPicker DTS 
      CausesValidation=   0   'False
      Height          =   345
      Left            =   2400
      TabIndex        =   1
      Top             =   480
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   609
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   14737632
      Format          =   58130433
      CurrentDate     =   38099
   End
   Begin MSComCtl2.DTPicker DTE 
      CausesValidation=   0   'False
      Height          =   345
      Left            =   2400
      TabIndex        =   2
      Top             =   960
      Width           =   1695
      _ExtentX        =   2990
      _ExtentY        =   609
      _Version        =   393216
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Arial"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      CalendarBackColor=   14737632
      CustomFormat    =   "dd-MMM-yy"
      Format          =   58130433
      CurrentDate     =   38099
   End
   Begin XPLayout.XPButton COMMAND1 
      Height          =   495
      Left            =   2400
      TabIndex        =   4
      Top             =   2040
      Width           =   1335
      _ExtentX        =   2355
      _ExtentY        =   873
      BTYPE           =   3
      TX              =   "&Show"
      ENAB            =   -1  'True
      BeginProperty FONT {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Verdana"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      COLTYPE         =   1
      FOCUSR          =   -1  'True
      BCOL            =   16777215
      BCOLO           =   16777215
      FCOL            =   0
      FCOLO           =   16711680
      MCOL            =   12632256
      MPTR            =   0
      MICON           =   "Accountledgerdate.frx":1596E2
      PICN            =   "Accountledgerdate.frx":1596FE
      UMCOL           =   -1  'True
      SOFT            =   0   'False
      PICPOS          =   0
      NGREY           =   0   'False
      FX              =   0
      HAND            =   0   'False
      CHECK           =   0   'False
      VALUE           =   0   'False
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Select Party"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   270
      Index           =   4
      Left            =   360
      TabIndex        =   8
      Top             =   1560
      Width           =   1275
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Starting Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   270
      Index           =   5
      Left            =   360
      TabIndex        =   7
      Top             =   600
      Width           =   1395
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      Caption         =   "Ending Date"
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   11.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H00000080&
      Height          =   270
      Index           =   0
      Left            =   360
      TabIndex        =   6
      Top             =   1080
      Width           =   1305
   End
   Begin VB.Label Label16 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      BackStyle       =   0  'Transparent
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   15.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   675
      Left            =   7080
      TabIndex        =   5
      Top             =   3240
      Width           =   120
   End
End
Attribute VB_Name = "frmACCLD"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub COMMAND1_Click()
If DTS.Value > DTE.Value Then
MsgBox "ENDING DATE CANNOT LESS THAN STARTING DATE"
Exit Sub
End If

If DC1.Text <> "" Then
frmACCL.Show vbModal
Else
MsgBox "Select A Valid Head Please"
End If
End Sub

Function Party_Leadger()
CN.Execute "DELETE FROM LEDGER"
Dim TT, RBal As Double
i = 1
Set PB = New ADODB.Recordset
PB.Open "SELECT SUM(DEBIT),SUM(CREDIT) FROM PLEDGER WHERE PID=" & ID_NaME("PARTY", "PID", "NAME", DC1.Text) & " AND DATE < '" & DTS.Value & "'", CN, adOpenStatic, adLockOptimistic
TT = IIf(IsNull(PB(0)), 0, PB(0)) - IIf(IsNull(PB(1)), 0, PB(1))
RBal = TT
''''''''''''''PREVIOUS BALANCE
If TT <> 0 Then
Set UP = New ADODB.Recordset
UP.Open "SELECT * FROM LEDGER ORDER BY SNO", CN, adOpenStatic, adLockOptimistic
UP.AddNew
UP("SNO") = i
i = i + 1
UP("DATE") = DTS.Value
UP("DESCRIPTION") = "BROUGHT FORWARD"
'''''''''''''''''''''''''''''''''''''''''
If TT < 0 Then
UP("DEBIT") = -TT
UP("FBAL") = "CR"
Else
UP("DEBIT") = TT
UP("FCD") = "DR"
End If
UP("FBAL") = RBal
UP.Update
UP.Close
''''''''''''''''''''''''''''''
End If

Set Rs = New ADODB.Recordset
Rs.Open "SELECT * FROM PLEDGER WHERE PLEDID=(SELECT MAX(PLEDID) FROM PLEDGER) WHERE PID=" & ID_NaME("PARTY", "PID", "NAME", DC1.Text) & " AND DATE BETWEEN '" & DTS.Value & "' AND '" & DTE.Value & "' ORDER BY DATE,PLEDID", CN, adOpenStatic, adLockOptimistic
Do Until Rs.EOF
Set UP = New ADODB.Recordset
UP.Open "SELECT * FROM LEDGER WHERE SNO=(SELECT MAX(SNO) FROM LEDGER)", CN, adOpenStatic, adLockOptimistic
UP.AddNew
UP("SNO") = i
i = i + 1
UP("PID") = Rs("PID")
UP("DATE") = Rs("DATE")
UP("DESCRIPTION") = Rs("DESCRIPTION")
UP("INVOICE") = Rs!INVOICE
'''''''''''''''''''''''''''''''''''''''''''''''''
UP("DEBIT") = IIf(IsNull(Rs("DEBIT")), 0, Rs("DEBIT"))
UP("CREDIT") = IIf(IsNull(Rs("CREDIT")), 0, Rs("CREDIT"))
RBal = RBal + IIf(IsNull(Rs("DEBIT")), 0, Rs("DEBIT")) - IIf(IsNull(Rs("CREDIT")), 0, Rs("CREDIT"))
If RBal > 0 Then
    UP("FBAL") = RBal
    UP("FCD") = "DR"
ElseIf RBal < 0 Then
    UP("FBAL") = -RBal
    UP("FCD") = "CR"
Else
    UP("FBAL") = 0
    UP("FCD") = ""
End If
UP.Update
Rs.MoveNext
Loop
'Me.Caption = "Party ledger from " & DTS.Value & " To " & DTE.Value
End Function

Private Sub DC1_Change()
If CC = False Then
Set Rs3 = New ADODB.Recordset
If DC1.Text <> "" Then
Rs3.Open "SELECT NAME FROM PARTY WHERE NAME Like '" & UCase(DC1.Text) & "%' ORDER BY NAME", CN, adOpenStatic, adLockReadOnly
Else
Rs3.Open "SELECT NAME FROM PARTY ORDER BY NAME", CN, adOpenStatic, adLockReadOnly
End If
Set DC1.RowSource = Rs3
DC1.ListField = "NAME"
End If
End Sub

Private Sub DC1_GotFocus()
DC1_Change
SendKeys "{F4}"
End Sub

Private Sub DC1_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DC1_Validate(Cancel As Boolean)
If DC1.Text <> "" Then
Set Rs = New ADODB.Recordset
Rs.Open "SELECT PID,phone,TYPE FROM PARTY WHERE NAME='" & DC1.Text & "'", CN, adOpenStatic, adLockOptimistic
If Rs.RecordCount > 0 Then
Cancel = False
Else
Cancel = True
MsgBox "Select a valid party"
End If
End If
End Sub

Private Sub DCH_Change()
If CC = False Then
Set Rs3 = New ADODB.Recordset
If DCH.Text <> "" Then
Rs3.Open "SELECT NAME FROM HEAD WHERE NAME Like '" & DCH.Text & "%' ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
Else
Rs3.Open "SELECT NAME FROM HEAD ORDER BY NAME", CN, adOpenStatic, adLockOptimistic
End If
Set DCH.RowSource = Rs3
DCH.ListField = "NAME"
End If
End Sub

Private Sub DCH_GotFocus()
DCH_Change
End Sub

Private Sub DCH_KeyDown(KeyCode As Integer, Shift As Integer)
If KeyCode <> vbKeyUp And KeyCode <> vbKeyDown And KeyCode <> vbKeyLeft And KeyCode <> vbKeyRight Then
CC = False
Else
CC = True
End If
End Sub

Private Sub DCH_Validate(Cancel As Boolean)
If DCH.Text <> "" Then
Set Rs = New ADODB.Recordset
Rs.Open "SELECT HID FROM HEAD WHERE NAME='" & DCH.Text & "'", CN, adOpenStatic, adLockOptimistic
Set Thid.DataSource = Rs
Thid.DataField = "HID"
If Thid.Text = "" Then
MsgBox "Select a valid party please"
Cancel = True
Else
Cancel = False
End If
End If
End Sub

Private Sub Form_Load()
''''''''''''''''''' theme
Set OBJ = New clsFunction
OBJ.clear Me
'''''''''''''''''''''''''''''

DTE.Value = Date
End Sub
